/*
 * Copyright © 2019 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdint.h>
#include <stdbool.h>

#include "common.h"

void
test_simple(struct fd_ringbuffer *ring, struct ir3_shader_variant *vs,
		struct ir3_shader_variant *hs, struct ir3_shader_variant *ds,
		struct ir3_shader_variant *gs)
{
	OUT_PKT4(ring, REG_A6XX_HLSQ_VS_CNTL, 4);
	OUT_RING(ring, A6XX_HLSQ_VS_CNTL_CONSTLEN(align(vs->constlen, 4)) |
			A6XX_HLSQ_VS_CNTL_ENABLED);
	OUT_RING(ring, COND(hs,
			A6XX_HLSQ_HS_CNTL_ENABLED |
			A6XX_HLSQ_HS_CNTL_CONSTLEN(align(hs->constlen, 4))));
	OUT_RING(ring, COND(ds,
			A6XX_HLSQ_DS_CNTL_ENABLED |
			A6XX_HLSQ_DS_CNTL_CONSTLEN(align(ds->constlen, 4))));
	OUT_RING(ring, COND(gs,
			A6XX_HLSQ_GS_CNTL_ENABLED |
			A6XX_HLSQ_GS_CNTL_CONSTLEN(align(gs->constlen, 4))));
}

void
test_reloc(struct fd_ringbuffer *ring, int i, enum a6xx_format format,
		enum a6xx_tile_mode tile_mode, enum a3xx_color_swap swap,
		uint32_t stride, uint32_t size0, struct fd_bo *bo,
		uint32_t offset, uint32_t base)
{
	OUT_PKT4(ring, REG_A6XX_RB_MRT_BUF_INFO(i), 6);
	OUT_RING(ring, A6XX_RB_MRT_BUF_INFO_COLOR_FORMAT(format) |
			A6XX_RB_MRT_BUF_INFO_COLOR_TILE_MODE(tile_mode) |
			A6XX_RB_MRT_BUF_INFO_COLOR_SWAP(swap));
	OUT_RING(ring, A6XX_RB_MRT_PITCH(stride));
	OUT_RING(ring, A6XX_RB_MRT_ARRAY_PITCH(size0));
	OUT_RELOC(ring, bo, offset, 0, 0);	/* BASE_LO/HI */
	OUT_RING(ring, base);			/* RB_MRT[i].BASE_GMEM */
}
