#GCC = aarch64-linux-gnu-gcc
GCC = gcc

HDRS = a6xx.xml.h adreno_common.xml.h adreno_pm4.xml.h common.h

all: legacy.o new.o genxpp.o

legacy.o: legacy.c $(HDRS) Makefile
	$(GCC) -O3 -c -o legacy.o legacy.c

new.o: new.c $(HDRS) Makefile
	$(GCC) -O3 -c -o new.o new.c

genxpp.o: genxpp.cc $(HDRS) Makefile
	$(GCC) -Wno-narrowing -O3 -c -o genxpp.o genxpp.cc

clean:
	rm -f legacy.o new.o
