/*
 * Copyright © 2019 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __COMMON_H__
#define __COMMON_H__

struct fd_ringbuffer_funcs;

#include <stdint.h>
#include <stdio.h>

#define NDEBUG 1
#include <assert.h>

union fi {
   float f;
   int32_t i;
   uint32_t ui;
};

static inline unsigned
fui( float f )
{
   union fi fi;
   fi.f = f;
   return fi.ui;
}

/**
 * Align a value, only works pot alignemnts.
 */
static inline int
align(int value, int alignment)
{
   return (value + alignment - 1) & ~(alignment - 1);
}

/**
 * Static (compile-time) assertion.
 * Basically, use COND to dimension an array.  If COND is false/zero the
 * array size will be -1 and we'll get a compilation error.
 */
#define STATIC_ASSERT(COND) \
   do { \
      (void) sizeof(char [1 - 2*!(COND)]); \
   } while (0)

/*
 * Stub out enough stuff to compile, linking not required:
 */

struct fd_bo;

struct fd_ringbuffer {
	uint32_t *cur, *end, *start;
	const struct fd_ringbuffer_funcs *funcs;
};


struct fd_reloc {
   struct fd_bo *bo;
   uint64_t iova;
   uint64_t orval;
#define FD_RELOC_READ  0x0001
#define FD_RELOC_WRITE 0x0002
#define FD_RELOC_DUMP  0x0004
   uint32_t offset;
   int32_t shift;
};

uint64_t fd_bo_get_iova(struct fd_bo *bo);

#include "adreno_common.xml.h"
#include "adreno_pm4.xml.h"
#include "a6xx.xml.h"

void fd_ringbuffer_reloc(struct fd_ringbuffer *ring, const struct fd_reloc *reloc);
void fd_ringbuffer_reloc2(struct fd_ringbuffer *ring, struct fd_bo *bo);

static inline void
fd_ringbuffer_emit(struct fd_ringbuffer *ring,
		uint32_t data)
{
	(*ring->cur++) = data;
}

static inline void
OUT_RELOCW(struct fd_ringbuffer *ring, struct fd_bo *bo,
		uint32_t offset, uint64_t orval, int32_t shift)
{
   struct fd_reloc reloc = {
         .bo = bo,
         .orval = orval,
         .offset = offset,
         .shift = shift,
   };

   fd_ringbuffer_reloc(ring, &reloc);
}


static inline void BEGIN_RING(struct fd_ringbuffer *ring, uint32_t ndwords)
{
	// simplify the disasm slightly by ignoring the growable ring stuff:
	//if (ring->cur + ndwords > ring->end)
	//	fd_ringbuffer_grow(ring, ndwords);
}

static inline void
OUT_RING(struct fd_ringbuffer *ring, uint32_t data)
{
	fd_ringbuffer_emit(ring, data);
}

/*
 * Starting with a5xx, pkt4/pkt7 are used instead of pkt0/pkt3
 */

static inline unsigned
pm4_odd_parity_bit(unsigned val)
{
	/* See: http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
	 * note that we want odd parity so 0x6996 is inverted.
	 */
	val ^= val >> 16;
	val ^= val >> 8;
	val ^= val >> 4;
	val &= 0xf;
	return (~0x6996 >> val) & 1;
}

static inline uint32_t
pm4_pkt4_hdr(uint16_t regindx, uint16_t cnt)
{
   assert(cnt < 0x7f);
   return CP_TYPE4_PKT | cnt | (pm4_odd_parity_bit(cnt) << 7) |
         ((regindx & 0x3ffff) << 8) |
         ((pm4_odd_parity_bit(regindx) << 27));
}

static inline uint32_t
pm4_pkt7_hdr(uint8_t opcode, uint16_t cnt)
{
   return CP_TYPE7_PKT | cnt | (pm4_odd_parity_bit(cnt) << 15) |
         ((opcode & 0x7f) << 16) |
         ((pm4_odd_parity_bit(opcode) << 23));
}

static inline void
OUT_PKT4(struct fd_ringbuffer *ring, uint16_t regindx, uint16_t cnt)
{
	BEGIN_RING(ring, cnt+1);
	OUT_RING(ring, pm4_pkt4_hdr(regindx, cnt));
}

static uint64_t
relocaddr(struct fd_bo *bo, uint32_t offset, uint64_t orval, int32_t shift)
{
   uint64_t iova = fd_bo_get_iova(bo) + offset;

   if (shift < 0)
      iova >>= -shift;
   else
      iova <<= shift;

   iova |= orval;

   return iova;
}

static inline void
OUT_RELOC(struct fd_ringbuffer *ring, struct fd_bo *bo, uint32_t offset,
          uint64_t orval, int32_t shift)
{
   assert(offset < fd_bo_size(bo));

   uint64_t iova = relocaddr(bo, offset, orval, shift);

   struct fd_reloc reloc = {
         .bo = bo,
         .iova = iova,
         .orval = orval,
         .offset = offset,
         .shift = shift,
   };

   fd_ringbuffer_reloc(ring, &reloc);
}

#define ARRAY_SIZE(a) (sizeof (a) / sizeof *(a))


struct fd_reg_pair {
   uint32_t reg;
   uint64_t value;
   struct fd_bo *bo;
   bool is_address;
   bool bo_write;
   uint32_t bo_offset;
   uint32_t bo_shift;
};

#define __bo_type struct fd_bo *

#define __assert_eq(a, b)                                                      \
   do {                                                                        \
      if ((a) != (b)) {                                                        \
         fprintf(stderr, "assert failed: " #a " (0x%x) != " #b " (0x%x)\n", a, \
                 b);                                                           \
         assert((a) == (b));                                                   \
      }                                                                        \
   } while (0)

#define __ONE_REG(i, ...)                                                      \
   do {                                                                        \
      const struct fd_reg_pair regs[] = {__VA_ARGS__};                         \
      /* NOTE: allow regs[0].reg==0, this happens in OUT_PKT() */              \
      if (i < ARRAY_SIZE(regs) && (i == 0 || regs[i].reg > 0)) {               \
         __assert_eq(regs[0].reg + i, regs[i].reg);                            \
         if (regs[i].bo) {                                                     \
            uint64_t iova = relocaddr(regs[i].bo, regs[i].bo_offset,           \
                                      regs[i].value, regs[i].bo_shift);        \
            *(uint64_t *)p = iova;                                             \
            p += 2;                                                            \
            fd_ringbuffer_reloc2(ring, regs[i].bo);                            \
         } else {                                                              \
            *p++ = regs[i].value;                                              \
            if (regs[i].is_address)                                            \
               *p++ = regs[i].value >> 32;                                     \
         }                                                                     \
      }                                                                        \
   } while (0)

#define OUT_REG(ring, ...)                                                     \
   do {                                                                        \
      const struct fd_reg_pair regs[] = {__VA_ARGS__};                         \
      unsigned count = ARRAY_SIZE(regs);                                       \
                                                                               \
      STATIC_ASSERT(ARRAY_SIZE(regs) > 0);                                     \
      STATIC_ASSERT(ARRAY_SIZE(regs) <= 16);                                   \
                                                                               \
      BEGIN_RING(ring, count + 1);                                             \
      uint32_t *p = ring->cur;                                                 \
      *p++ = pm4_pkt4_hdr(regs[0].reg, count);                                 \
                                                                               \
      __ONE_REG(0, __VA_ARGS__);                                               \
      __ONE_REG(1, __VA_ARGS__);                                               \
      __ONE_REG(2, __VA_ARGS__);                                               \
      __ONE_REG(3, __VA_ARGS__);                                               \
      __ONE_REG(4, __VA_ARGS__);                                               \
      __ONE_REG(5, __VA_ARGS__);                                               \
      __ONE_REG(6, __VA_ARGS__);                                               \
      __ONE_REG(7, __VA_ARGS__);                                               \
      __ONE_REG(8, __VA_ARGS__);                                               \
      __ONE_REG(9, __VA_ARGS__);                                               \
      __ONE_REG(10, __VA_ARGS__);                                              \
      __ONE_REG(11, __VA_ARGS__);                                              \
      __ONE_REG(12, __VA_ARGS__);                                              \
      __ONE_REG(13, __VA_ARGS__);                                              \
      __ONE_REG(14, __VA_ARGS__);                                              \
      __ONE_REG(15, __VA_ARGS__);                                              \
      ring->cur = p;                                                           \
   } while (0)

#define OUT_PKT(ring, opcode, ...)                                             \
   do {                                                                        \
      const struct fd_reg_pair regs[] = {__VA_ARGS__};                         \
      unsigned count = ARRAY_SIZE(regs);                                       \
                                                                               \
      STATIC_ASSERT(ARRAY_SIZE(regs) <= 16);                                   \
                                                                               \
      BEGIN_RING(ring, count + 1);                                             \
      uint32_t *p = ring->cur;                                                 \
      *p++ = pm4_pkt7_hdr(opcode, count);                                      \
                                                                               \
      __ONE_REG(0, __VA_ARGS__);                                               \
      __ONE_REG(1, __VA_ARGS__);                                               \
      __ONE_REG(2, __VA_ARGS__);                                               \
      __ONE_REG(3, __VA_ARGS__);                                               \
      __ONE_REG(4, __VA_ARGS__);                                               \
      __ONE_REG(5, __VA_ARGS__);                                               \
      __ONE_REG(6, __VA_ARGS__);                                               \
      __ONE_REG(7, __VA_ARGS__);                                               \
      __ONE_REG(8, __VA_ARGS__);                                               \
      __ONE_REG(9, __VA_ARGS__);                                               \
      __ONE_REG(10, __VA_ARGS__);                                              \
      __ONE_REG(11, __VA_ARGS__);                                              \
      __ONE_REG(12, __VA_ARGS__);                                              \
      __ONE_REG(13, __VA_ARGS__);                                              \
      __ONE_REG(14, __VA_ARGS__);                                              \
      __ONE_REG(15, __VA_ARGS__);                                              \
      ring->cur = p;                                                           \
   } while (0)

/* similar to OUT_PKT() but appends specified # of dwords
 * copied for buf to the end of the packet (ie. for use-
 * cases like CP_LOAD_STATE)
 */
#define OUT_PKTBUF(ring, opcode, dwords, sizedwords, ...)                      \
   do {                                                                        \
      const struct fd_reg_pair regs[] = {__VA_ARGS__};                         \
      unsigned count = ARRAY_SIZE(regs);                                       \
                                                                               \
      STATIC_ASSERT(ARRAY_SIZE(regs) <= 16);                                   \
      count += sizedwords;                                                     \
                                                                               \
      BEGIN_RING(ring, count + 1);                                             \
      uint32_t *p = ring->cur;                                                 \
      *p++ = pm4_pkt7_hdr(opcode, count);                                      \
                                                                               \
      __ONE_REG(0, __VA_ARGS__);                                               \
      __ONE_REG(1, __VA_ARGS__);                                               \
      __ONE_REG(2, __VA_ARGS__);                                               \
      __ONE_REG(3, __VA_ARGS__);                                               \
      __ONE_REG(4, __VA_ARGS__);                                               \
      __ONE_REG(5, __VA_ARGS__);                                               \
      __ONE_REG(6, __VA_ARGS__);                                               \
      __ONE_REG(7, __VA_ARGS__);                                               \
      __ONE_REG(8, __VA_ARGS__);                                               \
      __ONE_REG(9, __VA_ARGS__);                                               \
      __ONE_REG(10, __VA_ARGS__);                                              \
      __ONE_REG(11, __VA_ARGS__);                                              \
      __ONE_REG(12, __VA_ARGS__);                                              \
      __ONE_REG(13, __VA_ARGS__);                                              \
      __ONE_REG(14, __VA_ARGS__);                                              \
      __ONE_REG(15, __VA_ARGS__);                                              \
      memcpy(p, dwords, 4 * sizedwords);                                       \
      p += sizedwords;                                                         \
      ring->cur = p;                                                           \
   } while (0)

struct ir3_shader_variant {
	struct fd_bo *bo;

	/* variant id (for debug) */
	uint32_t id;

	/* vertex shaders can have an extra version for hwbinning pass,
	 * which is pointed to by so->binning:
	 */
	bool binning_pass;
//	union {
		struct ir3_shader_variant *binning;
		struct ir3_shader_variant *nonbinning;
//	};


	/* Levels of nesting of flow control:
	 */
	unsigned branchstack;

	unsigned max_sun;
	unsigned loops;
	uint32_t constlen;
};

/* for conditionally setting boolean flag(s): */
#define COND(bool, val) ((bool) ? (val) : 0)

#endif  /*  __COMMON_H__ */
