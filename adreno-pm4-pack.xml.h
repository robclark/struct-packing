#ifndef ADRENO_PM4_XML_STRUCTS
#define ADRENO_PM4_XML_STRUCTS

struct CP_LOAD_STATE_0 {
    uint32_t							dst_off;
    enum adreno_state_src					state_src;
    enum adreno_state_block					state_block;
    uint32_t							num_unit;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE_0(struct CP_LOAD_STATE_0 fields)
{
#ifndef NDEBUG
    assert((fields.dst_off                           & 0xffff0000) == 0);
    assert((fields.state_src                         & 0xfffffff8) == 0);
    assert((fields.state_block                       & 0xfffffff8) == 0);
    assert((fields.num_unit                          & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE_0,
        .value =
            (fields.dst_off                           <<  0) |
            (fields.state_src                         << 16) |
            (fields.state_block                       << 19) |
            (fields.num_unit                          << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE_0(...) pack_CP_LOAD_STATE_0((struct CP_LOAD_STATE_0) { __VA_ARGS__ })

struct CP_LOAD_STATE_1 {
    enum adreno_state_type					state_type;
    uint32_t							ext_src_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE_1(struct CP_LOAD_STATE_1 fields)
{
#ifndef NDEBUG
    assert((fields.state_type                        & 0xfffffffc) == 0);
    assert(((fields.ext_src_addr >> 2)               & 0xc0000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE_1,
        .value =
            (fields.state_type                        <<  0) |
            ((fields.ext_src_addr >> 2)               <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE_1(...) pack_CP_LOAD_STATE_1((struct CP_LOAD_STATE_1) { __VA_ARGS__ })

struct CP_LOAD_STATE4_0 {
    uint32_t							dst_off;
    enum a4xx_state_src						state_src;
    enum a4xx_state_block					state_block;
    uint32_t							num_unit;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE4_0(struct CP_LOAD_STATE4_0 fields)
{
#ifndef NDEBUG
    assert((fields.dst_off                           & 0xffffc000) == 0);
    assert((fields.state_src                         & 0xfffffffc) == 0);
    assert((fields.state_block                       & 0xfffffff0) == 0);
    assert((fields.num_unit                          & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xffff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE4_0,
        .value =
            (fields.dst_off                           <<  0) |
            (fields.state_src                         << 16) |
            (fields.state_block                       << 18) |
            (fields.num_unit                          << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE4_0(...) pack_CP_LOAD_STATE4_0((struct CP_LOAD_STATE4_0) { __VA_ARGS__ })

struct CP_LOAD_STATE4_1 {
    enum a4xx_state_type					state_type;
    uint32_t							ext_src_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE4_1(struct CP_LOAD_STATE4_1 fields)
{
#ifndef NDEBUG
    assert((fields.state_type                        & 0xfffffffc) == 0);
    assert(((fields.ext_src_addr >> 2)               & 0xc0000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE4_1,
        .value =
            (fields.state_type                        <<  0) |
            ((fields.ext_src_addr >> 2)               <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE4_1(...) pack_CP_LOAD_STATE4_1((struct CP_LOAD_STATE4_1) { __VA_ARGS__ })

struct CP_LOAD_STATE4_2 {
    uint32_t							ext_src_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE4_2(struct CP_LOAD_STATE4_2 fields)
{
#ifndef NDEBUG
    assert((fields.ext_src_addr_hi                   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE4_2,
        .value =
            (fields.ext_src_addr_hi                   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE4_2(...) pack_CP_LOAD_STATE4_2((struct CP_LOAD_STATE4_2) { __VA_ARGS__ })

struct CP_LOAD_STATE6_0 {
    uint32_t							dst_off;
    enum a6xx_state_type					state_type;
    enum a6xx_state_src						state_src;
    enum a6xx_state_block					state_block;
    uint32_t							num_unit;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE6_0(struct CP_LOAD_STATE6_0 fields)
{
#ifndef NDEBUG
    assert((fields.dst_off                           & 0xffffc000) == 0);
    assert((fields.state_type                        & 0xfffffffc) == 0);
    assert((fields.state_src                         & 0xfffffffc) == 0);
    assert((fields.state_block                       & 0xfffffff0) == 0);
    assert((fields.num_unit                          & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE6_0,
        .value =
            (fields.dst_off                           <<  0) |
            (fields.state_type                        << 14) |
            (fields.state_src                         << 16) |
            (fields.state_block                       << 18) |
            (fields.num_unit                          << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE6_0(...) pack_CP_LOAD_STATE6_0((struct CP_LOAD_STATE6_0) { __VA_ARGS__ })

struct CP_LOAD_STATE6_1 {
    uint32_t							ext_src_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE6_1(struct CP_LOAD_STATE6_1 fields)
{
#ifndef NDEBUG
    assert(((fields.ext_src_addr >> 2)               & 0xc0000000) == 0);
    assert((fields.unknown                           & 0xfffffffc) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE6_1,
        .value =
            ((fields.ext_src_addr >> 2)               <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE6_1(...) pack_CP_LOAD_STATE6_1((struct CP_LOAD_STATE6_1) { __VA_ARGS__ })

struct CP_LOAD_STATE6_2 {
    uint32_t							ext_src_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE6_2(struct CP_LOAD_STATE6_2 fields)
{
#ifndef NDEBUG
    assert((fields.ext_src_addr_hi                   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE6_2,
        .value =
            (fields.ext_src_addr_hi                   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_LOAD_STATE6_2(...) pack_CP_LOAD_STATE6_2((struct CP_LOAD_STATE6_2) { __VA_ARGS__ })

struct CP_LOAD_STATE6_EXT_SRC_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_CP_LOAD_STATE6_EXT_SRC_ADDR(struct CP_LOAD_STATE6_EXT_SRC_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_LOAD_STATE6_EXT_SRC_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define CP_LOAD_STATE6_EXT_SRC_ADDR(...) pack_CP_LOAD_STATE6_EXT_SRC_ADDR((struct CP_LOAD_STATE6_EXT_SRC_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct CP_DRAW_INDX_0 {
    uint32_t							viz_query;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_0(struct CP_DRAW_INDX_0 fields)
{
#ifndef NDEBUG
    assert((fields.viz_query                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_0,
        .value =
            (fields.viz_query                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_0(...) pack_CP_DRAW_INDX_0((struct CP_DRAW_INDX_0) { __VA_ARGS__ })

struct CP_DRAW_INDX_1 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum pc_di_index_size					index_size;
    bool							not_eop;
    bool							small_index;
    bool							pre_draw_initiator_enable;
    uint32_t							num_instances;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_1(struct CP_DRAW_INDX_1 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffe) == 0);
    assert((fields.num_instances                     & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff007eff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_1,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  9) |
            (fields.index_size                        << 11) |
            (fields.not_eop                           << 12) |
            (fields.small_index                       << 13) |
            (fields.pre_draw_initiator_enable         << 14) |
            (fields.num_instances                     << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_1(...) pack_CP_DRAW_INDX_1((struct CP_DRAW_INDX_1) { __VA_ARGS__ })

struct CP_DRAW_INDX_2 {
    uint32_t							num_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_2(struct CP_DRAW_INDX_2 fields)
{
#ifndef NDEBUG
    assert((fields.num_indices                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_2,
        .value =
            (fields.num_indices                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_2(...) pack_CP_DRAW_INDX_2((struct CP_DRAW_INDX_2) { __VA_ARGS__ })

struct CP_DRAW_INDX_3 {
    uint32_t							indx_base;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_3(struct CP_DRAW_INDX_3 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_3,
        .value =
            (fields.indx_base                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_3(...) pack_CP_DRAW_INDX_3((struct CP_DRAW_INDX_3) { __VA_ARGS__ })

struct CP_DRAW_INDX_4 {
    uint32_t							indx_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_4(struct CP_DRAW_INDX_4 fields)
{
#ifndef NDEBUG
    assert((fields.indx_size                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_4,
        .value =
            (fields.indx_size                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_4(...) pack_CP_DRAW_INDX_4((struct CP_DRAW_INDX_4) { __VA_ARGS__ })

struct CP_DRAW_INDX_2_0 {
    uint32_t							viz_query;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_2_0(struct CP_DRAW_INDX_2_0 fields)
{
#ifndef NDEBUG
    assert((fields.viz_query                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_2_0,
        .value =
            (fields.viz_query                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_2_0(...) pack_CP_DRAW_INDX_2_0((struct CP_DRAW_INDX_2_0) { __VA_ARGS__ })

struct CP_DRAW_INDX_2_1 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum pc_di_index_size					index_size;
    bool							not_eop;
    bool							small_index;
    bool							pre_draw_initiator_enable;
    uint32_t							num_instances;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_2_1(struct CP_DRAW_INDX_2_1 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffe) == 0);
    assert((fields.num_instances                     & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff007eff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_2_1,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  9) |
            (fields.index_size                        << 11) |
            (fields.not_eop                           << 12) |
            (fields.small_index                       << 13) |
            (fields.pre_draw_initiator_enable         << 14) |
            (fields.num_instances                     << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_2_1(...) pack_CP_DRAW_INDX_2_1((struct CP_DRAW_INDX_2_1) { __VA_ARGS__ })

struct CP_DRAW_INDX_2_2 {
    uint32_t							num_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_2_2(struct CP_DRAW_INDX_2_2 fields)
{
#ifndef NDEBUG
    assert((fields.num_indices                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_2_2,
        .value =
            (fields.num_indices                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_2_2(...) pack_CP_DRAW_INDX_2_2((struct CP_DRAW_INDX_2_2) { __VA_ARGS__ })

struct CP_DRAW_INDX_OFFSET_0 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum a4xx_index_size					index_size;
    enum a6xx_patch_type					patch_type;
    bool							gs_enable;
    bool							tess_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_0(struct CP_DRAW_INDX_OFFSET_0 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffc) == 0);
    assert((fields.patch_type                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00033fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_0,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  8) |
            (fields.index_size                        << 10) |
            (fields.patch_type                        << 12) |
            (fields.gs_enable                         << 16) |
            (fields.tess_enable                       << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_0(...) pack_CP_DRAW_INDX_OFFSET_0((struct CP_DRAW_INDX_OFFSET_0) { __VA_ARGS__ })

struct CP_DRAW_INDX_OFFSET_1 {
    uint32_t							num_instances;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_1(struct CP_DRAW_INDX_OFFSET_1 fields)
{
#ifndef NDEBUG
    assert((fields.num_instances                     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_1,
        .value =
            (fields.num_instances                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_1(...) pack_CP_DRAW_INDX_OFFSET_1((struct CP_DRAW_INDX_OFFSET_1) { __VA_ARGS__ })

struct CP_DRAW_INDX_OFFSET_2 {
    uint32_t							num_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_2(struct CP_DRAW_INDX_OFFSET_2 fields)
{
#ifndef NDEBUG
    assert((fields.num_indices                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_2,
        .value =
            (fields.num_indices                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_2(...) pack_CP_DRAW_INDX_OFFSET_2((struct CP_DRAW_INDX_OFFSET_2) { __VA_ARGS__ })

struct CP_DRAW_INDX_OFFSET_3 {
    uint32_t							first_indx;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_3(struct CP_DRAW_INDX_OFFSET_3 fields)
{
#ifndef NDEBUG
    assert((fields.first_indx                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_3,
        .value =
            (fields.first_indx                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_3(...) pack_CP_DRAW_INDX_OFFSET_3((struct CP_DRAW_INDX_OFFSET_3) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_OFFSET_4 {
    uint32_t							indx_base_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_OFFSET_4(struct A5XX_CP_DRAW_INDX_OFFSET_4 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base_lo                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_OFFSET_4,
        .value =
            (fields.indx_base_lo                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_OFFSET_4(...) pack_A5XX_CP_DRAW_INDX_OFFSET_4((struct A5XX_CP_DRAW_INDX_OFFSET_4) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_OFFSET_5 {
    uint32_t							indx_base_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_OFFSET_5(struct A5XX_CP_DRAW_INDX_OFFSET_5 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base_hi                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_OFFSET_5,
        .value =
            (fields.indx_base_hi                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_OFFSET_5(...) pack_A5XX_CP_DRAW_INDX_OFFSET_5((struct A5XX_CP_DRAW_INDX_OFFSET_5) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE(struct A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE(...) pack_A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE((struct A5XX_CP_DRAW_INDX_OFFSET_INDX_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A5XX_CP_DRAW_INDX_OFFSET_6 {
    uint32_t							max_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_OFFSET_6(struct A5XX_CP_DRAW_INDX_OFFSET_6 fields)
{
#ifndef NDEBUG
    assert((fields.max_indices                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_OFFSET_6,
        .value =
            (fields.max_indices                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_OFFSET_6(...) pack_A5XX_CP_DRAW_INDX_OFFSET_6((struct A5XX_CP_DRAW_INDX_OFFSET_6) { __VA_ARGS__ })

struct CP_DRAW_INDX_OFFSET_4 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_4(struct CP_DRAW_INDX_OFFSET_4 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_4,
        .value =
            fields.unknown | fields.dword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_4(...) pack_CP_DRAW_INDX_OFFSET_4((struct CP_DRAW_INDX_OFFSET_4) { __VA_ARGS__ }), { .reg = 0 }

struct CP_DRAW_INDX_OFFSET_5 {
    uint32_t							indx_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_INDX_OFFSET_5(struct CP_DRAW_INDX_OFFSET_5 fields)
{
#ifndef NDEBUG
    assert((fields.indx_size                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_INDX_OFFSET_5,
        .value =
            (fields.indx_size                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_INDX_OFFSET_5(...) pack_CP_DRAW_INDX_OFFSET_5((struct CP_DRAW_INDX_OFFSET_5) { __VA_ARGS__ })

struct A4XX_CP_DRAW_INDIRECT_0 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum a4xx_index_size					index_size;
    enum a6xx_patch_type					patch_type;
    bool							gs_enable;
    bool							tess_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDIRECT_0(struct A4XX_CP_DRAW_INDIRECT_0 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffc) == 0);
    assert((fields.patch_type                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00033fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDIRECT_0,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  8) |
            (fields.index_size                        << 10) |
            (fields.patch_type                        << 12) |
            (fields.gs_enable                         << 16) |
            (fields.tess_enable                       << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDIRECT_0(...) pack_A4XX_CP_DRAW_INDIRECT_0((struct A4XX_CP_DRAW_INDIRECT_0) { __VA_ARGS__ })

struct A4XX_CP_DRAW_INDIRECT_1 {
    uint32_t							indirect;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDIRECT_1(struct A4XX_CP_DRAW_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.indirect                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDIRECT_1,
        .value =
            (fields.indirect                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDIRECT_1(...) pack_A4XX_CP_DRAW_INDIRECT_1((struct A4XX_CP_DRAW_INDIRECT_1) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDIRECT_1 {
    uint32_t							indirect_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDIRECT_1(struct A5XX_CP_DRAW_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.indirect_lo                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDIRECT_1,
        .value =
            (fields.indirect_lo                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDIRECT_1(...) pack_A5XX_CP_DRAW_INDIRECT_1((struct A5XX_CP_DRAW_INDIRECT_1) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDIRECT_2 {
    uint32_t							indirect_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDIRECT_2(struct A5XX_CP_DRAW_INDIRECT_2 fields)
{
#ifndef NDEBUG
    assert((fields.indirect_hi                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDIRECT_2,
        .value =
            (fields.indirect_hi                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDIRECT_2(...) pack_A5XX_CP_DRAW_INDIRECT_2((struct A5XX_CP_DRAW_INDIRECT_2) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDIRECT_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDIRECT_INDIRECT(struct A5XX_CP_DRAW_INDIRECT_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDIRECT_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A5XX_CP_DRAW_INDIRECT_INDIRECT(...) pack_A5XX_CP_DRAW_INDIRECT_INDIRECT((struct A5XX_CP_DRAW_INDIRECT_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct A4XX_CP_DRAW_INDX_INDIRECT_0 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum a4xx_index_size					index_size;
    enum a6xx_patch_type					patch_type;
    bool							gs_enable;
    bool							tess_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDX_INDIRECT_0(struct A4XX_CP_DRAW_INDX_INDIRECT_0 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffc) == 0);
    assert((fields.patch_type                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00033fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDX_INDIRECT_0,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  8) |
            (fields.index_size                        << 10) |
            (fields.patch_type                        << 12) |
            (fields.gs_enable                         << 16) |
            (fields.tess_enable                       << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDX_INDIRECT_0(...) pack_A4XX_CP_DRAW_INDX_INDIRECT_0((struct A4XX_CP_DRAW_INDX_INDIRECT_0) { __VA_ARGS__ })

struct A4XX_CP_DRAW_INDX_INDIRECT_1 {
    uint32_t							indx_base;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDX_INDIRECT_1(struct A4XX_CP_DRAW_INDX_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDX_INDIRECT_1,
        .value =
            (fields.indx_base                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDX_INDIRECT_1(...) pack_A4XX_CP_DRAW_INDX_INDIRECT_1((struct A4XX_CP_DRAW_INDX_INDIRECT_1) { __VA_ARGS__ })

struct A4XX_CP_DRAW_INDX_INDIRECT_2 {
    uint32_t							indx_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDX_INDIRECT_2(struct A4XX_CP_DRAW_INDX_INDIRECT_2 fields)
{
#ifndef NDEBUG
    assert((fields.indx_size                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDX_INDIRECT_2,
        .value =
            (fields.indx_size                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDX_INDIRECT_2(...) pack_A4XX_CP_DRAW_INDX_INDIRECT_2((struct A4XX_CP_DRAW_INDX_INDIRECT_2) { __VA_ARGS__ })

struct A4XX_CP_DRAW_INDX_INDIRECT_3 {
    uint32_t							indirect;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_DRAW_INDX_INDIRECT_3(struct A4XX_CP_DRAW_INDX_INDIRECT_3 fields)
{
#ifndef NDEBUG
    assert((fields.indirect                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_DRAW_INDX_INDIRECT_3,
        .value =
            (fields.indirect                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_DRAW_INDX_INDIRECT_3(...) pack_A4XX_CP_DRAW_INDX_INDIRECT_3((struct A4XX_CP_DRAW_INDX_INDIRECT_3) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_1 {
    uint32_t							indx_base_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_1(struct A5XX_CP_DRAW_INDX_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base_lo                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_1,
        .value =
            (fields.indx_base_lo                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_1(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_1((struct A5XX_CP_DRAW_INDX_INDIRECT_1) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_2 {
    uint32_t							indx_base_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_2(struct A5XX_CP_DRAW_INDX_INDIRECT_2 fields)
{
#ifndef NDEBUG
    assert((fields.indx_base_hi                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_2,
        .value =
            (fields.indx_base_hi                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_2(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_2((struct A5XX_CP_DRAW_INDX_INDIRECT_2) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE(struct A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE((struct A5XX_CP_DRAW_INDX_INDIRECT_INDX_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A5XX_CP_DRAW_INDX_INDIRECT_3 {
    uint32_t							max_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_3(struct A5XX_CP_DRAW_INDX_INDIRECT_3 fields)
{
#ifndef NDEBUG
    assert((fields.max_indices                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_3,
        .value =
            (fields.max_indices                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_3(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_3((struct A5XX_CP_DRAW_INDX_INDIRECT_3) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_4 {
    uint32_t							indirect_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_4(struct A5XX_CP_DRAW_INDX_INDIRECT_4 fields)
{
#ifndef NDEBUG
    assert((fields.indirect_lo                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_4,
        .value =
            (fields.indirect_lo                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_4(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_4((struct A5XX_CP_DRAW_INDX_INDIRECT_4) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_5 {
    uint32_t							indirect_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_5(struct A5XX_CP_DRAW_INDX_INDIRECT_5 fields)
{
#ifndef NDEBUG
    assert((fields.indirect_hi                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_5,
        .value =
            (fields.indirect_hi                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_5(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_5((struct A5XX_CP_DRAW_INDX_INDIRECT_5) { __VA_ARGS__ })

struct A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT(struct A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT(...) pack_A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT((struct A5XX_CP_DRAW_INDX_INDIRECT_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_CP_DRAW_INDIRECT_MULTI_0 {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum a4xx_index_size					index_size;
    enum a6xx_patch_type					patch_type;
    bool							gs_enable;
    bool							tess_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DRAW_INDIRECT_MULTI_0(struct A6XX_CP_DRAW_INDIRECT_MULTI_0 fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffc) == 0);
    assert((fields.patch_type                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00033fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DRAW_INDIRECT_MULTI_0,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  8) |
            (fields.index_size                        << 10) |
            (fields.patch_type                        << 12) |
            (fields.gs_enable                         << 16) |
            (fields.tess_enable                       << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DRAW_INDIRECT_MULTI_0(...) pack_A6XX_CP_DRAW_INDIRECT_MULTI_0((struct A6XX_CP_DRAW_INDIRECT_MULTI_0) { __VA_ARGS__ })

struct A6XX_CP_DRAW_INDIRECT_MULTI_1 {
    enum a6xx_draw_indirect_opcode				opcode;
    uint32_t							dst_off;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DRAW_INDIRECT_MULTI_1(struct A6XX_CP_DRAW_INDIRECT_MULTI_1 fields)
{
#ifndef NDEBUG
    assert((fields.opcode                            & 0xfffffff0) == 0);
    assert((fields.dst_off                           & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x003fff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DRAW_INDIRECT_MULTI_1,
        .value =
            (fields.opcode                            <<  0) |
            (fields.dst_off                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DRAW_INDIRECT_MULTI_1(...) pack_A6XX_CP_DRAW_INDIRECT_MULTI_1((struct A6XX_CP_DRAW_INDIRECT_MULTI_1) { __VA_ARGS__ })

struct A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT {
    uint32_t							a6xx_cp_draw_indirect_multi_draw_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT(struct A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_cp_draw_indirect_multi_draw_count & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT,
        .value =
            (fields.a6xx_cp_draw_indirect_multi_draw_count <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT(...) pack_A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT((struct A6XX_CP_DRAW_INDIRECT_MULTI_DRAW_COUNT) { __VA_ARGS__ })

struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT(struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT(...) pack_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT((struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE {
    uint32_t							indirect_op_normal_cp_draw_indirect_multi_stride;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE(struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_normal_cp_draw_indirect_multi_stride & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE,
        .value =
            (fields.indirect_op_normal_cp_draw_indirect_multi_stride <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE(...) pack_INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE((struct INDIRECT_OP_NORMAL_CP_DRAW_INDIRECT_MULTI_STRIDE) { __VA_ARGS__ })

struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX(struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX(...) pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX((struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES {
    uint32_t							indirect_op_indexed_cp_draw_indirect_multi_max_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES(struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_indexed_cp_draw_indirect_multi_max_indices & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES,
        .value =
            (fields.indirect_op_indexed_cp_draw_indirect_multi_max_indices <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES(...) pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES((struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES) { __VA_ARGS__ })

struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT(struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT(...) pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT((struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE {
    uint32_t							indirect_op_indexed_cp_draw_indirect_multi_stride;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE(struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_indexed_cp_draw_indirect_multi_stride & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE,
        .value =
            (fields.indirect_op_indexed_cp_draw_indirect_multi_stride <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE(...) pack_INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE((struct INDIRECT_OP_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE) { __VA_ARGS__ })

struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT(struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT(...) pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT((struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT(struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT(...) pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT((struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE {
    uint32_t							indirect_op_indirect_count_cp_draw_indirect_multi_stride;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE(struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_indirect_count_cp_draw_indirect_multi_stride & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE,
        .value =
            (fields.indirect_op_indirect_count_cp_draw_indirect_multi_stride <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE(...) pack_INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE((struct INDIRECT_OP_INDIRECT_COUNT_CP_DRAW_INDIRECT_MULTI_STRIDE) { __VA_ARGS__ })

struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX(struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX(...) pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX((struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDEX) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES {
    uint32_t							indirect_op_indirect_count_indexed_cp_draw_indirect_multi_max_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES(struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_indirect_count_indexed_cp_draw_indirect_multi_max_indices & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES,
        .value =
            (fields.indirect_op_indirect_count_indexed_cp_draw_indirect_multi_max_indices <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES(...) pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES((struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_MAX_INDICES) { __VA_ARGS__ })

struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT(struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT(...) pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT((struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT(struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT(...) pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT((struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_INDIRECT_COUNT) { __VA_ARGS__ }), { .reg = 0 }

struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE {
    uint32_t							indirect_op_indirect_count_indexed_cp_draw_indirect_multi_stride;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE(struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.indirect_op_indirect_count_indexed_cp_draw_indirect_multi_stride & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE,
        .value =
            (fields.indirect_op_indirect_count_indexed_cp_draw_indirect_multi_stride <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE(...) pack_INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE((struct INDIRECT_OP_INDIRECT_COUNT_INDEXED_CP_DRAW_INDIRECT_MULTI_STRIDE) { __VA_ARGS__ })

struct CP_DRAW_PRED_ENABLE_GLOBAL_0 {
    bool							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_PRED_ENABLE_GLOBAL_0(struct CP_DRAW_PRED_ENABLE_GLOBAL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_PRED_ENABLE_GLOBAL_0,
        .value =
            (fields.enable                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_PRED_ENABLE_GLOBAL_0(...) pack_CP_DRAW_PRED_ENABLE_GLOBAL_0((struct CP_DRAW_PRED_ENABLE_GLOBAL_0) { __VA_ARGS__ })

struct CP_DRAW_PRED_ENABLE_LOCAL_0 {
    bool							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_PRED_ENABLE_LOCAL_0(struct CP_DRAW_PRED_ENABLE_LOCAL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_PRED_ENABLE_LOCAL_0,
        .value =
            (fields.enable                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_PRED_ENABLE_LOCAL_0(...) pack_CP_DRAW_PRED_ENABLE_LOCAL_0((struct CP_DRAW_PRED_ENABLE_LOCAL_0) { __VA_ARGS__ })

struct CP_DRAW_PRED_SET_0 {
    enum cp_draw_pred_src					src;
    enum cp_draw_pred_test					test;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_PRED_SET_0(struct CP_DRAW_PRED_SET_0 fields)
{
#ifndef NDEBUG
    assert((fields.src                               & 0xfffffff0) == 0);
    assert((fields.test                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x000001f0) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_PRED_SET_0,
        .value =
            (fields.src                               <<  4) |
            (fields.test                              <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DRAW_PRED_SET_0(...) pack_CP_DRAW_PRED_SET_0((struct CP_DRAW_PRED_SET_0) { __VA_ARGS__ })

struct CP_DRAW_PRED_SET_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_CP_DRAW_PRED_SET_MEM_ADDR(struct CP_DRAW_PRED_SET_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DRAW_PRED_SET_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define CP_DRAW_PRED_SET_MEM_ADDR(...) pack_CP_DRAW_PRED_SET_MEM_ADDR((struct CP_DRAW_PRED_SET_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct CP_SET_DRAW_STATE__0 {
    uint32_t							count;
    bool							dirty;
    bool							disable;
    bool							disable_all_groups;
    bool							load_immed;
    bool							binning;
    bool							gmem;
    bool							sysmem;
    uint32_t							group_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_DRAW_STATE__0(uint32_t i, struct CP_SET_DRAW_STATE__0 fields)
{
#ifndef NDEBUG
    assert((fields.count                             & 0xffff0000) == 0);
    assert((fields.group_id                          & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x1f7fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_DRAW_STATE__0(i),
        .value =
            (fields.count                             <<  0) |
            (fields.dirty                             << 16) |
            (fields.disable                           << 17) |
            (fields.disable_all_groups                << 18) |
            (fields.load_immed                        << 19) |
            (fields.binning                           << 20) |
            (fields.gmem                              << 21) |
            (fields.sysmem                            << 22) |
            (fields.group_id                          << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_DRAW_STATE__0(i, ...) pack_CP_SET_DRAW_STATE__0(i, (struct CP_SET_DRAW_STATE__0) { __VA_ARGS__ })

struct CP_SET_DRAW_STATE__1 {
    uint32_t							addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_DRAW_STATE__1(uint32_t i, struct CP_SET_DRAW_STATE__1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_lo                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_DRAW_STATE__1(i),
        .value =
            (fields.addr_lo                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_DRAW_STATE__1(i, ...) pack_CP_SET_DRAW_STATE__1(i, (struct CP_SET_DRAW_STATE__1) { __VA_ARGS__ })

struct CP_SET_DRAW_STATE__2 {
    uint32_t							addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_DRAW_STATE__2(uint32_t i, struct CP_SET_DRAW_STATE__2 fields)
{
#ifndef NDEBUG
    assert((fields.addr_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_DRAW_STATE__2(i),
        .value =
            (fields.addr_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_DRAW_STATE__2(i, ...) pack_CP_SET_DRAW_STATE__2(i, (struct CP_SET_DRAW_STATE__2) { __VA_ARGS__ })

struct CP_SET_BIN_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_0(struct CP_SET_BIN_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_0(...) pack_CP_SET_BIN_0((struct CP_SET_BIN_0) { __VA_ARGS__ })

struct CP_SET_BIN_1 {
    uint32_t							x1;
    uint32_t							y1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_1(struct CP_SET_BIN_1 fields)
{
#ifndef NDEBUG
    assert((fields.x1                                & 0xffff0000) == 0);
    assert((fields.y1                                & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_1,
        .value =
            (fields.x1                                <<  0) |
            (fields.y1                                << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_1(...) pack_CP_SET_BIN_1((struct CP_SET_BIN_1) { __VA_ARGS__ })

struct CP_SET_BIN_2 {
    uint32_t							x2;
    uint32_t							y2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_2(struct CP_SET_BIN_2 fields)
{
#ifndef NDEBUG
    assert((fields.x2                                & 0xffff0000) == 0);
    assert((fields.y2                                & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_2,
        .value =
            (fields.x2                                <<  0) |
            (fields.y2                                << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_2(...) pack_CP_SET_BIN_2((struct CP_SET_BIN_2) { __VA_ARGS__ })

struct CP_SET_BIN_DATA_0 {
    uint32_t							bin_data_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA_0(struct CP_SET_BIN_DATA_0 fields)
{
#ifndef NDEBUG
    assert((fields.bin_data_addr                     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA_0,
        .value =
            (fields.bin_data_addr                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA_0(...) pack_CP_SET_BIN_DATA_0((struct CP_SET_BIN_DATA_0) { __VA_ARGS__ })

struct CP_SET_BIN_DATA_1 {
    uint32_t							bin_size_address;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA_1(struct CP_SET_BIN_DATA_1 fields)
{
#ifndef NDEBUG
    assert((fields.bin_size_address                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA_1,
        .value =
            (fields.bin_size_address                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA_1(...) pack_CP_SET_BIN_DATA_1((struct CP_SET_BIN_DATA_1) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_0 {
    uint32_t							vsc_size;
    uint32_t							vsc_n;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_0(struct CP_SET_BIN_DATA5_0 fields)
{
#ifndef NDEBUG
    assert((fields.vsc_size                          & 0xffffffc0) == 0);
    assert((fields.vsc_n                             & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x07ff0000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_0,
        .value =
            (fields.vsc_size                          << 16) |
            (fields.vsc_n                             << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_0(...) pack_CP_SET_BIN_DATA5_0((struct CP_SET_BIN_DATA5_0) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_1 {
    uint32_t							bin_data_addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_1(struct CP_SET_BIN_DATA5_1 fields)
{
#ifndef NDEBUG
    assert((fields.bin_data_addr_lo                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_1,
        .value =
            (fields.bin_data_addr_lo                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_1(...) pack_CP_SET_BIN_DATA5_1((struct CP_SET_BIN_DATA5_1) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_2 {
    uint32_t							bin_data_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_2(struct CP_SET_BIN_DATA5_2 fields)
{
#ifndef NDEBUG
    assert((fields.bin_data_addr_hi                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_2,
        .value =
            (fields.bin_data_addr_hi                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_2(...) pack_CP_SET_BIN_DATA5_2((struct CP_SET_BIN_DATA5_2) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_3 {
    uint32_t							bin_size_address_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_3(struct CP_SET_BIN_DATA5_3 fields)
{
#ifndef NDEBUG
    assert((fields.bin_size_address_lo               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_3,
        .value =
            (fields.bin_size_address_lo               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_3(...) pack_CP_SET_BIN_DATA5_3((struct CP_SET_BIN_DATA5_3) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_4 {
    uint32_t							bin_size_address_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_4(struct CP_SET_BIN_DATA5_4 fields)
{
#ifndef NDEBUG
    assert((fields.bin_size_address_hi               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_4,
        .value =
            (fields.bin_size_address_hi               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_4(...) pack_CP_SET_BIN_DATA5_4((struct CP_SET_BIN_DATA5_4) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_5 {
    uint32_t							bin_prim_strm_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_5(struct CP_SET_BIN_DATA5_5 fields)
{
#ifndef NDEBUG
    assert((fields.bin_prim_strm_lo                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_5,
        .value =
            (fields.bin_prim_strm_lo                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_5(...) pack_CP_SET_BIN_DATA5_5((struct CP_SET_BIN_DATA5_5) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_6 {
    uint32_t							bin_prim_strm_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_6(struct CP_SET_BIN_DATA5_6 fields)
{
#ifndef NDEBUG
    assert((fields.bin_prim_strm_hi                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_6,
        .value =
            (fields.bin_prim_strm_hi                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_6(...) pack_CP_SET_BIN_DATA5_6((struct CP_SET_BIN_DATA5_6) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_OFFSET_0 {
    uint32_t							vsc_size;
    uint32_t							vsc_n;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_OFFSET_0(struct CP_SET_BIN_DATA5_OFFSET_0 fields)
{
#ifndef NDEBUG
    assert((fields.vsc_size                          & 0xffffffc0) == 0);
    assert((fields.vsc_n                             & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x07ff0000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_OFFSET_0,
        .value =
            (fields.vsc_size                          << 16) |
            (fields.vsc_n                             << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_OFFSET_0(...) pack_CP_SET_BIN_DATA5_OFFSET_0((struct CP_SET_BIN_DATA5_OFFSET_0) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_OFFSET_1 {
    uint32_t							bin_data_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_OFFSET_1(struct CP_SET_BIN_DATA5_OFFSET_1 fields)
{
#ifndef NDEBUG
    assert((fields.bin_data_offset                   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_OFFSET_1,
        .value =
            (fields.bin_data_offset                   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_OFFSET_1(...) pack_CP_SET_BIN_DATA5_OFFSET_1((struct CP_SET_BIN_DATA5_OFFSET_1) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_OFFSET_2 {
    uint32_t							bin_size_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_OFFSET_2(struct CP_SET_BIN_DATA5_OFFSET_2 fields)
{
#ifndef NDEBUG
    assert((fields.bin_size_offset                   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_OFFSET_2,
        .value =
            (fields.bin_size_offset                   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_OFFSET_2(...) pack_CP_SET_BIN_DATA5_OFFSET_2((struct CP_SET_BIN_DATA5_OFFSET_2) { __VA_ARGS__ })

struct CP_SET_BIN_DATA5_OFFSET_3 {
    uint32_t							bin_data2_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_BIN_DATA5_OFFSET_3(struct CP_SET_BIN_DATA5_OFFSET_3 fields)
{
#ifndef NDEBUG
    assert((fields.bin_data2_offset                  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_BIN_DATA5_OFFSET_3,
        .value =
            (fields.bin_data2_offset                  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_BIN_DATA5_OFFSET_3(...) pack_CP_SET_BIN_DATA5_OFFSET_3((struct CP_SET_BIN_DATA5_OFFSET_3) { __VA_ARGS__ })

struct CP_REG_RMW_0 {
    uint32_t							dst_reg;
    uint32_t							rotate;
    bool							src1_add;
    bool							src1_is_reg;
    bool							src0_is_reg;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_RMW_0(struct CP_REG_RMW_0 fields)
{
#ifndef NDEBUG
    assert((fields.dst_reg                           & 0xfffc0000) == 0);
    assert((fields.rotate                            & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0xff03ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_RMW_0,
        .value =
            (fields.dst_reg                           <<  0) |
            (fields.rotate                            << 24) |
            (fields.src1_add                          << 29) |
            (fields.src1_is_reg                       << 30) |
            (fields.src0_is_reg                       << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_RMW_0(...) pack_CP_REG_RMW_0((struct CP_REG_RMW_0) { __VA_ARGS__ })

struct CP_REG_RMW_1 {
    uint32_t							src0;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_RMW_1(struct CP_REG_RMW_1 fields)
{
#ifndef NDEBUG
    assert((fields.src0                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_RMW_1,
        .value =
            (fields.src0                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_RMW_1(...) pack_CP_REG_RMW_1((struct CP_REG_RMW_1) { __VA_ARGS__ })

struct CP_REG_RMW_2 {
    uint32_t							src1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_RMW_2(struct CP_REG_RMW_2 fields)
{
#ifndef NDEBUG
    assert((fields.src1                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_RMW_2,
        .value =
            (fields.src1                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_RMW_2(...) pack_CP_REG_RMW_2((struct CP_REG_RMW_2) { __VA_ARGS__ })

struct CP_REG_TO_MEM_0 {
    uint32_t							reg;
    uint32_t							cnt;
    bool							_64b;
    bool							accumulate;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_0(struct CP_REG_TO_MEM_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.cnt                               & 0xfffff000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.cnt                               << 18) |
            (fields._64b                              << 30) |
            (fields.accumulate                        << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_0(...) pack_CP_REG_TO_MEM_0((struct CP_REG_TO_MEM_0) { __VA_ARGS__ })

struct CP_REG_TO_MEM_1 {
    uint32_t							dest;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_1(struct CP_REG_TO_MEM_1 fields)
{
#ifndef NDEBUG
    assert((fields.dest                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_1,
        .value =
            (fields.dest                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_1(...) pack_CP_REG_TO_MEM_1((struct CP_REG_TO_MEM_1) { __VA_ARGS__ })

struct CP_REG_TO_MEM_2 {
    uint32_t							dest_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_2(struct CP_REG_TO_MEM_2 fields)
{
#ifndef NDEBUG
    assert((fields.dest_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_2,
        .value =
            (fields.dest_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_2(...) pack_CP_REG_TO_MEM_2((struct CP_REG_TO_MEM_2) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_REG_0 {
    uint32_t							reg;
    uint32_t							cnt;
    bool							_64b;
    bool							accumulate;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_REG_0(struct CP_REG_TO_MEM_OFFSET_REG_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.cnt                               & 0xfffff000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_REG_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.cnt                               << 18) |
            (fields._64b                              << 30) |
            (fields.accumulate                        << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_REG_0(...) pack_CP_REG_TO_MEM_OFFSET_REG_0((struct CP_REG_TO_MEM_OFFSET_REG_0) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_REG_1 {
    uint32_t							dest;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_REG_1(struct CP_REG_TO_MEM_OFFSET_REG_1 fields)
{
#ifndef NDEBUG
    assert((fields.dest                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_REG_1,
        .value =
            (fields.dest                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_REG_1(...) pack_CP_REG_TO_MEM_OFFSET_REG_1((struct CP_REG_TO_MEM_OFFSET_REG_1) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_REG_2 {
    uint32_t							dest_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_REG_2(struct CP_REG_TO_MEM_OFFSET_REG_2 fields)
{
#ifndef NDEBUG
    assert((fields.dest_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_REG_2,
        .value =
            (fields.dest_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_REG_2(...) pack_CP_REG_TO_MEM_OFFSET_REG_2((struct CP_REG_TO_MEM_OFFSET_REG_2) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_REG_3 {
    uint32_t							offset0;
    bool							offset0_scratch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_REG_3(struct CP_REG_TO_MEM_OFFSET_REG_3 fields)
{
#ifndef NDEBUG
    assert((fields.offset0                           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x000bffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_REG_3,
        .value =
            (fields.offset0                           <<  0) |
            (fields.offset0_scratch                   << 19) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_REG_3(...) pack_CP_REG_TO_MEM_OFFSET_REG_3((struct CP_REG_TO_MEM_OFFSET_REG_3) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_MEM_0 {
    uint32_t							reg;
    uint32_t							cnt;
    bool							_64b;
    bool							accumulate;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_MEM_0(struct CP_REG_TO_MEM_OFFSET_MEM_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.cnt                               & 0xfffff000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_MEM_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.cnt                               << 18) |
            (fields._64b                              << 30) |
            (fields.accumulate                        << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_MEM_0(...) pack_CP_REG_TO_MEM_OFFSET_MEM_0((struct CP_REG_TO_MEM_OFFSET_MEM_0) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_MEM_1 {
    uint32_t							dest;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_MEM_1(struct CP_REG_TO_MEM_OFFSET_MEM_1 fields)
{
#ifndef NDEBUG
    assert((fields.dest                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_MEM_1,
        .value =
            (fields.dest                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_MEM_1(...) pack_CP_REG_TO_MEM_OFFSET_MEM_1((struct CP_REG_TO_MEM_OFFSET_MEM_1) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_MEM_2 {
    uint32_t							dest_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_MEM_2(struct CP_REG_TO_MEM_OFFSET_MEM_2 fields)
{
#ifndef NDEBUG
    assert((fields.dest_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_MEM_2,
        .value =
            (fields.dest_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_MEM_2(...) pack_CP_REG_TO_MEM_OFFSET_MEM_2((struct CP_REG_TO_MEM_OFFSET_MEM_2) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_MEM_3 {
    uint32_t							offset_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_MEM_3(struct CP_REG_TO_MEM_OFFSET_MEM_3 fields)
{
#ifndef NDEBUG
    assert((fields.offset_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_MEM_3,
        .value =
            (fields.offset_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_MEM_3(...) pack_CP_REG_TO_MEM_OFFSET_MEM_3((struct CP_REG_TO_MEM_OFFSET_MEM_3) { __VA_ARGS__ })

struct CP_REG_TO_MEM_OFFSET_MEM_4 {
    uint32_t							offset_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_MEM_OFFSET_MEM_4(struct CP_REG_TO_MEM_OFFSET_MEM_4 fields)
{
#ifndef NDEBUG
    assert((fields.offset_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_MEM_OFFSET_MEM_4,
        .value =
            (fields.offset_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_MEM_OFFSET_MEM_4(...) pack_CP_REG_TO_MEM_OFFSET_MEM_4((struct CP_REG_TO_MEM_OFFSET_MEM_4) { __VA_ARGS__ })

struct CP_MEM_TO_REG_0 {
    uint32_t							reg;
    uint32_t							cnt;
    bool							shift_by_2;
    bool							unk31;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_TO_REG_0(struct CP_MEM_TO_REG_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.cnt                               & 0xfffff800) == 0);
    assert((fields.unknown                           & 0xfffbffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_TO_REG_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.cnt                               << 19) |
            (fields.shift_by_2                        << 30) |
            (fields.unk31                             << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_TO_REG_0(...) pack_CP_MEM_TO_REG_0((struct CP_MEM_TO_REG_0) { __VA_ARGS__ })

struct CP_MEM_TO_REG_1 {
    uint32_t							src;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_TO_REG_1(struct CP_MEM_TO_REG_1 fields)
{
#ifndef NDEBUG
    assert((fields.src                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_TO_REG_1,
        .value =
            (fields.src                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_TO_REG_1(...) pack_CP_MEM_TO_REG_1((struct CP_MEM_TO_REG_1) { __VA_ARGS__ })

struct CP_MEM_TO_REG_2 {
    uint32_t							src_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_TO_REG_2(struct CP_MEM_TO_REG_2 fields)
{
#ifndef NDEBUG
    assert((fields.src_hi                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_TO_REG_2,
        .value =
            (fields.src_hi                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_TO_REG_2(...) pack_CP_MEM_TO_REG_2((struct CP_MEM_TO_REG_2) { __VA_ARGS__ })

struct CP_MEM_TO_MEM_0 {
    bool							neg_a;
    bool							neg_b;
    bool							neg_c;
    bool							_double;
    bool							wait_for_mem_writes;
    bool							unk31;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_TO_MEM_0(struct CP_MEM_TO_MEM_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xe0000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_TO_MEM_0,
        .value =
            (fields.neg_a                             <<  0) |
            (fields.neg_b                             <<  1) |
            (fields.neg_c                             <<  2) |
            (fields._double                           << 29) |
            (fields.wait_for_mem_writes               << 30) |
            (fields.unk31                             << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_TO_MEM_0(...) pack_CP_MEM_TO_MEM_0((struct CP_MEM_TO_MEM_0) { __VA_ARGS__ })

struct CP_MEMCPY_0 {
    uint32_t							dwords;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEMCPY_0(struct CP_MEMCPY_0 fields)
{
#ifndef NDEBUG
    assert((fields.dwords                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEMCPY_0,
        .value =
            (fields.dwords                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEMCPY_0(...) pack_CP_MEMCPY_0((struct CP_MEMCPY_0) { __VA_ARGS__ })

struct CP_MEMCPY_1 {
    uint32_t							src_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEMCPY_1(struct CP_MEMCPY_1 fields)
{
#ifndef NDEBUG
    assert((fields.src_lo                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEMCPY_1,
        .value =
            (fields.src_lo                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEMCPY_1(...) pack_CP_MEMCPY_1((struct CP_MEMCPY_1) { __VA_ARGS__ })

struct CP_MEMCPY_2 {
    uint32_t							src_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEMCPY_2(struct CP_MEMCPY_2 fields)
{
#ifndef NDEBUG
    assert((fields.src_hi                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEMCPY_2,
        .value =
            (fields.src_hi                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEMCPY_2(...) pack_CP_MEMCPY_2((struct CP_MEMCPY_2) { __VA_ARGS__ })

struct CP_MEMCPY_3 {
    uint32_t							dst_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEMCPY_3(struct CP_MEMCPY_3 fields)
{
#ifndef NDEBUG
    assert((fields.dst_lo                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEMCPY_3,
        .value =
            (fields.dst_lo                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEMCPY_3(...) pack_CP_MEMCPY_3((struct CP_MEMCPY_3) { __VA_ARGS__ })

struct CP_MEMCPY_4 {
    uint32_t							dst_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEMCPY_4(struct CP_MEMCPY_4 fields)
{
#ifndef NDEBUG
    assert((fields.dst_hi                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEMCPY_4,
        .value =
            (fields.dst_hi                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEMCPY_4(...) pack_CP_MEMCPY_4((struct CP_MEMCPY_4) { __VA_ARGS__ })

struct CP_REG_TO_SCRATCH_0 {
    uint32_t							reg;
    uint32_t							scratch;
    uint32_t							cnt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_TO_SCRATCH_0(struct CP_REG_TO_SCRATCH_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.scratch                           & 0xfffffff8) == 0);
    assert((fields.cnt                               & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x0773ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_TO_SCRATCH_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.scratch                           << 20) |
            (fields.cnt                               << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_TO_SCRATCH_0(...) pack_CP_REG_TO_SCRATCH_0((struct CP_REG_TO_SCRATCH_0) { __VA_ARGS__ })

struct CP_SCRATCH_TO_REG_0 {
    uint32_t							reg;
    bool							unk18;
    uint32_t							scratch;
    uint32_t							cnt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SCRATCH_TO_REG_0(struct CP_SCRATCH_TO_REG_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.scratch                           & 0xfffffff8) == 0);
    assert((fields.cnt                               & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x0777ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SCRATCH_TO_REG_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.unk18                             << 18) |
            (fields.scratch                           << 20) |
            (fields.cnt                               << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SCRATCH_TO_REG_0(...) pack_CP_SCRATCH_TO_REG_0((struct CP_SCRATCH_TO_REG_0) { __VA_ARGS__ })

struct CP_SCRATCH_WRITE_0 {
    uint32_t							scratch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SCRATCH_WRITE_0(struct CP_SCRATCH_WRITE_0 fields)
{
#ifndef NDEBUG
    assert((fields.scratch                           & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00700000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SCRATCH_WRITE_0,
        .value =
            (fields.scratch                           << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SCRATCH_WRITE_0(...) pack_CP_SCRATCH_WRITE_0((struct CP_SCRATCH_WRITE_0) { __VA_ARGS__ })

struct CP_MEM_WRITE_0 {
    uint32_t							addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_WRITE_0(struct CP_MEM_WRITE_0 fields)
{
#ifndef NDEBUG
    assert((fields.addr_lo                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_WRITE_0,
        .value =
            (fields.addr_lo                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_WRITE_0(...) pack_CP_MEM_WRITE_0((struct CP_MEM_WRITE_0) { __VA_ARGS__ })

struct CP_MEM_WRITE_1 {
    uint32_t							addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_MEM_WRITE_1(struct CP_MEM_WRITE_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_MEM_WRITE_1,
        .value =
            (fields.addr_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_MEM_WRITE_1(...) pack_CP_MEM_WRITE_1((struct CP_MEM_WRITE_1) { __VA_ARGS__ })

struct CP_COND_WRITE_0 {
    enum cp_cond_function					function;
    bool							poll_memory;
    bool							write_memory;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_0(struct CP_COND_WRITE_0 fields)
{
#ifndef NDEBUG
    assert((fields.function                          & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00000117) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_0,
        .value =
            (fields.function                          <<  0) |
            (fields.poll_memory                       <<  4) |
            (fields.write_memory                      <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_0(...) pack_CP_COND_WRITE_0((struct CP_COND_WRITE_0) { __VA_ARGS__ })

struct CP_COND_WRITE_1 {
    uint32_t							poll_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_1(struct CP_COND_WRITE_1 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_1,
        .value =
            (fields.poll_addr                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_1(...) pack_CP_COND_WRITE_1((struct CP_COND_WRITE_1) { __VA_ARGS__ })

struct CP_COND_WRITE_2 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_2(struct CP_COND_WRITE_2 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_2,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_2(...) pack_CP_COND_WRITE_2((struct CP_COND_WRITE_2) { __VA_ARGS__ })

struct CP_COND_WRITE_3 {
    uint32_t							mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_3(struct CP_COND_WRITE_3 fields)
{
#ifndef NDEBUG
    assert((fields.mask                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_3,
        .value =
            (fields.mask                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_3(...) pack_CP_COND_WRITE_3((struct CP_COND_WRITE_3) { __VA_ARGS__ })

struct CP_COND_WRITE_4 {
    uint32_t							write_addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_4(struct CP_COND_WRITE_4 fields)
{
#ifndef NDEBUG
    assert((fields.write_addr                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_4,
        .value =
            (fields.write_addr                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_4(...) pack_CP_COND_WRITE_4((struct CP_COND_WRITE_4) { __VA_ARGS__ })

struct CP_COND_WRITE_5 {
    uint32_t							write_data;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE_5(struct CP_COND_WRITE_5 fields)
{
#ifndef NDEBUG
    assert((fields.write_data                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE_5,
        .value =
            (fields.write_data                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE_5(...) pack_CP_COND_WRITE_5((struct CP_COND_WRITE_5) { __VA_ARGS__ })

struct CP_COND_WRITE5_0 {
    enum cp_cond_function					function;
    bool							signed_compare;
    bool							poll_memory;
    bool							poll_scratch;
    bool							write_memory;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_0(struct CP_COND_WRITE5_0 fields)
{
#ifndef NDEBUG
    assert((fields.function                          & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x0000013f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_0,
        .value =
            (fields.function                          <<  0) |
            (fields.signed_compare                    <<  3) |
            (fields.poll_memory                       <<  4) |
            (fields.poll_scratch                      <<  5) |
            (fields.write_memory                      <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_0(...) pack_CP_COND_WRITE5_0((struct CP_COND_WRITE5_0) { __VA_ARGS__ })

struct CP_COND_WRITE5_1 {
    uint32_t							poll_addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_1(struct CP_COND_WRITE5_1 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_lo                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_1,
        .value =
            (fields.poll_addr_lo                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_1(...) pack_CP_COND_WRITE5_1((struct CP_COND_WRITE5_1) { __VA_ARGS__ })

struct CP_COND_WRITE5_2 {
    uint32_t							poll_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_2(struct CP_COND_WRITE5_2 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_hi                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_2,
        .value =
            (fields.poll_addr_hi                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_2(...) pack_CP_COND_WRITE5_2((struct CP_COND_WRITE5_2) { __VA_ARGS__ })

struct CP_COND_WRITE5_3 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_3(struct CP_COND_WRITE5_3 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_3,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_3(...) pack_CP_COND_WRITE5_3((struct CP_COND_WRITE5_3) { __VA_ARGS__ })

struct CP_COND_WRITE5_4 {
    uint32_t							mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_4(struct CP_COND_WRITE5_4 fields)
{
#ifndef NDEBUG
    assert((fields.mask                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_4,
        .value =
            (fields.mask                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_4(...) pack_CP_COND_WRITE5_4((struct CP_COND_WRITE5_4) { __VA_ARGS__ })

struct CP_COND_WRITE5_5 {
    uint32_t							write_addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_5(struct CP_COND_WRITE5_5 fields)
{
#ifndef NDEBUG
    assert((fields.write_addr_lo                     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_5,
        .value =
            (fields.write_addr_lo                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_5(...) pack_CP_COND_WRITE5_5((struct CP_COND_WRITE5_5) { __VA_ARGS__ })

struct CP_COND_WRITE5_6 {
    uint32_t							write_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_6(struct CP_COND_WRITE5_6 fields)
{
#ifndef NDEBUG
    assert((fields.write_addr_hi                     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_6,
        .value =
            (fields.write_addr_hi                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_6(...) pack_CP_COND_WRITE5_6((struct CP_COND_WRITE5_6) { __VA_ARGS__ })

struct CP_COND_WRITE5_7 {
    uint32_t							write_data;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_WRITE5_7(struct CP_COND_WRITE5_7 fields)
{
#ifndef NDEBUG
    assert((fields.write_data                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_WRITE5_7,
        .value =
            (fields.write_data                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_WRITE5_7(...) pack_CP_COND_WRITE5_7((struct CP_COND_WRITE5_7) { __VA_ARGS__ })

struct CP_WAIT_MEM_GTE_0 {
    uint32_t							reserved;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_MEM_GTE_0(struct CP_WAIT_MEM_GTE_0 fields)
{
#ifndef NDEBUG
    assert((fields.reserved                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_MEM_GTE_0,
        .value =
            (fields.reserved                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_MEM_GTE_0(...) pack_CP_WAIT_MEM_GTE_0((struct CP_WAIT_MEM_GTE_0) { __VA_ARGS__ })

struct CP_WAIT_MEM_GTE_1 {
    uint32_t							poll_addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_MEM_GTE_1(struct CP_WAIT_MEM_GTE_1 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_lo                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_MEM_GTE_1,
        .value =
            (fields.poll_addr_lo                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_MEM_GTE_1(...) pack_CP_WAIT_MEM_GTE_1((struct CP_WAIT_MEM_GTE_1) { __VA_ARGS__ })

struct CP_WAIT_MEM_GTE_2 {
    uint32_t							poll_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_MEM_GTE_2(struct CP_WAIT_MEM_GTE_2 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_hi                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_MEM_GTE_2,
        .value =
            (fields.poll_addr_hi                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_MEM_GTE_2(...) pack_CP_WAIT_MEM_GTE_2((struct CP_WAIT_MEM_GTE_2) { __VA_ARGS__ })

struct CP_WAIT_MEM_GTE_3 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_MEM_GTE_3(struct CP_WAIT_MEM_GTE_3 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_MEM_GTE_3,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_MEM_GTE_3(...) pack_CP_WAIT_MEM_GTE_3((struct CP_WAIT_MEM_GTE_3) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_0 {
    enum cp_cond_function					function;
    bool							signed_compare;
    bool							poll_memory;
    bool							poll_scratch;
    bool							write_memory;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_0(struct CP_WAIT_REG_MEM_0 fields)
{
#ifndef NDEBUG
    assert((fields.function                          & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x0000013f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_0,
        .value =
            (fields.function                          <<  0) |
            (fields.signed_compare                    <<  3) |
            (fields.poll_memory                       <<  4) |
            (fields.poll_scratch                      <<  5) |
            (fields.write_memory                      <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_0(...) pack_CP_WAIT_REG_MEM_0((struct CP_WAIT_REG_MEM_0) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_1 {
    uint32_t							poll_addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_1(struct CP_WAIT_REG_MEM_1 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_lo                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_1,
        .value =
            (fields.poll_addr_lo                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_1(...) pack_CP_WAIT_REG_MEM_1((struct CP_WAIT_REG_MEM_1) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_2 {
    uint32_t							poll_addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_2(struct CP_WAIT_REG_MEM_2 fields)
{
#ifndef NDEBUG
    assert((fields.poll_addr_hi                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_2,
        .value =
            (fields.poll_addr_hi                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_2(...) pack_CP_WAIT_REG_MEM_2((struct CP_WAIT_REG_MEM_2) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_3 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_3(struct CP_WAIT_REG_MEM_3 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_3,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_3(...) pack_CP_WAIT_REG_MEM_3((struct CP_WAIT_REG_MEM_3) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_4 {
    uint32_t							mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_4(struct CP_WAIT_REG_MEM_4 fields)
{
#ifndef NDEBUG
    assert((fields.mask                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_4,
        .value =
            (fields.mask                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_4(...) pack_CP_WAIT_REG_MEM_4((struct CP_WAIT_REG_MEM_4) { __VA_ARGS__ })

struct CP_WAIT_REG_MEM_5 {
    uint32_t							delay_loop_cycles;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_REG_MEM_5(struct CP_WAIT_REG_MEM_5 fields)
{
#ifndef NDEBUG
    assert((fields.delay_loop_cycles                 & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_REG_MEM_5,
        .value =
            (fields.delay_loop_cycles                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_REG_MEM_5(...) pack_CP_WAIT_REG_MEM_5((struct CP_WAIT_REG_MEM_5) { __VA_ARGS__ })

struct CP_WAIT_TWO_REGS_0 {
    uint32_t							reg0;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_TWO_REGS_0(struct CP_WAIT_TWO_REGS_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg0                              & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x0003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_TWO_REGS_0,
        .value =
            (fields.reg0                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_TWO_REGS_0(...) pack_CP_WAIT_TWO_REGS_0((struct CP_WAIT_TWO_REGS_0) { __VA_ARGS__ })

struct CP_WAIT_TWO_REGS_1 {
    uint32_t							reg1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_TWO_REGS_1(struct CP_WAIT_TWO_REGS_1 fields)
{
#ifndef NDEBUG
    assert((fields.reg1                              & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x0003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_TWO_REGS_1,
        .value =
            (fields.reg1                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_TWO_REGS_1(...) pack_CP_WAIT_TWO_REGS_1((struct CP_WAIT_TWO_REGS_1) { __VA_ARGS__ })

struct CP_WAIT_TWO_REGS_2 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_WAIT_TWO_REGS_2(struct CP_WAIT_TWO_REGS_2 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_WAIT_TWO_REGS_2,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_WAIT_TWO_REGS_2(...) pack_CP_WAIT_TWO_REGS_2((struct CP_WAIT_TWO_REGS_2) { __VA_ARGS__ })

struct CP_DISPATCH_COMPUTE_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DISPATCH_COMPUTE_0(struct CP_DISPATCH_COMPUTE_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DISPATCH_COMPUTE_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DISPATCH_COMPUTE_0(...) pack_CP_DISPATCH_COMPUTE_0((struct CP_DISPATCH_COMPUTE_0) { __VA_ARGS__ })

struct CP_DISPATCH_COMPUTE_1 {
    uint32_t							x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DISPATCH_COMPUTE_1(struct CP_DISPATCH_COMPUTE_1 fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DISPATCH_COMPUTE_1,
        .value =
            (fields.x                                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DISPATCH_COMPUTE_1(...) pack_CP_DISPATCH_COMPUTE_1((struct CP_DISPATCH_COMPUTE_1) { __VA_ARGS__ })

struct CP_DISPATCH_COMPUTE_2 {
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DISPATCH_COMPUTE_2(struct CP_DISPATCH_COMPUTE_2 fields)
{
#ifndef NDEBUG
    assert((fields.y                                 & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DISPATCH_COMPUTE_2,
        .value =
            (fields.y                                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DISPATCH_COMPUTE_2(...) pack_CP_DISPATCH_COMPUTE_2((struct CP_DISPATCH_COMPUTE_2) { __VA_ARGS__ })

struct CP_DISPATCH_COMPUTE_3 {
    uint32_t							z;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_DISPATCH_COMPUTE_3(struct CP_DISPATCH_COMPUTE_3 fields)
{
#ifndef NDEBUG
    assert((fields.z                                 & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_DISPATCH_COMPUTE_3,
        .value =
            (fields.z                                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_DISPATCH_COMPUTE_3(...) pack_CP_DISPATCH_COMPUTE_3((struct CP_DISPATCH_COMPUTE_3) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_0 {
    enum render_mode_cmd					mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_0(struct CP_SET_RENDER_MODE_0 fields)
{
#ifndef NDEBUG
    assert((fields.mode                              & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_0,
        .value =
            (fields.mode                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_0(...) pack_CP_SET_RENDER_MODE_0((struct CP_SET_RENDER_MODE_0) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_1 {
    uint32_t							addr_0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_1(struct CP_SET_RENDER_MODE_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_1,
        .value =
            (fields.addr_0_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_1(...) pack_CP_SET_RENDER_MODE_1((struct CP_SET_RENDER_MODE_1) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_2 {
    uint32_t							addr_0_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_2(struct CP_SET_RENDER_MODE_2 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_2,
        .value =
            (fields.addr_0_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_2(...) pack_CP_SET_RENDER_MODE_2((struct CP_SET_RENDER_MODE_2) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_3 {
    bool							vsc_enable;
    bool							gmem_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_3(struct CP_SET_RENDER_MODE_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000018) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_3,
        .value =
            (fields.vsc_enable                        <<  3) |
            (fields.gmem_enable                       <<  4) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_3(...) pack_CP_SET_RENDER_MODE_3((struct CP_SET_RENDER_MODE_3) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_4 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_4(struct CP_SET_RENDER_MODE_4 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_4,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_4(...) pack_CP_SET_RENDER_MODE_4((struct CP_SET_RENDER_MODE_4) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_5 {
    uint32_t							addr_1_len;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_5(struct CP_SET_RENDER_MODE_5 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_len                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_5,
        .value =
            (fields.addr_1_len                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_5(...) pack_CP_SET_RENDER_MODE_5((struct CP_SET_RENDER_MODE_5) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_6 {
    uint32_t							addr_1_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_6(struct CP_SET_RENDER_MODE_6 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_6,
        .value =
            (fields.addr_1_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_6(...) pack_CP_SET_RENDER_MODE_6((struct CP_SET_RENDER_MODE_6) { __VA_ARGS__ })

struct CP_SET_RENDER_MODE_7 {
    uint32_t							addr_1_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_RENDER_MODE_7(struct CP_SET_RENDER_MODE_7 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_RENDER_MODE_7,
        .value =
            (fields.addr_1_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_RENDER_MODE_7(...) pack_CP_SET_RENDER_MODE_7((struct CP_SET_RENDER_MODE_7) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_0 {
    uint32_t							addr_0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_0(struct CP_COMPUTE_CHECKPOINT_0 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_0,
        .value =
            (fields.addr_0_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_0(...) pack_CP_COMPUTE_CHECKPOINT_0((struct CP_COMPUTE_CHECKPOINT_0) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_1 {
    uint32_t							addr_0_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_1(struct CP_COMPUTE_CHECKPOINT_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_1,
        .value =
            (fields.addr_0_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_1(...) pack_CP_COMPUTE_CHECKPOINT_1((struct CP_COMPUTE_CHECKPOINT_1) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_2(struct CP_COMPUTE_CHECKPOINT_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_2(...) pack_CP_COMPUTE_CHECKPOINT_2((struct CP_COMPUTE_CHECKPOINT_2) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_3 {
    uint32_t							addr_1_len;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_3(struct CP_COMPUTE_CHECKPOINT_3 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_len                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_3,
        .value =
            (fields.addr_1_len                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_3(...) pack_CP_COMPUTE_CHECKPOINT_3((struct CP_COMPUTE_CHECKPOINT_3) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_4 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_4(struct CP_COMPUTE_CHECKPOINT_4 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_4,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_4(...) pack_CP_COMPUTE_CHECKPOINT_4((struct CP_COMPUTE_CHECKPOINT_4) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_5 {
    uint32_t							addr_1_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_5(struct CP_COMPUTE_CHECKPOINT_5 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_5,
        .value =
            (fields.addr_1_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_5(...) pack_CP_COMPUTE_CHECKPOINT_5((struct CP_COMPUTE_CHECKPOINT_5) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_6 {
    uint32_t							addr_1_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_6(struct CP_COMPUTE_CHECKPOINT_6 fields)
{
#ifndef NDEBUG
    assert((fields.addr_1_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_6,
        .value =
            (fields.addr_1_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_6(...) pack_CP_COMPUTE_CHECKPOINT_6((struct CP_COMPUTE_CHECKPOINT_6) { __VA_ARGS__ })

struct CP_COMPUTE_CHECKPOINT_7 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COMPUTE_CHECKPOINT_7(struct CP_COMPUTE_CHECKPOINT_7 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COMPUTE_CHECKPOINT_7,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COMPUTE_CHECKPOINT_7(...) pack_CP_COMPUTE_CHECKPOINT_7((struct CP_COMPUTE_CHECKPOINT_7) { __VA_ARGS__ })

struct CP_PERFCOUNTER_ACTION_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_PERFCOUNTER_ACTION_0(struct CP_PERFCOUNTER_ACTION_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_PERFCOUNTER_ACTION_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_PERFCOUNTER_ACTION_0(...) pack_CP_PERFCOUNTER_ACTION_0((struct CP_PERFCOUNTER_ACTION_0) { __VA_ARGS__ })

struct CP_PERFCOUNTER_ACTION_1 {
    uint32_t							addr_0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_PERFCOUNTER_ACTION_1(struct CP_PERFCOUNTER_ACTION_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_PERFCOUNTER_ACTION_1,
        .value =
            (fields.addr_0_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_PERFCOUNTER_ACTION_1(...) pack_CP_PERFCOUNTER_ACTION_1((struct CP_PERFCOUNTER_ACTION_1) { __VA_ARGS__ })

struct CP_PERFCOUNTER_ACTION_2 {
    uint32_t							addr_0_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_PERFCOUNTER_ACTION_2(struct CP_PERFCOUNTER_ACTION_2 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_PERFCOUNTER_ACTION_2,
        .value =
            (fields.addr_0_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_PERFCOUNTER_ACTION_2(...) pack_CP_PERFCOUNTER_ACTION_2((struct CP_PERFCOUNTER_ACTION_2) { __VA_ARGS__ })

struct CP_EVENT_WRITE_0 {
    enum vgt_event_type						event;
    bool							timestamp;
    bool							irq;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EVENT_WRITE_0(struct CP_EVENT_WRITE_0 fields)
{
#ifndef NDEBUG
    assert((fields.event                             & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xc00000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EVENT_WRITE_0,
        .value =
            (fields.event                             <<  0) |
            (fields.timestamp                         << 30) |
            (fields.irq                               << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EVENT_WRITE_0(...) pack_CP_EVENT_WRITE_0((struct CP_EVENT_WRITE_0) { __VA_ARGS__ })

struct CP_EVENT_WRITE_1 {
    uint32_t							addr_0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EVENT_WRITE_1(struct CP_EVENT_WRITE_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_lo                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EVENT_WRITE_1,
        .value =
            (fields.addr_0_lo                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EVENT_WRITE_1(...) pack_CP_EVENT_WRITE_1((struct CP_EVENT_WRITE_1) { __VA_ARGS__ })

struct CP_EVENT_WRITE_2 {
    uint32_t							addr_0_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EVENT_WRITE_2(struct CP_EVENT_WRITE_2 fields)
{
#ifndef NDEBUG
    assert((fields.addr_0_hi                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EVENT_WRITE_2,
        .value =
            (fields.addr_0_hi                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EVENT_WRITE_2(...) pack_CP_EVENT_WRITE_2((struct CP_EVENT_WRITE_2) { __VA_ARGS__ })

struct CP_EVENT_WRITE_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EVENT_WRITE_3(struct CP_EVENT_WRITE_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EVENT_WRITE_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EVENT_WRITE_3(...) pack_CP_EVENT_WRITE_3((struct CP_EVENT_WRITE_3) { __VA_ARGS__ })

struct CP_BLIT_0 {
    enum cp_blit_cmd						op;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_BLIT_0(struct CP_BLIT_0 fields)
{
#ifndef NDEBUG
    assert((fields.op                                & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_BLIT_0,
        .value =
            (fields.op                                <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_BLIT_0(...) pack_CP_BLIT_0((struct CP_BLIT_0) { __VA_ARGS__ })

struct CP_BLIT_1 {
    uint32_t							src_x1;
    uint32_t							src_y1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_BLIT_1(struct CP_BLIT_1 fields)
{
#ifndef NDEBUG
    assert((fields.src_x1                            & 0xffffc000) == 0);
    assert((fields.src_y1                            & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_BLIT_1,
        .value =
            (fields.src_x1                            <<  0) |
            (fields.src_y1                            << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_BLIT_1(...) pack_CP_BLIT_1((struct CP_BLIT_1) { __VA_ARGS__ })

struct CP_BLIT_2 {
    uint32_t							src_x2;
    uint32_t							src_y2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_BLIT_2(struct CP_BLIT_2 fields)
{
#ifndef NDEBUG
    assert((fields.src_x2                            & 0xffffc000) == 0);
    assert((fields.src_y2                            & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_BLIT_2,
        .value =
            (fields.src_x2                            <<  0) |
            (fields.src_y2                            << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_BLIT_2(...) pack_CP_BLIT_2((struct CP_BLIT_2) { __VA_ARGS__ })

struct CP_BLIT_3 {
    uint32_t							dst_x1;
    uint32_t							dst_y1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_BLIT_3(struct CP_BLIT_3 fields)
{
#ifndef NDEBUG
    assert((fields.dst_x1                            & 0xffffc000) == 0);
    assert((fields.dst_y1                            & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_BLIT_3,
        .value =
            (fields.dst_x1                            <<  0) |
            (fields.dst_y1                            << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_BLIT_3(...) pack_CP_BLIT_3((struct CP_BLIT_3) { __VA_ARGS__ })

struct CP_BLIT_4 {
    uint32_t							dst_x2;
    uint32_t							dst_y2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_BLIT_4(struct CP_BLIT_4 fields)
{
#ifndef NDEBUG
    assert((fields.dst_x2                            & 0xffffc000) == 0);
    assert((fields.dst_y2                            & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_BLIT_4,
        .value =
            (fields.dst_x2                            <<  0) |
            (fields.dst_y2                            << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_BLIT_4(...) pack_CP_BLIT_4((struct CP_BLIT_4) { __VA_ARGS__ })

struct CP_EXEC_CS_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EXEC_CS_0(struct CP_EXEC_CS_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EXEC_CS_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EXEC_CS_0(...) pack_CP_EXEC_CS_0((struct CP_EXEC_CS_0) { __VA_ARGS__ })

struct CP_EXEC_CS_1 {
    uint32_t							ngroups_x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EXEC_CS_1(struct CP_EXEC_CS_1 fields)
{
#ifndef NDEBUG
    assert((fields.ngroups_x                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EXEC_CS_1,
        .value =
            (fields.ngroups_x                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EXEC_CS_1(...) pack_CP_EXEC_CS_1((struct CP_EXEC_CS_1) { __VA_ARGS__ })

struct CP_EXEC_CS_2 {
    uint32_t							ngroups_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EXEC_CS_2(struct CP_EXEC_CS_2 fields)
{
#ifndef NDEBUG
    assert((fields.ngroups_y                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EXEC_CS_2,
        .value =
            (fields.ngroups_y                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EXEC_CS_2(...) pack_CP_EXEC_CS_2((struct CP_EXEC_CS_2) { __VA_ARGS__ })

struct CP_EXEC_CS_3 {
    uint32_t							ngroups_z;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_EXEC_CS_3(struct CP_EXEC_CS_3 fields)
{
#ifndef NDEBUG
    assert((fields.ngroups_z                         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_EXEC_CS_3,
        .value =
            (fields.ngroups_z                         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_EXEC_CS_3(...) pack_CP_EXEC_CS_3((struct CP_EXEC_CS_3) { __VA_ARGS__ })

struct A4XX_CP_EXEC_CS_INDIRECT_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_EXEC_CS_INDIRECT_0(struct A4XX_CP_EXEC_CS_INDIRECT_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_EXEC_CS_INDIRECT_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_EXEC_CS_INDIRECT_0(...) pack_A4XX_CP_EXEC_CS_INDIRECT_0((struct A4XX_CP_EXEC_CS_INDIRECT_0) { __VA_ARGS__ })

struct A4XX_CP_EXEC_CS_INDIRECT_1 {
    uint32_t							addr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_EXEC_CS_INDIRECT_1(struct A4XX_CP_EXEC_CS_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr                              & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_EXEC_CS_INDIRECT_1,
        .value =
            (fields.addr                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_EXEC_CS_INDIRECT_1(...) pack_A4XX_CP_EXEC_CS_INDIRECT_1((struct A4XX_CP_EXEC_CS_INDIRECT_1) { __VA_ARGS__ })

struct A4XX_CP_EXEC_CS_INDIRECT_2 {
    uint32_t							localsizex;
    uint32_t							localsizey;
    uint32_t							localsizez;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A4XX_CP_EXEC_CS_INDIRECT_2(struct A4XX_CP_EXEC_CS_INDIRECT_2 fields)
{
#ifndef NDEBUG
    assert((fields.localsizex                        & 0xfffffc00) == 0);
    assert((fields.localsizey                        & 0xfffffc00) == 0);
    assert((fields.localsizez                        & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xfffffffc) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A4XX_CP_EXEC_CS_INDIRECT_2,
        .value =
            (fields.localsizex                        <<  2) |
            (fields.localsizey                        << 12) |
            (fields.localsizez                        << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A4XX_CP_EXEC_CS_INDIRECT_2(...) pack_A4XX_CP_EXEC_CS_INDIRECT_2((struct A4XX_CP_EXEC_CS_INDIRECT_2) { __VA_ARGS__ })

struct A5XX_CP_EXEC_CS_INDIRECT_1 {
    uint32_t							addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_EXEC_CS_INDIRECT_1(struct A5XX_CP_EXEC_CS_INDIRECT_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_lo                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_EXEC_CS_INDIRECT_1,
        .value =
            (fields.addr_lo                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_EXEC_CS_INDIRECT_1(...) pack_A5XX_CP_EXEC_CS_INDIRECT_1((struct A5XX_CP_EXEC_CS_INDIRECT_1) { __VA_ARGS__ })

struct A5XX_CP_EXEC_CS_INDIRECT_2 {
    uint32_t							addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_EXEC_CS_INDIRECT_2(struct A5XX_CP_EXEC_CS_INDIRECT_2 fields)
{
#ifndef NDEBUG
    assert((fields.addr_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_EXEC_CS_INDIRECT_2,
        .value =
            (fields.addr_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_EXEC_CS_INDIRECT_2(...) pack_A5XX_CP_EXEC_CS_INDIRECT_2((struct A5XX_CP_EXEC_CS_INDIRECT_2) { __VA_ARGS__ })

struct A5XX_CP_EXEC_CS_INDIRECT_3 {
    uint32_t							localsizex;
    uint32_t							localsizey;
    uint32_t							localsizez;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A5XX_CP_EXEC_CS_INDIRECT_3(struct A5XX_CP_EXEC_CS_INDIRECT_3 fields)
{
#ifndef NDEBUG
    assert((fields.localsizex                        & 0xfffffc00) == 0);
    assert((fields.localsizey                        & 0xfffffc00) == 0);
    assert((fields.localsizez                        & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xfffffffc) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A5XX_CP_EXEC_CS_INDIRECT_3,
        .value =
            (fields.localsizex                        <<  2) |
            (fields.localsizey                        << 12) |
            (fields.localsizez                        << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A5XX_CP_EXEC_CS_INDIRECT_3(...) pack_A5XX_CP_EXEC_CS_INDIRECT_3((struct A5XX_CP_EXEC_CS_INDIRECT_3) { __VA_ARGS__ })

struct A6XX_CP_SET_MARKER_0 {
    enum a6xx_marker						mode;
    enum a6xx_marker						marker;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SET_MARKER_0(struct A6XX_CP_SET_MARKER_0 fields)
{
#ifndef NDEBUG
    assert((fields.mode                              & 0xfffffe00) == 0);
    assert((fields.marker                            & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SET_MARKER_0,
        .value =
            (fields.mode                              <<  0) |
            (fields.marker                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SET_MARKER_0(...) pack_A6XX_CP_SET_MARKER_0((struct A6XX_CP_SET_MARKER_0) { __VA_ARGS__ })

struct A6XX_CP_SET_PSEUDO_REG__0 {
    enum pseudo_reg						pseudo_reg;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SET_PSEUDO_REG__0(uint32_t i, struct A6XX_CP_SET_PSEUDO_REG__0 fields)
{
#ifndef NDEBUG
    assert((fields.pseudo_reg                        & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SET_PSEUDO_REG__0(i),
        .value =
            (fields.pseudo_reg                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SET_PSEUDO_REG__0(i, ...) pack_A6XX_CP_SET_PSEUDO_REG__0(i, (struct A6XX_CP_SET_PSEUDO_REG__0) { __VA_ARGS__ })

struct A6XX_CP_SET_PSEUDO_REG__1 {
    uint32_t							lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SET_PSEUDO_REG__1(uint32_t i, struct A6XX_CP_SET_PSEUDO_REG__1 fields)
{
#ifndef NDEBUG
    assert((fields.lo                                & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SET_PSEUDO_REG__1(i),
        .value =
            (fields.lo                                <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SET_PSEUDO_REG__1(i, ...) pack_A6XX_CP_SET_PSEUDO_REG__1(i, (struct A6XX_CP_SET_PSEUDO_REG__1) { __VA_ARGS__ })

struct A6XX_CP_SET_PSEUDO_REG__2 {
    uint32_t							hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SET_PSEUDO_REG__2(uint32_t i, struct A6XX_CP_SET_PSEUDO_REG__2 fields)
{
#ifndef NDEBUG
    assert((fields.hi                                & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SET_PSEUDO_REG__2(i),
        .value =
            (fields.hi                                <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SET_PSEUDO_REG__2(i, ...) pack_A6XX_CP_SET_PSEUDO_REG__2(i, (struct A6XX_CP_SET_PSEUDO_REG__2) { __VA_ARGS__ })

struct A6XX_CP_REG_TEST_0 {
    uint32_t							reg;
    uint32_t							bit;
    bool							wait_for_me;
    bool							unk31;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_REG_TEST_0(struct A6XX_CP_REG_TEST_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg                               & 0xfffc0000) == 0);
    assert((fields.bit                               & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x83f3ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_REG_TEST_0,
        .value =
            (fields.reg                               <<  0) |
            (fields.bit                               << 20) |
            (fields.wait_for_me                       << 25) |
            (fields.unk31                             << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_REG_TEST_0(...) pack_A6XX_CP_REG_TEST_0((struct A6XX_CP_REG_TEST_0) { __VA_ARGS__ })

struct CP_COND_REG_EXEC_0 {
    uint32_t							reg0;
    bool							unk18;
    bool							unk20;
    bool							binning;
    bool							gmem;
    bool							sysmem;
    enum compare_mode						mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_REG_EXEC_0(struct CP_COND_REG_EXEC_0 fields)
{
#ifndef NDEBUG
    assert((fields.reg0                              & 0xfffc0000) == 0);
    assert((fields.mode                              & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xfe17ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_REG_EXEC_0,
        .value =
            (fields.reg0                              <<  0) |
            (fields.unk18                             << 18) |
            (fields.unk20                             << 20) |
            (fields.binning                           << 25) |
            (fields.gmem                              << 26) |
            (fields.sysmem                            << 27) |
            (fields.mode                              << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_REG_EXEC_0(...) pack_CP_COND_REG_EXEC_0((struct CP_COND_REG_EXEC_0) { __VA_ARGS__ })

struct CP_COND_REG_EXEC_1 {
    uint32_t							dwords;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_REG_EXEC_1(struct CP_COND_REG_EXEC_1 fields)
{
#ifndef NDEBUG
    assert((fields.dwords                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_REG_EXEC_1,
        .value =
            (fields.dwords                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_REG_EXEC_1(...) pack_CP_COND_REG_EXEC_1((struct CP_COND_REG_EXEC_1) { __VA_ARGS__ })

struct CP_COND_EXEC_0 {
    uint32_t							addr0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_0(struct CP_COND_EXEC_0 fields)
{
#ifndef NDEBUG
    assert((fields.addr0_lo                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_0,
        .value =
            (fields.addr0_lo                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_0(...) pack_CP_COND_EXEC_0((struct CP_COND_EXEC_0) { __VA_ARGS__ })

struct CP_COND_EXEC_1 {
    uint32_t							addr0_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_1(struct CP_COND_EXEC_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr0_hi                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_1,
        .value =
            (fields.addr0_hi                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_1(...) pack_CP_COND_EXEC_1((struct CP_COND_EXEC_1) { __VA_ARGS__ })

struct CP_COND_EXEC_2 {
    uint32_t							addr1_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_2(struct CP_COND_EXEC_2 fields)
{
#ifndef NDEBUG
    assert((fields.addr1_lo                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_2,
        .value =
            (fields.addr1_lo                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_2(...) pack_CP_COND_EXEC_2((struct CP_COND_EXEC_2) { __VA_ARGS__ })

struct CP_COND_EXEC_3 {
    uint32_t							addr1_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_3(struct CP_COND_EXEC_3 fields)
{
#ifndef NDEBUG
    assert((fields.addr1_hi                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_3,
        .value =
            (fields.addr1_hi                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_3(...) pack_CP_COND_EXEC_3((struct CP_COND_EXEC_3) { __VA_ARGS__ })

struct CP_COND_EXEC_4 {
    uint32_t							ref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_4(struct CP_COND_EXEC_4 fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_4,
        .value =
            (fields.ref                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_4(...) pack_CP_COND_EXEC_4((struct CP_COND_EXEC_4) { __VA_ARGS__ })

struct CP_COND_EXEC_5 {
    uint32_t							dwords;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_COND_EXEC_5(struct CP_COND_EXEC_5 fields)
{
#ifndef NDEBUG
    assert((fields.dwords                            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_COND_EXEC_5,
        .value =
            (fields.dwords                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_COND_EXEC_5(...) pack_CP_COND_EXEC_5((struct CP_COND_EXEC_5) { __VA_ARGS__ })

struct CP_SET_CTXSWITCH_IB_0 {
    uint32_t							addr_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_CTXSWITCH_IB_0(struct CP_SET_CTXSWITCH_IB_0 fields)
{
#ifndef NDEBUG
    assert((fields.addr_lo                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_CTXSWITCH_IB_0,
        .value =
            (fields.addr_lo                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_CTXSWITCH_IB_0(...) pack_CP_SET_CTXSWITCH_IB_0((struct CP_SET_CTXSWITCH_IB_0) { __VA_ARGS__ })

struct CP_SET_CTXSWITCH_IB_1 {
    uint32_t							addr_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_CTXSWITCH_IB_1(struct CP_SET_CTXSWITCH_IB_1 fields)
{
#ifndef NDEBUG
    assert((fields.addr_hi                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_CTXSWITCH_IB_1,
        .value =
            (fields.addr_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_CTXSWITCH_IB_1(...) pack_CP_SET_CTXSWITCH_IB_1((struct CP_SET_CTXSWITCH_IB_1) { __VA_ARGS__ })

struct CP_SET_CTXSWITCH_IB_2 {
    uint32_t							dwords;
    enum ctxswitch_ib						type;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SET_CTXSWITCH_IB_2(struct CP_SET_CTXSWITCH_IB_2 fields)
{
#ifndef NDEBUG
    assert((fields.dwords                            & 0xfff00000) == 0);
    assert((fields.type                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x003fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SET_CTXSWITCH_IB_2,
        .value =
            (fields.dwords                            <<  0) |
            (fields.type                              << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SET_CTXSWITCH_IB_2(...) pack_CP_SET_CTXSWITCH_IB_2((struct CP_SET_CTXSWITCH_IB_2) { __VA_ARGS__ })

struct CP_REG_WRITE_0 {
    enum reg_tracker						tracker;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_REG_WRITE_0(struct CP_REG_WRITE_0 fields)
{
#ifndef NDEBUG
    assert((fields.tracker                           & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_REG_WRITE_0,
        .value =
            (fields.tracker                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_REG_WRITE_0(...) pack_CP_REG_WRITE_0((struct CP_REG_WRITE_0) { __VA_ARGS__ })

struct CP_SMMU_TABLE_UPDATE_0 {
    uint32_t							ttbr0_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SMMU_TABLE_UPDATE_0(struct CP_SMMU_TABLE_UPDATE_0 fields)
{
#ifndef NDEBUG
    assert((fields.ttbr0_lo                          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SMMU_TABLE_UPDATE_0,
        .value =
            (fields.ttbr0_lo                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SMMU_TABLE_UPDATE_0(...) pack_CP_SMMU_TABLE_UPDATE_0((struct CP_SMMU_TABLE_UPDATE_0) { __VA_ARGS__ })

struct CP_SMMU_TABLE_UPDATE_1 {
    uint32_t							ttbr0_hi;
    uint32_t							asid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SMMU_TABLE_UPDATE_1(struct CP_SMMU_TABLE_UPDATE_1 fields)
{
#ifndef NDEBUG
    assert((fields.ttbr0_hi                          & 0xffff0000) == 0);
    assert((fields.asid                              & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SMMU_TABLE_UPDATE_1,
        .value =
            (fields.ttbr0_hi                          <<  0) |
            (fields.asid                              << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SMMU_TABLE_UPDATE_1(...) pack_CP_SMMU_TABLE_UPDATE_1((struct CP_SMMU_TABLE_UPDATE_1) { __VA_ARGS__ })

struct CP_SMMU_TABLE_UPDATE_2 {
    uint32_t							contextidr;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SMMU_TABLE_UPDATE_2(struct CP_SMMU_TABLE_UPDATE_2 fields)
{
#ifndef NDEBUG
    assert((fields.contextidr                        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SMMU_TABLE_UPDATE_2,
        .value =
            (fields.contextidr                        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SMMU_TABLE_UPDATE_2(...) pack_CP_SMMU_TABLE_UPDATE_2((struct CP_SMMU_TABLE_UPDATE_2) { __VA_ARGS__ })

struct CP_SMMU_TABLE_UPDATE_3 {
    uint32_t							contextbank;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_SMMU_TABLE_UPDATE_3(struct CP_SMMU_TABLE_UPDATE_3 fields)
{
#ifndef NDEBUG
    assert((fields.contextbank                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_SMMU_TABLE_UPDATE_3,
        .value =
            (fields.contextbank                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_SMMU_TABLE_UPDATE_3(...) pack_CP_SMMU_TABLE_UPDATE_3((struct CP_SMMU_TABLE_UPDATE_3) { __VA_ARGS__ })

struct CP_START_BIN_BIN_COUNT {
    uint32_t							cp_start_bin_bin_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_START_BIN_BIN_COUNT(struct CP_START_BIN_BIN_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.cp_start_bin_bin_count            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_START_BIN_BIN_COUNT,
        .value =
            (fields.cp_start_bin_bin_count            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_START_BIN_BIN_COUNT(...) pack_CP_START_BIN_BIN_COUNT((struct CP_START_BIN_BIN_COUNT) { __VA_ARGS__ })

struct CP_START_BIN_PREFIX_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_CP_START_BIN_PREFIX_ADDR(struct CP_START_BIN_PREFIX_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_START_BIN_PREFIX_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define CP_START_BIN_PREFIX_ADDR(...) pack_CP_START_BIN_PREFIX_ADDR((struct CP_START_BIN_PREFIX_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct CP_START_BIN_PREFIX_DWORDS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_START_BIN_PREFIX_DWORDS(struct CP_START_BIN_PREFIX_DWORDS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_START_BIN_PREFIX_DWORDS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_START_BIN_PREFIX_DWORDS(...) pack_CP_START_BIN_PREFIX_DWORDS((struct CP_START_BIN_PREFIX_DWORDS) { __VA_ARGS__ })

struct CP_START_BIN_BODY_DWORDS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_START_BIN_BODY_DWORDS(struct CP_START_BIN_BODY_DWORDS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_START_BIN_BODY_DWORDS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_START_BIN_BODY_DWORDS(...) pack_CP_START_BIN_BODY_DWORDS((struct CP_START_BIN_BODY_DWORDS) { __VA_ARGS__ })

struct CP_THREAD_CONTROL_0 {
    enum cp_thread						thread;
    bool							concurrent_bin_disable;
    bool							sync_threads;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_CP_THREAD_CONTROL_0(struct CP_THREAD_CONTROL_0 fields)
{
#ifndef NDEBUG
    assert((fields.thread                            & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x88000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_CP_THREAD_CONTROL_0,
        .value =
            (fields.thread                            <<  0) |
            (fields.concurrent_bin_disable            << 27) |
            (fields.sync_threads                      << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define CP_THREAD_CONTROL_0(...) pack_CP_THREAD_CONTROL_0((struct CP_THREAD_CONTROL_0) { __VA_ARGS__ })


#endif /* ADRENO_PM4_XML_STRUCTS */
