/*
 * Copyright © 2019 Google, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


//typedef unsigned int uint32_t;

#include "common.h"

#include "a6xx-pack.xml.h"
#include "adreno-pm4-pack.xml.h"

///////////////////////////////////
struct __HLSQ_VS_CNTL_params {
   uint32_t                                                    constlen;
   bool                                                        enabled;
   uint32_t                                                    unknown;
};
template <chip CHIP>
struct fd_reg_pair HLSQ_VS_CNTL(__HLSQ_VS_CNTL_params fields)
{
   if (CHIP == A6XX) {
      struct fd_reg_pair pair = {
          .reg = REG_A6XX_HLSQ_VS_CNTL,
          .value =
              (fields.constlen >> 2                     <<  0) |
              (fields.enabled                           <<  8) |
              (fields.unknown)
      };
      return pair;
   } else {
      struct fd_reg_pair pair = {
          .reg = REG_A6XX_HLSQ_VS_CNTL,
          .value =
              (fields.constlen >> 2                     <<  0) |
              (fields.enabled                           <<  9) |
              (fields.unknown)
      };
      return pair;
   }
}
///////////////////////////////////

void
test_simple(struct fd_ringbuffer *ring, struct ir3_shader_variant *vs,
		struct ir3_shader_variant *hs, struct ir3_shader_variant *ds,
		struct ir3_shader_variant *gs)
{
   const enum chip CHIP = A6XX;
   HLSQ_VS_CNTL<CHIP>({
         .constlen = align(vs->constlen, 4),
         .enabled = true});


   OUT_REG(ring,
           HLSQ_VS_CNTL<CHIP>({
                 .constlen = align(vs->constlen, 4),
                 .enabled = true}),
           A6XX_HLSQ_HS_CNTL(
                 .constlen = COND(hs, align(hs->constlen, 4)),
                 .enabled = hs),
           A6XX_HLSQ_DS_CNTL(
                 .constlen = COND(ds, align(ds->constlen, 4)),
                 .enabled = ds),
           A6XX_HLSQ_GS_CNTL(
                 .constlen = COND(gs, align(gs->constlen, 4)),
                 .enabled = gs));
}

void
test_reloc(struct fd_ringbuffer *ring, int i, enum a6xx_format format,
		enum a6xx_tile_mode tile_mode, enum a3xx_color_swap swap,
		uint32_t stride, uint32_t size0, struct fd_bo *bo,
		uint32_t offset, uint32_t base)
{
#if 0
	OUT_REG(ring,
			A6XX_RB_MRT_BUF_INFO(i,
					.color_format = format,
					.color_tile_mode = tile_mode,
					.color_swap = swap),
			_A6XX_RB_MRT_PITCH(i,
					.pitch = stride),
			_A6XX_RB_MRT_ARRAY_PITCH(i,
					.array_pitch = size0),
			RELOCW(bo, offset, 0, 0),
			_A6XX_RB_MRT_BASE_GMEM(i,
					.base_gmem = base)
		);
#endif
}
