#ifndef A6XX_XML_STRUCTS
#define A6XX_XML_STRUCTS

struct A6XX_CP_RB_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_RB_BASE(struct A6XX_CP_RB_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_RB_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_RB_BASE(...) pack_A6XX_CP_RB_BASE((struct A6XX_CP_RB_BASE) { __VA_ARGS__ })

struct A6XX_CP_RB_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_RB_CNTL(struct A6XX_CP_RB_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_RB_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_RB_CNTL(...) pack_A6XX_CP_RB_CNTL((struct A6XX_CP_RB_CNTL) { __VA_ARGS__ })

struct A6XX_CP_RB_RPTR_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_RB_RPTR_ADDR(struct A6XX_CP_RB_RPTR_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_RB_RPTR_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_RB_RPTR_ADDR(...) pack_A6XX_CP_RB_RPTR_ADDR((struct A6XX_CP_RB_RPTR_ADDR) { __VA_ARGS__ })

struct A6XX_CP_RB_RPTR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_RB_RPTR(struct A6XX_CP_RB_RPTR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_RB_RPTR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_RB_RPTR(...) pack_A6XX_CP_RB_RPTR((struct A6XX_CP_RB_RPTR) { __VA_ARGS__ })

struct A6XX_CP_RB_WPTR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_RB_WPTR(struct A6XX_CP_RB_WPTR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_RB_WPTR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_RB_WPTR(...) pack_A6XX_CP_RB_WPTR((struct A6XX_CP_RB_WPTR) { __VA_ARGS__ })

struct A6XX_CP_SQE_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_CNTL(struct A6XX_CP_SQE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_CNTL(...) pack_A6XX_CP_SQE_CNTL((struct A6XX_CP_SQE_CNTL) { __VA_ARGS__ })

struct A6XX_CP_CP2GMU_STATUS {
    bool							ifpc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CP2GMU_STATUS(struct A6XX_CP_CP2GMU_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CP2GMU_STATUS,
        .value =
            (fields.ifpc                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CP2GMU_STATUS(...) pack_A6XX_CP_CP2GMU_STATUS((struct A6XX_CP_CP2GMU_STATUS) { __VA_ARGS__ })

struct A6XX_CP_HW_FAULT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_HW_FAULT(struct A6XX_CP_HW_FAULT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_HW_FAULT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_HW_FAULT(...) pack_A6XX_CP_HW_FAULT((struct A6XX_CP_HW_FAULT) { __VA_ARGS__ })

struct A6XX_CP_INTERRUPT_STATUS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_INTERRUPT_STATUS(struct A6XX_CP_INTERRUPT_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_INTERRUPT_STATUS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_INTERRUPT_STATUS(...) pack_A6XX_CP_INTERRUPT_STATUS((struct A6XX_CP_INTERRUPT_STATUS) { __VA_ARGS__ })

struct A6XX_CP_PROTECT_STATUS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_PROTECT_STATUS(struct A6XX_CP_PROTECT_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_PROTECT_STATUS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_PROTECT_STATUS(...) pack_A6XX_CP_PROTECT_STATUS((struct A6XX_CP_PROTECT_STATUS) { __VA_ARGS__ })

struct A6XX_CP_SQE_INSTR_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_INSTR_BASE(struct A6XX_CP_SQE_INSTR_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_INSTR_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_INSTR_BASE(...) pack_A6XX_CP_SQE_INSTR_BASE((struct A6XX_CP_SQE_INSTR_BASE) { __VA_ARGS__ })

struct A6XX_CP_MISC_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MISC_CNTL(struct A6XX_CP_MISC_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MISC_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MISC_CNTL(...) pack_A6XX_CP_MISC_CNTL((struct A6XX_CP_MISC_CNTL) { __VA_ARGS__ })

struct A6XX_CP_APRIV_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_APRIV_CNTL(struct A6XX_CP_APRIV_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_APRIV_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_APRIV_CNTL(...) pack_A6XX_CP_APRIV_CNTL((struct A6XX_CP_APRIV_CNTL) { __VA_ARGS__ })

struct A6XX_CP_ROQ_THRESHOLDS_1 {
    uint32_t							rb_lo;
    uint32_t							rb_hi;
    uint32_t							ib1_start;
    uint32_t							ib2_start;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ROQ_THRESHOLDS_1(struct A6XX_CP_ROQ_THRESHOLDS_1 fields)
{
#ifndef NDEBUG
    assert(((fields.rb_lo >> 2)                      & 0xffffff00) == 0);
    assert(((fields.rb_hi >> 2)                      & 0xffffff00) == 0);
    assert(((fields.ib1_start >> 2)                  & 0xffffff00) == 0);
    assert(((fields.ib2_start >> 2)                  & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ROQ_THRESHOLDS_1,
        .value =
            ((fields.rb_lo >> 2)                      <<  0) |
            ((fields.rb_hi >> 2)                      <<  8) |
            ((fields.ib1_start >> 2)                  << 16) |
            ((fields.ib2_start >> 2)                  << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ROQ_THRESHOLDS_1(...) pack_A6XX_CP_ROQ_THRESHOLDS_1((struct A6XX_CP_ROQ_THRESHOLDS_1) { __VA_ARGS__ })

struct A6XX_CP_ROQ_THRESHOLDS_2 {
    uint32_t							sds_start;
    uint32_t							roq_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ROQ_THRESHOLDS_2(struct A6XX_CP_ROQ_THRESHOLDS_2 fields)
{
#ifndef NDEBUG
    assert(((fields.sds_start >> 2)                  & 0xfffffe00) == 0);
    assert(((fields.roq_size >> 2)                   & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffff01ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ROQ_THRESHOLDS_2,
        .value =
            ((fields.sds_start >> 2)                  <<  0) |
            ((fields.roq_size >> 2)                   << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ROQ_THRESHOLDS_2(...) pack_A6XX_CP_ROQ_THRESHOLDS_2((struct A6XX_CP_ROQ_THRESHOLDS_2) { __VA_ARGS__ })

struct A6XX_CP_MEM_POOL_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MEM_POOL_SIZE(struct A6XX_CP_MEM_POOL_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MEM_POOL_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MEM_POOL_SIZE(...) pack_A6XX_CP_MEM_POOL_SIZE((struct A6XX_CP_MEM_POOL_SIZE) { __VA_ARGS__ })

struct A6XX_CP_CHICKEN_DBG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CHICKEN_DBG(struct A6XX_CP_CHICKEN_DBG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CHICKEN_DBG,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CHICKEN_DBG(...) pack_A6XX_CP_CHICKEN_DBG((struct A6XX_CP_CHICKEN_DBG) { __VA_ARGS__ })

struct A6XX_CP_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_cp_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ADDR_MODE_CNTL(struct A6XX_CP_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_cp_addr_mode_cntl            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_cp_addr_mode_cntl            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ADDR_MODE_CNTL(...) pack_A6XX_CP_ADDR_MODE_CNTL((struct A6XX_CP_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_CP_DBG_ECO_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DBG_ECO_CNTL(struct A6XX_CP_DBG_ECO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DBG_ECO_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DBG_ECO_CNTL(...) pack_A6XX_CP_DBG_ECO_CNTL((struct A6XX_CP_DBG_ECO_CNTL) { __VA_ARGS__ })

struct A6XX_CP_PROTECT_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_PROTECT_CNTL(struct A6XX_CP_PROTECT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_PROTECT_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_PROTECT_CNTL(...) pack_A6XX_CP_PROTECT_CNTL((struct A6XX_CP_PROTECT_CNTL) { __VA_ARGS__ })

struct A6XX_CP_SCRATCH_REG {
    uint32_t							a6xx_cp_scratch_reg;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SCRATCH_REG(uint32_t i, struct A6XX_CP_SCRATCH_REG fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_cp_scratch_reg               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SCRATCH_REG(i),
        .value =
            (fields.a6xx_cp_scratch_reg               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SCRATCH_REG(i, ...) pack_A6XX_CP_SCRATCH_REG(i, (struct A6XX_CP_SCRATCH_REG) { __VA_ARGS__ })

struct A6XX_CP_PROTECT_REG {
    uint32_t							base_addr;
    uint32_t							mask_len;
    bool							read;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_PROTECT_REG(uint32_t i, struct A6XX_CP_PROTECT_REG fields)
{
#ifndef NDEBUG
    assert((fields.base_addr                         & 0xfffc0000) == 0);
    assert((fields.mask_len                          & 0xffffe000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_PROTECT_REG(i),
        .value =
            (fields.base_addr                         <<  0) |
            (fields.mask_len                          << 18) |
            (fields.read                              << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_PROTECT_REG(i, ...) pack_A6XX_CP_PROTECT_REG(i, (struct A6XX_CP_PROTECT_REG) { __VA_ARGS__ })

struct A6XX_CP_CONTEXT_SWITCH_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CONTEXT_SWITCH_CNTL(struct A6XX_CP_CONTEXT_SWITCH_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CONTEXT_SWITCH_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CONTEXT_SWITCH_CNTL(...) pack_A6XX_CP_CONTEXT_SWITCH_CNTL((struct A6XX_CP_CONTEXT_SWITCH_CNTL) { __VA_ARGS__ })

struct A6XX_CP_CONTEXT_SWITCH_SMMU_INFO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CONTEXT_SWITCH_SMMU_INFO(struct A6XX_CP_CONTEXT_SWITCH_SMMU_INFO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CONTEXT_SWITCH_SMMU_INFO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CONTEXT_SWITCH_SMMU_INFO(...) pack_A6XX_CP_CONTEXT_SWITCH_SMMU_INFO((struct A6XX_CP_CONTEXT_SWITCH_SMMU_INFO) { __VA_ARGS__ })

struct A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR(struct A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR(...) pack_A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR((struct A6XX_CP_CONTEXT_SWITCH_PRIV_NON_SECURE_RESTORE_ADDR) { __VA_ARGS__ })

struct A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR(struct A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR(...) pack_A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR((struct A6XX_CP_CONTEXT_SWITCH_PRIV_SECURE_RESTORE_ADDR) { __VA_ARGS__ })

struct A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR(struct A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR(...) pack_A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR((struct A6XX_CP_CONTEXT_SWITCH_NON_PRIV_RESTORE_ADDR) { __VA_ARGS__ })

struct A6XX_CP_CRASH_SCRIPT_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CRASH_SCRIPT_BASE(struct A6XX_CP_CRASH_SCRIPT_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CRASH_SCRIPT_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CRASH_SCRIPT_BASE(...) pack_A6XX_CP_CRASH_SCRIPT_BASE((struct A6XX_CP_CRASH_SCRIPT_BASE) { __VA_ARGS__ })

struct A6XX_CP_CRASH_DUMP_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CRASH_DUMP_CNTL(struct A6XX_CP_CRASH_DUMP_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CRASH_DUMP_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CRASH_DUMP_CNTL(...) pack_A6XX_CP_CRASH_DUMP_CNTL((struct A6XX_CP_CRASH_DUMP_CNTL) { __VA_ARGS__ })

struct A6XX_CP_CRASH_DUMP_STATUS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CRASH_DUMP_STATUS(struct A6XX_CP_CRASH_DUMP_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CRASH_DUMP_STATUS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CRASH_DUMP_STATUS(...) pack_A6XX_CP_CRASH_DUMP_STATUS((struct A6XX_CP_CRASH_DUMP_STATUS) { __VA_ARGS__ })

struct A6XX_CP_SQE_STAT_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_STAT_ADDR(struct A6XX_CP_SQE_STAT_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_STAT_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_STAT_ADDR(...) pack_A6XX_CP_SQE_STAT_ADDR((struct A6XX_CP_SQE_STAT_ADDR) { __VA_ARGS__ })

struct A6XX_CP_SQE_STAT_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_STAT_DATA(struct A6XX_CP_SQE_STAT_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_STAT_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_STAT_DATA(...) pack_A6XX_CP_SQE_STAT_DATA((struct A6XX_CP_SQE_STAT_DATA) { __VA_ARGS__ })

struct A6XX_CP_DRAW_STATE_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DRAW_STATE_ADDR(struct A6XX_CP_DRAW_STATE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DRAW_STATE_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DRAW_STATE_ADDR(...) pack_A6XX_CP_DRAW_STATE_ADDR((struct A6XX_CP_DRAW_STATE_ADDR) { __VA_ARGS__ })

struct A6XX_CP_DRAW_STATE_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_DRAW_STATE_DATA(struct A6XX_CP_DRAW_STATE_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_DRAW_STATE_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_DRAW_STATE_DATA(...) pack_A6XX_CP_DRAW_STATE_DATA((struct A6XX_CP_DRAW_STATE_DATA) { __VA_ARGS__ })

struct A6XX_CP_ROQ_DBG_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ROQ_DBG_ADDR(struct A6XX_CP_ROQ_DBG_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ROQ_DBG_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ROQ_DBG_ADDR(...) pack_A6XX_CP_ROQ_DBG_ADDR((struct A6XX_CP_ROQ_DBG_ADDR) { __VA_ARGS__ })

struct A6XX_CP_ROQ_DBG_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ROQ_DBG_DATA(struct A6XX_CP_ROQ_DBG_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ROQ_DBG_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ROQ_DBG_DATA(...) pack_A6XX_CP_ROQ_DBG_DATA((struct A6XX_CP_ROQ_DBG_DATA) { __VA_ARGS__ })

struct A6XX_CP_MEM_POOL_DBG_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MEM_POOL_DBG_ADDR(struct A6XX_CP_MEM_POOL_DBG_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MEM_POOL_DBG_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MEM_POOL_DBG_ADDR(...) pack_A6XX_CP_MEM_POOL_DBG_ADDR((struct A6XX_CP_MEM_POOL_DBG_ADDR) { __VA_ARGS__ })

struct A6XX_CP_MEM_POOL_DBG_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MEM_POOL_DBG_DATA(struct A6XX_CP_MEM_POOL_DBG_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MEM_POOL_DBG_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MEM_POOL_DBG_DATA(...) pack_A6XX_CP_MEM_POOL_DBG_DATA((struct A6XX_CP_MEM_POOL_DBG_DATA) { __VA_ARGS__ })

struct A6XX_CP_SQE_UCODE_DBG_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_UCODE_DBG_ADDR(struct A6XX_CP_SQE_UCODE_DBG_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_UCODE_DBG_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_UCODE_DBG_ADDR(...) pack_A6XX_CP_SQE_UCODE_DBG_ADDR((struct A6XX_CP_SQE_UCODE_DBG_ADDR) { __VA_ARGS__ })

struct A6XX_CP_SQE_UCODE_DBG_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SQE_UCODE_DBG_DATA(struct A6XX_CP_SQE_UCODE_DBG_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SQE_UCODE_DBG_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SQE_UCODE_DBG_DATA(...) pack_A6XX_CP_SQE_UCODE_DBG_DATA((struct A6XX_CP_SQE_UCODE_DBG_DATA) { __VA_ARGS__ })

struct A6XX_CP_IB1_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_IB1_BASE(struct A6XX_CP_IB1_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_IB1_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_IB1_BASE(...) pack_A6XX_CP_IB1_BASE((struct A6XX_CP_IB1_BASE) { __VA_ARGS__ })

struct A6XX_CP_IB1_REM_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_IB1_REM_SIZE(struct A6XX_CP_IB1_REM_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_IB1_REM_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_IB1_REM_SIZE(...) pack_A6XX_CP_IB1_REM_SIZE((struct A6XX_CP_IB1_REM_SIZE) { __VA_ARGS__ })

struct A6XX_CP_IB2_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_IB2_BASE(struct A6XX_CP_IB2_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_IB2_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_IB2_BASE(...) pack_A6XX_CP_IB2_BASE((struct A6XX_CP_IB2_BASE) { __VA_ARGS__ })

struct A6XX_CP_IB2_REM_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_IB2_REM_SIZE(struct A6XX_CP_IB2_REM_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_IB2_REM_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_IB2_REM_SIZE(...) pack_A6XX_CP_IB2_REM_SIZE((struct A6XX_CP_IB2_REM_SIZE) { __VA_ARGS__ })

struct A6XX_CP_SDS_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SDS_BASE(struct A6XX_CP_SDS_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SDS_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SDS_BASE(...) pack_A6XX_CP_SDS_BASE((struct A6XX_CP_SDS_BASE) { __VA_ARGS__ })

struct A6XX_CP_SDS_REM_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_SDS_REM_SIZE(struct A6XX_CP_SDS_REM_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_SDS_REM_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_SDS_REM_SIZE(...) pack_A6XX_CP_SDS_REM_SIZE((struct A6XX_CP_SDS_REM_SIZE) { __VA_ARGS__ })

struct A6XX_CP_MRB_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MRB_BASE(struct A6XX_CP_MRB_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MRB_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MRB_BASE(...) pack_A6XX_CP_MRB_BASE((struct A6XX_CP_MRB_BASE) { __VA_ARGS__ })

struct A6XX_CP_MRB_REM_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MRB_REM_SIZE(struct A6XX_CP_MRB_REM_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MRB_REM_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MRB_REM_SIZE(...) pack_A6XX_CP_MRB_REM_SIZE((struct A6XX_CP_MRB_REM_SIZE) { __VA_ARGS__ })

struct A6XX_CP_VSD_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_VSD_BASE(struct A6XX_CP_VSD_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_VSD_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_VSD_BASE(...) pack_A6XX_CP_VSD_BASE((struct A6XX_CP_VSD_BASE) { __VA_ARGS__ })

struct A6XX_CP_MRB_DWORDS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MRB_DWORDS(struct A6XX_CP_MRB_DWORDS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MRB_DWORDS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MRB_DWORDS(...) pack_A6XX_CP_MRB_DWORDS((struct A6XX_CP_MRB_DWORDS) { __VA_ARGS__ })

struct A6XX_CP_VSD_DWORDS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_VSD_DWORDS(struct A6XX_CP_VSD_DWORDS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_VSD_DWORDS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_VSD_DWORDS(...) pack_A6XX_CP_VSD_DWORDS((struct A6XX_CP_VSD_DWORDS) { __VA_ARGS__ })

struct A6XX_CP_CSQ_IB1_STAT {
    uint32_t							rem;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CSQ_IB1_STAT(struct A6XX_CP_CSQ_IB1_STAT fields)
{
#ifndef NDEBUG
    assert((fields.rem                               & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffff0000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CSQ_IB1_STAT,
        .value =
            (fields.rem                               << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CSQ_IB1_STAT(...) pack_A6XX_CP_CSQ_IB1_STAT((struct A6XX_CP_CSQ_IB1_STAT) { __VA_ARGS__ })

struct A6XX_CP_CSQ_IB2_STAT {
    uint32_t							rem;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_CSQ_IB2_STAT(struct A6XX_CP_CSQ_IB2_STAT fields)
{
#ifndef NDEBUG
    assert((fields.rem                               & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffff0000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_CSQ_IB2_STAT,
        .value =
            (fields.rem                               << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_CSQ_IB2_STAT(...) pack_A6XX_CP_CSQ_IB2_STAT((struct A6XX_CP_CSQ_IB2_STAT) { __VA_ARGS__ })

struct A6XX_CP_MRQ_MRB_STAT {
    uint32_t							rem;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_MRQ_MRB_STAT(struct A6XX_CP_MRQ_MRB_STAT fields)
{
#ifndef NDEBUG
    assert((fields.rem                               & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffff0000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_MRQ_MRB_STAT,
        .value =
            (fields.rem                               << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_MRQ_MRB_STAT(...) pack_A6XX_CP_MRQ_MRB_STAT((struct A6XX_CP_MRQ_MRB_STAT) { __VA_ARGS__ })

struct A6XX_CP_ALWAYS_ON_COUNTER {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_ALWAYS_ON_COUNTER(struct A6XX_CP_ALWAYS_ON_COUNTER fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_ALWAYS_ON_COUNTER,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_ALWAYS_ON_COUNTER(...) pack_A6XX_CP_ALWAYS_ON_COUNTER((struct A6XX_CP_ALWAYS_ON_COUNTER) { __VA_ARGS__ })

struct A6XX_CP_AHB_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_AHB_CNTL(struct A6XX_CP_AHB_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_AHB_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_AHB_CNTL(...) pack_A6XX_CP_AHB_CNTL((struct A6XX_CP_AHB_CNTL) { __VA_ARGS__ })

struct A6XX_CP_APERTURE_CNTL_HOST {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_APERTURE_CNTL_HOST(struct A6XX_CP_APERTURE_CNTL_HOST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_APERTURE_CNTL_HOST,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_APERTURE_CNTL_HOST(...) pack_A6XX_CP_APERTURE_CNTL_HOST((struct A6XX_CP_APERTURE_CNTL_HOST) { __VA_ARGS__ })

struct A6XX_CP_APERTURE_CNTL_CD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_APERTURE_CNTL_CD(struct A6XX_CP_APERTURE_CNTL_CD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_APERTURE_CNTL_CD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_APERTURE_CNTL_CD(...) pack_A6XX_CP_APERTURE_CNTL_CD((struct A6XX_CP_APERTURE_CNTL_CD) { __VA_ARGS__ })

struct A6XX_CP_LPAC_PROG_FIFO_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_LPAC_PROG_FIFO_SIZE(struct A6XX_CP_LPAC_PROG_FIFO_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_LPAC_PROG_FIFO_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_LPAC_PROG_FIFO_SIZE(...) pack_A6XX_CP_LPAC_PROG_FIFO_SIZE((struct A6XX_CP_LPAC_PROG_FIFO_SIZE) { __VA_ARGS__ })

struct A6XX_CP_LPAC_SQE_INSTR_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_LPAC_SQE_INSTR_BASE(struct A6XX_CP_LPAC_SQE_INSTR_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_LPAC_SQE_INSTR_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_LPAC_SQE_INSTR_BASE(...) pack_A6XX_CP_LPAC_SQE_INSTR_BASE((struct A6XX_CP_LPAC_SQE_INSTR_BASE) { __VA_ARGS__ })

struct A6XX_VSC_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_vsc_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_ADDR_MODE_CNTL(struct A6XX_VSC_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vsc_addr_mode_cntl           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_vsc_addr_mode_cntl           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_ADDR_MODE_CNTL(...) pack_A6XX_VSC_ADDR_MODE_CNTL((struct A6XX_VSC_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_STATUS {
    bool							gpu_busy_ign_ahb;
    bool							gpu_busy_ign_ahb_cp;
    bool							hlsq_busy;
    bool							vsc_busy;
    bool							tpl1_busy;
    bool							sp_busy;
    bool							uche_busy;
    bool							vpc_busy;
    bool							vfd_busy;
    bool							tess_busy;
    bool							pc_vsd_busy;
    bool							pc_dcall_busy;
    bool							com_dcom_busy;
    bool							lrz_busy;
    bool							a2d_busy;
    bool							ccu_busy;
    bool							rb_busy;
    bool							ras_busy;
    bool							tse_busy;
    bool							vbif_busy;
    bool							gfx_dbgc_busy;
    bool							cp_busy;
    bool							cp_ahb_busy_cp_master;
    bool							cp_ahb_busy_cx_master;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_STATUS(struct A6XX_RBBM_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_STATUS,
        .value =
            (fields.gpu_busy_ign_ahb                  << 23) |
            (fields.gpu_busy_ign_ahb_cp               << 22) |
            (fields.hlsq_busy                         << 21) |
            (fields.vsc_busy                          << 20) |
            (fields.tpl1_busy                         << 19) |
            (fields.sp_busy                           << 18) |
            (fields.uche_busy                         << 17) |
            (fields.vpc_busy                          << 16) |
            (fields.vfd_busy                          << 15) |
            (fields.tess_busy                         << 14) |
            (fields.pc_vsd_busy                       << 13) |
            (fields.pc_dcall_busy                     << 12) |
            (fields.com_dcom_busy                     << 11) |
            (fields.lrz_busy                          << 10) |
            (fields.a2d_busy                          <<  9) |
            (fields.ccu_busy                          <<  8) |
            (fields.rb_busy                           <<  7) |
            (fields.ras_busy                          <<  6) |
            (fields.tse_busy                          <<  5) |
            (fields.vbif_busy                         <<  4) |
            (fields.gfx_dbgc_busy                     <<  3) |
            (fields.cp_busy                           <<  2) |
            (fields.cp_ahb_busy_cp_master             <<  1) |
            (fields.cp_ahb_busy_cx_master             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_STATUS(...) pack_A6XX_RBBM_STATUS((struct A6XX_RBBM_STATUS) { __VA_ARGS__ })

struct A6XX_RBBM_STATUS3 {
    bool							smmu_stalled_on_fault;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_STATUS3(struct A6XX_RBBM_STATUS3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x01000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_STATUS3,
        .value =
            (fields.smmu_stalled_on_fault             << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_STATUS3(...) pack_A6XX_RBBM_STATUS3((struct A6XX_RBBM_STATUS3) { __VA_ARGS__ })

struct A6XX_RBBM_VBIF_GX_RESET_STATUS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_VBIF_GX_RESET_STATUS(struct A6XX_RBBM_VBIF_GX_RESET_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_VBIF_GX_RESET_STATUS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_VBIF_GX_RESET_STATUS(...) pack_A6XX_RBBM_VBIF_GX_RESET_STATUS((struct A6XX_RBBM_VBIF_GX_RESET_STATUS) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_CNTL(struct A6XX_RBBM_PERFCTR_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_CNTL(...) pack_A6XX_RBBM_PERFCTR_CNTL((struct A6XX_RBBM_PERFCTR_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_CMD0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_CMD0(struct A6XX_RBBM_PERFCTR_LOAD_CMD0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_CMD0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_CMD0(...) pack_A6XX_RBBM_PERFCTR_LOAD_CMD0((struct A6XX_RBBM_PERFCTR_LOAD_CMD0) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_CMD1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_CMD1(struct A6XX_RBBM_PERFCTR_LOAD_CMD1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_CMD1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_CMD1(...) pack_A6XX_RBBM_PERFCTR_LOAD_CMD1((struct A6XX_RBBM_PERFCTR_LOAD_CMD1) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_CMD2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_CMD2(struct A6XX_RBBM_PERFCTR_LOAD_CMD2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_CMD2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_CMD2(...) pack_A6XX_RBBM_PERFCTR_LOAD_CMD2((struct A6XX_RBBM_PERFCTR_LOAD_CMD2) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_CMD3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_CMD3(struct A6XX_RBBM_PERFCTR_LOAD_CMD3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_CMD3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_CMD3(...) pack_A6XX_RBBM_PERFCTR_LOAD_CMD3((struct A6XX_RBBM_PERFCTR_LOAD_CMD3) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_VALUE_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_VALUE_LO(struct A6XX_RBBM_PERFCTR_LOAD_VALUE_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_VALUE_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_VALUE_LO(...) pack_A6XX_RBBM_PERFCTR_LOAD_VALUE_LO((struct A6XX_RBBM_PERFCTR_LOAD_VALUE_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_LOAD_VALUE_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_LOAD_VALUE_HI(struct A6XX_RBBM_PERFCTR_LOAD_VALUE_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_LOAD_VALUE_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_LOAD_VALUE_HI(...) pack_A6XX_RBBM_PERFCTR_LOAD_VALUE_HI((struct A6XX_RBBM_PERFCTR_LOAD_VALUE_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED(struct A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED(...) pack_A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED((struct A6XX_RBBM_PERFCTR_GPU_BUSY_MASKED) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_SRAM_INIT_CMD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_SRAM_INIT_CMD(struct A6XX_RBBM_PERFCTR_SRAM_INIT_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_SRAM_INIT_CMD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_SRAM_INIT_CMD(...) pack_A6XX_RBBM_PERFCTR_SRAM_INIT_CMD((struct A6XX_RBBM_PERFCTR_SRAM_INIT_CMD) { __VA_ARGS__ })

struct A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS(struct A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS(...) pack_A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS((struct A6XX_RBBM_PERFCTR_SRAM_INIT_STATUS) { __VA_ARGS__ })

struct A6XX_RBBM_ISDB_CNT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_ISDB_CNT(struct A6XX_RBBM_ISDB_CNT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_ISDB_CNT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_ISDB_CNT(...) pack_A6XX_RBBM_ISDB_CNT((struct A6XX_RBBM_ISDB_CNT) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_0_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_0_LO(struct A6XX_RBBM_PRIMCTR_0_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_0_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_0_LO(...) pack_A6XX_RBBM_PRIMCTR_0_LO((struct A6XX_RBBM_PRIMCTR_0_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_0_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_0_HI(struct A6XX_RBBM_PRIMCTR_0_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_0_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_0_HI(...) pack_A6XX_RBBM_PRIMCTR_0_HI((struct A6XX_RBBM_PRIMCTR_0_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_1_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_1_LO(struct A6XX_RBBM_PRIMCTR_1_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_1_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_1_LO(...) pack_A6XX_RBBM_PRIMCTR_1_LO((struct A6XX_RBBM_PRIMCTR_1_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_1_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_1_HI(struct A6XX_RBBM_PRIMCTR_1_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_1_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_1_HI(...) pack_A6XX_RBBM_PRIMCTR_1_HI((struct A6XX_RBBM_PRIMCTR_1_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_2_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_2_LO(struct A6XX_RBBM_PRIMCTR_2_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_2_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_2_LO(...) pack_A6XX_RBBM_PRIMCTR_2_LO((struct A6XX_RBBM_PRIMCTR_2_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_2_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_2_HI(struct A6XX_RBBM_PRIMCTR_2_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_2_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_2_HI(...) pack_A6XX_RBBM_PRIMCTR_2_HI((struct A6XX_RBBM_PRIMCTR_2_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_3_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_3_LO(struct A6XX_RBBM_PRIMCTR_3_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_3_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_3_LO(...) pack_A6XX_RBBM_PRIMCTR_3_LO((struct A6XX_RBBM_PRIMCTR_3_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_3_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_3_HI(struct A6XX_RBBM_PRIMCTR_3_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_3_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_3_HI(...) pack_A6XX_RBBM_PRIMCTR_3_HI((struct A6XX_RBBM_PRIMCTR_3_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_4_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_4_LO(struct A6XX_RBBM_PRIMCTR_4_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_4_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_4_LO(...) pack_A6XX_RBBM_PRIMCTR_4_LO((struct A6XX_RBBM_PRIMCTR_4_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_4_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_4_HI(struct A6XX_RBBM_PRIMCTR_4_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_4_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_4_HI(...) pack_A6XX_RBBM_PRIMCTR_4_HI((struct A6XX_RBBM_PRIMCTR_4_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_5_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_5_LO(struct A6XX_RBBM_PRIMCTR_5_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_5_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_5_LO(...) pack_A6XX_RBBM_PRIMCTR_5_LO((struct A6XX_RBBM_PRIMCTR_5_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_5_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_5_HI(struct A6XX_RBBM_PRIMCTR_5_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_5_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_5_HI(...) pack_A6XX_RBBM_PRIMCTR_5_HI((struct A6XX_RBBM_PRIMCTR_5_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_6_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_6_LO(struct A6XX_RBBM_PRIMCTR_6_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_6_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_6_LO(...) pack_A6XX_RBBM_PRIMCTR_6_LO((struct A6XX_RBBM_PRIMCTR_6_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_6_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_6_HI(struct A6XX_RBBM_PRIMCTR_6_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_6_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_6_HI(...) pack_A6XX_RBBM_PRIMCTR_6_HI((struct A6XX_RBBM_PRIMCTR_6_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_7_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_7_LO(struct A6XX_RBBM_PRIMCTR_7_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_7_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_7_LO(...) pack_A6XX_RBBM_PRIMCTR_7_LO((struct A6XX_RBBM_PRIMCTR_7_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_7_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_7_HI(struct A6XX_RBBM_PRIMCTR_7_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_7_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_7_HI(...) pack_A6XX_RBBM_PRIMCTR_7_HI((struct A6XX_RBBM_PRIMCTR_7_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_8_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_8_LO(struct A6XX_RBBM_PRIMCTR_8_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_8_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_8_LO(...) pack_A6XX_RBBM_PRIMCTR_8_LO((struct A6XX_RBBM_PRIMCTR_8_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_8_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_8_HI(struct A6XX_RBBM_PRIMCTR_8_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_8_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_8_HI(...) pack_A6XX_RBBM_PRIMCTR_8_HI((struct A6XX_RBBM_PRIMCTR_8_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_9_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_9_LO(struct A6XX_RBBM_PRIMCTR_9_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_9_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_9_LO(...) pack_A6XX_RBBM_PRIMCTR_9_LO((struct A6XX_RBBM_PRIMCTR_9_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_9_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_9_HI(struct A6XX_RBBM_PRIMCTR_9_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_9_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_9_HI(...) pack_A6XX_RBBM_PRIMCTR_9_HI((struct A6XX_RBBM_PRIMCTR_9_HI) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_10_LO {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_10_LO(struct A6XX_RBBM_PRIMCTR_10_LO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_10_LO,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_10_LO(...) pack_A6XX_RBBM_PRIMCTR_10_LO((struct A6XX_RBBM_PRIMCTR_10_LO) { __VA_ARGS__ })

struct A6XX_RBBM_PRIMCTR_10_HI {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_PRIMCTR_10_HI(struct A6XX_RBBM_PRIMCTR_10_HI fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_PRIMCTR_10_HI,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_PRIMCTR_10_HI(...) pack_A6XX_RBBM_PRIMCTR_10_HI((struct A6XX_RBBM_PRIMCTR_10_HI) { __VA_ARGS__ })

struct A6XX_RBBM_SECVID_TRUST_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SECVID_TRUST_CNTL(struct A6XX_RBBM_SECVID_TRUST_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SECVID_TRUST_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SECVID_TRUST_CNTL(...) pack_A6XX_RBBM_SECVID_TRUST_CNTL((struct A6XX_RBBM_SECVID_TRUST_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_SECVID_TSB_TRUSTED_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SECVID_TSB_TRUSTED_BASE(struct A6XX_RBBM_SECVID_TSB_TRUSTED_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SECVID_TSB_TRUSTED_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SECVID_TSB_TRUSTED_BASE(...) pack_A6XX_RBBM_SECVID_TSB_TRUSTED_BASE((struct A6XX_RBBM_SECVID_TSB_TRUSTED_BASE) { __VA_ARGS__ })

struct A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE(struct A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE(...) pack_A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE((struct A6XX_RBBM_SECVID_TSB_TRUSTED_SIZE) { __VA_ARGS__ })

struct A6XX_RBBM_SECVID_TSB_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SECVID_TSB_CNTL(struct A6XX_RBBM_SECVID_TSB_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SECVID_TSB_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SECVID_TSB_CNTL(...) pack_A6XX_RBBM_SECVID_TSB_CNTL((struct A6XX_RBBM_SECVID_TSB_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_rbbm_secvid_tsb_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL(struct A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_rbbm_secvid_tsb_addr_mode_cntl & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_rbbm_secvid_tsb_addr_mode_cntl <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL(...) pack_A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL((struct A6XX_RBBM_SECVID_TSB_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_VBIF_CLIENT_QOS_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_VBIF_CLIENT_QOS_CNTL(struct A6XX_RBBM_VBIF_CLIENT_QOS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_VBIF_CLIENT_QOS_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_VBIF_CLIENT_QOS_CNTL(...) pack_A6XX_RBBM_VBIF_CLIENT_QOS_CNTL((struct A6XX_RBBM_VBIF_CLIENT_QOS_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_GBIF_CLIENT_QOS_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_GBIF_CLIENT_QOS_CNTL(struct A6XX_RBBM_GBIF_CLIENT_QOS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_GBIF_CLIENT_QOS_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_GBIF_CLIENT_QOS_CNTL(...) pack_A6XX_RBBM_GBIF_CLIENT_QOS_CNTL((struct A6XX_RBBM_GBIF_CLIENT_QOS_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD {
    bool							wait_gpu_idle;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD(struct A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD,
        .value =
            (fields.wait_gpu_idle                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD(...) pack_A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD((struct A6XX_RBBM_WAIT_FOR_GPU_IDLE_CMD) { __VA_ARGS__ })

struct A6XX_RBBM_INTERFACE_HANG_INT_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_INTERFACE_HANG_INT_CNTL(struct A6XX_RBBM_INTERFACE_HANG_INT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_INTERFACE_HANG_INT_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_INTERFACE_HANG_INT_CNTL(...) pack_A6XX_RBBM_INTERFACE_HANG_INT_CNTL((struct A6XX_RBBM_INTERFACE_HANG_INT_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_INT_0_MASK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_INT_0_MASK(struct A6XX_RBBM_INT_0_MASK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_INT_0_MASK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_INT_0_MASK(...) pack_A6XX_RBBM_INT_0_MASK((struct A6XX_RBBM_INT_0_MASK) { __VA_ARGS__ })

struct A6XX_RBBM_SP_HYST_CNT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SP_HYST_CNT(struct A6XX_RBBM_SP_HYST_CNT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SP_HYST_CNT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SP_HYST_CNT(...) pack_A6XX_RBBM_SP_HYST_CNT((struct A6XX_RBBM_SP_HYST_CNT) { __VA_ARGS__ })

struct A6XX_RBBM_SW_RESET_CMD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_SW_RESET_CMD(struct A6XX_RBBM_SW_RESET_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_SW_RESET_CMD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_SW_RESET_CMD(...) pack_A6XX_RBBM_SW_RESET_CMD((struct A6XX_RBBM_SW_RESET_CMD) { __VA_ARGS__ })

struct A6XX_RBBM_RAC_THRESHOLD_CNT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_RAC_THRESHOLD_CNT(struct A6XX_RBBM_RAC_THRESHOLD_CNT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_RAC_THRESHOLD_CNT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_RAC_THRESHOLD_CNT(...) pack_A6XX_RBBM_RAC_THRESHOLD_CNT((struct A6XX_RBBM_RAC_THRESHOLD_CNT) { __VA_ARGS__ })

struct A6XX_RBBM_BLOCK_SW_RESET_CMD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_BLOCK_SW_RESET_CMD(struct A6XX_RBBM_BLOCK_SW_RESET_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_BLOCK_SW_RESET_CMD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_BLOCK_SW_RESET_CMD(...) pack_A6XX_RBBM_BLOCK_SW_RESET_CMD((struct A6XX_RBBM_BLOCK_SW_RESET_CMD) { __VA_ARGS__ })

struct A6XX_RBBM_BLOCK_SW_RESET_CMD2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_BLOCK_SW_RESET_CMD2(struct A6XX_RBBM_BLOCK_SW_RESET_CMD2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_BLOCK_SW_RESET_CMD2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_BLOCK_SW_RESET_CMD2(...) pack_A6XX_RBBM_BLOCK_SW_RESET_CMD2((struct A6XX_RBBM_BLOCK_SW_RESET_CMD2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL(struct A6XX_RBBM_CLOCK_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL(...) pack_A6XX_RBBM_CLOCK_CNTL((struct A6XX_RBBM_CLOCK_CNTL) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_SP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_SP0(struct A6XX_RBBM_CLOCK_CNTL_SP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_SP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_SP0(...) pack_A6XX_RBBM_CLOCK_CNTL_SP0((struct A6XX_RBBM_CLOCK_CNTL_SP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_SP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_SP1(struct A6XX_RBBM_CLOCK_CNTL_SP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_SP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_SP1(...) pack_A6XX_RBBM_CLOCK_CNTL_SP1((struct A6XX_RBBM_CLOCK_CNTL_SP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_SP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_SP2(struct A6XX_RBBM_CLOCK_CNTL_SP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_SP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_SP2(...) pack_A6XX_RBBM_CLOCK_CNTL_SP2((struct A6XX_RBBM_CLOCK_CNTL_SP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_SP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_SP3(struct A6XX_RBBM_CLOCK_CNTL_SP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_SP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_SP3(...) pack_A6XX_RBBM_CLOCK_CNTL_SP3((struct A6XX_RBBM_CLOCK_CNTL_SP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_SP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_SP0(struct A6XX_RBBM_CLOCK_CNTL2_SP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_SP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_SP0(...) pack_A6XX_RBBM_CLOCK_CNTL2_SP0((struct A6XX_RBBM_CLOCK_CNTL2_SP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_SP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_SP1(struct A6XX_RBBM_CLOCK_CNTL2_SP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_SP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_SP1(...) pack_A6XX_RBBM_CLOCK_CNTL2_SP1((struct A6XX_RBBM_CLOCK_CNTL2_SP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_SP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_SP2(struct A6XX_RBBM_CLOCK_CNTL2_SP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_SP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_SP2(...) pack_A6XX_RBBM_CLOCK_CNTL2_SP2((struct A6XX_RBBM_CLOCK_CNTL2_SP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_SP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_SP3(struct A6XX_RBBM_CLOCK_CNTL2_SP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_SP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_SP3(...) pack_A6XX_RBBM_CLOCK_CNTL2_SP3((struct A6XX_RBBM_CLOCK_CNTL2_SP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_SP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_SP0(struct A6XX_RBBM_CLOCK_DELAY_SP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_SP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_SP0(...) pack_A6XX_RBBM_CLOCK_DELAY_SP0((struct A6XX_RBBM_CLOCK_DELAY_SP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_SP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_SP1(struct A6XX_RBBM_CLOCK_DELAY_SP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_SP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_SP1(...) pack_A6XX_RBBM_CLOCK_DELAY_SP1((struct A6XX_RBBM_CLOCK_DELAY_SP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_SP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_SP2(struct A6XX_RBBM_CLOCK_DELAY_SP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_SP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_SP2(...) pack_A6XX_RBBM_CLOCK_DELAY_SP2((struct A6XX_RBBM_CLOCK_DELAY_SP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_SP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_SP3(struct A6XX_RBBM_CLOCK_DELAY_SP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_SP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_SP3(...) pack_A6XX_RBBM_CLOCK_DELAY_SP3((struct A6XX_RBBM_CLOCK_DELAY_SP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_SP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_SP0(struct A6XX_RBBM_CLOCK_HYST_SP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_SP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_SP0(...) pack_A6XX_RBBM_CLOCK_HYST_SP0((struct A6XX_RBBM_CLOCK_HYST_SP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_SP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_SP1(struct A6XX_RBBM_CLOCK_HYST_SP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_SP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_SP1(...) pack_A6XX_RBBM_CLOCK_HYST_SP1((struct A6XX_RBBM_CLOCK_HYST_SP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_SP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_SP2(struct A6XX_RBBM_CLOCK_HYST_SP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_SP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_SP2(...) pack_A6XX_RBBM_CLOCK_HYST_SP2((struct A6XX_RBBM_CLOCK_HYST_SP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_SP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_SP3(struct A6XX_RBBM_CLOCK_HYST_SP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_SP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_SP3(...) pack_A6XX_RBBM_CLOCK_HYST_SP3((struct A6XX_RBBM_CLOCK_HYST_SP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TP0(struct A6XX_RBBM_CLOCK_CNTL_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TP0(...) pack_A6XX_RBBM_CLOCK_CNTL_TP0((struct A6XX_RBBM_CLOCK_CNTL_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TP1(struct A6XX_RBBM_CLOCK_CNTL_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TP1(...) pack_A6XX_RBBM_CLOCK_CNTL_TP1((struct A6XX_RBBM_CLOCK_CNTL_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TP2(struct A6XX_RBBM_CLOCK_CNTL_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TP2(...) pack_A6XX_RBBM_CLOCK_CNTL_TP2((struct A6XX_RBBM_CLOCK_CNTL_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TP3(struct A6XX_RBBM_CLOCK_CNTL_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TP3(...) pack_A6XX_RBBM_CLOCK_CNTL_TP3((struct A6XX_RBBM_CLOCK_CNTL_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_TP0(struct A6XX_RBBM_CLOCK_CNTL2_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_TP0(...) pack_A6XX_RBBM_CLOCK_CNTL2_TP0((struct A6XX_RBBM_CLOCK_CNTL2_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_TP1(struct A6XX_RBBM_CLOCK_CNTL2_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_TP1(...) pack_A6XX_RBBM_CLOCK_CNTL2_TP1((struct A6XX_RBBM_CLOCK_CNTL2_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_TP2(struct A6XX_RBBM_CLOCK_CNTL2_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_TP2(...) pack_A6XX_RBBM_CLOCK_CNTL2_TP2((struct A6XX_RBBM_CLOCK_CNTL2_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_TP3(struct A6XX_RBBM_CLOCK_CNTL2_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_TP3(...) pack_A6XX_RBBM_CLOCK_CNTL2_TP3((struct A6XX_RBBM_CLOCK_CNTL2_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL3_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL3_TP0(struct A6XX_RBBM_CLOCK_CNTL3_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL3_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL3_TP0(...) pack_A6XX_RBBM_CLOCK_CNTL3_TP0((struct A6XX_RBBM_CLOCK_CNTL3_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL3_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL3_TP1(struct A6XX_RBBM_CLOCK_CNTL3_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL3_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL3_TP1(...) pack_A6XX_RBBM_CLOCK_CNTL3_TP1((struct A6XX_RBBM_CLOCK_CNTL3_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL3_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL3_TP2(struct A6XX_RBBM_CLOCK_CNTL3_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL3_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL3_TP2(...) pack_A6XX_RBBM_CLOCK_CNTL3_TP2((struct A6XX_RBBM_CLOCK_CNTL3_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL3_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL3_TP3(struct A6XX_RBBM_CLOCK_CNTL3_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL3_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL3_TP3(...) pack_A6XX_RBBM_CLOCK_CNTL3_TP3((struct A6XX_RBBM_CLOCK_CNTL3_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL4_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL4_TP0(struct A6XX_RBBM_CLOCK_CNTL4_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL4_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL4_TP0(...) pack_A6XX_RBBM_CLOCK_CNTL4_TP0((struct A6XX_RBBM_CLOCK_CNTL4_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL4_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL4_TP1(struct A6XX_RBBM_CLOCK_CNTL4_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL4_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL4_TP1(...) pack_A6XX_RBBM_CLOCK_CNTL4_TP1((struct A6XX_RBBM_CLOCK_CNTL4_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL4_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL4_TP2(struct A6XX_RBBM_CLOCK_CNTL4_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL4_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL4_TP2(...) pack_A6XX_RBBM_CLOCK_CNTL4_TP2((struct A6XX_RBBM_CLOCK_CNTL4_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL4_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL4_TP3(struct A6XX_RBBM_CLOCK_CNTL4_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL4_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL4_TP3(...) pack_A6XX_RBBM_CLOCK_CNTL4_TP3((struct A6XX_RBBM_CLOCK_CNTL4_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TP0(struct A6XX_RBBM_CLOCK_DELAY_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TP0(...) pack_A6XX_RBBM_CLOCK_DELAY_TP0((struct A6XX_RBBM_CLOCK_DELAY_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TP1(struct A6XX_RBBM_CLOCK_DELAY_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TP1(...) pack_A6XX_RBBM_CLOCK_DELAY_TP1((struct A6XX_RBBM_CLOCK_DELAY_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TP2(struct A6XX_RBBM_CLOCK_DELAY_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TP2(...) pack_A6XX_RBBM_CLOCK_DELAY_TP2((struct A6XX_RBBM_CLOCK_DELAY_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TP3(struct A6XX_RBBM_CLOCK_DELAY_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TP3(...) pack_A6XX_RBBM_CLOCK_DELAY_TP3((struct A6XX_RBBM_CLOCK_DELAY_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY2_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY2_TP0(struct A6XX_RBBM_CLOCK_DELAY2_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY2_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY2_TP0(...) pack_A6XX_RBBM_CLOCK_DELAY2_TP0((struct A6XX_RBBM_CLOCK_DELAY2_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY2_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY2_TP1(struct A6XX_RBBM_CLOCK_DELAY2_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY2_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY2_TP1(...) pack_A6XX_RBBM_CLOCK_DELAY2_TP1((struct A6XX_RBBM_CLOCK_DELAY2_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY2_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY2_TP2(struct A6XX_RBBM_CLOCK_DELAY2_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY2_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY2_TP2(...) pack_A6XX_RBBM_CLOCK_DELAY2_TP2((struct A6XX_RBBM_CLOCK_DELAY2_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY2_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY2_TP3(struct A6XX_RBBM_CLOCK_DELAY2_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY2_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY2_TP3(...) pack_A6XX_RBBM_CLOCK_DELAY2_TP3((struct A6XX_RBBM_CLOCK_DELAY2_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY3_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY3_TP0(struct A6XX_RBBM_CLOCK_DELAY3_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY3_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY3_TP0(...) pack_A6XX_RBBM_CLOCK_DELAY3_TP0((struct A6XX_RBBM_CLOCK_DELAY3_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY3_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY3_TP1(struct A6XX_RBBM_CLOCK_DELAY3_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY3_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY3_TP1(...) pack_A6XX_RBBM_CLOCK_DELAY3_TP1((struct A6XX_RBBM_CLOCK_DELAY3_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY3_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY3_TP2(struct A6XX_RBBM_CLOCK_DELAY3_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY3_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY3_TP2(...) pack_A6XX_RBBM_CLOCK_DELAY3_TP2((struct A6XX_RBBM_CLOCK_DELAY3_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY3_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY3_TP3(struct A6XX_RBBM_CLOCK_DELAY3_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY3_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY3_TP3(...) pack_A6XX_RBBM_CLOCK_DELAY3_TP3((struct A6XX_RBBM_CLOCK_DELAY3_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY4_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY4_TP0(struct A6XX_RBBM_CLOCK_DELAY4_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY4_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY4_TP0(...) pack_A6XX_RBBM_CLOCK_DELAY4_TP0((struct A6XX_RBBM_CLOCK_DELAY4_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY4_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY4_TP1(struct A6XX_RBBM_CLOCK_DELAY4_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY4_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY4_TP1(...) pack_A6XX_RBBM_CLOCK_DELAY4_TP1((struct A6XX_RBBM_CLOCK_DELAY4_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY4_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY4_TP2(struct A6XX_RBBM_CLOCK_DELAY4_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY4_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY4_TP2(...) pack_A6XX_RBBM_CLOCK_DELAY4_TP2((struct A6XX_RBBM_CLOCK_DELAY4_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY4_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY4_TP3(struct A6XX_RBBM_CLOCK_DELAY4_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY4_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY4_TP3(...) pack_A6XX_RBBM_CLOCK_DELAY4_TP3((struct A6XX_RBBM_CLOCK_DELAY4_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TP0(struct A6XX_RBBM_CLOCK_HYST_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TP0(...) pack_A6XX_RBBM_CLOCK_HYST_TP0((struct A6XX_RBBM_CLOCK_HYST_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TP1(struct A6XX_RBBM_CLOCK_HYST_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TP1(...) pack_A6XX_RBBM_CLOCK_HYST_TP1((struct A6XX_RBBM_CLOCK_HYST_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TP2(struct A6XX_RBBM_CLOCK_HYST_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TP2(...) pack_A6XX_RBBM_CLOCK_HYST_TP2((struct A6XX_RBBM_CLOCK_HYST_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TP3(struct A6XX_RBBM_CLOCK_HYST_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TP3(...) pack_A6XX_RBBM_CLOCK_HYST_TP3((struct A6XX_RBBM_CLOCK_HYST_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST2_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST2_TP0(struct A6XX_RBBM_CLOCK_HYST2_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST2_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST2_TP0(...) pack_A6XX_RBBM_CLOCK_HYST2_TP0((struct A6XX_RBBM_CLOCK_HYST2_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST2_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST2_TP1(struct A6XX_RBBM_CLOCK_HYST2_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST2_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST2_TP1(...) pack_A6XX_RBBM_CLOCK_HYST2_TP1((struct A6XX_RBBM_CLOCK_HYST2_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST2_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST2_TP2(struct A6XX_RBBM_CLOCK_HYST2_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST2_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST2_TP2(...) pack_A6XX_RBBM_CLOCK_HYST2_TP2((struct A6XX_RBBM_CLOCK_HYST2_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST2_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST2_TP3(struct A6XX_RBBM_CLOCK_HYST2_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST2_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST2_TP3(...) pack_A6XX_RBBM_CLOCK_HYST2_TP3((struct A6XX_RBBM_CLOCK_HYST2_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST3_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST3_TP0(struct A6XX_RBBM_CLOCK_HYST3_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST3_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST3_TP0(...) pack_A6XX_RBBM_CLOCK_HYST3_TP0((struct A6XX_RBBM_CLOCK_HYST3_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST3_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST3_TP1(struct A6XX_RBBM_CLOCK_HYST3_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST3_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST3_TP1(...) pack_A6XX_RBBM_CLOCK_HYST3_TP1((struct A6XX_RBBM_CLOCK_HYST3_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST3_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST3_TP2(struct A6XX_RBBM_CLOCK_HYST3_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST3_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST3_TP2(...) pack_A6XX_RBBM_CLOCK_HYST3_TP2((struct A6XX_RBBM_CLOCK_HYST3_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST3_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST3_TP3(struct A6XX_RBBM_CLOCK_HYST3_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST3_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST3_TP3(...) pack_A6XX_RBBM_CLOCK_HYST3_TP3((struct A6XX_RBBM_CLOCK_HYST3_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST4_TP0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST4_TP0(struct A6XX_RBBM_CLOCK_HYST4_TP0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST4_TP0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST4_TP0(...) pack_A6XX_RBBM_CLOCK_HYST4_TP0((struct A6XX_RBBM_CLOCK_HYST4_TP0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST4_TP1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST4_TP1(struct A6XX_RBBM_CLOCK_HYST4_TP1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST4_TP1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST4_TP1(...) pack_A6XX_RBBM_CLOCK_HYST4_TP1((struct A6XX_RBBM_CLOCK_HYST4_TP1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST4_TP2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST4_TP2(struct A6XX_RBBM_CLOCK_HYST4_TP2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST4_TP2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST4_TP2(...) pack_A6XX_RBBM_CLOCK_HYST4_TP2((struct A6XX_RBBM_CLOCK_HYST4_TP2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST4_TP3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST4_TP3(struct A6XX_RBBM_CLOCK_HYST4_TP3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST4_TP3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST4_TP3(...) pack_A6XX_RBBM_CLOCK_HYST4_TP3((struct A6XX_RBBM_CLOCK_HYST4_TP3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_RB0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_RB0(struct A6XX_RBBM_CLOCK_CNTL_RB0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_RB0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_RB0(...) pack_A6XX_RBBM_CLOCK_CNTL_RB0((struct A6XX_RBBM_CLOCK_CNTL_RB0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_RB1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_RB1(struct A6XX_RBBM_CLOCK_CNTL_RB1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_RB1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_RB1(...) pack_A6XX_RBBM_CLOCK_CNTL_RB1((struct A6XX_RBBM_CLOCK_CNTL_RB1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_RB2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_RB2(struct A6XX_RBBM_CLOCK_CNTL_RB2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_RB2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_RB2(...) pack_A6XX_RBBM_CLOCK_CNTL_RB2((struct A6XX_RBBM_CLOCK_CNTL_RB2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_RB3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_RB3(struct A6XX_RBBM_CLOCK_CNTL_RB3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_RB3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_RB3(...) pack_A6XX_RBBM_CLOCK_CNTL_RB3((struct A6XX_RBBM_CLOCK_CNTL_RB3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_RB0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_RB0(struct A6XX_RBBM_CLOCK_CNTL2_RB0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_RB0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_RB0(...) pack_A6XX_RBBM_CLOCK_CNTL2_RB0((struct A6XX_RBBM_CLOCK_CNTL2_RB0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_RB1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_RB1(struct A6XX_RBBM_CLOCK_CNTL2_RB1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_RB1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_RB1(...) pack_A6XX_RBBM_CLOCK_CNTL2_RB1((struct A6XX_RBBM_CLOCK_CNTL2_RB1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_RB2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_RB2(struct A6XX_RBBM_CLOCK_CNTL2_RB2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_RB2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_RB2(...) pack_A6XX_RBBM_CLOCK_CNTL2_RB2((struct A6XX_RBBM_CLOCK_CNTL2_RB2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_RB3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_RB3(struct A6XX_RBBM_CLOCK_CNTL2_RB3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_RB3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_RB3(...) pack_A6XX_RBBM_CLOCK_CNTL2_RB3((struct A6XX_RBBM_CLOCK_CNTL2_RB3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_CCU0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_CCU0(struct A6XX_RBBM_CLOCK_CNTL_CCU0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_CCU0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_CCU0(...) pack_A6XX_RBBM_CLOCK_CNTL_CCU0((struct A6XX_RBBM_CLOCK_CNTL_CCU0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_CCU1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_CCU1(struct A6XX_RBBM_CLOCK_CNTL_CCU1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_CCU1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_CCU1(...) pack_A6XX_RBBM_CLOCK_CNTL_CCU1((struct A6XX_RBBM_CLOCK_CNTL_CCU1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_CCU2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_CCU2(struct A6XX_RBBM_CLOCK_CNTL_CCU2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_CCU2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_CCU2(...) pack_A6XX_RBBM_CLOCK_CNTL_CCU2((struct A6XX_RBBM_CLOCK_CNTL_CCU2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_CCU3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_CCU3(struct A6XX_RBBM_CLOCK_CNTL_CCU3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_CCU3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_CCU3(...) pack_A6XX_RBBM_CLOCK_CNTL_CCU3((struct A6XX_RBBM_CLOCK_CNTL_CCU3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_RB_CCU0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_RB_CCU0(struct A6XX_RBBM_CLOCK_HYST_RB_CCU0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_RB_CCU0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_RB_CCU0(...) pack_A6XX_RBBM_CLOCK_HYST_RB_CCU0((struct A6XX_RBBM_CLOCK_HYST_RB_CCU0) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_RB_CCU1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_RB_CCU1(struct A6XX_RBBM_CLOCK_HYST_RB_CCU1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_RB_CCU1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_RB_CCU1(...) pack_A6XX_RBBM_CLOCK_HYST_RB_CCU1((struct A6XX_RBBM_CLOCK_HYST_RB_CCU1) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_RB_CCU2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_RB_CCU2(struct A6XX_RBBM_CLOCK_HYST_RB_CCU2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_RB_CCU2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_RB_CCU2(...) pack_A6XX_RBBM_CLOCK_HYST_RB_CCU2((struct A6XX_RBBM_CLOCK_HYST_RB_CCU2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_RB_CCU3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_RB_CCU3(struct A6XX_RBBM_CLOCK_HYST_RB_CCU3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_RB_CCU3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_RB_CCU3(...) pack_A6XX_RBBM_CLOCK_HYST_RB_CCU3((struct A6XX_RBBM_CLOCK_HYST_RB_CCU3) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_RAC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_RAC(struct A6XX_RBBM_CLOCK_CNTL_RAC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_RAC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_RAC(...) pack_A6XX_RBBM_CLOCK_CNTL_RAC((struct A6XX_RBBM_CLOCK_CNTL_RAC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_RAC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_RAC(struct A6XX_RBBM_CLOCK_CNTL2_RAC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_RAC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_RAC(...) pack_A6XX_RBBM_CLOCK_CNTL2_RAC((struct A6XX_RBBM_CLOCK_CNTL2_RAC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_RAC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_RAC(struct A6XX_RBBM_CLOCK_DELAY_RAC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_RAC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_RAC(...) pack_A6XX_RBBM_CLOCK_DELAY_RAC((struct A6XX_RBBM_CLOCK_DELAY_RAC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_RAC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_RAC(struct A6XX_RBBM_CLOCK_HYST_RAC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_RAC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_RAC(...) pack_A6XX_RBBM_CLOCK_HYST_RAC((struct A6XX_RBBM_CLOCK_HYST_RAC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM(struct A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM(...) pack_A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM((struct A6XX_RBBM_CLOCK_CNTL_TSE_RAS_RBBM) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM(struct A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM(...) pack_A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM((struct A6XX_RBBM_CLOCK_DELAY_TSE_RAS_RBBM) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM(struct A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM(...) pack_A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM((struct A6XX_RBBM_CLOCK_HYST_TSE_RAS_RBBM) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_UCHE(struct A6XX_RBBM_CLOCK_CNTL_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_UCHE(...) pack_A6XX_RBBM_CLOCK_CNTL_UCHE((struct A6XX_RBBM_CLOCK_CNTL_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL2_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL2_UCHE(struct A6XX_RBBM_CLOCK_CNTL2_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL2_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL2_UCHE(...) pack_A6XX_RBBM_CLOCK_CNTL2_UCHE((struct A6XX_RBBM_CLOCK_CNTL2_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL3_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL3_UCHE(struct A6XX_RBBM_CLOCK_CNTL3_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL3_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL3_UCHE(...) pack_A6XX_RBBM_CLOCK_CNTL3_UCHE((struct A6XX_RBBM_CLOCK_CNTL3_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL4_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL4_UCHE(struct A6XX_RBBM_CLOCK_CNTL4_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL4_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL4_UCHE(...) pack_A6XX_RBBM_CLOCK_CNTL4_UCHE((struct A6XX_RBBM_CLOCK_CNTL4_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_UCHE(struct A6XX_RBBM_CLOCK_DELAY_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_UCHE(...) pack_A6XX_RBBM_CLOCK_DELAY_UCHE((struct A6XX_RBBM_CLOCK_DELAY_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_UCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_UCHE(struct A6XX_RBBM_CLOCK_HYST_UCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_UCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_UCHE(...) pack_A6XX_RBBM_CLOCK_HYST_UCHE((struct A6XX_RBBM_CLOCK_HYST_UCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_MODE_VFD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_MODE_VFD(struct A6XX_RBBM_CLOCK_MODE_VFD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_MODE_VFD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_MODE_VFD(...) pack_A6XX_RBBM_CLOCK_MODE_VFD((struct A6XX_RBBM_CLOCK_MODE_VFD) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_VFD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_VFD(struct A6XX_RBBM_CLOCK_DELAY_VFD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_VFD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_VFD(...) pack_A6XX_RBBM_CLOCK_DELAY_VFD((struct A6XX_RBBM_CLOCK_DELAY_VFD) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_VFD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_VFD(struct A6XX_RBBM_CLOCK_HYST_VFD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_VFD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_VFD(...) pack_A6XX_RBBM_CLOCK_HYST_VFD((struct A6XX_RBBM_CLOCK_HYST_VFD) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_MODE_GPC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_MODE_GPC(struct A6XX_RBBM_CLOCK_MODE_GPC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_MODE_GPC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_MODE_GPC(...) pack_A6XX_RBBM_CLOCK_MODE_GPC((struct A6XX_RBBM_CLOCK_MODE_GPC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_GPC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_GPC(struct A6XX_RBBM_CLOCK_DELAY_GPC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_GPC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_GPC(...) pack_A6XX_RBBM_CLOCK_DELAY_GPC((struct A6XX_RBBM_CLOCK_DELAY_GPC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_GPC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_GPC(struct A6XX_RBBM_CLOCK_HYST_GPC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_GPC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_GPC(...) pack_A6XX_RBBM_CLOCK_HYST_GPC((struct A6XX_RBBM_CLOCK_HYST_GPC) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_HLSQ_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_HLSQ_2(struct A6XX_RBBM_CLOCK_DELAY_HLSQ_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_HLSQ_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_HLSQ_2(...) pack_A6XX_RBBM_CLOCK_DELAY_HLSQ_2((struct A6XX_RBBM_CLOCK_DELAY_HLSQ_2) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_GMU_GX {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_GMU_GX(struct A6XX_RBBM_CLOCK_CNTL_GMU_GX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_GMU_GX,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_GMU_GX(...) pack_A6XX_RBBM_CLOCK_CNTL_GMU_GX((struct A6XX_RBBM_CLOCK_CNTL_GMU_GX) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_GMU_GX {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_GMU_GX(struct A6XX_RBBM_CLOCK_DELAY_GMU_GX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_GMU_GX,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_GMU_GX(...) pack_A6XX_RBBM_CLOCK_DELAY_GMU_GX((struct A6XX_RBBM_CLOCK_DELAY_GMU_GX) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_GMU_GX {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_GMU_GX(struct A6XX_RBBM_CLOCK_HYST_GMU_GX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_GMU_GX,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_GMU_GX(...) pack_A6XX_RBBM_CLOCK_HYST_GMU_GX((struct A6XX_RBBM_CLOCK_HYST_GMU_GX) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_MODE_HLSQ {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_MODE_HLSQ(struct A6XX_RBBM_CLOCK_MODE_HLSQ fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_MODE_HLSQ,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_MODE_HLSQ(...) pack_A6XX_RBBM_CLOCK_MODE_HLSQ((struct A6XX_RBBM_CLOCK_MODE_HLSQ) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_HLSQ {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_HLSQ(struct A6XX_RBBM_CLOCK_DELAY_HLSQ fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_HLSQ,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_HLSQ(...) pack_A6XX_RBBM_CLOCK_DELAY_HLSQ((struct A6XX_RBBM_CLOCK_DELAY_HLSQ) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_HLSQ {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_HLSQ(struct A6XX_RBBM_CLOCK_HYST_HLSQ fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_HLSQ,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_HLSQ(...) pack_A6XX_RBBM_CLOCK_HYST_HLSQ((struct A6XX_RBBM_CLOCK_HYST_HLSQ) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_CNTL_TEX_FCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_CNTL_TEX_FCHE(struct A6XX_RBBM_CLOCK_CNTL_TEX_FCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_CNTL_TEX_FCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_CNTL_TEX_FCHE(...) pack_A6XX_RBBM_CLOCK_CNTL_TEX_FCHE((struct A6XX_RBBM_CLOCK_CNTL_TEX_FCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_DELAY_TEX_FCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_DELAY_TEX_FCHE(struct A6XX_RBBM_CLOCK_DELAY_TEX_FCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_DELAY_TEX_FCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_DELAY_TEX_FCHE(...) pack_A6XX_RBBM_CLOCK_DELAY_TEX_FCHE((struct A6XX_RBBM_CLOCK_DELAY_TEX_FCHE) { __VA_ARGS__ })

struct A6XX_RBBM_CLOCK_HYST_TEX_FCHE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RBBM_CLOCK_HYST_TEX_FCHE(struct A6XX_RBBM_CLOCK_HYST_TEX_FCHE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RBBM_CLOCK_HYST_TEX_FCHE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RBBM_CLOCK_HYST_TEX_FCHE(...) pack_A6XX_RBBM_CLOCK_HYST_TEX_FCHE((struct A6XX_RBBM_CLOCK_HYST_TEX_FCHE) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_SEL_A {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_SEL_A(struct A6XX_DBGC_CFG_DBGBUS_SEL_A fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_SEL_A,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_SEL_A(...) pack_A6XX_DBGC_CFG_DBGBUS_SEL_A((struct A6XX_DBGC_CFG_DBGBUS_SEL_A) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_SEL_B {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_SEL_B(struct A6XX_DBGC_CFG_DBGBUS_SEL_B fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_SEL_B,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_SEL_B(...) pack_A6XX_DBGC_CFG_DBGBUS_SEL_B((struct A6XX_DBGC_CFG_DBGBUS_SEL_B) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_SEL_C {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_SEL_C(struct A6XX_DBGC_CFG_DBGBUS_SEL_C fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_SEL_C,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_SEL_C(...) pack_A6XX_DBGC_CFG_DBGBUS_SEL_C((struct A6XX_DBGC_CFG_DBGBUS_SEL_C) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_SEL_D {
    uint32_t							ping_index;
    uint32_t							ping_blk_sel;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_SEL_D(struct A6XX_DBGC_CFG_DBGBUS_SEL_D fields)
{
#ifndef NDEBUG
    assert((fields.ping_index                        & 0xffffff00) == 0);
    assert((fields.ping_blk_sel                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_SEL_D,
        .value =
            (fields.ping_index                        <<  0) |
            (fields.ping_blk_sel                      <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_SEL_D(...) pack_A6XX_DBGC_CFG_DBGBUS_SEL_D((struct A6XX_DBGC_CFG_DBGBUS_SEL_D) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_CNTLT {
    uint32_t							traceen;
    uint32_t							granu;
    uint32_t							segt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_CNTLT(struct A6XX_DBGC_CFG_DBGBUS_CNTLT fields)
{
#ifndef NDEBUG
    assert((fields.traceen                           & 0xffffffc0) == 0);
    assert((fields.granu                             & 0xfffffff8) == 0);
    assert((fields.segt                              & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xf000703f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_CNTLT,
        .value =
            (fields.traceen                           <<  0) |
            (fields.granu                             << 12) |
            (fields.segt                              << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_CNTLT(...) pack_A6XX_DBGC_CFG_DBGBUS_CNTLT((struct A6XX_DBGC_CFG_DBGBUS_CNTLT) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_CNTLM {
    uint32_t							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_CNTLM(struct A6XX_DBGC_CFG_DBGBUS_CNTLM fields)
{
#ifndef NDEBUG
    assert((fields.enable                            & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0f000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_CNTLM,
        .value =
            (fields.enable                            << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_CNTLM(...) pack_A6XX_DBGC_CFG_DBGBUS_CNTLM((struct A6XX_DBGC_CFG_DBGBUS_CNTLM) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_IVTL_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_IVTL_0(struct A6XX_DBGC_CFG_DBGBUS_IVTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_IVTL_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_IVTL_0(...) pack_A6XX_DBGC_CFG_DBGBUS_IVTL_0((struct A6XX_DBGC_CFG_DBGBUS_IVTL_0) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_IVTL_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_IVTL_1(struct A6XX_DBGC_CFG_DBGBUS_IVTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_IVTL_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_IVTL_1(...) pack_A6XX_DBGC_CFG_DBGBUS_IVTL_1((struct A6XX_DBGC_CFG_DBGBUS_IVTL_1) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_IVTL_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_IVTL_2(struct A6XX_DBGC_CFG_DBGBUS_IVTL_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_IVTL_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_IVTL_2(...) pack_A6XX_DBGC_CFG_DBGBUS_IVTL_2((struct A6XX_DBGC_CFG_DBGBUS_IVTL_2) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_IVTL_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_IVTL_3(struct A6XX_DBGC_CFG_DBGBUS_IVTL_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_IVTL_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_IVTL_3(...) pack_A6XX_DBGC_CFG_DBGBUS_IVTL_3((struct A6XX_DBGC_CFG_DBGBUS_IVTL_3) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_MASKL_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_MASKL_0(struct A6XX_DBGC_CFG_DBGBUS_MASKL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_MASKL_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_MASKL_0(...) pack_A6XX_DBGC_CFG_DBGBUS_MASKL_0((struct A6XX_DBGC_CFG_DBGBUS_MASKL_0) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_MASKL_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_MASKL_1(struct A6XX_DBGC_CFG_DBGBUS_MASKL_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_MASKL_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_MASKL_1(...) pack_A6XX_DBGC_CFG_DBGBUS_MASKL_1((struct A6XX_DBGC_CFG_DBGBUS_MASKL_1) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_MASKL_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_MASKL_2(struct A6XX_DBGC_CFG_DBGBUS_MASKL_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_MASKL_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_MASKL_2(...) pack_A6XX_DBGC_CFG_DBGBUS_MASKL_2((struct A6XX_DBGC_CFG_DBGBUS_MASKL_2) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_MASKL_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_MASKL_3(struct A6XX_DBGC_CFG_DBGBUS_MASKL_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_MASKL_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_MASKL_3(...) pack_A6XX_DBGC_CFG_DBGBUS_MASKL_3((struct A6XX_DBGC_CFG_DBGBUS_MASKL_3) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_BYTEL_0 {
    uint32_t							bytel0;
    uint32_t							bytel1;
    uint32_t							bytel2;
    uint32_t							bytel3;
    uint32_t							bytel4;
    uint32_t							bytel5;
    uint32_t							bytel6;
    uint32_t							bytel7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_BYTEL_0(struct A6XX_DBGC_CFG_DBGBUS_BYTEL_0 fields)
{
#ifndef NDEBUG
    assert((fields.bytel0                            & 0xfffffff0) == 0);
    assert((fields.bytel1                            & 0xfffffff0) == 0);
    assert((fields.bytel2                            & 0xfffffff0) == 0);
    assert((fields.bytel3                            & 0xfffffff0) == 0);
    assert((fields.bytel4                            & 0xfffffff0) == 0);
    assert((fields.bytel5                            & 0xfffffff0) == 0);
    assert((fields.bytel6                            & 0xfffffff0) == 0);
    assert((fields.bytel7                            & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_BYTEL_0,
        .value =
            (fields.bytel0                            <<  0) |
            (fields.bytel1                            <<  4) |
            (fields.bytel2                            <<  8) |
            (fields.bytel3                            << 12) |
            (fields.bytel4                            << 16) |
            (fields.bytel5                            << 20) |
            (fields.bytel6                            << 24) |
            (fields.bytel7                            << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_BYTEL_0(...) pack_A6XX_DBGC_CFG_DBGBUS_BYTEL_0((struct A6XX_DBGC_CFG_DBGBUS_BYTEL_0) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_BYTEL_1 {
    uint32_t							bytel8;
    uint32_t							bytel9;
    uint32_t							bytel10;
    uint32_t							bytel11;
    uint32_t							bytel12;
    uint32_t							bytel13;
    uint32_t							bytel14;
    uint32_t							bytel15;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_BYTEL_1(struct A6XX_DBGC_CFG_DBGBUS_BYTEL_1 fields)
{
#ifndef NDEBUG
    assert((fields.bytel8                            & 0xfffffff0) == 0);
    assert((fields.bytel9                            & 0xfffffff0) == 0);
    assert((fields.bytel10                           & 0xfffffff0) == 0);
    assert((fields.bytel11                           & 0xfffffff0) == 0);
    assert((fields.bytel12                           & 0xfffffff0) == 0);
    assert((fields.bytel13                           & 0xfffffff0) == 0);
    assert((fields.bytel14                           & 0xfffffff0) == 0);
    assert((fields.bytel15                           & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_BYTEL_1,
        .value =
            (fields.bytel8                            <<  0) |
            (fields.bytel9                            <<  4) |
            (fields.bytel10                           <<  8) |
            (fields.bytel11                           << 12) |
            (fields.bytel12                           << 16) |
            (fields.bytel13                           << 20) |
            (fields.bytel14                           << 24) |
            (fields.bytel15                           << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_BYTEL_1(...) pack_A6XX_DBGC_CFG_DBGBUS_BYTEL_1((struct A6XX_DBGC_CFG_DBGBUS_BYTEL_1) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1(struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1(...) pack_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1((struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF1) { __VA_ARGS__ })

struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2(struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2(...) pack_A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2((struct A6XX_DBGC_CFG_DBGBUS_TRACE_BUF2) { __VA_ARGS__ })

struct A6XX_HLSQ_DBG_AHB_READ_APERTURE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_DBG_AHB_READ_APERTURE(struct A6XX_HLSQ_DBG_AHB_READ_APERTURE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_DBG_AHB_READ_APERTURE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_DBG_AHB_READ_APERTURE(...) pack_A6XX_HLSQ_DBG_AHB_READ_APERTURE((struct A6XX_HLSQ_DBG_AHB_READ_APERTURE) { __VA_ARGS__ })

struct A6XX_HLSQ_DBG_READ_SEL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_DBG_READ_SEL(struct A6XX_HLSQ_DBG_READ_SEL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_DBG_READ_SEL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_DBG_READ_SEL(...) pack_A6XX_HLSQ_DBG_READ_SEL((struct A6XX_HLSQ_DBG_READ_SEL) { __VA_ARGS__ })

struct A6XX_UCHE_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_uche_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_ADDR_MODE_CNTL(struct A6XX_UCHE_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_uche_addr_mode_cntl          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_uche_addr_mode_cntl          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_ADDR_MODE_CNTL(...) pack_A6XX_UCHE_ADDR_MODE_CNTL((struct A6XX_UCHE_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_UCHE_MODE_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_MODE_CNTL(struct A6XX_UCHE_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_MODE_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_MODE_CNTL(...) pack_A6XX_UCHE_MODE_CNTL((struct A6XX_UCHE_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_UCHE_WRITE_RANGE_MAX {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_WRITE_RANGE_MAX(struct A6XX_UCHE_WRITE_RANGE_MAX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_WRITE_RANGE_MAX,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_WRITE_RANGE_MAX(...) pack_A6XX_UCHE_WRITE_RANGE_MAX((struct A6XX_UCHE_WRITE_RANGE_MAX) { __VA_ARGS__ })

struct A6XX_UCHE_WRITE_THRU_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_WRITE_THRU_BASE(struct A6XX_UCHE_WRITE_THRU_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_WRITE_THRU_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_WRITE_THRU_BASE(...) pack_A6XX_UCHE_WRITE_THRU_BASE((struct A6XX_UCHE_WRITE_THRU_BASE) { __VA_ARGS__ })

struct A6XX_UCHE_TRAP_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_TRAP_BASE(struct A6XX_UCHE_TRAP_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_TRAP_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_TRAP_BASE(...) pack_A6XX_UCHE_TRAP_BASE((struct A6XX_UCHE_TRAP_BASE) { __VA_ARGS__ })

struct A6XX_UCHE_GMEM_RANGE_MIN {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_GMEM_RANGE_MIN(struct A6XX_UCHE_GMEM_RANGE_MIN fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_GMEM_RANGE_MIN,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_GMEM_RANGE_MIN(...) pack_A6XX_UCHE_GMEM_RANGE_MIN((struct A6XX_UCHE_GMEM_RANGE_MIN) { __VA_ARGS__ })

struct A6XX_UCHE_GMEM_RANGE_MAX {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_GMEM_RANGE_MAX(struct A6XX_UCHE_GMEM_RANGE_MAX fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_GMEM_RANGE_MAX,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_GMEM_RANGE_MAX(...) pack_A6XX_UCHE_GMEM_RANGE_MAX((struct A6XX_UCHE_GMEM_RANGE_MAX) { __VA_ARGS__ })

struct A6XX_UCHE_CACHE_WAYS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_CACHE_WAYS(struct A6XX_UCHE_CACHE_WAYS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_CACHE_WAYS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_CACHE_WAYS(...) pack_A6XX_UCHE_CACHE_WAYS((struct A6XX_UCHE_CACHE_WAYS) { __VA_ARGS__ })

struct A6XX_UCHE_FILTER_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_FILTER_CNTL(struct A6XX_UCHE_FILTER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_FILTER_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_FILTER_CNTL(...) pack_A6XX_UCHE_FILTER_CNTL((struct A6XX_UCHE_FILTER_CNTL) { __VA_ARGS__ })

struct A6XX_UCHE_CLIENT_PF {
    uint32_t							perfsel;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_CLIENT_PF(struct A6XX_UCHE_CLIENT_PF fields)
{
#ifndef NDEBUG
    assert((fields.perfsel                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_CLIENT_PF,
        .value =
            (fields.perfsel                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_CLIENT_PF(...) pack_A6XX_UCHE_CLIENT_PF((struct A6XX_UCHE_CLIENT_PF) { __VA_ARGS__ })

struct A6XX_UCHE_CMDQ_CONFIG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_CMDQ_CONFIG(struct A6XX_UCHE_CMDQ_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_CMDQ_CONFIG,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_CMDQ_CONFIG(...) pack_A6XX_UCHE_CMDQ_CONFIG((struct A6XX_UCHE_CMDQ_CONFIG) { __VA_ARGS__ })

struct A6XX_VBIF_VERSION {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_VERSION(struct A6XX_VBIF_VERSION fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_VERSION,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_VERSION(...) pack_A6XX_VBIF_VERSION((struct A6XX_VBIF_VERSION) { __VA_ARGS__ })

struct A6XX_VBIF_CLKON {
    bool							force_on_testbus;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_CLKON(struct A6XX_VBIF_CLKON fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000002) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_CLKON,
        .value =
            (fields.force_on_testbus                  <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_CLKON(...) pack_A6XX_VBIF_CLKON((struct A6XX_VBIF_CLKON) { __VA_ARGS__ })

struct A6XX_VBIF_GATE_OFF_WRREQ_EN {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_GATE_OFF_WRREQ_EN(struct A6XX_VBIF_GATE_OFF_WRREQ_EN fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_GATE_OFF_WRREQ_EN,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_GATE_OFF_WRREQ_EN(...) pack_A6XX_VBIF_GATE_OFF_WRREQ_EN((struct A6XX_VBIF_GATE_OFF_WRREQ_EN) { __VA_ARGS__ })

struct A6XX_VBIF_XIN_HALT_CTRL0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_XIN_HALT_CTRL0(struct A6XX_VBIF_XIN_HALT_CTRL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_XIN_HALT_CTRL0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_XIN_HALT_CTRL0(...) pack_A6XX_VBIF_XIN_HALT_CTRL0((struct A6XX_VBIF_XIN_HALT_CTRL0) { __VA_ARGS__ })

struct A6XX_VBIF_XIN_HALT_CTRL1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_XIN_HALT_CTRL1(struct A6XX_VBIF_XIN_HALT_CTRL1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_XIN_HALT_CTRL1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_XIN_HALT_CTRL1(...) pack_A6XX_VBIF_XIN_HALT_CTRL1((struct A6XX_VBIF_XIN_HALT_CTRL1) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS_OUT_CTRL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS_OUT_CTRL(struct A6XX_VBIF_TEST_BUS_OUT_CTRL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS_OUT_CTRL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS_OUT_CTRL(...) pack_A6XX_VBIF_TEST_BUS_OUT_CTRL((struct A6XX_VBIF_TEST_BUS_OUT_CTRL) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS1_CTRL0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS1_CTRL0(struct A6XX_VBIF_TEST_BUS1_CTRL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS1_CTRL0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS1_CTRL0(...) pack_A6XX_VBIF_TEST_BUS1_CTRL0((struct A6XX_VBIF_TEST_BUS1_CTRL0) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS1_CTRL1 {
    uint32_t							data_sel;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS1_CTRL1(struct A6XX_VBIF_TEST_BUS1_CTRL1 fields)
{
#ifndef NDEBUG
    assert((fields.data_sel                          & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS1_CTRL1,
        .value =
            (fields.data_sel                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS1_CTRL1(...) pack_A6XX_VBIF_TEST_BUS1_CTRL1((struct A6XX_VBIF_TEST_BUS1_CTRL1) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS2_CTRL0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS2_CTRL0(struct A6XX_VBIF_TEST_BUS2_CTRL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS2_CTRL0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS2_CTRL0(...) pack_A6XX_VBIF_TEST_BUS2_CTRL0((struct A6XX_VBIF_TEST_BUS2_CTRL0) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS2_CTRL1 {
    uint32_t							data_sel;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS2_CTRL1(struct A6XX_VBIF_TEST_BUS2_CTRL1 fields)
{
#ifndef NDEBUG
    assert((fields.data_sel                          & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS2_CTRL1,
        .value =
            (fields.data_sel                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS2_CTRL1(...) pack_A6XX_VBIF_TEST_BUS2_CTRL1((struct A6XX_VBIF_TEST_BUS2_CTRL1) { __VA_ARGS__ })

struct A6XX_VBIF_TEST_BUS_OUT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_TEST_BUS_OUT(struct A6XX_VBIF_TEST_BUS_OUT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_TEST_BUS_OUT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_TEST_BUS_OUT(...) pack_A6XX_VBIF_TEST_BUS_OUT((struct A6XX_VBIF_TEST_BUS_OUT) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_SEL0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_SEL0(struct A6XX_VBIF_PERF_CNT_SEL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_SEL0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_SEL0(...) pack_A6XX_VBIF_PERF_CNT_SEL0((struct A6XX_VBIF_PERF_CNT_SEL0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_SEL1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_SEL1(struct A6XX_VBIF_PERF_CNT_SEL1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_SEL1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_SEL1(...) pack_A6XX_VBIF_PERF_CNT_SEL1((struct A6XX_VBIF_PERF_CNT_SEL1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_SEL2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_SEL2(struct A6XX_VBIF_PERF_CNT_SEL2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_SEL2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_SEL2(...) pack_A6XX_VBIF_PERF_CNT_SEL2((struct A6XX_VBIF_PERF_CNT_SEL2) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_SEL3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_SEL3(struct A6XX_VBIF_PERF_CNT_SEL3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_SEL3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_SEL3(...) pack_A6XX_VBIF_PERF_CNT_SEL3((struct A6XX_VBIF_PERF_CNT_SEL3) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_LOW0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_LOW0(struct A6XX_VBIF_PERF_CNT_LOW0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_LOW0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_LOW0(...) pack_A6XX_VBIF_PERF_CNT_LOW0((struct A6XX_VBIF_PERF_CNT_LOW0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_LOW1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_LOW1(struct A6XX_VBIF_PERF_CNT_LOW1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_LOW1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_LOW1(...) pack_A6XX_VBIF_PERF_CNT_LOW1((struct A6XX_VBIF_PERF_CNT_LOW1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_LOW2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_LOW2(struct A6XX_VBIF_PERF_CNT_LOW2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_LOW2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_LOW2(...) pack_A6XX_VBIF_PERF_CNT_LOW2((struct A6XX_VBIF_PERF_CNT_LOW2) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_LOW3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_LOW3(struct A6XX_VBIF_PERF_CNT_LOW3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_LOW3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_LOW3(...) pack_A6XX_VBIF_PERF_CNT_LOW3((struct A6XX_VBIF_PERF_CNT_LOW3) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_HIGH0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_HIGH0(struct A6XX_VBIF_PERF_CNT_HIGH0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_HIGH0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_HIGH0(...) pack_A6XX_VBIF_PERF_CNT_HIGH0((struct A6XX_VBIF_PERF_CNT_HIGH0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_HIGH1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_HIGH1(struct A6XX_VBIF_PERF_CNT_HIGH1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_HIGH1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_HIGH1(...) pack_A6XX_VBIF_PERF_CNT_HIGH1((struct A6XX_VBIF_PERF_CNT_HIGH1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_HIGH2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_HIGH2(struct A6XX_VBIF_PERF_CNT_HIGH2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_HIGH2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_HIGH2(...) pack_A6XX_VBIF_PERF_CNT_HIGH2((struct A6XX_VBIF_PERF_CNT_HIGH2) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_CNT_HIGH3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_CNT_HIGH3(struct A6XX_VBIF_PERF_CNT_HIGH3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_CNT_HIGH3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_CNT_HIGH3(...) pack_A6XX_VBIF_PERF_CNT_HIGH3((struct A6XX_VBIF_PERF_CNT_HIGH3) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_EN0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_EN0(struct A6XX_VBIF_PERF_PWR_CNT_EN0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_EN0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_EN0(...) pack_A6XX_VBIF_PERF_PWR_CNT_EN0((struct A6XX_VBIF_PERF_PWR_CNT_EN0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_EN1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_EN1(struct A6XX_VBIF_PERF_PWR_CNT_EN1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_EN1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_EN1(...) pack_A6XX_VBIF_PERF_PWR_CNT_EN1((struct A6XX_VBIF_PERF_PWR_CNT_EN1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_EN2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_EN2(struct A6XX_VBIF_PERF_PWR_CNT_EN2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_EN2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_EN2(...) pack_A6XX_VBIF_PERF_PWR_CNT_EN2((struct A6XX_VBIF_PERF_PWR_CNT_EN2) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_LOW0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_LOW0(struct A6XX_VBIF_PERF_PWR_CNT_LOW0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_LOW0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_LOW0(...) pack_A6XX_VBIF_PERF_PWR_CNT_LOW0((struct A6XX_VBIF_PERF_PWR_CNT_LOW0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_LOW1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_LOW1(struct A6XX_VBIF_PERF_PWR_CNT_LOW1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_LOW1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_LOW1(...) pack_A6XX_VBIF_PERF_PWR_CNT_LOW1((struct A6XX_VBIF_PERF_PWR_CNT_LOW1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_LOW2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_LOW2(struct A6XX_VBIF_PERF_PWR_CNT_LOW2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_LOW2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_LOW2(...) pack_A6XX_VBIF_PERF_PWR_CNT_LOW2((struct A6XX_VBIF_PERF_PWR_CNT_LOW2) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_HIGH0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_HIGH0(struct A6XX_VBIF_PERF_PWR_CNT_HIGH0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_HIGH0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_HIGH0(...) pack_A6XX_VBIF_PERF_PWR_CNT_HIGH0((struct A6XX_VBIF_PERF_PWR_CNT_HIGH0) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_HIGH1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_HIGH1(struct A6XX_VBIF_PERF_PWR_CNT_HIGH1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_HIGH1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_HIGH1(...) pack_A6XX_VBIF_PERF_PWR_CNT_HIGH1((struct A6XX_VBIF_PERF_PWR_CNT_HIGH1) { __VA_ARGS__ })

struct A6XX_VBIF_PERF_PWR_CNT_HIGH2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VBIF_PERF_PWR_CNT_HIGH2(struct A6XX_VBIF_PERF_PWR_CNT_HIGH2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VBIF_PERF_PWR_CNT_HIGH2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VBIF_PERF_PWR_CNT_HIGH2(...) pack_A6XX_VBIF_PERF_PWR_CNT_HIGH2((struct A6XX_VBIF_PERF_PWR_CNT_HIGH2) { __VA_ARGS__ })

struct A6XX_GBIF_SCACHE_CNTL0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_SCACHE_CNTL0(struct A6XX_GBIF_SCACHE_CNTL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_SCACHE_CNTL0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_SCACHE_CNTL0(...) pack_A6XX_GBIF_SCACHE_CNTL0((struct A6XX_GBIF_SCACHE_CNTL0) { __VA_ARGS__ })

struct A6XX_GBIF_SCACHE_CNTL1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_SCACHE_CNTL1(struct A6XX_GBIF_SCACHE_CNTL1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_SCACHE_CNTL1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_SCACHE_CNTL1(...) pack_A6XX_GBIF_SCACHE_CNTL1((struct A6XX_GBIF_SCACHE_CNTL1) { __VA_ARGS__ })

struct A6XX_GBIF_QSB_SIDE0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_QSB_SIDE0(struct A6XX_GBIF_QSB_SIDE0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_QSB_SIDE0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_QSB_SIDE0(...) pack_A6XX_GBIF_QSB_SIDE0((struct A6XX_GBIF_QSB_SIDE0) { __VA_ARGS__ })

struct A6XX_GBIF_QSB_SIDE1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_QSB_SIDE1(struct A6XX_GBIF_QSB_SIDE1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_QSB_SIDE1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_QSB_SIDE1(...) pack_A6XX_GBIF_QSB_SIDE1((struct A6XX_GBIF_QSB_SIDE1) { __VA_ARGS__ })

struct A6XX_GBIF_QSB_SIDE2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_QSB_SIDE2(struct A6XX_GBIF_QSB_SIDE2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_QSB_SIDE2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_QSB_SIDE2(...) pack_A6XX_GBIF_QSB_SIDE2((struct A6XX_GBIF_QSB_SIDE2) { __VA_ARGS__ })

struct A6XX_GBIF_QSB_SIDE3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_QSB_SIDE3(struct A6XX_GBIF_QSB_SIDE3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_QSB_SIDE3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_QSB_SIDE3(...) pack_A6XX_GBIF_QSB_SIDE3((struct A6XX_GBIF_QSB_SIDE3) { __VA_ARGS__ })

struct A6XX_GBIF_HALT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_HALT(struct A6XX_GBIF_HALT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_HALT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_HALT(...) pack_A6XX_GBIF_HALT((struct A6XX_GBIF_HALT) { __VA_ARGS__ })

struct A6XX_GBIF_HALT_ACK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_HALT_ACK(struct A6XX_GBIF_HALT_ACK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_HALT_ACK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_HALT_ACK(...) pack_A6XX_GBIF_HALT_ACK((struct A6XX_GBIF_HALT_ACK) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_PWR_CNT_EN {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_PWR_CNT_EN(struct A6XX_GBIF_PERF_PWR_CNT_EN fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_PWR_CNT_EN,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_PWR_CNT_EN(...) pack_A6XX_GBIF_PERF_PWR_CNT_EN((struct A6XX_GBIF_PERF_PWR_CNT_EN) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_SEL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_SEL(struct A6XX_GBIF_PERF_CNT_SEL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_SEL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_SEL(...) pack_A6XX_GBIF_PERF_CNT_SEL((struct A6XX_GBIF_PERF_CNT_SEL) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_PWR_CNT_SEL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_PWR_CNT_SEL(struct A6XX_GBIF_PERF_PWR_CNT_SEL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_PWR_CNT_SEL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_PWR_CNT_SEL(...) pack_A6XX_GBIF_PERF_PWR_CNT_SEL((struct A6XX_GBIF_PERF_PWR_CNT_SEL) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_LOW0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_LOW0(struct A6XX_GBIF_PERF_CNT_LOW0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_LOW0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_LOW0(...) pack_A6XX_GBIF_PERF_CNT_LOW0((struct A6XX_GBIF_PERF_CNT_LOW0) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_LOW1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_LOW1(struct A6XX_GBIF_PERF_CNT_LOW1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_LOW1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_LOW1(...) pack_A6XX_GBIF_PERF_CNT_LOW1((struct A6XX_GBIF_PERF_CNT_LOW1) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_LOW2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_LOW2(struct A6XX_GBIF_PERF_CNT_LOW2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_LOW2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_LOW2(...) pack_A6XX_GBIF_PERF_CNT_LOW2((struct A6XX_GBIF_PERF_CNT_LOW2) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_LOW3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_LOW3(struct A6XX_GBIF_PERF_CNT_LOW3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_LOW3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_LOW3(...) pack_A6XX_GBIF_PERF_CNT_LOW3((struct A6XX_GBIF_PERF_CNT_LOW3) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_HIGH0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_HIGH0(struct A6XX_GBIF_PERF_CNT_HIGH0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_HIGH0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_HIGH0(...) pack_A6XX_GBIF_PERF_CNT_HIGH0((struct A6XX_GBIF_PERF_CNT_HIGH0) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_HIGH1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_HIGH1(struct A6XX_GBIF_PERF_CNT_HIGH1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_HIGH1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_HIGH1(...) pack_A6XX_GBIF_PERF_CNT_HIGH1((struct A6XX_GBIF_PERF_CNT_HIGH1) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_HIGH2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_HIGH2(struct A6XX_GBIF_PERF_CNT_HIGH2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_HIGH2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_HIGH2(...) pack_A6XX_GBIF_PERF_CNT_HIGH2((struct A6XX_GBIF_PERF_CNT_HIGH2) { __VA_ARGS__ })

struct A6XX_GBIF_PERF_CNT_HIGH3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PERF_CNT_HIGH3(struct A6XX_GBIF_PERF_CNT_HIGH3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PERF_CNT_HIGH3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PERF_CNT_HIGH3(...) pack_A6XX_GBIF_PERF_CNT_HIGH3((struct A6XX_GBIF_PERF_CNT_HIGH3) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_LOW0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_LOW0(struct A6XX_GBIF_PWR_CNT_LOW0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_LOW0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_LOW0(...) pack_A6XX_GBIF_PWR_CNT_LOW0((struct A6XX_GBIF_PWR_CNT_LOW0) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_LOW1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_LOW1(struct A6XX_GBIF_PWR_CNT_LOW1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_LOW1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_LOW1(...) pack_A6XX_GBIF_PWR_CNT_LOW1((struct A6XX_GBIF_PWR_CNT_LOW1) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_LOW2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_LOW2(struct A6XX_GBIF_PWR_CNT_LOW2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_LOW2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_LOW2(...) pack_A6XX_GBIF_PWR_CNT_LOW2((struct A6XX_GBIF_PWR_CNT_LOW2) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_HIGH0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_HIGH0(struct A6XX_GBIF_PWR_CNT_HIGH0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_HIGH0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_HIGH0(...) pack_A6XX_GBIF_PWR_CNT_HIGH0((struct A6XX_GBIF_PWR_CNT_HIGH0) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_HIGH1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_HIGH1(struct A6XX_GBIF_PWR_CNT_HIGH1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_HIGH1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_HIGH1(...) pack_A6XX_GBIF_PWR_CNT_HIGH1((struct A6XX_GBIF_PWR_CNT_HIGH1) { __VA_ARGS__ })

struct A6XX_GBIF_PWR_CNT_HIGH2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GBIF_PWR_CNT_HIGH2(struct A6XX_GBIF_PWR_CNT_HIGH2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GBIF_PWR_CNT_HIGH2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GBIF_PWR_CNT_HIGH2(...) pack_A6XX_GBIF_PWR_CNT_HIGH2((struct A6XX_GBIF_PWR_CNT_HIGH2) { __VA_ARGS__ })

struct A6XX_VSC_DBG_ECO_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DBG_ECO_CNTL(struct A6XX_VSC_DBG_ECO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DBG_ECO_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_DBG_ECO_CNTL(...) pack_A6XX_VSC_DBG_ECO_CNTL((struct A6XX_VSC_DBG_ECO_CNTL) { __VA_ARGS__ })

struct A6XX_VSC_BIN_SIZE {
    uint32_t							width;
    uint32_t							height;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_BIN_SIZE(struct A6XX_VSC_BIN_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.width >> 5)                      & 0xffffff00) == 0);
    assert(((fields.height >> 4)                     & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0x0001ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_BIN_SIZE,
        .value =
            ((fields.width >> 5)                      <<  0) |
            ((fields.height >> 4)                     <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_BIN_SIZE(...) pack_A6XX_VSC_BIN_SIZE((struct A6XX_VSC_BIN_SIZE) { __VA_ARGS__ })

struct A6XX_VSC_DRAW_STRM_SIZE_ADDRESS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DRAW_STRM_SIZE_ADDRESS(struct A6XX_VSC_DRAW_STRM_SIZE_ADDRESS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DRAW_STRM_SIZE_ADDRESS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VSC_DRAW_STRM_SIZE_ADDRESS(...) pack_A6XX_VSC_DRAW_STRM_SIZE_ADDRESS((struct A6XX_VSC_DRAW_STRM_SIZE_ADDRESS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VSC_BIN_COUNT {
    uint32_t							nx;
    uint32_t							ny;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_BIN_COUNT(struct A6XX_VSC_BIN_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.nx                                & 0xfffffc00) == 0);
    assert((fields.ny                                & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0x001ffffe) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_BIN_COUNT,
        .value =
            (fields.nx                                <<  1) |
            (fields.ny                                << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_BIN_COUNT(...) pack_A6XX_VSC_BIN_COUNT((struct A6XX_VSC_BIN_COUNT) { __VA_ARGS__ })

struct A6XX_VSC_PIPE_CONFIG_REG {
    uint32_t							x;
    uint32_t							y;
    uint32_t							w;
    uint32_t							h;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_PIPE_CONFIG_REG(uint32_t i, struct A6XX_VSC_PIPE_CONFIG_REG fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xfffffc00) == 0);
    assert((fields.y                                 & 0xfffffc00) == 0);
    assert((fields.w                                 & 0xffffffc0) == 0);
    assert((fields.h                                 & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_PIPE_CONFIG_REG(i),
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 10) |
            (fields.w                                 << 20) |
            (fields.h                                 << 26) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_PIPE_CONFIG_REG(i, ...) pack_A6XX_VSC_PIPE_CONFIG_REG(i, (struct A6XX_VSC_PIPE_CONFIG_REG) { __VA_ARGS__ })

struct A6XX_VSC_PRIM_STRM_ADDRESS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_PRIM_STRM_ADDRESS(struct A6XX_VSC_PRIM_STRM_ADDRESS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_PRIM_STRM_ADDRESS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VSC_PRIM_STRM_ADDRESS(...) pack_A6XX_VSC_PRIM_STRM_ADDRESS((struct A6XX_VSC_PRIM_STRM_ADDRESS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VSC_PRIM_STRM_PITCH {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_PRIM_STRM_PITCH(struct A6XX_VSC_PRIM_STRM_PITCH fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_PRIM_STRM_PITCH,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_PRIM_STRM_PITCH(...) pack_A6XX_VSC_PRIM_STRM_PITCH((struct A6XX_VSC_PRIM_STRM_PITCH) { __VA_ARGS__ })

struct A6XX_VSC_PRIM_STRM_LIMIT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_PRIM_STRM_LIMIT(struct A6XX_VSC_PRIM_STRM_LIMIT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_PRIM_STRM_LIMIT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_PRIM_STRM_LIMIT(...) pack_A6XX_VSC_PRIM_STRM_LIMIT((struct A6XX_VSC_PRIM_STRM_LIMIT) { __VA_ARGS__ })

struct A6XX_VSC_DRAW_STRM_ADDRESS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DRAW_STRM_ADDRESS(struct A6XX_VSC_DRAW_STRM_ADDRESS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DRAW_STRM_ADDRESS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VSC_DRAW_STRM_ADDRESS(...) pack_A6XX_VSC_DRAW_STRM_ADDRESS((struct A6XX_VSC_DRAW_STRM_ADDRESS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VSC_DRAW_STRM_PITCH {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DRAW_STRM_PITCH(struct A6XX_VSC_DRAW_STRM_PITCH fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DRAW_STRM_PITCH,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_DRAW_STRM_PITCH(...) pack_A6XX_VSC_DRAW_STRM_PITCH((struct A6XX_VSC_DRAW_STRM_PITCH) { __VA_ARGS__ })

struct A6XX_VSC_DRAW_STRM_LIMIT {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DRAW_STRM_LIMIT(struct A6XX_VSC_DRAW_STRM_LIMIT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DRAW_STRM_LIMIT,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_DRAW_STRM_LIMIT(...) pack_A6XX_VSC_DRAW_STRM_LIMIT((struct A6XX_VSC_DRAW_STRM_LIMIT) { __VA_ARGS__ })

struct A6XX_VSC_STATE_REG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_STATE_REG(uint32_t i, struct A6XX_VSC_STATE_REG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_STATE_REG(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_STATE_REG(i, ...) pack_A6XX_VSC_STATE_REG(i, (struct A6XX_VSC_STATE_REG) { __VA_ARGS__ })

struct A6XX_VSC_PRIM_STRM_SIZE_REG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_PRIM_STRM_SIZE_REG(uint32_t i, struct A6XX_VSC_PRIM_STRM_SIZE_REG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_PRIM_STRM_SIZE_REG(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_PRIM_STRM_SIZE_REG(i, ...) pack_A6XX_VSC_PRIM_STRM_SIZE_REG(i, (struct A6XX_VSC_PRIM_STRM_SIZE_REG) { __VA_ARGS__ })

struct A6XX_VSC_DRAW_STRM_SIZE_REG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VSC_DRAW_STRM_SIZE_REG(uint32_t i, struct A6XX_VSC_DRAW_STRM_SIZE_REG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VSC_DRAW_STRM_SIZE_REG(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VSC_DRAW_STRM_SIZE_REG(i, ...) pack_A6XX_VSC_DRAW_STRM_SIZE_REG(i, (struct A6XX_VSC_DRAW_STRM_SIZE_REG) { __VA_ARGS__ })

struct A6XX_UCHE_UNKNOWN_0E12 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UCHE_UNKNOWN_0E12(struct A6XX_UCHE_UNKNOWN_0E12 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UCHE_UNKNOWN_0E12,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UCHE_UNKNOWN_0E12(...) pack_A6XX_UCHE_UNKNOWN_0E12((struct A6XX_UCHE_UNKNOWN_0E12) { __VA_ARGS__ })

struct A6XX_GRAS_CL_CNTL {
    bool							clip_disable;
    bool							znear_clip_disable;
    bool							zfar_clip_disable;
    bool							unk5;
    bool							zero_gb_scale_z;
    bool							vp_clip_code_ignore;
    bool							vp_xform_disable;
    bool							persp_division_disable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_CNTL(struct A6XX_GRAS_CL_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x000003e7) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_CNTL,
        .value =
            (fields.clip_disable                      <<  0) |
            (fields.znear_clip_disable                <<  1) |
            (fields.zfar_clip_disable                 <<  2) |
            (fields.unk5                              <<  5) |
            (fields.zero_gb_scale_z                   <<  6) |
            (fields.vp_clip_code_ignore               <<  7) |
            (fields.vp_xform_disable                  <<  8) |
            (fields.persp_division_disable            <<  9) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_CNTL(...) pack_A6XX_GRAS_CL_CNTL((struct A6XX_GRAS_CL_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_VS_CL_CNTL {
    uint32_t							clip_mask;
    uint32_t							cull_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_VS_CL_CNTL(struct A6XX_GRAS_VS_CL_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.cull_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_VS_CL_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.cull_mask                         <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_VS_CL_CNTL(...) pack_A6XX_GRAS_VS_CL_CNTL((struct A6XX_GRAS_VS_CL_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_DS_CL_CNTL {
    uint32_t							clip_mask;
    uint32_t							cull_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_DS_CL_CNTL(struct A6XX_GRAS_DS_CL_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.cull_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_DS_CL_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.cull_mask                         <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_DS_CL_CNTL(...) pack_A6XX_GRAS_DS_CL_CNTL((struct A6XX_GRAS_DS_CL_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_GS_CL_CNTL {
    uint32_t							clip_mask;
    uint32_t							cull_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_GS_CL_CNTL(struct A6XX_GRAS_GS_CL_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.cull_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_GS_CL_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.cull_mask                         <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_GS_CL_CNTL(...) pack_A6XX_GRAS_GS_CL_CNTL((struct A6XX_GRAS_GS_CL_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_MAX_LAYER_INDEX {
    uint32_t							a6xx_gras_max_layer_index;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_MAX_LAYER_INDEX(struct A6XX_GRAS_MAX_LAYER_INDEX fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_max_layer_index         & 0xfffff800) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_MAX_LAYER_INDEX,
        .value =
            (fields.a6xx_gras_max_layer_index         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_MAX_LAYER_INDEX(...) pack_A6XX_GRAS_MAX_LAYER_INDEX((struct A6XX_GRAS_MAX_LAYER_INDEX) { __VA_ARGS__ })

struct A6XX_GRAS_CNTL {
    bool							ij_persp_pixel;
    bool							ij_persp_centroid;
    bool							ij_persp_sample;
    bool							ij_linear_pixel;
    bool							ij_linear_centroid;
    bool							ij_linear_sample;
    uint32_t							coord_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CNTL(struct A6XX_GRAS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.coord_mask                        & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x000003ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CNTL,
        .value =
            (fields.ij_persp_pixel                    <<  0) |
            (fields.ij_persp_centroid                 <<  1) |
            (fields.ij_persp_sample                   <<  2) |
            (fields.ij_linear_pixel                   <<  3) |
            (fields.ij_linear_centroid                <<  4) |
            (fields.ij_linear_sample                  <<  5) |
            (fields.coord_mask                        <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CNTL(...) pack_A6XX_GRAS_CNTL((struct A6XX_GRAS_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ {
    uint32_t							horz;
    uint32_t							vert;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ(struct A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ fields)
{
#ifndef NDEBUG
    assert((fields.horz                              & 0xfffffe00) == 0);
    assert((fields.vert                              & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0x0007fdff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ,
        .value =
            (fields.horz                              <<  0) |
            (fields.vert                              << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ(...) pack_A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ((struct A6XX_GRAS_CL_GUARDBAND_CLIP_ADJ) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_XOFFSET {
    float							a6xx_gras_cl_vport_xoffset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_XOFFSET(uint32_t i, struct A6XX_GRAS_CL_VPORT_XOFFSET fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_xoffset)   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_XOFFSET(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_xoffset)   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_XOFFSET(i, ...) pack_A6XX_GRAS_CL_VPORT_XOFFSET(i, (struct A6XX_GRAS_CL_VPORT_XOFFSET) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_XSCALE {
    float							a6xx_gras_cl_vport_xscale;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_XSCALE(uint32_t i, struct A6XX_GRAS_CL_VPORT_XSCALE fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_xscale)    & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_XSCALE(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_xscale)    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_XSCALE(i, ...) pack_A6XX_GRAS_CL_VPORT_XSCALE(i, (struct A6XX_GRAS_CL_VPORT_XSCALE) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_YOFFSET {
    float							a6xx_gras_cl_vport_yoffset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_YOFFSET(uint32_t i, struct A6XX_GRAS_CL_VPORT_YOFFSET fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_yoffset)   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_YOFFSET(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_yoffset)   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_YOFFSET(i, ...) pack_A6XX_GRAS_CL_VPORT_YOFFSET(i, (struct A6XX_GRAS_CL_VPORT_YOFFSET) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_YSCALE {
    float							a6xx_gras_cl_vport_yscale;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_YSCALE(uint32_t i, struct A6XX_GRAS_CL_VPORT_YSCALE fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_yscale)    & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_YSCALE(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_yscale)    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_YSCALE(i, ...) pack_A6XX_GRAS_CL_VPORT_YSCALE(i, (struct A6XX_GRAS_CL_VPORT_YSCALE) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_ZOFFSET {
    float							a6xx_gras_cl_vport_zoffset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_ZOFFSET(uint32_t i, struct A6XX_GRAS_CL_VPORT_ZOFFSET fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_zoffset)   & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_ZOFFSET(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_zoffset)   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_ZOFFSET(i, ...) pack_A6XX_GRAS_CL_VPORT_ZOFFSET(i, (struct A6XX_GRAS_CL_VPORT_ZOFFSET) { __VA_ARGS__ })

struct A6XX_GRAS_CL_VPORT_ZSCALE {
    float							a6xx_gras_cl_vport_zscale;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_VPORT_ZSCALE(uint32_t i, struct A6XX_GRAS_CL_VPORT_ZSCALE fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_vport_zscale)    & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_VPORT_ZSCALE(i),
        .value =
            (fui(fields.a6xx_gras_cl_vport_zscale)    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_VPORT_ZSCALE(i, ...) pack_A6XX_GRAS_CL_VPORT_ZSCALE(i, (struct A6XX_GRAS_CL_VPORT_ZSCALE) { __VA_ARGS__ })

struct A6XX_GRAS_CL_Z_CLAMP_MIN {
    float							a6xx_gras_cl_z_clamp_min;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_Z_CLAMP_MIN(uint32_t i, struct A6XX_GRAS_CL_Z_CLAMP_MIN fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_z_clamp_min)     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_Z_CLAMP_MIN(i),
        .value =
            (fui(fields.a6xx_gras_cl_z_clamp_min)     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_Z_CLAMP_MIN(i, ...) pack_A6XX_GRAS_CL_Z_CLAMP_MIN(i, (struct A6XX_GRAS_CL_Z_CLAMP_MIN) { __VA_ARGS__ })

struct A6XX_GRAS_CL_Z_CLAMP_MAX {
    float							a6xx_gras_cl_z_clamp_max;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_CL_Z_CLAMP_MAX(uint32_t i, struct A6XX_GRAS_CL_Z_CLAMP_MAX fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_cl_z_clamp_max)     & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_CL_Z_CLAMP_MAX(i),
        .value =
            (fui(fields.a6xx_gras_cl_z_clamp_max)     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_CL_Z_CLAMP_MAX(i, ...) pack_A6XX_GRAS_CL_Z_CLAMP_MAX(i, (struct A6XX_GRAS_CL_Z_CLAMP_MAX) { __VA_ARGS__ })

struct A6XX_GRAS_SU_CNTL {
    bool							cull_front;
    bool							cull_back;
    bool							front_cw;
    float							linehalfwidth;
    bool							poly_offset;
    uint32_t							unk12;
    enum a5xx_line_mode						line_mode;
    uint32_t							unk15;
    bool							unk17;
    bool							multiview_enable;
    uint32_t							unk19;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_CNTL(struct A6XX_GRAS_SU_CNTL fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.linehalfwidth * 4.0))  & 0xffffff00) == 0);
    assert((fields.unk12                             & 0xfffffffe) == 0);
    assert((fields.line_mode                         & 0xfffffffe) == 0);
    assert((fields.unk15                             & 0xfffffffc) == 0);
    assert((fields.unk19                             & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x007fbfff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_CNTL,
        .value =
            (fields.cull_front                        <<  0) |
            (fields.cull_back                         <<  1) |
            (fields.front_cw                          <<  2) |
            (((int32_t)(fields.linehalfwidth * 4.0))  <<  3) |
            (fields.poly_offset                       << 11) |
            (fields.unk12                             << 12) |
            (fields.line_mode                         << 13) |
            (fields.unk15                             << 15) |
            (fields.unk17                             << 17) |
            (fields.multiview_enable                  << 18) |
            (fields.unk19                             << 19) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_CNTL(...) pack_A6XX_GRAS_SU_CNTL((struct A6XX_GRAS_SU_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_SU_POINT_MINMAX {
    float							min;
    float							max;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_POINT_MINMAX(struct A6XX_GRAS_SU_POINT_MINMAX fields)
{
#ifndef NDEBUG
    assert((((uint32_t)(fields.min * 16.0))          & 0xffff0000) == 0);
    assert((((uint32_t)(fields.max * 16.0))          & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_POINT_MINMAX,
        .value =
            (((uint32_t)(fields.min * 16.0))          <<  0) |
            (((uint32_t)(fields.max * 16.0))          << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_POINT_MINMAX(...) pack_A6XX_GRAS_SU_POINT_MINMAX((struct A6XX_GRAS_SU_POINT_MINMAX) { __VA_ARGS__ })

struct A6XX_GRAS_SU_POINT_SIZE {
    float							a6xx_gras_su_point_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_POINT_SIZE(struct A6XX_GRAS_SU_POINT_SIZE fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.a6xx_gras_su_point_size * 16.0)) & 0xffff0000) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_POINT_SIZE,
        .value =
            (((int32_t)(fields.a6xx_gras_su_point_size * 16.0)) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_POINT_SIZE(...) pack_A6XX_GRAS_SU_POINT_SIZE((struct A6XX_GRAS_SU_POINT_SIZE) { __VA_ARGS__ })

struct A6XX_GRAS_SU_DEPTH_PLANE_CNTL {
    enum a6xx_ztest_mode					z_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_DEPTH_PLANE_CNTL(struct A6XX_GRAS_SU_DEPTH_PLANE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.z_mode                            & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_DEPTH_PLANE_CNTL,
        .value =
            (fields.z_mode                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_DEPTH_PLANE_CNTL(...) pack_A6XX_GRAS_SU_DEPTH_PLANE_CNTL((struct A6XX_GRAS_SU_DEPTH_PLANE_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_SU_POLY_OFFSET_SCALE {
    float							a6xx_gras_su_poly_offset_scale;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_POLY_OFFSET_SCALE(struct A6XX_GRAS_SU_POLY_OFFSET_SCALE fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_su_poly_offset_scale) & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_POLY_OFFSET_SCALE,
        .value =
            (fui(fields.a6xx_gras_su_poly_offset_scale) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_POLY_OFFSET_SCALE(...) pack_A6XX_GRAS_SU_POLY_OFFSET_SCALE((struct A6XX_GRAS_SU_POLY_OFFSET_SCALE) { __VA_ARGS__ })

struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET {
    float							a6xx_gras_su_poly_offset_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_POLY_OFFSET_OFFSET(struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_su_poly_offset_offset) & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_POLY_OFFSET_OFFSET,
        .value =
            (fui(fields.a6xx_gras_su_poly_offset_offset) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_POLY_OFFSET_OFFSET(...) pack_A6XX_GRAS_SU_POLY_OFFSET_OFFSET((struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET) { __VA_ARGS__ })

struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP {
    float							a6xx_gras_su_poly_offset_offset_clamp;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP(struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_gras_su_poly_offset_offset_clamp) & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP,
        .value =
            (fui(fields.a6xx_gras_su_poly_offset_offset_clamp) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP(...) pack_A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP((struct A6XX_GRAS_SU_POLY_OFFSET_OFFSET_CLAMP) { __VA_ARGS__ })

struct A6XX_GRAS_SU_DEPTH_BUFFER_INFO {
    enum a6xx_depth_format					depth_format;
    uint32_t							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_DEPTH_BUFFER_INFO(struct A6XX_GRAS_SU_DEPTH_BUFFER_INFO fields)
{
#ifndef NDEBUG
    assert((fields.depth_format                      & 0xfffffff8) == 0);
    assert((fields.unk3                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_DEPTH_BUFFER_INFO,
        .value =
            (fields.depth_format                      <<  0) |
            (fields.unk3                              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_DEPTH_BUFFER_INFO(...) pack_A6XX_GRAS_SU_DEPTH_BUFFER_INFO((struct A6XX_GRAS_SU_DEPTH_BUFFER_INFO) { __VA_ARGS__ })

struct A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL {
    bool							conservativerasen;
    uint32_t							shiftamount;
    bool							innerconservativerasen;
    uint32_t							unk4;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL(struct A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.shiftamount                       & 0xfffffffc) == 0);
    assert((fields.unk4                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000003f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL,
        .value =
            (fields.conservativerasen                 <<  0) |
            (fields.shiftamount                       <<  1) |
            (fields.innerconservativerasen            <<  3) |
            (fields.unk4                              <<  4) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL(...) pack_A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL((struct A6XX_GRAS_SU_CONSERVATIVE_RAS_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_SU_PATH_RENDERING_CNTL {
    bool							unk0;
    bool							linelengthen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SU_PATH_RENDERING_CNTL(struct A6XX_GRAS_SU_PATH_RENDERING_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SU_PATH_RENDERING_CNTL,
        .value =
            (fields.unk0                              <<  0) |
            (fields.linelengthen                      <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SU_PATH_RENDERING_CNTL(...) pack_A6XX_GRAS_SU_PATH_RENDERING_CNTL((struct A6XX_GRAS_SU_PATH_RENDERING_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_VS_LAYER_CNTL {
    bool							writes_layer;
    bool							writes_view;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_VS_LAYER_CNTL(struct A6XX_GRAS_VS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_VS_LAYER_CNTL,
        .value =
            (fields.writes_layer                      <<  0) |
            (fields.writes_view                       <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_VS_LAYER_CNTL(...) pack_A6XX_GRAS_VS_LAYER_CNTL((struct A6XX_GRAS_VS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_GS_LAYER_CNTL {
    bool							writes_layer;
    bool							writes_view;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_GS_LAYER_CNTL(struct A6XX_GRAS_GS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_GS_LAYER_CNTL,
        .value =
            (fields.writes_layer                      <<  0) |
            (fields.writes_view                       <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_GS_LAYER_CNTL(...) pack_A6XX_GRAS_GS_LAYER_CNTL((struct A6XX_GRAS_GS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_DS_LAYER_CNTL {
    bool							writes_layer;
    bool							writes_view;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_DS_LAYER_CNTL(struct A6XX_GRAS_DS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_DS_LAYER_CNTL,
        .value =
            (fields.writes_layer                      <<  0) |
            (fields.writes_view                       <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_DS_LAYER_CNTL(...) pack_A6XX_GRAS_DS_LAYER_CNTL((struct A6XX_GRAS_DS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_SC_CNTL {
    uint32_t							ccusinglecachelinesize;
    enum a6xx_single_prim_mode					single_prim_mode;
    enum a6xx_raster_mode					raster_mode;
    enum a6xx_raster_direction					raster_direction;
    enum a6xx_sequenced_thread_dist				sequenced_thread_distribution;
    uint32_t							unk9;
    bool							earlyvizouten;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_CNTL(struct A6XX_GRAS_SC_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.ccusinglecachelinesize            & 0xfffffff8) == 0);
    assert((fields.single_prim_mode                  & 0xfffffffc) == 0);
    assert((fields.raster_mode                       & 0xfffffffe) == 0);
    assert((fields.raster_direction                  & 0xfffffffc) == 0);
    assert((fields.sequenced_thread_distribution     & 0xfffffffe) == 0);
    assert((fields.unk9                              & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00001fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_CNTL,
        .value =
            (fields.ccusinglecachelinesize            <<  0) |
            (fields.single_prim_mode                  <<  3) |
            (fields.raster_mode                       <<  5) |
            (fields.raster_direction                  <<  6) |
            (fields.sequenced_thread_distribution     <<  8) |
            (fields.unk9                              <<  9) |
            (fields.earlyvizouten                     << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_CNTL(...) pack_A6XX_GRAS_SC_CNTL((struct A6XX_GRAS_SC_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_BIN_CONTROL {
    uint32_t							binw;
    uint32_t							binh;
    enum a6xx_render_mode					render_mode;
    bool							force_lrz_write_dis;
    enum a6xx_buffers_location					buffers_location;
    uint32_t							lrz_feedback_zmode_mask;
    uint32_t							unk27;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_BIN_CONTROL(struct A6XX_GRAS_BIN_CONTROL fields)
{
#ifndef NDEBUG
    assert(((fields.binw >> 5)                       & 0xffffffc0) == 0);
    assert(((fields.binh >> 4)                       & 0xffffff80) == 0);
    assert((fields.render_mode                       & 0xfffffff8) == 0);
    assert((fields.buffers_location                  & 0xfffffffc) == 0);
    assert((fields.lrz_feedback_zmode_mask           & 0xfffffff8) == 0);
    assert((fields.unk27                             & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x0ffc7f3f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_BIN_CONTROL,
        .value =
            ((fields.binw >> 5)                       <<  0) |
            ((fields.binh >> 4)                       <<  8) |
            (fields.render_mode                       << 18) |
            (fields.force_lrz_write_dis               << 21) |
            (fields.buffers_location                  << 22) |
            (fields.lrz_feedback_zmode_mask           << 24) |
            (fields.unk27                             << 27) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_BIN_CONTROL(...) pack_A6XX_GRAS_BIN_CONTROL((struct A6XX_GRAS_BIN_CONTROL) { __VA_ARGS__ })

struct A6XX_GRAS_RAS_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    uint32_t							unk2;
    uint32_t							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_RAS_MSAA_CNTL(struct A6XX_GRAS_RAS_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unk2                              & 0xfffffffe) == 0);
    assert((fields.unk3                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_RAS_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.unk2                              <<  2) |
            (fields.unk3                              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_RAS_MSAA_CNTL(...) pack_A6XX_GRAS_RAS_MSAA_CNTL((struct A6XX_GRAS_RAS_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_DEST_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    bool							msaa_disable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_DEST_MSAA_CNTL(struct A6XX_GRAS_DEST_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_DEST_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.msaa_disable                      <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_DEST_MSAA_CNTL(...) pack_A6XX_GRAS_DEST_MSAA_CNTL((struct A6XX_GRAS_DEST_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_SAMPLE_CONFIG {
    uint32_t							unk0;
    bool							location_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SAMPLE_CONFIG(struct A6XX_GRAS_SAMPLE_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SAMPLE_CONFIG,
        .value =
            (fields.unk0                              <<  0) |
            (fields.location_enable                   <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SAMPLE_CONFIG(...) pack_A6XX_GRAS_SAMPLE_CONFIG((struct A6XX_GRAS_SAMPLE_CONFIG) { __VA_ARGS__ })

struct A6XX_GRAS_SAMPLE_LOCATION_0 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SAMPLE_LOCATION_0(struct A6XX_GRAS_SAMPLE_LOCATION_0 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SAMPLE_LOCATION_0,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SAMPLE_LOCATION_0(...) pack_A6XX_GRAS_SAMPLE_LOCATION_0((struct A6XX_GRAS_SAMPLE_LOCATION_0) { __VA_ARGS__ })

struct A6XX_GRAS_SAMPLE_LOCATION_1 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SAMPLE_LOCATION_1(struct A6XX_GRAS_SAMPLE_LOCATION_1 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SAMPLE_LOCATION_1,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SAMPLE_LOCATION_1(...) pack_A6XX_GRAS_SAMPLE_LOCATION_1((struct A6XX_GRAS_SAMPLE_LOCATION_1) { __VA_ARGS__ })

struct A6XX_GRAS_UNKNOWN_80AF {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_UNKNOWN_80AF(struct A6XX_GRAS_UNKNOWN_80AF fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_UNKNOWN_80AF,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_UNKNOWN_80AF(...) pack_A6XX_GRAS_UNKNOWN_80AF((struct A6XX_GRAS_UNKNOWN_80AF) { __VA_ARGS__ })

struct A6XX_GRAS_SC_SCREEN_SCISSOR_TL {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_SCREEN_SCISSOR_TL(uint32_t i, struct A6XX_GRAS_SC_SCREEN_SCISSOR_TL fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffff0000) == 0);
    assert((fields.y                                 & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_SCREEN_SCISSOR_TL(i),
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_SCREEN_SCISSOR_TL(i, ...) pack_A6XX_GRAS_SC_SCREEN_SCISSOR_TL(i, (struct A6XX_GRAS_SC_SCREEN_SCISSOR_TL) { __VA_ARGS__ })

struct A6XX_GRAS_SC_SCREEN_SCISSOR_BR {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_SCREEN_SCISSOR_BR(uint32_t i, struct A6XX_GRAS_SC_SCREEN_SCISSOR_BR fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffff0000) == 0);
    assert((fields.y                                 & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_SCREEN_SCISSOR_BR(i),
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_SCREEN_SCISSOR_BR(i, ...) pack_A6XX_GRAS_SC_SCREEN_SCISSOR_BR(i, (struct A6XX_GRAS_SC_SCREEN_SCISSOR_BR) { __VA_ARGS__ })

struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL(uint32_t i, struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffff0000) == 0);
    assert((fields.y                                 & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL(i),
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL(i, ...) pack_A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL(i, (struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_TL) { __VA_ARGS__ })

struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR(uint32_t i, struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffff0000) == 0);
    assert((fields.y                                 & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR(i),
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR(i, ...) pack_A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR(i, (struct A6XX_GRAS_SC_VIEWPORT_SCISSOR_BR) { __VA_ARGS__ })

struct A6XX_GRAS_SC_WINDOW_SCISSOR_TL {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_WINDOW_SCISSOR_TL(struct A6XX_GRAS_SC_WINDOW_SCISSOR_TL fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_WINDOW_SCISSOR_TL,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_WINDOW_SCISSOR_TL(...) pack_A6XX_GRAS_SC_WINDOW_SCISSOR_TL((struct A6XX_GRAS_SC_WINDOW_SCISSOR_TL) { __VA_ARGS__ })

struct A6XX_GRAS_SC_WINDOW_SCISSOR_BR {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SC_WINDOW_SCISSOR_BR(struct A6XX_GRAS_SC_WINDOW_SCISSOR_BR fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SC_WINDOW_SCISSOR_BR,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SC_WINDOW_SCISSOR_BR(...) pack_A6XX_GRAS_SC_WINDOW_SCISSOR_BR((struct A6XX_GRAS_SC_WINDOW_SCISSOR_BR) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_CNTL {
    bool							enable;
    bool							lrz_write;
    bool							greater;
    bool							fc_enable;
    bool							z_test_enable;
    bool							z_bounds_enable;
    enum a6xx_lrz_dir_status					dir;
    bool							dir_write;
    bool							disable_on_wrong_dir;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_CNTL(struct A6XX_GRAS_LRZ_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.dir                               & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x000003ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_CNTL,
        .value =
            (fields.enable                            <<  0) |
            (fields.lrz_write                         <<  1) |
            (fields.greater                           <<  2) |
            (fields.fc_enable                         <<  3) |
            (fields.z_test_enable                     <<  4) |
            (fields.z_bounds_enable                   <<  5) |
            (fields.dir                               <<  6) |
            (fields.dir_write                         <<  8) |
            (fields.disable_on_wrong_dir              <<  9) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_LRZ_CNTL(...) pack_A6XX_GRAS_LRZ_CNTL((struct A6XX_GRAS_LRZ_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_PS_INPUT_CNTL {
    bool							sampleid;
    enum a6xx_fragcoord_sample_mode				fragcoordsamplemode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_PS_INPUT_CNTL(struct A6XX_GRAS_LRZ_PS_INPUT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.fragcoordsamplemode               & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_PS_INPUT_CNTL,
        .value =
            (fields.sampleid                          <<  0) |
            (fields.fragcoordsamplemode               <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_LRZ_PS_INPUT_CNTL(...) pack_A6XX_GRAS_LRZ_PS_INPUT_CNTL((struct A6XX_GRAS_LRZ_PS_INPUT_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_MRT_BUF_INFO_0 {
    enum a6xx_format						color_format;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_MRT_BUF_INFO_0(struct A6XX_GRAS_LRZ_MRT_BUF_INFO_0 fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_MRT_BUF_INFO_0,
        .value =
            (fields.color_format                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_LRZ_MRT_BUF_INFO_0(...) pack_A6XX_GRAS_LRZ_MRT_BUF_INFO_0((struct A6XX_GRAS_LRZ_MRT_BUF_INFO_0) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_BUFFER_BASE(struct A6XX_GRAS_LRZ_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_GRAS_LRZ_BUFFER_BASE(...) pack_A6XX_GRAS_LRZ_BUFFER_BASE((struct A6XX_GRAS_LRZ_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_GRAS_LRZ_BUFFER_PITCH {
    uint32_t							pitch;
    uint32_t							array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_BUFFER_PITCH(struct A6XX_GRAS_LRZ_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.pitch >> 5)                      & 0xffffff00) == 0);
    assert(((fields.array_pitch >> 4)                & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x1ffffcff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_BUFFER_PITCH,
        .value =
            ((fields.pitch >> 5)                      <<  0) |
            ((fields.array_pitch >> 4)                << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_LRZ_BUFFER_PITCH(...) pack_A6XX_GRAS_LRZ_BUFFER_PITCH((struct A6XX_GRAS_LRZ_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE(struct A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE(...) pack_A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE((struct A6XX_GRAS_LRZ_FAST_CLEAR_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_GRAS_SAMPLE_CNTL {
    bool							per_samp_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_SAMPLE_CNTL(struct A6XX_GRAS_SAMPLE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_SAMPLE_CNTL,
        .value =
            (fields.per_samp_mode                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_SAMPLE_CNTL(...) pack_A6XX_GRAS_SAMPLE_CNTL((struct A6XX_GRAS_SAMPLE_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_LRZ_DEPTH_VIEW {
    uint32_t							base_layer;
    uint32_t							layer_count;
    uint32_t							base_mip_level;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_LRZ_DEPTH_VIEW(struct A6XX_GRAS_LRZ_DEPTH_VIEW fields)
{
#ifndef NDEBUG
    assert((fields.base_layer                        & 0xfffff800) == 0);
    assert((fields.layer_count                       & 0xfffff800) == 0);
    assert((fields.base_mip_level                    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xf7ff07ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_LRZ_DEPTH_VIEW,
        .value =
            (fields.base_layer                        <<  0) |
            (fields.layer_count                       << 16) |
            (fields.base_mip_level                    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_LRZ_DEPTH_VIEW(...) pack_A6XX_GRAS_LRZ_DEPTH_VIEW((struct A6XX_GRAS_LRZ_DEPTH_VIEW) { __VA_ARGS__ })

struct A6XX_GRAS_UNKNOWN_8110 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_UNKNOWN_8110(struct A6XX_GRAS_UNKNOWN_8110 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_UNKNOWN_8110,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_UNKNOWN_8110(...) pack_A6XX_GRAS_UNKNOWN_8110((struct A6XX_GRAS_UNKNOWN_8110) { __VA_ARGS__ })

struct A6XX_GRAS_2D_BLIT_CNTL {
    enum a6xx_rotation						rotate;
    bool							overwriteen;
    uint32_t							unk4;
    bool							solid_color;
    enum a6xx_format						color_format;
    bool							scissor;
    uint32_t							unk17;
    bool							d24s8;
    uint32_t							mask;
    enum a6xx_2d_ifmt						ifmt;
    enum a6xx_raster_mode					raster_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_BLIT_CNTL(struct A6XX_GRAS_2D_BLIT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.rotate                            & 0xfffffff8) == 0);
    assert((fields.unk4                              & 0xfffffff8) == 0);
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.unk17                             & 0xfffffffc) == 0);
    assert((fields.mask                              & 0xfffffff0) == 0);
    assert((fields.ifmt                              & 0xffffffe0) == 0);
    assert((fields.raster_mode                       & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x3fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_BLIT_CNTL,
        .value =
            (fields.rotate                            <<  0) |
            (fields.overwriteen                       <<  3) |
            (fields.unk4                              <<  4) |
            (fields.solid_color                       <<  7) |
            (fields.color_format                      <<  8) |
            (fields.scissor                           << 16) |
            (fields.unk17                             << 17) |
            (fields.d24s8                             << 19) |
            (fields.mask                              << 20) |
            (fields.ifmt                              << 24) |
            (fields.raster_mode                       << 29) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_BLIT_CNTL(...) pack_A6XX_GRAS_2D_BLIT_CNTL((struct A6XX_GRAS_2D_BLIT_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_2D_SRC_TL_X {
    int32_t							a6xx_gras_2d_src_tl_x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_SRC_TL_X(struct A6XX_GRAS_2D_SRC_TL_X fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_2d_src_tl_x             & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x01ffff00) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_SRC_TL_X,
        .value =
            (fields.a6xx_gras_2d_src_tl_x             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_SRC_TL_X(...) pack_A6XX_GRAS_2D_SRC_TL_X((struct A6XX_GRAS_2D_SRC_TL_X) { __VA_ARGS__ })

struct A6XX_GRAS_2D_SRC_BR_X {
    int32_t							a6xx_gras_2d_src_br_x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_SRC_BR_X(struct A6XX_GRAS_2D_SRC_BR_X fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_2d_src_br_x             & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x01ffff00) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_SRC_BR_X,
        .value =
            (fields.a6xx_gras_2d_src_br_x             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_SRC_BR_X(...) pack_A6XX_GRAS_2D_SRC_BR_X((struct A6XX_GRAS_2D_SRC_BR_X) { __VA_ARGS__ })

struct A6XX_GRAS_2D_SRC_TL_Y {
    int32_t							a6xx_gras_2d_src_tl_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_SRC_TL_Y(struct A6XX_GRAS_2D_SRC_TL_Y fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_2d_src_tl_y             & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x01ffff00) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_SRC_TL_Y,
        .value =
            (fields.a6xx_gras_2d_src_tl_y             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_SRC_TL_Y(...) pack_A6XX_GRAS_2D_SRC_TL_Y((struct A6XX_GRAS_2D_SRC_TL_Y) { __VA_ARGS__ })

struct A6XX_GRAS_2D_SRC_BR_Y {
    int32_t							a6xx_gras_2d_src_br_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_SRC_BR_Y(struct A6XX_GRAS_2D_SRC_BR_Y fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_2d_src_br_y             & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x01ffff00) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_SRC_BR_Y,
        .value =
            (fields.a6xx_gras_2d_src_br_y             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_SRC_BR_Y(...) pack_A6XX_GRAS_2D_SRC_BR_Y((struct A6XX_GRAS_2D_SRC_BR_Y) { __VA_ARGS__ })

struct A6XX_GRAS_2D_DST_TL {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_DST_TL(struct A6XX_GRAS_2D_DST_TL fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_DST_TL,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_DST_TL(...) pack_A6XX_GRAS_2D_DST_TL((struct A6XX_GRAS_2D_DST_TL) { __VA_ARGS__ })

struct A6XX_GRAS_2D_DST_BR {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_DST_BR(struct A6XX_GRAS_2D_DST_BR fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_DST_BR,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_DST_BR(...) pack_A6XX_GRAS_2D_DST_BR((struct A6XX_GRAS_2D_DST_BR) { __VA_ARGS__ })

struct A6XX_GRAS_2D_UNKNOWN_8407 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_UNKNOWN_8407(struct A6XX_GRAS_2D_UNKNOWN_8407 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_UNKNOWN_8407,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_UNKNOWN_8407(...) pack_A6XX_GRAS_2D_UNKNOWN_8407((struct A6XX_GRAS_2D_UNKNOWN_8407) { __VA_ARGS__ })

struct A6XX_GRAS_2D_UNKNOWN_8408 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_UNKNOWN_8408(struct A6XX_GRAS_2D_UNKNOWN_8408 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_UNKNOWN_8408,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_UNKNOWN_8408(...) pack_A6XX_GRAS_2D_UNKNOWN_8408((struct A6XX_GRAS_2D_UNKNOWN_8408) { __VA_ARGS__ })

struct A6XX_GRAS_2D_UNKNOWN_8409 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_UNKNOWN_8409(struct A6XX_GRAS_2D_UNKNOWN_8409 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_UNKNOWN_8409,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_UNKNOWN_8409(...) pack_A6XX_GRAS_2D_UNKNOWN_8409((struct A6XX_GRAS_2D_UNKNOWN_8409) { __VA_ARGS__ })

struct A6XX_GRAS_2D_RESOLVE_CNTL_1 {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_RESOLVE_CNTL_1(struct A6XX_GRAS_2D_RESOLVE_CNTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_RESOLVE_CNTL_1,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_RESOLVE_CNTL_1(...) pack_A6XX_GRAS_2D_RESOLVE_CNTL_1((struct A6XX_GRAS_2D_RESOLVE_CNTL_1) { __VA_ARGS__ })

struct A6XX_GRAS_2D_RESOLVE_CNTL_2 {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_2D_RESOLVE_CNTL_2(struct A6XX_GRAS_2D_RESOLVE_CNTL_2 fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_2D_RESOLVE_CNTL_2,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_2D_RESOLVE_CNTL_2(...) pack_A6XX_GRAS_2D_RESOLVE_CNTL_2((struct A6XX_GRAS_2D_RESOLVE_CNTL_2) { __VA_ARGS__ })

struct A6XX_GRAS_DBG_ECO_CNTL {
    bool							unk7;
    bool							lrzcachelockdis;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_DBG_ECO_CNTL(struct A6XX_GRAS_DBG_ECO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000880) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_DBG_ECO_CNTL,
        .value =
            (fields.unk7                              <<  7) |
            (fields.lrzcachelockdis                   << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_DBG_ECO_CNTL(...) pack_A6XX_GRAS_DBG_ECO_CNTL((struct A6XX_GRAS_DBG_ECO_CNTL) { __VA_ARGS__ })

struct A6XX_GRAS_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_gras_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_GRAS_ADDR_MODE_CNTL(struct A6XX_GRAS_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_gras_addr_mode_cntl          & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_GRAS_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_gras_addr_mode_cntl          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_GRAS_ADDR_MODE_CNTL(...) pack_A6XX_GRAS_ADDR_MODE_CNTL((struct A6XX_GRAS_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_RB_BIN_CONTROL {
    uint32_t							binw;
    uint32_t							binh;
    enum a6xx_render_mode					render_mode;
    bool							force_lrz_write_dis;
    enum a6xx_buffers_location					buffers_location;
    uint32_t							lrz_feedback_zmode_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BIN_CONTROL(struct A6XX_RB_BIN_CONTROL fields)
{
#ifndef NDEBUG
    assert(((fields.binw >> 5)                       & 0xffffffc0) == 0);
    assert(((fields.binh >> 4)                       & 0xffffff80) == 0);
    assert((fields.render_mode                       & 0xfffffff8) == 0);
    assert((fields.buffers_location                  & 0xfffffffc) == 0);
    assert((fields.lrz_feedback_zmode_mask           & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x07fc7f3f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BIN_CONTROL,
        .value =
            ((fields.binw >> 5)                       <<  0) |
            ((fields.binh >> 4)                       <<  8) |
            (fields.render_mode                       << 18) |
            (fields.force_lrz_write_dis               << 21) |
            (fields.buffers_location                  << 22) |
            (fields.lrz_feedback_zmode_mask           << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BIN_CONTROL(...) pack_A6XX_RB_BIN_CONTROL((struct A6XX_RB_BIN_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_RENDER_CNTL {
    uint32_t							ccusinglecachelinesize;
    bool							earlyvizouten;
    bool							binning;
    uint32_t							unk8;
    enum a6xx_raster_mode					raster_mode;
    enum a6xx_raster_direction					raster_direction;
    bool							conservativerasen;
    bool							innerconservativerasen;
    bool							flag_depth;
    uint32_t							flag_mrts;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RENDER_CNTL(struct A6XX_RB_RENDER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.ccusinglecachelinesize            & 0xfffffff8) == 0);
    assert((fields.unk8                              & 0xfffffff8) == 0);
    assert((fields.raster_mode                       & 0xfffffffe) == 0);
    assert((fields.raster_direction                  & 0xfffffffc) == 0);
    assert((fields.flag_mrts                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ff5ff8) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RENDER_CNTL,
        .value =
            (fields.ccusinglecachelinesize            <<  3) |
            (fields.earlyvizouten                     <<  6) |
            (fields.binning                           <<  7) |
            (fields.unk8                              <<  8) |
            (fields.raster_mode                       <<  8) |
            (fields.raster_direction                  <<  9) |
            (fields.conservativerasen                 << 11) |
            (fields.innerconservativerasen            << 12) |
            (fields.flag_depth                        << 14) |
            (fields.flag_mrts                         << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RENDER_CNTL(...) pack_A6XX_RB_RENDER_CNTL((struct A6XX_RB_RENDER_CNTL) { __VA_ARGS__ })

struct A6XX_RB_RAS_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    uint32_t							unk2;
    uint32_t							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RAS_MSAA_CNTL(struct A6XX_RB_RAS_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unk2                              & 0xfffffffe) == 0);
    assert((fields.unk3                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RAS_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.unk2                              <<  2) |
            (fields.unk3                              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RAS_MSAA_CNTL(...) pack_A6XX_RB_RAS_MSAA_CNTL((struct A6XX_RB_RAS_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_RB_DEST_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    bool							msaa_disable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEST_MSAA_CNTL(struct A6XX_RB_DEST_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEST_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.msaa_disable                      <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEST_MSAA_CNTL(...) pack_A6XX_RB_DEST_MSAA_CNTL((struct A6XX_RB_DEST_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_CONFIG {
    uint32_t							unk0;
    bool							location_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_CONFIG(struct A6XX_RB_SAMPLE_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_CONFIG,
        .value =
            (fields.unk0                              <<  0) |
            (fields.location_enable                   <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SAMPLE_CONFIG(...) pack_A6XX_RB_SAMPLE_CONFIG((struct A6XX_RB_SAMPLE_CONFIG) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_LOCATION_0 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_LOCATION_0(struct A6XX_RB_SAMPLE_LOCATION_0 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_LOCATION_0,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SAMPLE_LOCATION_0(...) pack_A6XX_RB_SAMPLE_LOCATION_0((struct A6XX_RB_SAMPLE_LOCATION_0) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_LOCATION_1 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_LOCATION_1(struct A6XX_RB_SAMPLE_LOCATION_1 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_LOCATION_1,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SAMPLE_LOCATION_1(...) pack_A6XX_RB_SAMPLE_LOCATION_1((struct A6XX_RB_SAMPLE_LOCATION_1) { __VA_ARGS__ })

struct A6XX_RB_RENDER_CONTROL0 {
    bool							ij_persp_pixel;
    bool							ij_persp_centroid;
    bool							ij_persp_sample;
    bool							ij_linear_pixel;
    bool							ij_linear_centroid;
    bool							ij_linear_sample;
    uint32_t							coord_mask;
    bool							unk10;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RENDER_CONTROL0(struct A6XX_RB_RENDER_CONTROL0 fields)
{
#ifndef NDEBUG
    assert((fields.coord_mask                        & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RENDER_CONTROL0,
        .value =
            (fields.ij_persp_pixel                    <<  0) |
            (fields.ij_persp_centroid                 <<  1) |
            (fields.ij_persp_sample                   <<  2) |
            (fields.ij_linear_pixel                   <<  3) |
            (fields.ij_linear_centroid                <<  4) |
            (fields.ij_linear_sample                  <<  5) |
            (fields.coord_mask                        <<  6) |
            (fields.unk10                             << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RENDER_CONTROL0(...) pack_A6XX_RB_RENDER_CONTROL0((struct A6XX_RB_RENDER_CONTROL0) { __VA_ARGS__ })

struct A6XX_RB_RENDER_CONTROL1 {
    bool							samplemask;
    bool							unk1;
    bool							faceness;
    bool							sampleid;
    enum a6xx_fragcoord_sample_mode				fragcoordsamplemode;
    bool							centerrhw;
    bool							linelengthen;
    bool							foveation;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RENDER_CONTROL1(struct A6XX_RB_RENDER_CONTROL1 fields)
{
#ifndef NDEBUG
    assert((fields.fragcoordsamplemode               & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RENDER_CONTROL1,
        .value =
            (fields.samplemask                        <<  0) |
            (fields.unk1                              <<  1) |
            (fields.faceness                          <<  2) |
            (fields.sampleid                          <<  3) |
            (fields.fragcoordsamplemode               <<  4) |
            (fields.centerrhw                         <<  6) |
            (fields.linelengthen                      <<  7) |
            (fields.foveation                         <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RENDER_CONTROL1(...) pack_A6XX_RB_RENDER_CONTROL1((struct A6XX_RB_RENDER_CONTROL1) { __VA_ARGS__ })

struct A6XX_RB_FS_OUTPUT_CNTL0 {
    bool							dual_color_in_enable;
    bool							frag_writes_z;
    bool							frag_writes_sampmask;
    bool							frag_writes_stencilref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_FS_OUTPUT_CNTL0(struct A6XX_RB_FS_OUTPUT_CNTL0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_FS_OUTPUT_CNTL0,
        .value =
            (fields.dual_color_in_enable              <<  0) |
            (fields.frag_writes_z                     <<  1) |
            (fields.frag_writes_sampmask              <<  2) |
            (fields.frag_writes_stencilref            <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_FS_OUTPUT_CNTL0(...) pack_A6XX_RB_FS_OUTPUT_CNTL0((struct A6XX_RB_FS_OUTPUT_CNTL0) { __VA_ARGS__ })

struct A6XX_RB_FS_OUTPUT_CNTL1 {
    uint32_t							mrt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_FS_OUTPUT_CNTL1(struct A6XX_RB_FS_OUTPUT_CNTL1 fields)
{
#ifndef NDEBUG
    assert((fields.mrt                               & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_FS_OUTPUT_CNTL1,
        .value =
            (fields.mrt                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_FS_OUTPUT_CNTL1(...) pack_A6XX_RB_FS_OUTPUT_CNTL1((struct A6XX_RB_FS_OUTPUT_CNTL1) { __VA_ARGS__ })

struct A6XX_RB_RENDER_COMPONENTS {
    uint32_t							rt0;
    uint32_t							rt1;
    uint32_t							rt2;
    uint32_t							rt3;
    uint32_t							rt4;
    uint32_t							rt5;
    uint32_t							rt6;
    uint32_t							rt7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RENDER_COMPONENTS(struct A6XX_RB_RENDER_COMPONENTS fields)
{
#ifndef NDEBUG
    assert((fields.rt0                               & 0xfffffff0) == 0);
    assert((fields.rt1                               & 0xfffffff0) == 0);
    assert((fields.rt2                               & 0xfffffff0) == 0);
    assert((fields.rt3                               & 0xfffffff0) == 0);
    assert((fields.rt4                               & 0xfffffff0) == 0);
    assert((fields.rt5                               & 0xfffffff0) == 0);
    assert((fields.rt6                               & 0xfffffff0) == 0);
    assert((fields.rt7                               & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RENDER_COMPONENTS,
        .value =
            (fields.rt0                               <<  0) |
            (fields.rt1                               <<  4) |
            (fields.rt2                               <<  8) |
            (fields.rt3                               << 12) |
            (fields.rt4                               << 16) |
            (fields.rt5                               << 20) |
            (fields.rt6                               << 24) |
            (fields.rt7                               << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RENDER_COMPONENTS(...) pack_A6XX_RB_RENDER_COMPONENTS((struct A6XX_RB_RENDER_COMPONENTS) { __VA_ARGS__ })

struct A6XX_RB_DITHER_CNTL {
    enum adreno_rb_dither_mode					dither_mode_mrt0;
    enum adreno_rb_dither_mode					dither_mode_mrt1;
    enum adreno_rb_dither_mode					dither_mode_mrt2;
    enum adreno_rb_dither_mode					dither_mode_mrt3;
    enum adreno_rb_dither_mode					dither_mode_mrt4;
    enum adreno_rb_dither_mode					dither_mode_mrt5;
    enum adreno_rb_dither_mode					dither_mode_mrt6;
    enum adreno_rb_dither_mode					dither_mode_mrt7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DITHER_CNTL(struct A6XX_RB_DITHER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.dither_mode_mrt0                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt1                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt2                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt3                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt4                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt5                  & 0xfffffffc) == 0);
    assert((fields.dither_mode_mrt6                  & 0xfffffffe) == 0);
    assert((fields.dither_mode_mrt7                  & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000dfff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DITHER_CNTL,
        .value =
            (fields.dither_mode_mrt0                  <<  0) |
            (fields.dither_mode_mrt1                  <<  2) |
            (fields.dither_mode_mrt2                  <<  4) |
            (fields.dither_mode_mrt3                  <<  6) |
            (fields.dither_mode_mrt4                  <<  8) |
            (fields.dither_mode_mrt5                  << 10) |
            (fields.dither_mode_mrt6                  << 12) |
            (fields.dither_mode_mrt7                  << 14) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DITHER_CNTL(...) pack_A6XX_RB_DITHER_CNTL((struct A6XX_RB_DITHER_CNTL) { __VA_ARGS__ })

struct A6XX_RB_SRGB_CNTL {
    bool							srgb_mrt0;
    bool							srgb_mrt1;
    bool							srgb_mrt2;
    bool							srgb_mrt3;
    bool							srgb_mrt4;
    bool							srgb_mrt5;
    bool							srgb_mrt6;
    bool							srgb_mrt7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SRGB_CNTL(struct A6XX_RB_SRGB_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SRGB_CNTL,
        .value =
            (fields.srgb_mrt0                         <<  0) |
            (fields.srgb_mrt1                         <<  1) |
            (fields.srgb_mrt2                         <<  2) |
            (fields.srgb_mrt3                         <<  3) |
            (fields.srgb_mrt4                         <<  4) |
            (fields.srgb_mrt5                         <<  5) |
            (fields.srgb_mrt6                         <<  6) |
            (fields.srgb_mrt7                         <<  7) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SRGB_CNTL(...) pack_A6XX_RB_SRGB_CNTL((struct A6XX_RB_SRGB_CNTL) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_CNTL {
    bool							per_samp_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_CNTL(struct A6XX_RB_SAMPLE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_CNTL,
        .value =
            (fields.per_samp_mode                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SAMPLE_CNTL(...) pack_A6XX_RB_SAMPLE_CNTL((struct A6XX_RB_SAMPLE_CNTL) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8811 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8811(struct A6XX_RB_UNKNOWN_8811 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8811,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8811(...) pack_A6XX_RB_UNKNOWN_8811((struct A6XX_RB_UNKNOWN_8811) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8818 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8818(struct A6XX_RB_UNKNOWN_8818 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8818,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8818(...) pack_A6XX_RB_UNKNOWN_8818((struct A6XX_RB_UNKNOWN_8818) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8819 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8819(struct A6XX_RB_UNKNOWN_8819 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8819,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8819(...) pack_A6XX_RB_UNKNOWN_8819((struct A6XX_RB_UNKNOWN_8819) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_881A {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_881A(struct A6XX_RB_UNKNOWN_881A fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_881A,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_881A(...) pack_A6XX_RB_UNKNOWN_881A((struct A6XX_RB_UNKNOWN_881A) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_881B {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_881B(struct A6XX_RB_UNKNOWN_881B fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_881B,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_881B(...) pack_A6XX_RB_UNKNOWN_881B((struct A6XX_RB_UNKNOWN_881B) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_881C {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_881C(struct A6XX_RB_UNKNOWN_881C fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_881C,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_881C(...) pack_A6XX_RB_UNKNOWN_881C((struct A6XX_RB_UNKNOWN_881C) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_881D {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_881D(struct A6XX_RB_UNKNOWN_881D fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_881D,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_881D(...) pack_A6XX_RB_UNKNOWN_881D((struct A6XX_RB_UNKNOWN_881D) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_881E {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_881E(struct A6XX_RB_UNKNOWN_881E fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_881E,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_881E(...) pack_A6XX_RB_UNKNOWN_881E((struct A6XX_RB_UNKNOWN_881E) { __VA_ARGS__ })

struct A6XX_RB_MRT_CONTROL {
    bool							blend;
    bool							blend2;
    bool							rop_enable;
    enum a3xx_rop_code						rop_code;
    uint32_t							component_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_CONTROL(uint32_t i, struct A6XX_RB_MRT_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.rop_code                          & 0xfffffff0) == 0);
    assert((fields.component_enable                  & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_CONTROL(i),
        .value =
            (fields.blend                             <<  0) |
            (fields.blend2                            <<  1) |
            (fields.rop_enable                        <<  2) |
            (fields.rop_code                          <<  3) |
            (fields.component_enable                  <<  7) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_CONTROL(i, ...) pack_A6XX_RB_MRT_CONTROL(i, (struct A6XX_RB_MRT_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_MRT_BLEND_CONTROL {
    enum adreno_rb_blend_factor					rgb_src_factor;
    enum a3xx_rb_blend_opcode					rgb_blend_opcode;
    enum adreno_rb_blend_factor					rgb_dest_factor;
    enum adreno_rb_blend_factor					alpha_src_factor;
    enum a3xx_rb_blend_opcode					alpha_blend_opcode;
    enum adreno_rb_blend_factor					alpha_dest_factor;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_BLEND_CONTROL(uint32_t i, struct A6XX_RB_MRT_BLEND_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.rgb_src_factor                    & 0xffffffe0) == 0);
    assert((fields.rgb_blend_opcode                  & 0xfffffff8) == 0);
    assert((fields.rgb_dest_factor                   & 0xffffffe0) == 0);
    assert((fields.alpha_src_factor                  & 0xffffffe0) == 0);
    assert((fields.alpha_blend_opcode                & 0xfffffff8) == 0);
    assert((fields.alpha_dest_factor                 & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x1fff1fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_BLEND_CONTROL(i),
        .value =
            (fields.rgb_src_factor                    <<  0) |
            (fields.rgb_blend_opcode                  <<  5) |
            (fields.rgb_dest_factor                   <<  8) |
            (fields.alpha_src_factor                  << 16) |
            (fields.alpha_blend_opcode                << 21) |
            (fields.alpha_dest_factor                 << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_BLEND_CONTROL(i, ...) pack_A6XX_RB_MRT_BLEND_CONTROL(i, (struct A6XX_RB_MRT_BLEND_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_MRT_BUF_INFO {
    enum a6xx_format						color_format;
    enum a6xx_tile_mode						color_tile_mode;
    uint32_t							unk10;
    enum a3xx_color_swap					color_swap;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_BUF_INFO(uint32_t i, struct A6XX_RB_MRT_BUF_INFO fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.color_tile_mode                   & 0xfffffffc) == 0);
    assert((fields.unk10                             & 0xfffffffe) == 0);
    assert((fields.color_swap                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x000067ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_BUF_INFO(i),
        .value =
            (fields.color_format                      <<  0) |
            (fields.color_tile_mode                   <<  8) |
            (fields.unk10                             << 10) |
            (fields.color_swap                        << 13) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_BUF_INFO(i, ...) pack_A6XX_RB_MRT_BUF_INFO(i, (struct A6XX_RB_MRT_BUF_INFO) { __VA_ARGS__ })

struct A6XX_RB_MRT_PITCH {
    uint32_t							a6xx_rb_mrt_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_PITCH(uint32_t i, struct A6XX_RB_MRT_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_mrt_pitch >> 6)          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_PITCH(i),
        .value =
            ((fields.a6xx_rb_mrt_pitch >> 6)          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_PITCH(i, ...) pack_A6XX_RB_MRT_PITCH(i, (struct A6XX_RB_MRT_PITCH) { __VA_ARGS__ })

struct A6XX_RB_MRT_ARRAY_PITCH {
    uint32_t							a6xx_rb_mrt_array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_ARRAY_PITCH(uint32_t i, struct A6XX_RB_MRT_ARRAY_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_mrt_array_pitch >> 6)    & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_ARRAY_PITCH(i),
        .value =
            ((fields.a6xx_rb_mrt_array_pitch >> 6)    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_ARRAY_PITCH(i, ...) pack_A6XX_RB_MRT_ARRAY_PITCH(i, (struct A6XX_RB_MRT_ARRAY_PITCH) { __VA_ARGS__ })

struct A6XX_RB_MRT_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_BASE(uint32_t i, struct A6XX_RB_MRT_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_BASE(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_MRT_BASE(i, ...) pack_A6XX_RB_MRT_BASE(i, (struct A6XX_RB_MRT_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_MRT_BASE_GMEM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_BASE_GMEM(uint32_t i, struct A6XX_RB_MRT_BASE_GMEM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_BASE_GMEM(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_BASE_GMEM(i, ...) pack_A6XX_RB_MRT_BASE_GMEM(i, (struct A6XX_RB_MRT_BASE_GMEM) { __VA_ARGS__ })

struct A6XX_RB_BLEND_RED_F32 {
    float							a6xx_rb_blend_red_f32;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLEND_RED_F32(struct A6XX_RB_BLEND_RED_F32 fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_blend_red_f32)        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLEND_RED_F32,
        .value =
            (fui(fields.a6xx_rb_blend_red_f32)        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLEND_RED_F32(...) pack_A6XX_RB_BLEND_RED_F32((struct A6XX_RB_BLEND_RED_F32) { __VA_ARGS__ })

struct A6XX_RB_BLEND_GREEN_F32 {
    float							a6xx_rb_blend_green_f32;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLEND_GREEN_F32(struct A6XX_RB_BLEND_GREEN_F32 fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_blend_green_f32)      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLEND_GREEN_F32,
        .value =
            (fui(fields.a6xx_rb_blend_green_f32)      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLEND_GREEN_F32(...) pack_A6XX_RB_BLEND_GREEN_F32((struct A6XX_RB_BLEND_GREEN_F32) { __VA_ARGS__ })

struct A6XX_RB_BLEND_BLUE_F32 {
    float							a6xx_rb_blend_blue_f32;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLEND_BLUE_F32(struct A6XX_RB_BLEND_BLUE_F32 fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_blend_blue_f32)       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLEND_BLUE_F32,
        .value =
            (fui(fields.a6xx_rb_blend_blue_f32)       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLEND_BLUE_F32(...) pack_A6XX_RB_BLEND_BLUE_F32((struct A6XX_RB_BLEND_BLUE_F32) { __VA_ARGS__ })

struct A6XX_RB_BLEND_ALPHA_F32 {
    float							a6xx_rb_blend_alpha_f32;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLEND_ALPHA_F32(struct A6XX_RB_BLEND_ALPHA_F32 fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_blend_alpha_f32)      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLEND_ALPHA_F32,
        .value =
            (fui(fields.a6xx_rb_blend_alpha_f32)      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLEND_ALPHA_F32(...) pack_A6XX_RB_BLEND_ALPHA_F32((struct A6XX_RB_BLEND_ALPHA_F32) { __VA_ARGS__ })

struct A6XX_RB_ALPHA_CONTROL {
    uint32_t							alpha_ref;
    bool							alpha_test;
    enum adreno_compare_func					alpha_test_func;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_ALPHA_CONTROL(struct A6XX_RB_ALPHA_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.alpha_ref                         & 0xffffff00) == 0);
    assert((fields.alpha_test_func                   & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_ALPHA_CONTROL,
        .value =
            (fields.alpha_ref                         <<  0) |
            (fields.alpha_test                        <<  8) |
            (fields.alpha_test_func                   <<  9) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_ALPHA_CONTROL(...) pack_A6XX_RB_ALPHA_CONTROL((struct A6XX_RB_ALPHA_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_BLEND_CNTL {
    uint32_t							enable_blend;
    bool							independent_blend;
    bool							dual_color_in_enable;
    bool							alpha_to_coverage;
    bool							alpha_to_one;
    uint32_t							sample_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLEND_CNTL(struct A6XX_RB_BLEND_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.enable_blend                      & 0xffffff00) == 0);
    assert((fields.sample_mask                       & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLEND_CNTL,
        .value =
            (fields.enable_blend                      <<  0) |
            (fields.independent_blend                 <<  8) |
            (fields.dual_color_in_enable              <<  9) |
            (fields.alpha_to_coverage                 << 10) |
            (fields.alpha_to_one                      << 11) |
            (fields.sample_mask                       << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLEND_CNTL(...) pack_A6XX_RB_BLEND_CNTL((struct A6XX_RB_BLEND_CNTL) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_PLANE_CNTL {
    enum a6xx_ztest_mode					z_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_PLANE_CNTL(struct A6XX_RB_DEPTH_PLANE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.z_mode                            & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_PLANE_CNTL,
        .value =
            (fields.z_mode                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_PLANE_CNTL(...) pack_A6XX_RB_DEPTH_PLANE_CNTL((struct A6XX_RB_DEPTH_PLANE_CNTL) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_CNTL {
    bool							z_test_enable;
    bool							z_write_enable;
    enum adreno_compare_func					zfunc;
    bool							z_clip_disable;
    bool							z_read_enable;
    bool							z_bounds_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_CNTL(struct A6XX_RB_DEPTH_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.zfunc                             & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_CNTL,
        .value =
            (fields.z_test_enable                     <<  0) |
            (fields.z_write_enable                    <<  1) |
            (fields.zfunc                             <<  2) |
            (fields.z_clip_disable                    <<  5) |
            (fields.z_read_enable                     <<  6) |
            (fields.z_bounds_enable                   <<  7) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_CNTL(...) pack_A6XX_RB_DEPTH_CNTL((struct A6XX_RB_DEPTH_CNTL) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_BUFFER_INFO {
    enum a6xx_depth_format					depth_format;
    uint32_t							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_BUFFER_INFO(struct A6XX_RB_DEPTH_BUFFER_INFO fields)
{
#ifndef NDEBUG
    assert((fields.depth_format                      & 0xfffffff8) == 0);
    assert((fields.unk3                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000001f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_BUFFER_INFO,
        .value =
            (fields.depth_format                      <<  0) |
            (fields.unk3                              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_BUFFER_INFO(...) pack_A6XX_RB_DEPTH_BUFFER_INFO((struct A6XX_RB_DEPTH_BUFFER_INFO) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_BUFFER_PITCH {
    uint32_t							a6xx_rb_depth_buffer_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_BUFFER_PITCH(struct A6XX_RB_DEPTH_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_depth_buffer_pitch >> 6) & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x00003fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_BUFFER_PITCH,
        .value =
            ((fields.a6xx_rb_depth_buffer_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_BUFFER_PITCH(...) pack_A6XX_RB_DEPTH_BUFFER_PITCH((struct A6XX_RB_DEPTH_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH {
    uint32_t							a6xx_rb_depth_buffer_array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH(struct A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_depth_buffer_array_pitch >> 6) & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH,
        .value =
            ((fields.a6xx_rb_depth_buffer_array_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH(...) pack_A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH((struct A6XX_RB_DEPTH_BUFFER_ARRAY_PITCH) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_BUFFER_BASE(struct A6XX_RB_DEPTH_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_DEPTH_BUFFER_BASE(...) pack_A6XX_RB_DEPTH_BUFFER_BASE((struct A6XX_RB_DEPTH_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_DEPTH_BUFFER_BASE_GMEM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_BUFFER_BASE_GMEM(struct A6XX_RB_DEPTH_BUFFER_BASE_GMEM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_BUFFER_BASE_GMEM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_BUFFER_BASE_GMEM(...) pack_A6XX_RB_DEPTH_BUFFER_BASE_GMEM((struct A6XX_RB_DEPTH_BUFFER_BASE_GMEM) { __VA_ARGS__ })

struct A6XX_RB_Z_BOUNDS_MIN {
    float							a6xx_rb_z_bounds_min;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_Z_BOUNDS_MIN(struct A6XX_RB_Z_BOUNDS_MIN fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_z_bounds_min)         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_Z_BOUNDS_MIN,
        .value =
            (fui(fields.a6xx_rb_z_bounds_min)         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_Z_BOUNDS_MIN(...) pack_A6XX_RB_Z_BOUNDS_MIN((struct A6XX_RB_Z_BOUNDS_MIN) { __VA_ARGS__ })

struct A6XX_RB_Z_BOUNDS_MAX {
    float							a6xx_rb_z_bounds_max;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_Z_BOUNDS_MAX(struct A6XX_RB_Z_BOUNDS_MAX fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_z_bounds_max)         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_Z_BOUNDS_MAX,
        .value =
            (fui(fields.a6xx_rb_z_bounds_max)         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_Z_BOUNDS_MAX(...) pack_A6XX_RB_Z_BOUNDS_MAX((struct A6XX_RB_Z_BOUNDS_MAX) { __VA_ARGS__ })

struct A6XX_RB_STENCIL_CONTROL {
    bool							stencil_enable;
    bool							stencil_enable_bf;
    bool							stencil_read;
    enum adreno_compare_func					func;
    enum adreno_stencil_op					fail;
    enum adreno_stencil_op					zpass;
    enum adreno_stencil_op					zfail;
    enum adreno_compare_func					func_bf;
    enum adreno_stencil_op					fail_bf;
    enum adreno_stencil_op					zpass_bf;
    enum adreno_stencil_op					zfail_bf;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_CONTROL(struct A6XX_RB_STENCIL_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.func                              & 0xfffffff8) == 0);
    assert((fields.fail                              & 0xfffffff8) == 0);
    assert((fields.zpass                             & 0xfffffff8) == 0);
    assert((fields.zfail                             & 0xfffffff8) == 0);
    assert((fields.func_bf                           & 0xfffffff8) == 0);
    assert((fields.fail_bf                           & 0xfffffff8) == 0);
    assert((fields.zpass_bf                          & 0xfffffff8) == 0);
    assert((fields.zfail_bf                          & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0xffffff07) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_CONTROL,
        .value =
            (fields.stencil_enable                    <<  0) |
            (fields.stencil_enable_bf                 <<  1) |
            (fields.stencil_read                      <<  2) |
            (fields.func                              <<  8) |
            (fields.fail                              << 11) |
            (fields.zpass                             << 14) |
            (fields.zfail                             << 17) |
            (fields.func_bf                           << 20) |
            (fields.fail_bf                           << 23) |
            (fields.zpass_bf                          << 26) |
            (fields.zfail_bf                          << 29) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCIL_CONTROL(...) pack_A6XX_RB_STENCIL_CONTROL((struct A6XX_RB_STENCIL_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_STENCIL_INFO {
    bool							separate_stencil;
    bool							unk1;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_INFO(struct A6XX_RB_STENCIL_INFO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_INFO,
        .value =
            (fields.separate_stencil                  <<  0) |
            (fields.unk1                              <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCIL_INFO(...) pack_A6XX_RB_STENCIL_INFO((struct A6XX_RB_STENCIL_INFO) { __VA_ARGS__ })

struct A6XX_RB_STENCIL_BUFFER_PITCH {
    uint32_t							a6xx_rb_stencil_buffer_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_BUFFER_PITCH(struct A6XX_RB_STENCIL_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_stencil_buffer_pitch >> 6) & 0xfffff000) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_BUFFER_PITCH,
        .value =
            ((fields.a6xx_rb_stencil_buffer_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCIL_BUFFER_PITCH(...) pack_A6XX_RB_STENCIL_BUFFER_PITCH((struct A6XX_RB_STENCIL_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH {
    uint32_t							a6xx_rb_stencil_buffer_array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH(struct A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_stencil_buffer_array_pitch >> 6) & 0xff000000) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH,
        .value =
            ((fields.a6xx_rb_stencil_buffer_array_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH(...) pack_A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH((struct A6XX_RB_STENCIL_BUFFER_ARRAY_PITCH) { __VA_ARGS__ })

struct A6XX_RB_STENCIL_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_BUFFER_BASE(struct A6XX_RB_STENCIL_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_STENCIL_BUFFER_BASE(...) pack_A6XX_RB_STENCIL_BUFFER_BASE((struct A6XX_RB_STENCIL_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_STENCIL_BUFFER_BASE_GMEM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCIL_BUFFER_BASE_GMEM(struct A6XX_RB_STENCIL_BUFFER_BASE_GMEM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCIL_BUFFER_BASE_GMEM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCIL_BUFFER_BASE_GMEM(...) pack_A6XX_RB_STENCIL_BUFFER_BASE_GMEM((struct A6XX_RB_STENCIL_BUFFER_BASE_GMEM) { __VA_ARGS__ })

struct A6XX_RB_STENCILREF {
    uint32_t							ref;
    uint32_t							bfref;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCILREF(struct A6XX_RB_STENCILREF fields)
{
#ifndef NDEBUG
    assert((fields.ref                               & 0xffffff00) == 0);
    assert((fields.bfref                             & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCILREF,
        .value =
            (fields.ref                               <<  0) |
            (fields.bfref                             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCILREF(...) pack_A6XX_RB_STENCILREF((struct A6XX_RB_STENCILREF) { __VA_ARGS__ })

struct A6XX_RB_STENCILMASK {
    uint32_t							mask;
    uint32_t							bfmask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCILMASK(struct A6XX_RB_STENCILMASK fields)
{
#ifndef NDEBUG
    assert((fields.mask                              & 0xffffff00) == 0);
    assert((fields.bfmask                            & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCILMASK,
        .value =
            (fields.mask                              <<  0) |
            (fields.bfmask                            <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCILMASK(...) pack_A6XX_RB_STENCILMASK((struct A6XX_RB_STENCILMASK) { __VA_ARGS__ })

struct A6XX_RB_STENCILWRMASK {
    uint32_t							wrmask;
    uint32_t							bfwrmask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_STENCILWRMASK(struct A6XX_RB_STENCILWRMASK fields)
{
#ifndef NDEBUG
    assert((fields.wrmask                            & 0xffffff00) == 0);
    assert((fields.bfwrmask                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_STENCILWRMASK,
        .value =
            (fields.wrmask                            <<  0) |
            (fields.bfwrmask                          <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_STENCILWRMASK(...) pack_A6XX_RB_STENCILWRMASK((struct A6XX_RB_STENCILWRMASK) { __VA_ARGS__ })

struct A6XX_RB_WINDOW_OFFSET {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_WINDOW_OFFSET(struct A6XX_RB_WINDOW_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_WINDOW_OFFSET,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_WINDOW_OFFSET(...) pack_A6XX_RB_WINDOW_OFFSET((struct A6XX_RB_WINDOW_OFFSET) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_COUNT_CONTROL {
    bool							disable;
    bool							copy;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_COUNT_CONTROL(struct A6XX_RB_SAMPLE_COUNT_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_COUNT_CONTROL,
        .value =
            (fields.disable                           <<  0) |
            (fields.copy                              <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_SAMPLE_COUNT_CONTROL(...) pack_A6XX_RB_SAMPLE_COUNT_CONTROL((struct A6XX_RB_SAMPLE_COUNT_CONTROL) { __VA_ARGS__ })

struct A6XX_RB_LRZ_CNTL {
    bool							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_LRZ_CNTL(struct A6XX_RB_LRZ_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_LRZ_CNTL,
        .value =
            (fields.enable                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_LRZ_CNTL(...) pack_A6XX_RB_LRZ_CNTL((struct A6XX_RB_LRZ_CNTL) { __VA_ARGS__ })

struct A6XX_RB_Z_CLAMP_MIN {
    float							a6xx_rb_z_clamp_min;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_Z_CLAMP_MIN(struct A6XX_RB_Z_CLAMP_MIN fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_z_clamp_min)          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_Z_CLAMP_MIN,
        .value =
            (fui(fields.a6xx_rb_z_clamp_min)          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_Z_CLAMP_MIN(...) pack_A6XX_RB_Z_CLAMP_MIN((struct A6XX_RB_Z_CLAMP_MIN) { __VA_ARGS__ })

struct A6XX_RB_Z_CLAMP_MAX {
    float							a6xx_rb_z_clamp_max;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_Z_CLAMP_MAX(struct A6XX_RB_Z_CLAMP_MAX fields)
{
#ifndef NDEBUG
    assert((fui(fields.a6xx_rb_z_clamp_max)          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_Z_CLAMP_MAX,
        .value =
            (fui(fields.a6xx_rb_z_clamp_max)          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_Z_CLAMP_MAX(...) pack_A6XX_RB_Z_CLAMP_MAX((struct A6XX_RB_Z_CLAMP_MAX) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_88D0 {
    uint32_t							unk0;
    uint32_t							unk16;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_88D0(struct A6XX_RB_UNKNOWN_88D0 fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xffffe000) == 0);
    assert((fields.unk16                             & 0xfffff800) == 0);
    assert((fields.unknown                           & 0x07ff1fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_88D0,
        .value =
            (fields.unk0                              <<  0) |
            (fields.unk16                             << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_88D0(...) pack_A6XX_RB_UNKNOWN_88D0((struct A6XX_RB_UNKNOWN_88D0) { __VA_ARGS__ })

struct A6XX_RB_BLIT_SCISSOR_TL {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_SCISSOR_TL(struct A6XX_RB_BLIT_SCISSOR_TL fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_SCISSOR_TL,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_SCISSOR_TL(...) pack_A6XX_RB_BLIT_SCISSOR_TL((struct A6XX_RB_BLIT_SCISSOR_TL) { __VA_ARGS__ })

struct A6XX_RB_BLIT_SCISSOR_BR {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_SCISSOR_BR(struct A6XX_RB_BLIT_SCISSOR_BR fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_SCISSOR_BR,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_SCISSOR_BR(...) pack_A6XX_RB_BLIT_SCISSOR_BR((struct A6XX_RB_BLIT_SCISSOR_BR) { __VA_ARGS__ })

struct A6XX_RB_BIN_CONTROL2 {
    uint32_t							binw;
    uint32_t							binh;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BIN_CONTROL2(struct A6XX_RB_BIN_CONTROL2 fields)
{
#ifndef NDEBUG
    assert(((fields.binw >> 5)                       & 0xffffffc0) == 0);
    assert(((fields.binh >> 4)                       & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x00007f3f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BIN_CONTROL2,
        .value =
            ((fields.binw >> 5)                       <<  0) |
            ((fields.binh >> 4)                       <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BIN_CONTROL2(...) pack_A6XX_RB_BIN_CONTROL2((struct A6XX_RB_BIN_CONTROL2) { __VA_ARGS__ })

struct A6XX_RB_WINDOW_OFFSET2 {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_WINDOW_OFFSET2(struct A6XX_RB_WINDOW_OFFSET2 fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_WINDOW_OFFSET2,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_WINDOW_OFFSET2(...) pack_A6XX_RB_WINDOW_OFFSET2((struct A6XX_RB_WINDOW_OFFSET2) { __VA_ARGS__ })

struct A6XX_RB_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MSAA_CNTL(struct A6XX_RB_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000018) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MSAA_CNTL,
        .value =
            (fields.samples                           <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MSAA_CNTL(...) pack_A6XX_RB_MSAA_CNTL((struct A6XX_RB_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_RB_BLIT_BASE_GMEM {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_BASE_GMEM(struct A6XX_RB_BLIT_BASE_GMEM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_BASE_GMEM,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_BASE_GMEM(...) pack_A6XX_RB_BLIT_BASE_GMEM((struct A6XX_RB_BLIT_BASE_GMEM) { __VA_ARGS__ })

struct A6XX_RB_BLIT_DST_INFO {
    enum a6xx_tile_mode						tile_mode;
    bool							flags;
    enum a3xx_msaa_samples					samples;
    enum a3xx_color_swap					color_swap;
    enum a6xx_format						color_format;
    bool							unk15;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_DST_INFO(struct A6XX_RB_BLIT_DST_INFO fields)
{
#ifndef NDEBUG
    assert((fields.tile_mode                         & 0xfffffffc) == 0);
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.color_swap                        & 0xfffffffc) == 0);
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_DST_INFO,
        .value =
            (fields.tile_mode                         <<  0) |
            (fields.flags                             <<  2) |
            (fields.samples                           <<  3) |
            (fields.color_swap                        <<  5) |
            (fields.color_format                      <<  7) |
            (fields.unk15                             << 15) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_DST_INFO(...) pack_A6XX_RB_BLIT_DST_INFO((struct A6XX_RB_BLIT_DST_INFO) { __VA_ARGS__ })

struct A6XX_RB_BLIT_DST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_DST(struct A6XX_RB_BLIT_DST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_DST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_BLIT_DST(...) pack_A6XX_RB_BLIT_DST((struct A6XX_RB_BLIT_DST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_BLIT_DST_PITCH {
    uint32_t							a6xx_rb_blit_dst_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_DST_PITCH(struct A6XX_RB_BLIT_DST_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_blit_dst_pitch >> 6)     & 0xffff0000) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_DST_PITCH,
        .value =
            ((fields.a6xx_rb_blit_dst_pitch >> 6)     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_DST_PITCH(...) pack_A6XX_RB_BLIT_DST_PITCH((struct A6XX_RB_BLIT_DST_PITCH) { __VA_ARGS__ })

struct A6XX_RB_BLIT_DST_ARRAY_PITCH {
    uint32_t							a6xx_rb_blit_dst_array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_DST_ARRAY_PITCH(struct A6XX_RB_BLIT_DST_ARRAY_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_blit_dst_array_pitch >> 6) & 0xe0000000) == 0);
    assert((fields.unknown                           & 0x1fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_DST_ARRAY_PITCH,
        .value =
            ((fields.a6xx_rb_blit_dst_array_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_DST_ARRAY_PITCH(...) pack_A6XX_RB_BLIT_DST_ARRAY_PITCH((struct A6XX_RB_BLIT_DST_ARRAY_PITCH) { __VA_ARGS__ })

struct A6XX_RB_BLIT_FLAG_DST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_FLAG_DST(struct A6XX_RB_BLIT_FLAG_DST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_FLAG_DST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_BLIT_FLAG_DST(...) pack_A6XX_RB_BLIT_FLAG_DST((struct A6XX_RB_BLIT_FLAG_DST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_BLIT_FLAG_DST_PITCH {
    uint32_t							pitch;
    uint32_t							array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_FLAG_DST_PITCH(struct A6XX_RB_BLIT_FLAG_DST_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.pitch >> 6)                      & 0xfffff800) == 0);
    assert(((fields.array_pitch >> 7)                & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_FLAG_DST_PITCH,
        .value =
            ((fields.pitch >> 6)                      <<  0) |
            ((fields.array_pitch >> 7)                << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_FLAG_DST_PITCH(...) pack_A6XX_RB_BLIT_FLAG_DST_PITCH((struct A6XX_RB_BLIT_FLAG_DST_PITCH) { __VA_ARGS__ })

struct A6XX_RB_BLIT_CLEAR_COLOR_DW0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_CLEAR_COLOR_DW0(struct A6XX_RB_BLIT_CLEAR_COLOR_DW0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_CLEAR_COLOR_DW0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_CLEAR_COLOR_DW0(...) pack_A6XX_RB_BLIT_CLEAR_COLOR_DW0((struct A6XX_RB_BLIT_CLEAR_COLOR_DW0) { __VA_ARGS__ })

struct A6XX_RB_BLIT_CLEAR_COLOR_DW1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_CLEAR_COLOR_DW1(struct A6XX_RB_BLIT_CLEAR_COLOR_DW1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_CLEAR_COLOR_DW1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_CLEAR_COLOR_DW1(...) pack_A6XX_RB_BLIT_CLEAR_COLOR_DW1((struct A6XX_RB_BLIT_CLEAR_COLOR_DW1) { __VA_ARGS__ })

struct A6XX_RB_BLIT_CLEAR_COLOR_DW2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_CLEAR_COLOR_DW2(struct A6XX_RB_BLIT_CLEAR_COLOR_DW2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_CLEAR_COLOR_DW2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_CLEAR_COLOR_DW2(...) pack_A6XX_RB_BLIT_CLEAR_COLOR_DW2((struct A6XX_RB_BLIT_CLEAR_COLOR_DW2) { __VA_ARGS__ })

struct A6XX_RB_BLIT_CLEAR_COLOR_DW3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_CLEAR_COLOR_DW3(struct A6XX_RB_BLIT_CLEAR_COLOR_DW3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_CLEAR_COLOR_DW3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_CLEAR_COLOR_DW3(...) pack_A6XX_RB_BLIT_CLEAR_COLOR_DW3((struct A6XX_RB_BLIT_CLEAR_COLOR_DW3) { __VA_ARGS__ })

struct A6XX_RB_BLIT_INFO {
    bool							unk0;
    bool							gmem;
    bool							sample_0;
    bool							depth;
    uint32_t							clear_mask;
    uint32_t							last;
    uint32_t							buffer_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_BLIT_INFO(struct A6XX_RB_BLIT_INFO fields)
{
#ifndef NDEBUG
    assert((fields.clear_mask                        & 0xfffffff0) == 0);
    assert((fields.last                              & 0xfffffffc) == 0);
    assert((fields.buffer_id                         & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000f3ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_BLIT_INFO,
        .value =
            (fields.unk0                              <<  0) |
            (fields.gmem                              <<  1) |
            (fields.sample_0                          <<  2) |
            (fields.depth                             <<  3) |
            (fields.clear_mask                        <<  4) |
            (fields.last                              <<  8) |
            (fields.buffer_id                         << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_BLIT_INFO(...) pack_A6XX_RB_BLIT_INFO((struct A6XX_RB_BLIT_INFO) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_88F0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_88F0(struct A6XX_RB_UNKNOWN_88F0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_88F0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_88F0(...) pack_A6XX_RB_UNKNOWN_88F0((struct A6XX_RB_UNKNOWN_88F0) { __VA_ARGS__ })

struct A6XX_RB_UNK_FLAG_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNK_FLAG_BUFFER_BASE(struct A6XX_RB_UNK_FLAG_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNK_FLAG_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_UNK_FLAG_BUFFER_BASE(...) pack_A6XX_RB_UNK_FLAG_BUFFER_BASE((struct A6XX_RB_UNK_FLAG_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_UNK_FLAG_BUFFER_PITCH {
    uint32_t							pitch;
    uint32_t							array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNK_FLAG_BUFFER_PITCH(struct A6XX_RB_UNK_FLAG_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.pitch >> 6)                      & 0xfffff800) == 0);
    assert(((fields.array_pitch >> 7)                & 0xffffe000) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNK_FLAG_BUFFER_PITCH,
        .value =
            ((fields.pitch >> 6)                      <<  0) |
            ((fields.array_pitch >> 7)                << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNK_FLAG_BUFFER_PITCH(...) pack_A6XX_RB_UNK_FLAG_BUFFER_PITCH((struct A6XX_RB_UNK_FLAG_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_88F4 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_88F4(struct A6XX_RB_UNKNOWN_88F4 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_88F4,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_88F4(...) pack_A6XX_RB_UNKNOWN_88F4((struct A6XX_RB_UNKNOWN_88F4) { __VA_ARGS__ })

struct A6XX_RB_DEPTH_FLAG_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_FLAG_BUFFER_BASE(struct A6XX_RB_DEPTH_FLAG_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_FLAG_BUFFER_BASE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_DEPTH_FLAG_BUFFER_BASE(...) pack_A6XX_RB_DEPTH_FLAG_BUFFER_BASE((struct A6XX_RB_DEPTH_FLAG_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_DEPTH_FLAG_BUFFER_PITCH {
    uint32_t							pitch;
    uint32_t							unk8;
    uint32_t							array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_DEPTH_FLAG_BUFFER_PITCH(struct A6XX_RB_DEPTH_FLAG_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.pitch >> 6)                      & 0xffffff80) == 0);
    assert((fields.unk8                              & 0xfffffff8) == 0);
    assert(((fields.array_pitch >> 7)                & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x0fffff7f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_DEPTH_FLAG_BUFFER_PITCH,
        .value =
            ((fields.pitch >> 6)                      <<  0) |
            (fields.unk8                              <<  8) |
            ((fields.array_pitch >> 7)                << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_DEPTH_FLAG_BUFFER_PITCH(...) pack_A6XX_RB_DEPTH_FLAG_BUFFER_PITCH((struct A6XX_RB_DEPTH_FLAG_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_RB_MRT_FLAG_BUFFER_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_FLAG_BUFFER_ADDR(uint32_t i, struct A6XX_RB_MRT_FLAG_BUFFER_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_FLAG_BUFFER_ADDR(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_MRT_FLAG_BUFFER_ADDR(i, ...) pack_A6XX_RB_MRT_FLAG_BUFFER_ADDR(i, (struct A6XX_RB_MRT_FLAG_BUFFER_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_MRT_FLAG_BUFFER_PITCH {
    uint32_t							pitch;
    uint32_t							array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_MRT_FLAG_BUFFER_PITCH(uint32_t i, struct A6XX_RB_MRT_FLAG_BUFFER_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.pitch >> 6)                      & 0xfffff800) == 0);
    assert(((fields.array_pitch >> 7)                & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x1fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_MRT_FLAG_BUFFER_PITCH(i),
        .value =
            ((fields.pitch >> 6)                      <<  0) |
            ((fields.array_pitch >> 7)                << 11) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_MRT_FLAG_BUFFER_PITCH(i, ...) pack_A6XX_RB_MRT_FLAG_BUFFER_PITCH(i, (struct A6XX_RB_MRT_FLAG_BUFFER_PITCH) { __VA_ARGS__ })

struct A6XX_RB_SAMPLE_COUNT_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_SAMPLE_COUNT_ADDR(struct A6XX_RB_SAMPLE_COUNT_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_SAMPLE_COUNT_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_SAMPLE_COUNT_ADDR(...) pack_A6XX_RB_SAMPLE_COUNT_ADDR((struct A6XX_RB_SAMPLE_COUNT_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_UNKNOWN_8A00 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8A00(struct A6XX_RB_UNKNOWN_8A00 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8A00,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8A00(...) pack_A6XX_RB_UNKNOWN_8A00((struct A6XX_RB_UNKNOWN_8A00) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8A10 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8A10(struct A6XX_RB_UNKNOWN_8A10 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8A10,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8A10(...) pack_A6XX_RB_UNKNOWN_8A10((struct A6XX_RB_UNKNOWN_8A10) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8A20 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8A20(struct A6XX_RB_UNKNOWN_8A20 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8A20,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8A20(...) pack_A6XX_RB_UNKNOWN_8A20((struct A6XX_RB_UNKNOWN_8A20) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8A30 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8A30(struct A6XX_RB_UNKNOWN_8A30 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8A30,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8A30(...) pack_A6XX_RB_UNKNOWN_8A30((struct A6XX_RB_UNKNOWN_8A30) { __VA_ARGS__ })

struct A6XX_RB_2D_BLIT_CNTL {
    enum a6xx_rotation						rotate;
    bool							overwriteen;
    uint32_t							unk4;
    bool							solid_color;
    enum a6xx_format						color_format;
    bool							scissor;
    uint32_t							unk17;
    bool							d24s8;
    uint32_t							mask;
    enum a6xx_2d_ifmt						ifmt;
    enum a6xx_raster_mode					raster_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_BLIT_CNTL(struct A6XX_RB_2D_BLIT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.rotate                            & 0xfffffff8) == 0);
    assert((fields.unk4                              & 0xfffffff8) == 0);
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.unk17                             & 0xfffffffc) == 0);
    assert((fields.mask                              & 0xfffffff0) == 0);
    assert((fields.ifmt                              & 0xffffffe0) == 0);
    assert((fields.raster_mode                       & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x3fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_BLIT_CNTL,
        .value =
            (fields.rotate                            <<  0) |
            (fields.overwriteen                       <<  3) |
            (fields.unk4                              <<  4) |
            (fields.solid_color                       <<  7) |
            (fields.color_format                      <<  8) |
            (fields.scissor                           << 16) |
            (fields.unk17                             << 17) |
            (fields.d24s8                             << 19) |
            (fields.mask                              << 20) |
            (fields.ifmt                              << 24) |
            (fields.raster_mode                       << 29) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_BLIT_CNTL(...) pack_A6XX_RB_2D_BLIT_CNTL((struct A6XX_RB_2D_BLIT_CNTL) { __VA_ARGS__ })

struct A6XX_RB_2D_UNKNOWN_8C01 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_UNKNOWN_8C01(struct A6XX_RB_2D_UNKNOWN_8C01 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_UNKNOWN_8C01,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_UNKNOWN_8C01(...) pack_A6XX_RB_2D_UNKNOWN_8C01((struct A6XX_RB_2D_UNKNOWN_8C01) { __VA_ARGS__ })

struct A6XX_RB_2D_DST_INFO {
    enum a6xx_format						color_format;
    enum a6xx_tile_mode						tile_mode;
    enum a3xx_color_swap					color_swap;
    bool							flags;
    bool							srgb;
    enum a3xx_msaa_samples					samples;
    bool							filter;
    bool							unk17;
    bool							samples_average;
    bool							unk19;
    bool							unk20;
    bool							unk21;
    bool							unk22;
    uint32_t							unk23;
    bool							unk28;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_INFO(struct A6XX_RB_2D_DST_INFO fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.tile_mode                         & 0xfffffffc) == 0);
    assert((fields.color_swap                        & 0xfffffffc) == 0);
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unk23                             & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x17ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_INFO,
        .value =
            (fields.color_format                      <<  0) |
            (fields.tile_mode                         <<  8) |
            (fields.color_swap                        << 10) |
            (fields.flags                             << 12) |
            (fields.srgb                              << 13) |
            (fields.samples                           << 14) |
            (fields.filter                            << 16) |
            (fields.unk17                             << 17) |
            (fields.samples_average                   << 18) |
            (fields.unk19                             << 19) |
            (fields.unk20                             << 20) |
            (fields.unk21                             << 21) |
            (fields.unk22                             << 22) |
            (fields.unk23                             << 23) |
            (fields.unk28                             << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_DST_INFO(...) pack_A6XX_RB_2D_DST_INFO((struct A6XX_RB_2D_DST_INFO) { __VA_ARGS__ })

struct A6XX_RB_2D_DST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST(struct A6XX_RB_2D_DST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_2D_DST(...) pack_A6XX_RB_2D_DST((struct A6XX_RB_2D_DST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_2D_DST_PITCH {
    uint32_t							a6xx_rb_2d_dst_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_PITCH(struct A6XX_RB_2D_DST_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_2d_dst_pitch >> 6)       & 0xffff0000) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_PITCH,
        .value =
            ((fields.a6xx_rb_2d_dst_pitch >> 6)       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_DST_PITCH(...) pack_A6XX_RB_2D_DST_PITCH((struct A6XX_RB_2D_DST_PITCH) { __VA_ARGS__ })

struct A6XX_RB_2D_DST_PLANE1 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_PLANE1(struct A6XX_RB_2D_DST_PLANE1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_PLANE1,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_2D_DST_PLANE1(...) pack_A6XX_RB_2D_DST_PLANE1((struct A6XX_RB_2D_DST_PLANE1) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_2D_DST_PLANE_PITCH {
    uint32_t							a6xx_rb_2d_dst_plane_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_PLANE_PITCH(struct A6XX_RB_2D_DST_PLANE_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_2d_dst_plane_pitch >> 6) & 0xffff0000) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_PLANE_PITCH,
        .value =
            ((fields.a6xx_rb_2d_dst_plane_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_DST_PLANE_PITCH(...) pack_A6XX_RB_2D_DST_PLANE_PITCH((struct A6XX_RB_2D_DST_PLANE_PITCH) { __VA_ARGS__ })

struct A6XX_RB_2D_DST_PLANE2 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_PLANE2(struct A6XX_RB_2D_DST_PLANE2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_PLANE2,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_2D_DST_PLANE2(...) pack_A6XX_RB_2D_DST_PLANE2((struct A6XX_RB_2D_DST_PLANE2) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_2D_DST_FLAGS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_FLAGS(struct A6XX_RB_2D_DST_FLAGS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_FLAGS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_2D_DST_FLAGS(...) pack_A6XX_RB_2D_DST_FLAGS((struct A6XX_RB_2D_DST_FLAGS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_2D_DST_FLAGS_PITCH {
    uint32_t							a6xx_rb_2d_dst_flags_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_FLAGS_PITCH(struct A6XX_RB_2D_DST_FLAGS_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_2d_dst_flags_pitch >> 6) & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_FLAGS_PITCH,
        .value =
            ((fields.a6xx_rb_2d_dst_flags_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_DST_FLAGS_PITCH(...) pack_A6XX_RB_2D_DST_FLAGS_PITCH((struct A6XX_RB_2D_DST_FLAGS_PITCH) { __VA_ARGS__ })

struct A6XX_RB_2D_DST_FLAGS_PLANE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_FLAGS_PLANE(struct A6XX_RB_2D_DST_FLAGS_PLANE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_FLAGS_PLANE,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_2D_DST_FLAGS_PLANE(...) pack_A6XX_RB_2D_DST_FLAGS_PLANE((struct A6XX_RB_2D_DST_FLAGS_PLANE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_RB_2D_DST_FLAGS_PLANE_PITCH {
    uint32_t							a6xx_rb_2d_dst_flags_plane_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_DST_FLAGS_PLANE_PITCH(struct A6XX_RB_2D_DST_FLAGS_PLANE_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_rb_2d_dst_flags_plane_pitch >> 6) & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_DST_FLAGS_PLANE_PITCH,
        .value =
            ((fields.a6xx_rb_2d_dst_flags_plane_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_DST_FLAGS_PLANE_PITCH(...) pack_A6XX_RB_2D_DST_FLAGS_PLANE_PITCH((struct A6XX_RB_2D_DST_FLAGS_PLANE_PITCH) { __VA_ARGS__ })

struct A6XX_RB_2D_SRC_SOLID_C0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_SRC_SOLID_C0(struct A6XX_RB_2D_SRC_SOLID_C0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_SRC_SOLID_C0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_SRC_SOLID_C0(...) pack_A6XX_RB_2D_SRC_SOLID_C0((struct A6XX_RB_2D_SRC_SOLID_C0) { __VA_ARGS__ })

struct A6XX_RB_2D_SRC_SOLID_C1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_SRC_SOLID_C1(struct A6XX_RB_2D_SRC_SOLID_C1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_SRC_SOLID_C1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_SRC_SOLID_C1(...) pack_A6XX_RB_2D_SRC_SOLID_C1((struct A6XX_RB_2D_SRC_SOLID_C1) { __VA_ARGS__ })

struct A6XX_RB_2D_SRC_SOLID_C2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_SRC_SOLID_C2(struct A6XX_RB_2D_SRC_SOLID_C2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_SRC_SOLID_C2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_SRC_SOLID_C2(...) pack_A6XX_RB_2D_SRC_SOLID_C2((struct A6XX_RB_2D_SRC_SOLID_C2) { __VA_ARGS__ })

struct A6XX_RB_2D_SRC_SOLID_C3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_2D_SRC_SOLID_C3(struct A6XX_RB_2D_SRC_SOLID_C3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_2D_SRC_SOLID_C3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_2D_SRC_SOLID_C3(...) pack_A6XX_RB_2D_SRC_SOLID_C3((struct A6XX_RB_2D_SRC_SOLID_C3) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8E01 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8E01(struct A6XX_RB_UNKNOWN_8E01 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8E01,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8E01(...) pack_A6XX_RB_UNKNOWN_8E01((struct A6XX_RB_UNKNOWN_8E01) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8E04 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8E04(struct A6XX_RB_UNKNOWN_8E04 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8E04,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8E04(...) pack_A6XX_RB_UNKNOWN_8E04((struct A6XX_RB_UNKNOWN_8E04) { __VA_ARGS__ })

struct A6XX_RB_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_rb_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_ADDR_MODE_CNTL(struct A6XX_RB_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_rb_addr_mode_cntl            & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_rb_addr_mode_cntl            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_ADDR_MODE_CNTL(...) pack_A6XX_RB_ADDR_MODE_CNTL((struct A6XX_RB_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_RB_CCU_CNTL {
    uint32_t							color_offset;
    uint32_t							depth_offset;
    bool							gmem;
    bool							concurrent_resolve;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_CCU_CNTL(struct A6XX_RB_CCU_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.color_offset >> 12)              & 0xfffffe00) == 0);
    assert(((fields.depth_offset >> 12)              & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0xffdff004) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_CCU_CNTL,
        .value =
            ((fields.color_offset >> 12)              << 23) |
            ((fields.depth_offset >> 12)              << 12) |
            (fields.gmem                              << 22) |
            (fields.concurrent_resolve                <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_CCU_CNTL(...) pack_A6XX_RB_CCU_CNTL((struct A6XX_RB_CCU_CNTL) { __VA_ARGS__ })

struct A6XX_RB_NC_MODE_CNTL {
    bool							mode;
    uint32_t							lower_bit;
    bool							min_access_length;
    bool							amsbc;
    uint32_t							upper_bit;
    bool							rgb565_predicator;
    uint32_t							unk12;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_NC_MODE_CNTL(struct A6XX_RB_NC_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.lower_bit                         & 0xfffffffc) == 0);
    assert((fields.upper_bit                         & 0xfffffffe) == 0);
    assert((fields.unk12                             & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00003c1f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_NC_MODE_CNTL,
        .value =
            (fields.mode                              <<  0) |
            (fields.lower_bit                         <<  1) |
            (fields.min_access_length                 <<  3) |
            (fields.amsbc                             <<  4) |
            (fields.upper_bit                         << 10) |
            (fields.rgb565_predicator                 << 11) |
            (fields.unk12                             << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_NC_MODE_CNTL(...) pack_A6XX_RB_NC_MODE_CNTL((struct A6XX_RB_NC_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8E28 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8E28(struct A6XX_RB_UNKNOWN_8E28 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8E28,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8E28(...) pack_A6XX_RB_UNKNOWN_8E28((struct A6XX_RB_UNKNOWN_8E28) { __VA_ARGS__ })

struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST(struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST(...) pack_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST((struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_HOST) { __VA_ARGS__ })

struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD(struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD(...) pack_A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD((struct A6XX_RB_RB_SUB_BLOCK_SEL_CNTL_CD) { __VA_ARGS__ })

struct A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE {
    bool							a6xx_rb_context_switch_gmem_save_restore;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE(struct A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE,
        .value =
            (fields.a6xx_rb_context_switch_gmem_save_restore <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE(...) pack_A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE((struct A6XX_RB_CONTEXT_SWITCH_GMEM_SAVE_RESTORE) { __VA_ARGS__ })

struct A6XX_RB_UNKNOWN_8E51 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_RB_UNKNOWN_8E51(struct A6XX_RB_UNKNOWN_8E51 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_RB_UNKNOWN_8E51,
        .value =
            fields.unknown | fields.dword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_RB_UNKNOWN_8E51(...) pack_A6XX_RB_UNKNOWN_8E51((struct A6XX_RB_UNKNOWN_8E51) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VPC_GS_PARAM {
    uint32_t							linelengthloc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_GS_PARAM(struct A6XX_VPC_GS_PARAM fields)
{
#ifndef NDEBUG
    assert((fields.linelengthloc                     & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_GS_PARAM,
        .value =
            (fields.linelengthloc                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_GS_PARAM(...) pack_A6XX_VPC_GS_PARAM((struct A6XX_VPC_GS_PARAM) { __VA_ARGS__ })

struct A6XX_VPC_VS_CLIP_CNTL {
    uint32_t							clip_mask;
    uint32_t							clip_dist_03_loc;
    uint32_t							clip_dist_47_loc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VS_CLIP_CNTL(struct A6XX_VPC_VS_CLIP_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.clip_dist_03_loc                  & 0xffffff00) == 0);
    assert((fields.clip_dist_47_loc                  & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VS_CLIP_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.clip_dist_03_loc                  <<  8) |
            (fields.clip_dist_47_loc                  << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VS_CLIP_CNTL(...) pack_A6XX_VPC_VS_CLIP_CNTL((struct A6XX_VPC_VS_CLIP_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_GS_CLIP_CNTL {
    uint32_t							clip_mask;
    uint32_t							clip_dist_03_loc;
    uint32_t							clip_dist_47_loc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_GS_CLIP_CNTL(struct A6XX_VPC_GS_CLIP_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.clip_dist_03_loc                  & 0xffffff00) == 0);
    assert((fields.clip_dist_47_loc                  & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_GS_CLIP_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.clip_dist_03_loc                  <<  8) |
            (fields.clip_dist_47_loc                  << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_GS_CLIP_CNTL(...) pack_A6XX_VPC_GS_CLIP_CNTL((struct A6XX_VPC_GS_CLIP_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_DS_CLIP_CNTL {
    uint32_t							clip_mask;
    uint32_t							clip_dist_03_loc;
    uint32_t							clip_dist_47_loc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_DS_CLIP_CNTL(struct A6XX_VPC_DS_CLIP_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.clip_dist_03_loc                  & 0xffffff00) == 0);
    assert((fields.clip_dist_47_loc                  & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_DS_CLIP_CNTL,
        .value =
            (fields.clip_mask                         <<  0) |
            (fields.clip_dist_03_loc                  <<  8) |
            (fields.clip_dist_47_loc                  << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_DS_CLIP_CNTL(...) pack_A6XX_VPC_DS_CLIP_CNTL((struct A6XX_VPC_DS_CLIP_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_VS_LAYER_CNTL {
    uint32_t							layerloc;
    uint32_t							viewloc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VS_LAYER_CNTL(struct A6XX_VPC_VS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.layerloc                          & 0xffffff00) == 0);
    assert((fields.viewloc                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VS_LAYER_CNTL,
        .value =
            (fields.layerloc                          <<  0) |
            (fields.viewloc                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VS_LAYER_CNTL(...) pack_A6XX_VPC_VS_LAYER_CNTL((struct A6XX_VPC_VS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_GS_LAYER_CNTL {
    uint32_t							layerloc;
    uint32_t							viewloc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_GS_LAYER_CNTL(struct A6XX_VPC_GS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.layerloc                          & 0xffffff00) == 0);
    assert((fields.viewloc                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_GS_LAYER_CNTL,
        .value =
            (fields.layerloc                          <<  0) |
            (fields.viewloc                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_GS_LAYER_CNTL(...) pack_A6XX_VPC_GS_LAYER_CNTL((struct A6XX_VPC_GS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_DS_LAYER_CNTL {
    uint32_t							layerloc;
    uint32_t							viewloc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_DS_LAYER_CNTL(struct A6XX_VPC_DS_LAYER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.layerloc                          & 0xffffff00) == 0);
    assert((fields.viewloc                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_DS_LAYER_CNTL,
        .value =
            (fields.layerloc                          <<  0) |
            (fields.viewloc                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_DS_LAYER_CNTL(...) pack_A6XX_VPC_DS_LAYER_CNTL((struct A6XX_VPC_DS_LAYER_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9107 {
    bool							raster_discard;
    bool							unk2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9107(struct A6XX_VPC_UNKNOWN_9107 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000005) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9107,
        .value =
            (fields.raster_discard                    <<  0) |
            (fields.unk2                              <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9107(...) pack_A6XX_VPC_UNKNOWN_9107((struct A6XX_VPC_UNKNOWN_9107) { __VA_ARGS__ })

struct A6XX_VPC_POLYGON_MODE {
    enum a6xx_polygon_mode					mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_POLYGON_MODE(struct A6XX_VPC_POLYGON_MODE fields)
{
#ifndef NDEBUG
    assert((fields.mode                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_POLYGON_MODE,
        .value =
            (fields.mode                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_POLYGON_MODE(...) pack_A6XX_VPC_POLYGON_MODE((struct A6XX_VPC_POLYGON_MODE) { __VA_ARGS__ })

struct A6XX_VPC_VARYING_INTERP_MODE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VARYING_INTERP_MODE(uint32_t i, struct A6XX_VPC_VARYING_INTERP_MODE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VARYING_INTERP_MODE(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VARYING_INTERP_MODE(i, ...) pack_A6XX_VPC_VARYING_INTERP_MODE(i, (struct A6XX_VPC_VARYING_INTERP_MODE) { __VA_ARGS__ })

struct A6XX_VPC_VARYING_PS_REPL_MODE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VARYING_PS_REPL_MODE(uint32_t i, struct A6XX_VPC_VARYING_PS_REPL_MODE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VARYING_PS_REPL_MODE(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VARYING_PS_REPL_MODE(i, ...) pack_A6XX_VPC_VARYING_PS_REPL_MODE(i, (struct A6XX_VPC_VARYING_PS_REPL_MODE) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9210 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9210(struct A6XX_VPC_UNKNOWN_9210 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9210,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9210(...) pack_A6XX_VPC_UNKNOWN_9210((struct A6XX_VPC_UNKNOWN_9210) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9211 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9211(struct A6XX_VPC_UNKNOWN_9211 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9211,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9211(...) pack_A6XX_VPC_UNKNOWN_9211((struct A6XX_VPC_UNKNOWN_9211) { __VA_ARGS__ })

struct A6XX_VPC_VAR_DISABLE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VAR_DISABLE(uint32_t i, struct A6XX_VPC_VAR_DISABLE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VAR_DISABLE(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VAR_DISABLE(i, ...) pack_A6XX_VPC_VAR_DISABLE(i, (struct A6XX_VPC_VAR_DISABLE) { __VA_ARGS__ })

struct A6XX_VPC_SO_CNTL {
    uint32_t							addr;
    bool							reset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_CNTL(struct A6XX_VPC_SO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.addr                              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000100ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_CNTL,
        .value =
            (fields.addr                              <<  0) |
            (fields.reset                             << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_CNTL(...) pack_A6XX_VPC_SO_CNTL((struct A6XX_VPC_SO_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_SO_PROG {
    uint32_t							a_buf;
    uint32_t							a_off;
    bool							a_en;
    uint32_t							b_buf;
    uint32_t							b_off;
    bool							b_en;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_PROG(struct A6XX_VPC_SO_PROG fields)
{
#ifndef NDEBUG
    assert((fields.a_buf                             & 0xfffffffc) == 0);
    assert(((fields.a_off >> 2)                      & 0xfffffe00) == 0);
    assert((fields.b_buf                             & 0xfffffffc) == 0);
    assert(((fields.b_off >> 2)                      & 0xfffffe00) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_PROG,
        .value =
            (fields.a_buf                             <<  0) |
            ((fields.a_off >> 2)                      <<  2) |
            (fields.a_en                              << 11) |
            (fields.b_buf                             << 12) |
            ((fields.b_off >> 2)                      << 14) |
            (fields.b_en                              << 23) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_PROG(...) pack_A6XX_VPC_SO_PROG((struct A6XX_VPC_SO_PROG) { __VA_ARGS__ })

struct A6XX_VPC_SO_STREAM_COUNTS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_STREAM_COUNTS(struct A6XX_VPC_SO_STREAM_COUNTS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_STREAM_COUNTS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VPC_SO_STREAM_COUNTS(...) pack_A6XX_VPC_SO_STREAM_COUNTS((struct A6XX_VPC_SO_STREAM_COUNTS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VPC_SO_BUFFER_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_BUFFER_BASE(uint32_t i, struct A6XX_VPC_SO_BUFFER_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_BUFFER_BASE(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VPC_SO_BUFFER_BASE(i, ...) pack_A6XX_VPC_SO_BUFFER_BASE(i, (struct A6XX_VPC_SO_BUFFER_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VPC_SO_BUFFER_SIZE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_BUFFER_SIZE(uint32_t i, struct A6XX_VPC_SO_BUFFER_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_BUFFER_SIZE(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_BUFFER_SIZE(i, ...) pack_A6XX_VPC_SO_BUFFER_SIZE(i, (struct A6XX_VPC_SO_BUFFER_SIZE) { __VA_ARGS__ })

struct A6XX_VPC_SO_BUFFER_STRIDE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_BUFFER_STRIDE(uint32_t i, struct A6XX_VPC_SO_BUFFER_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_BUFFER_STRIDE(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_BUFFER_STRIDE(i, ...) pack_A6XX_VPC_SO_BUFFER_STRIDE(i, (struct A6XX_VPC_SO_BUFFER_STRIDE) { __VA_ARGS__ })

struct A6XX_VPC_SO_BUFFER_OFFSET {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_BUFFER_OFFSET(uint32_t i, struct A6XX_VPC_SO_BUFFER_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_BUFFER_OFFSET(i),
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_BUFFER_OFFSET(i, ...) pack_A6XX_VPC_SO_BUFFER_OFFSET(i, (struct A6XX_VPC_SO_BUFFER_OFFSET) { __VA_ARGS__ })

struct A6XX_VPC_SO_FLUSH_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_FLUSH_BASE(uint32_t i, struct A6XX_VPC_SO_FLUSH_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_FLUSH_BASE(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VPC_SO_FLUSH_BASE(i, ...) pack_A6XX_VPC_SO_FLUSH_BASE(i, (struct A6XX_VPC_SO_FLUSH_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VPC_POINT_COORD_INVERT {
    bool							invert;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_POINT_COORD_INVERT(struct A6XX_VPC_POINT_COORD_INVERT fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_POINT_COORD_INVERT,
        .value =
            (fields.invert                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_POINT_COORD_INVERT(...) pack_A6XX_VPC_POINT_COORD_INVERT((struct A6XX_VPC_POINT_COORD_INVERT) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9300 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9300(struct A6XX_VPC_UNKNOWN_9300 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9300,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9300(...) pack_A6XX_VPC_UNKNOWN_9300((struct A6XX_VPC_UNKNOWN_9300) { __VA_ARGS__ })

struct A6XX_VPC_VS_PACK {
    uint32_t							stride_in_vpc;
    uint32_t							positionloc;
    uint32_t							psizeloc;
    uint32_t							extrapos;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_VS_PACK(struct A6XX_VPC_VS_PACK fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.positionloc                       & 0xffffff00) == 0);
    assert((fields.psizeloc                          & 0xffffff00) == 0);
    assert((fields.extrapos                          & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_VS_PACK,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.positionloc                       <<  8) |
            (fields.psizeloc                          << 16) |
            (fields.extrapos                          << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_VS_PACK(...) pack_A6XX_VPC_VS_PACK((struct A6XX_VPC_VS_PACK) { __VA_ARGS__ })

struct A6XX_VPC_GS_PACK {
    uint32_t							stride_in_vpc;
    uint32_t							positionloc;
    uint32_t							psizeloc;
    uint32_t							extrapos;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_GS_PACK(struct A6XX_VPC_GS_PACK fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.positionloc                       & 0xffffff00) == 0);
    assert((fields.psizeloc                          & 0xffffff00) == 0);
    assert((fields.extrapos                          & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_GS_PACK,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.positionloc                       <<  8) |
            (fields.psizeloc                          << 16) |
            (fields.extrapos                          << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_GS_PACK(...) pack_A6XX_VPC_GS_PACK((struct A6XX_VPC_GS_PACK) { __VA_ARGS__ })

struct A6XX_VPC_DS_PACK {
    uint32_t							stride_in_vpc;
    uint32_t							positionloc;
    uint32_t							psizeloc;
    uint32_t							extrapos;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_DS_PACK(struct A6XX_VPC_DS_PACK fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.positionloc                       & 0xffffff00) == 0);
    assert((fields.psizeloc                          & 0xffffff00) == 0);
    assert((fields.extrapos                          & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_DS_PACK,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.positionloc                       <<  8) |
            (fields.psizeloc                          << 16) |
            (fields.extrapos                          << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_DS_PACK(...) pack_A6XX_VPC_DS_PACK((struct A6XX_VPC_DS_PACK) { __VA_ARGS__ })

struct A6XX_VPC_CNTL_0 {
    uint32_t							numnonposvar;
    uint32_t							primidloc;
    bool							varying;
    uint32_t							viewidloc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_CNTL_0(struct A6XX_VPC_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.numnonposvar                      & 0xffffff00) == 0);
    assert((fields.primidloc                         & 0xffffff00) == 0);
    assert((fields.viewidloc                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff01ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_CNTL_0,
        .value =
            (fields.numnonposvar                      <<  0) |
            (fields.primidloc                         <<  8) |
            (fields.varying                           << 16) |
            (fields.viewidloc                         << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_CNTL_0(...) pack_A6XX_VPC_CNTL_0((struct A6XX_VPC_CNTL_0) { __VA_ARGS__ })

struct A6XX_VPC_SO_STREAM_CNTL {
    uint32_t							buf0_stream;
    uint32_t							buf1_stream;
    uint32_t							buf2_stream;
    uint32_t							buf3_stream;
    uint32_t							stream_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_STREAM_CNTL(struct A6XX_VPC_SO_STREAM_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.buf0_stream                       & 0xfffffff8) == 0);
    assert((fields.buf1_stream                       & 0xfffffff8) == 0);
    assert((fields.buf2_stream                       & 0xfffffff8) == 0);
    assert((fields.buf3_stream                       & 0xfffffff8) == 0);
    assert((fields.stream_enable                     & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x00078fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_STREAM_CNTL,
        .value =
            (fields.buf0_stream                       <<  0) |
            (fields.buf1_stream                       <<  3) |
            (fields.buf2_stream                       <<  6) |
            (fields.buf3_stream                       <<  9) |
            (fields.stream_enable                     << 15) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_STREAM_CNTL(...) pack_A6XX_VPC_SO_STREAM_CNTL((struct A6XX_VPC_SO_STREAM_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_SO_DISABLE {
    bool							disable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_SO_DISABLE(struct A6XX_VPC_SO_DISABLE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_SO_DISABLE,
        .value =
            (fields.disable                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_SO_DISABLE(...) pack_A6XX_VPC_SO_DISABLE((struct A6XX_VPC_SO_DISABLE) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9600 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9600(struct A6XX_VPC_UNKNOWN_9600 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9600,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9600(...) pack_A6XX_VPC_UNKNOWN_9600((struct A6XX_VPC_UNKNOWN_9600) { __VA_ARGS__ })

struct A6XX_VPC_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_vpc_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_ADDR_MODE_CNTL(struct A6XX_VPC_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vpc_addr_mode_cntl           & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_vpc_addr_mode_cntl           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_ADDR_MODE_CNTL(...) pack_A6XX_VPC_ADDR_MODE_CNTL((struct A6XX_VPC_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9602 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9602(struct A6XX_VPC_UNKNOWN_9602 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9602,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9602(...) pack_A6XX_VPC_UNKNOWN_9602((struct A6XX_VPC_UNKNOWN_9602) { __VA_ARGS__ })

struct A6XX_VPC_UNKNOWN_9603 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VPC_UNKNOWN_9603(struct A6XX_VPC_UNKNOWN_9603 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VPC_UNKNOWN_9603,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VPC_UNKNOWN_9603(...) pack_A6XX_VPC_UNKNOWN_9603((struct A6XX_VPC_UNKNOWN_9603) { __VA_ARGS__ })

struct A6XX_PC_TESS_NUM_VERTEX {
    uint32_t							a6xx_pc_tess_num_vertex;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_TESS_NUM_VERTEX(struct A6XX_PC_TESS_NUM_VERTEX fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_tess_num_vertex           & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x0000003f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_TESS_NUM_VERTEX,
        .value =
            (fields.a6xx_pc_tess_num_vertex           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_TESS_NUM_VERTEX(...) pack_A6XX_PC_TESS_NUM_VERTEX((struct A6XX_PC_TESS_NUM_VERTEX) { __VA_ARGS__ })

struct A6XX_PC_HS_INPUT_SIZE {
    uint32_t							size;
    uint32_t							unk13;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_HS_INPUT_SIZE(struct A6XX_PC_HS_INPUT_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.size                              & 0xfffff800) == 0);
    assert((fields.unk13                             & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x000027ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_HS_INPUT_SIZE,
        .value =
            (fields.size                              <<  0) |
            (fields.unk13                             << 13) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_HS_INPUT_SIZE(...) pack_A6XX_PC_HS_INPUT_SIZE((struct A6XX_PC_HS_INPUT_SIZE) { __VA_ARGS__ })

struct A6XX_PC_TESS_CNTL {
    enum a6xx_tess_spacing					spacing;
    enum a6xx_tess_output					output;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_TESS_CNTL(struct A6XX_PC_TESS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.spacing                           & 0xfffffffc) == 0);
    assert((fields.output                            & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_TESS_CNTL,
        .value =
            (fields.spacing                           <<  0) |
            (fields.output                            <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_TESS_CNTL(...) pack_A6XX_PC_TESS_CNTL((struct A6XX_PC_TESS_CNTL) { __VA_ARGS__ })

struct A6XX_PC_RESTART_INDEX {
    uint32_t							a6xx_pc_restart_index;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_RESTART_INDEX(struct A6XX_PC_RESTART_INDEX fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_restart_index             & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_RESTART_INDEX,
        .value =
            (fields.a6xx_pc_restart_index             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_RESTART_INDEX(...) pack_A6XX_PC_RESTART_INDEX((struct A6XX_PC_RESTART_INDEX) { __VA_ARGS__ })

struct A6XX_PC_MODE_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_MODE_CNTL(struct A6XX_PC_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_MODE_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_MODE_CNTL(...) pack_A6XX_PC_MODE_CNTL((struct A6XX_PC_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_PC_POWER_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_POWER_CNTL(struct A6XX_PC_POWER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_POWER_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_POWER_CNTL(...) pack_A6XX_PC_POWER_CNTL((struct A6XX_PC_POWER_CNTL) { __VA_ARGS__ })

struct A6XX_PC_PRIMID_PASSTHRU {
    bool							a6xx_pc_primid_passthru;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_PRIMID_PASSTHRU(struct A6XX_PC_PRIMID_PASSTHRU fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_PRIMID_PASSTHRU,
        .value =
            (fields.a6xx_pc_primid_passthru           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_PRIMID_PASSTHRU(...) pack_A6XX_PC_PRIMID_PASSTHRU((struct A6XX_PC_PRIMID_PASSTHRU) { __VA_ARGS__ })

struct A6XX_PC_SO_STREAM_CNTL {
    bool							stream_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_SO_STREAM_CNTL(struct A6XX_PC_SO_STREAM_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00008000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_SO_STREAM_CNTL,
        .value =
            (fields.stream_enable                     << 15) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_SO_STREAM_CNTL(...) pack_A6XX_PC_SO_STREAM_CNTL((struct A6XX_PC_SO_STREAM_CNTL) { __VA_ARGS__ })

struct A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL {
    bool							conservativerasen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL(struct A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL,
        .value =
            (fields.conservativerasen                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL(...) pack_A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL((struct A6XX_PC_DGEN_SU_CONSERVATIVE_RAS_CNTL) { __VA_ARGS__ })

struct A6XX_PC_DRAW_CMD {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_CMD(struct A6XX_PC_DRAW_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_CMD,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_CMD(...) pack_A6XX_PC_DRAW_CMD((struct A6XX_PC_DRAW_CMD) { __VA_ARGS__ })

struct A6XX_PC_DISPATCH_CMD {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DISPATCH_CMD(struct A6XX_PC_DISPATCH_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DISPATCH_CMD,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DISPATCH_CMD(...) pack_A6XX_PC_DISPATCH_CMD((struct A6XX_PC_DISPATCH_CMD) { __VA_ARGS__ })

struct A6XX_PC_EVENT_CMD {
    uint32_t							state_id;
    enum vgt_event_type						event;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_EVENT_CMD(struct A6XX_PC_EVENT_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.event                             & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x00ff007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_EVENT_CMD,
        .value =
            (fields.state_id                          << 16) |
            (fields.event                             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_EVENT_CMD(...) pack_A6XX_PC_EVENT_CMD((struct A6XX_PC_EVENT_CMD) { __VA_ARGS__ })

struct A6XX_PC_MARKER {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_MARKER(struct A6XX_PC_MARKER fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_MARKER,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_MARKER(...) pack_A6XX_PC_MARKER((struct A6XX_PC_MARKER) { __VA_ARGS__ })

struct A6XX_PC_POLYGON_MODE {
    enum a6xx_polygon_mode					mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_POLYGON_MODE(struct A6XX_PC_POLYGON_MODE fields)
{
#ifndef NDEBUG
    assert((fields.mode                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_POLYGON_MODE,
        .value =
            (fields.mode                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_POLYGON_MODE(...) pack_A6XX_PC_POLYGON_MODE((struct A6XX_PC_POLYGON_MODE) { __VA_ARGS__ })

struct A6XX_PC_RASTER_CNTL {
    uint32_t							stream;
    bool							discard;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_RASTER_CNTL(struct A6XX_PC_RASTER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.stream                            & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_RASTER_CNTL,
        .value =
            (fields.stream                            <<  0) |
            (fields.discard                           <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_RASTER_CNTL(...) pack_A6XX_PC_RASTER_CNTL((struct A6XX_PC_RASTER_CNTL) { __VA_ARGS__ })

struct A6XX_PC_PRIMITIVE_CNTL_0 {
    bool							primitive_restart;
    bool							provoking_vtx_last;
    bool							tess_upper_left_domain_origin;
    bool							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_PRIMITIVE_CNTL_0(struct A6XX_PC_PRIMITIVE_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_PRIMITIVE_CNTL_0,
        .value =
            (fields.primitive_restart                 <<  0) |
            (fields.provoking_vtx_last                <<  1) |
            (fields.tess_upper_left_domain_origin     <<  2) |
            (fields.unk3                              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_PRIMITIVE_CNTL_0(...) pack_A6XX_PC_PRIMITIVE_CNTL_0((struct A6XX_PC_PRIMITIVE_CNTL_0) { __VA_ARGS__ })

struct A6XX_PC_VS_OUT_CNTL {
    uint32_t							stride_in_vpc;
    bool							psize;
    bool							layer;
    bool							view;
    bool							primitive_id;
    uint32_t							clip_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_VS_OUT_CNTL(struct A6XX_PC_VS_OUT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_VS_OUT_CNTL,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.psize                             <<  8) |
            (fields.layer                             <<  9) |
            (fields.view                              << 10) |
            (fields.primitive_id                      << 11) |
            (fields.clip_mask                         << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_VS_OUT_CNTL(...) pack_A6XX_PC_VS_OUT_CNTL((struct A6XX_PC_VS_OUT_CNTL) { __VA_ARGS__ })

struct A6XX_PC_GS_OUT_CNTL {
    uint32_t							stride_in_vpc;
    bool							psize;
    bool							layer;
    bool							view;
    bool							primitive_id;
    uint32_t							clip_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_GS_OUT_CNTL(struct A6XX_PC_GS_OUT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_GS_OUT_CNTL,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.psize                             <<  8) |
            (fields.layer                             <<  9) |
            (fields.view                              << 10) |
            (fields.primitive_id                      << 11) |
            (fields.clip_mask                         << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_GS_OUT_CNTL(...) pack_A6XX_PC_GS_OUT_CNTL((struct A6XX_PC_GS_OUT_CNTL) { __VA_ARGS__ })

struct A6XX_PC_HS_OUT_CNTL {
    uint32_t							stride_in_vpc;
    bool							psize;
    bool							layer;
    bool							view;
    bool							primitive_id;
    uint32_t							clip_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_HS_OUT_CNTL(struct A6XX_PC_HS_OUT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_HS_OUT_CNTL,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.psize                             <<  8) |
            (fields.layer                             <<  9) |
            (fields.view                              << 10) |
            (fields.primitive_id                      << 11) |
            (fields.clip_mask                         << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_HS_OUT_CNTL(...) pack_A6XX_PC_HS_OUT_CNTL((struct A6XX_PC_HS_OUT_CNTL) { __VA_ARGS__ })

struct A6XX_PC_DS_OUT_CNTL {
    uint32_t							stride_in_vpc;
    bool							psize;
    bool							layer;
    bool							view;
    bool							primitive_id;
    uint32_t							clip_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DS_OUT_CNTL(struct A6XX_PC_DS_OUT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xffffff00) == 0);
    assert((fields.clip_mask                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00ff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DS_OUT_CNTL,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            (fields.psize                             <<  8) |
            (fields.layer                             <<  9) |
            (fields.view                              << 10) |
            (fields.primitive_id                      << 11) |
            (fields.clip_mask                         << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DS_OUT_CNTL(...) pack_A6XX_PC_DS_OUT_CNTL((struct A6XX_PC_DS_OUT_CNTL) { __VA_ARGS__ })

struct A6XX_PC_PRIMITIVE_CNTL_5 {
    uint32_t							gs_vertices_out;
    uint32_t							gs_invocations;
    bool							linelengthen;
    enum a6xx_tess_output					gs_output;
    uint32_t							unk18;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_PRIMITIVE_CNTL_5(struct A6XX_PC_PRIMITIVE_CNTL_5 fields)
{
#ifndef NDEBUG
    assert((fields.gs_vertices_out                   & 0xffffff00) == 0);
    assert((fields.gs_invocations                    & 0xffffffe0) == 0);
    assert((fields.gs_output                         & 0xfffffffc) == 0);
    assert((fields.unk18                             & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x0007fcff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_PRIMITIVE_CNTL_5,
        .value =
            (fields.gs_vertices_out                   <<  0) |
            (fields.gs_invocations                    << 10) |
            (fields.linelengthen                      << 15) |
            (fields.gs_output                         << 16) |
            (fields.unk18                             << 18) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_PRIMITIVE_CNTL_5(...) pack_A6XX_PC_PRIMITIVE_CNTL_5((struct A6XX_PC_PRIMITIVE_CNTL_5) { __VA_ARGS__ })

struct A6XX_PC_PRIMITIVE_CNTL_6 {
    uint32_t							stride_in_vpc;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_PRIMITIVE_CNTL_6(struct A6XX_PC_PRIMITIVE_CNTL_6 fields)
{
#ifndef NDEBUG
    assert((fields.stride_in_vpc                     & 0xfffff800) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_PRIMITIVE_CNTL_6,
        .value =
            (fields.stride_in_vpc                     <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_PRIMITIVE_CNTL_6(...) pack_A6XX_PC_PRIMITIVE_CNTL_6((struct A6XX_PC_PRIMITIVE_CNTL_6) { __VA_ARGS__ })

struct A6XX_PC_MULTIVIEW_CNTL {
    bool							enable;
    bool							disablemultipos;
    uint32_t							views;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_MULTIVIEW_CNTL(struct A6XX_PC_MULTIVIEW_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.views                             & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_MULTIVIEW_CNTL,
        .value =
            (fields.enable                            <<  0) |
            (fields.disablemultipos                   <<  1) |
            (fields.views                             <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_MULTIVIEW_CNTL(...) pack_A6XX_PC_MULTIVIEW_CNTL((struct A6XX_PC_MULTIVIEW_CNTL) { __VA_ARGS__ })

struct A6XX_PC_MULTIVIEW_MASK {
    uint32_t							a6xx_pc_multiview_mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_MULTIVIEW_MASK(struct A6XX_PC_MULTIVIEW_MASK fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_multiview_mask            & 0xffff0000) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_MULTIVIEW_MASK,
        .value =
            (fields.a6xx_pc_multiview_mask            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_MULTIVIEW_MASK(...) pack_A6XX_PC_MULTIVIEW_MASK((struct A6XX_PC_MULTIVIEW_MASK) { __VA_ARGS__ })

struct A6XX_PC_2D_EVENT_CMD {
    enum vgt_event_type						event;
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_2D_EVENT_CMD(struct A6XX_PC_2D_EVENT_CMD fields)
{
#ifndef NDEBUG
    assert((fields.event                             & 0xffffff80) == 0);
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ff7f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_2D_EVENT_CMD,
        .value =
            (fields.event                             <<  0) |
            (fields.state_id                          <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_2D_EVENT_CMD(...) pack_A6XX_PC_2D_EVENT_CMD((struct A6XX_PC_2D_EVENT_CMD) { __VA_ARGS__ })

struct A6XX_PC_DBG_ECO_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DBG_ECO_CNTL(struct A6XX_PC_DBG_ECO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DBG_ECO_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DBG_ECO_CNTL(...) pack_A6XX_PC_DBG_ECO_CNTL((struct A6XX_PC_DBG_ECO_CNTL) { __VA_ARGS__ })

struct A6XX_PC_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_pc_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_ADDR_MODE_CNTL(struct A6XX_PC_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_addr_mode_cntl            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_pc_addr_mode_cntl            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_ADDR_MODE_CNTL(...) pack_A6XX_PC_ADDR_MODE_CNTL((struct A6XX_PC_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_PC_DRAW_INDX_BASE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_INDX_BASE(struct A6XX_PC_DRAW_INDX_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_INDX_BASE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_INDX_BASE(...) pack_A6XX_PC_DRAW_INDX_BASE((struct A6XX_PC_DRAW_INDX_BASE) { __VA_ARGS__ })

struct A6XX_PC_DRAW_FIRST_INDX {
    uint32_t							a6xx_pc_draw_first_indx;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_FIRST_INDX(struct A6XX_PC_DRAW_FIRST_INDX fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_draw_first_indx           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_FIRST_INDX,
        .value =
            (fields.a6xx_pc_draw_first_indx           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_FIRST_INDX(...) pack_A6XX_PC_DRAW_FIRST_INDX((struct A6XX_PC_DRAW_FIRST_INDX) { __VA_ARGS__ })

struct A6XX_PC_DRAW_MAX_INDICES {
    uint32_t							a6xx_pc_draw_max_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_MAX_INDICES(struct A6XX_PC_DRAW_MAX_INDICES fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_draw_max_indices          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_MAX_INDICES,
        .value =
            (fields.a6xx_pc_draw_max_indices          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_MAX_INDICES(...) pack_A6XX_PC_DRAW_MAX_INDICES((struct A6XX_PC_DRAW_MAX_INDICES) { __VA_ARGS__ })

struct A6XX_PC_TESSFACTOR_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_TESSFACTOR_ADDR(struct A6XX_PC_TESSFACTOR_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_TESSFACTOR_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_PC_TESSFACTOR_ADDR(...) pack_A6XX_PC_TESSFACTOR_ADDR((struct A6XX_PC_TESSFACTOR_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_PC_DRAW_INITIATOR {
    enum pc_di_primtype						prim_type;
    enum pc_di_src_sel						source_select;
    enum pc_di_vis_cull_mode					vis_cull;
    enum a4xx_index_size					index_size;
    enum a6xx_patch_type					patch_type;
    bool							gs_enable;
    bool							tess_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_INITIATOR(struct A6XX_PC_DRAW_INITIATOR fields)
{
#ifndef NDEBUG
    assert((fields.prim_type                         & 0xffffffc0) == 0);
    assert((fields.source_select                     & 0xfffffffc) == 0);
    assert((fields.vis_cull                          & 0xfffffffc) == 0);
    assert((fields.index_size                        & 0xfffffffc) == 0);
    assert((fields.patch_type                        & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00033fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_INITIATOR,
        .value =
            (fields.prim_type                         <<  0) |
            (fields.source_select                     <<  6) |
            (fields.vis_cull                          <<  8) |
            (fields.index_size                        << 10) |
            (fields.patch_type                        << 12) |
            (fields.gs_enable                         << 16) |
            (fields.tess_enable                       << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_INITIATOR(...) pack_A6XX_PC_DRAW_INITIATOR((struct A6XX_PC_DRAW_INITIATOR) { __VA_ARGS__ })

struct A6XX_PC_DRAW_NUM_INSTANCES {
    uint32_t							a6xx_pc_draw_num_instances;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_NUM_INSTANCES(struct A6XX_PC_DRAW_NUM_INSTANCES fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_draw_num_instances        & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_NUM_INSTANCES,
        .value =
            (fields.a6xx_pc_draw_num_instances        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_NUM_INSTANCES(...) pack_A6XX_PC_DRAW_NUM_INSTANCES((struct A6XX_PC_DRAW_NUM_INSTANCES) { __VA_ARGS__ })

struct A6XX_PC_DRAW_NUM_INDICES {
    uint32_t							a6xx_pc_draw_num_indices;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_DRAW_NUM_INDICES(struct A6XX_PC_DRAW_NUM_INDICES fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_pc_draw_num_indices          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_DRAW_NUM_INDICES,
        .value =
            (fields.a6xx_pc_draw_num_indices          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_DRAW_NUM_INDICES(...) pack_A6XX_PC_DRAW_NUM_INDICES((struct A6XX_PC_DRAW_NUM_INDICES) { __VA_ARGS__ })

struct A6XX_PC_VSTREAM_CONTROL {
    uint32_t							unk0;
    uint32_t							vsc_size;
    uint32_t							vsc_n;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_VSTREAM_CONTROL(struct A6XX_PC_VSTREAM_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xffff0000) == 0);
    assert((fields.vsc_size                          & 0xffffffc0) == 0);
    assert((fields.vsc_n                             & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x07ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_VSTREAM_CONTROL,
        .value =
            (fields.unk0                              <<  0) |
            (fields.vsc_size                          << 16) |
            (fields.vsc_n                             << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_VSTREAM_CONTROL(...) pack_A6XX_PC_VSTREAM_CONTROL((struct A6XX_PC_VSTREAM_CONTROL) { __VA_ARGS__ })

struct A6XX_PC_BIN_PRIM_STRM {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_BIN_PRIM_STRM(struct A6XX_PC_BIN_PRIM_STRM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_BIN_PRIM_STRM,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_PC_BIN_PRIM_STRM(...) pack_A6XX_PC_BIN_PRIM_STRM((struct A6XX_PC_BIN_PRIM_STRM) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_PC_BIN_DRAW_STRM {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_BIN_DRAW_STRM(struct A6XX_PC_BIN_DRAW_STRM fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_BIN_DRAW_STRM,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_PC_BIN_DRAW_STRM(...) pack_A6XX_PC_BIN_DRAW_STRM((struct A6XX_PC_BIN_DRAW_STRM) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_PC_VISIBILITY_OVERRIDE {
    bool							override;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_VISIBILITY_OVERRIDE(struct A6XX_PC_VISIBILITY_OVERRIDE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_VISIBILITY_OVERRIDE,
        .value =
            (fields.override                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_VISIBILITY_OVERRIDE(...) pack_A6XX_PC_VISIBILITY_OVERRIDE((struct A6XX_PC_VISIBILITY_OVERRIDE) { __VA_ARGS__ })

struct A6XX_PC_UNKNOWN_9E72 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PC_UNKNOWN_9E72(struct A6XX_PC_UNKNOWN_9E72 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PC_UNKNOWN_9E72,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PC_UNKNOWN_9E72(...) pack_A6XX_PC_UNKNOWN_9E72((struct A6XX_PC_UNKNOWN_9E72) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_0 {
    uint32_t							fetch_cnt;
    uint32_t							decode_cnt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_0(struct A6XX_VFD_CONTROL_0 fields)
{
#ifndef NDEBUG
    assert((fields.fetch_cnt                         & 0xffffffc0) == 0);
    assert((fields.decode_cnt                        & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x00003f3f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_0,
        .value =
            (fields.fetch_cnt                         <<  0) |
            (fields.decode_cnt                        <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_0(...) pack_A6XX_VFD_CONTROL_0((struct A6XX_VFD_CONTROL_0) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_1 {
    uint32_t							regid4vtx;
    uint32_t							regid4inst;
    uint32_t							regid4primid;
    uint32_t							regid4viewid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_1(struct A6XX_VFD_CONTROL_1 fields)
{
#ifndef NDEBUG
    assert((fields.regid4vtx                         & 0xffffff00) == 0);
    assert((fields.regid4inst                        & 0xffffff00) == 0);
    assert((fields.regid4primid                      & 0xffffff00) == 0);
    assert((fields.regid4viewid                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_1,
        .value =
            (fields.regid4vtx                         <<  0) |
            (fields.regid4inst                        <<  8) |
            (fields.regid4primid                      << 16) |
            (fields.regid4viewid                      << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_1(...) pack_A6XX_VFD_CONTROL_1((struct A6XX_VFD_CONTROL_1) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_2 {
    uint32_t							regid_hsrelpatchid;
    uint32_t							regid_invocationid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_2(struct A6XX_VFD_CONTROL_2 fields)
{
#ifndef NDEBUG
    assert((fields.regid_hsrelpatchid                & 0xffffff00) == 0);
    assert((fields.regid_invocationid                & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_2,
        .value =
            (fields.regid_hsrelpatchid                <<  0) |
            (fields.regid_invocationid                <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_2(...) pack_A6XX_VFD_CONTROL_2((struct A6XX_VFD_CONTROL_2) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_3 {
    uint32_t							regid_dsprimid;
    uint32_t							regid_dsrelpatchid;
    uint32_t							regid_tessx;
    uint32_t							regid_tessy;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_3(struct A6XX_VFD_CONTROL_3 fields)
{
#ifndef NDEBUG
    assert((fields.regid_dsprimid                    & 0xffffff00) == 0);
    assert((fields.regid_dsrelpatchid                & 0xffffff00) == 0);
    assert((fields.regid_tessx                       & 0xffffff00) == 0);
    assert((fields.regid_tessy                       & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_3,
        .value =
            (fields.regid_dsprimid                    <<  0) |
            (fields.regid_dsrelpatchid                <<  8) |
            (fields.regid_tessx                       << 16) |
            (fields.regid_tessy                       << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_3(...) pack_A6XX_VFD_CONTROL_3((struct A6XX_VFD_CONTROL_3) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_4 {
    uint32_t							unk0;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_4(struct A6XX_VFD_CONTROL_4 fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_4,
        .value =
            (fields.unk0                              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_4(...) pack_A6XX_VFD_CONTROL_4((struct A6XX_VFD_CONTROL_4) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_5 {
    uint32_t							regid_gsheader;
    uint32_t							unk8;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_5(struct A6XX_VFD_CONTROL_5 fields)
{
#ifndef NDEBUG
    assert((fields.regid_gsheader                    & 0xffffff00) == 0);
    assert((fields.unk8                              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_5,
        .value =
            (fields.regid_gsheader                    <<  0) |
            (fields.unk8                              <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_5(...) pack_A6XX_VFD_CONTROL_5((struct A6XX_VFD_CONTROL_5) { __VA_ARGS__ })

struct A6XX_VFD_CONTROL_6 {
    bool							primid_passthru;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_CONTROL_6(struct A6XX_VFD_CONTROL_6 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_CONTROL_6,
        .value =
            (fields.primid_passthru                   <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_CONTROL_6(...) pack_A6XX_VFD_CONTROL_6((struct A6XX_VFD_CONTROL_6) { __VA_ARGS__ })

struct A6XX_VFD_MODE_CNTL {
    enum a6xx_render_mode					render_mode;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_MODE_CNTL(struct A6XX_VFD_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.render_mode                       & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_MODE_CNTL,
        .value =
            (fields.render_mode                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_MODE_CNTL(...) pack_A6XX_VFD_MODE_CNTL((struct A6XX_VFD_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_VFD_MULTIVIEW_CNTL {
    bool							enable;
    bool							disablemultipos;
    uint32_t							views;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_MULTIVIEW_CNTL(struct A6XX_VFD_MULTIVIEW_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.views                             & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_MULTIVIEW_CNTL,
        .value =
            (fields.enable                            <<  0) |
            (fields.disablemultipos                   <<  1) |
            (fields.views                             <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_MULTIVIEW_CNTL(...) pack_A6XX_VFD_MULTIVIEW_CNTL((struct A6XX_VFD_MULTIVIEW_CNTL) { __VA_ARGS__ })

struct A6XX_VFD_ADD_OFFSET {
    bool							vertex;
    bool							instance;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_ADD_OFFSET(struct A6XX_VFD_ADD_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_ADD_OFFSET,
        .value =
            (fields.vertex                            <<  0) |
            (fields.instance                          <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_ADD_OFFSET(...) pack_A6XX_VFD_ADD_OFFSET((struct A6XX_VFD_ADD_OFFSET) { __VA_ARGS__ })

struct A6XX_VFD_INDEX_OFFSET {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_INDEX_OFFSET(struct A6XX_VFD_INDEX_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_INDEX_OFFSET,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_INDEX_OFFSET(...) pack_A6XX_VFD_INDEX_OFFSET((struct A6XX_VFD_INDEX_OFFSET) { __VA_ARGS__ })

struct A6XX_VFD_INSTANCE_START_OFFSET {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_INSTANCE_START_OFFSET(struct A6XX_VFD_INSTANCE_START_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_INSTANCE_START_OFFSET,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_INSTANCE_START_OFFSET(...) pack_A6XX_VFD_INSTANCE_START_OFFSET((struct A6XX_VFD_INSTANCE_START_OFFSET) { __VA_ARGS__ })

struct A6XX_VFD_FETCH_BASE {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_FETCH_BASE(uint32_t i, struct A6XX_VFD_FETCH_BASE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_FETCH_BASE(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_VFD_FETCH_BASE(i, ...) pack_A6XX_VFD_FETCH_BASE(i, (struct A6XX_VFD_FETCH_BASE) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_VFD_FETCH_SIZE {
    uint32_t							a6xx_vfd_fetch_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_FETCH_SIZE(uint32_t i, struct A6XX_VFD_FETCH_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vfd_fetch_size               & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_FETCH_SIZE(i),
        .value =
            (fields.a6xx_vfd_fetch_size               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_FETCH_SIZE(i, ...) pack_A6XX_VFD_FETCH_SIZE(i, (struct A6XX_VFD_FETCH_SIZE) { __VA_ARGS__ })

struct A6XX_VFD_FETCH_STRIDE {
    uint32_t							a6xx_vfd_fetch_stride;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_FETCH_STRIDE(uint32_t i, struct A6XX_VFD_FETCH_STRIDE fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vfd_fetch_stride             & 0xfffff000) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_FETCH_STRIDE(i),
        .value =
            (fields.a6xx_vfd_fetch_stride             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_FETCH_STRIDE(i, ...) pack_A6XX_VFD_FETCH_STRIDE(i, (struct A6XX_VFD_FETCH_STRIDE) { __VA_ARGS__ })

struct A6XX_VFD_DECODE_INSTR {
    uint32_t							idx;
    uint32_t							offset;
    bool							instanced;
    enum a6xx_format						format;
    enum a3xx_color_swap					swap;
    bool							unk30;
    bool							_float;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_DECODE_INSTR(uint32_t i, struct A6XX_VFD_DECODE_INSTR fields)
{
#ifndef NDEBUG
    assert((fields.idx                               & 0xffffffe0) == 0);
    assert((fields.offset                            & 0xfffff000) == 0);
    assert((fields.format                            & 0xffffff00) == 0);
    assert((fields.swap                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0xfff3ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_DECODE_INSTR(i),
        .value =
            (fields.idx                               <<  0) |
            (fields.offset                            <<  5) |
            (fields.instanced                         << 17) |
            (fields.format                            << 20) |
            (fields.swap                              << 28) |
            (fields.unk30                             << 30) |
            (fields._float                            << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_DECODE_INSTR(i, ...) pack_A6XX_VFD_DECODE_INSTR(i, (struct A6XX_VFD_DECODE_INSTR) { __VA_ARGS__ })

struct A6XX_VFD_DECODE_STEP_RATE {
    uint32_t							a6xx_vfd_decode_step_rate;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_DECODE_STEP_RATE(uint32_t i, struct A6XX_VFD_DECODE_STEP_RATE fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vfd_decode_step_rate         & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_DECODE_STEP_RATE(i),
        .value =
            (fields.a6xx_vfd_decode_step_rate         <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_DECODE_STEP_RATE(i, ...) pack_A6XX_VFD_DECODE_STEP_RATE(i, (struct A6XX_VFD_DECODE_STEP_RATE) { __VA_ARGS__ })

struct A6XX_VFD_DEST_CNTL_INSTR {
    uint32_t							writemask;
    uint32_t							regid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_DEST_CNTL_INSTR(uint32_t i, struct A6XX_VFD_DEST_CNTL_INSTR fields)
{
#ifndef NDEBUG
    assert((fields.writemask                         & 0xfffffff0) == 0);
    assert((fields.regid                             & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_DEST_CNTL_INSTR(i),
        .value =
            (fields.writemask                         <<  0) |
            (fields.regid                             <<  4) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_DEST_CNTL_INSTR(i, ...) pack_A6XX_VFD_DEST_CNTL_INSTR(i, (struct A6XX_VFD_DEST_CNTL_INSTR) { __VA_ARGS__ })

struct A6XX_VFD_POWER_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_POWER_CNTL(struct A6XX_VFD_POWER_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_POWER_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_POWER_CNTL(...) pack_A6XX_VFD_POWER_CNTL((struct A6XX_VFD_POWER_CNTL) { __VA_ARGS__ })

struct A6XX_VFD_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_vfd_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_VFD_ADDR_MODE_CNTL(struct A6XX_VFD_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_vfd_addr_mode_cntl           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_VFD_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_vfd_addr_mode_cntl           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_VFD_ADDR_MODE_CNTL(...) pack_A6XX_VFD_ADDR_MODE_CNTL((struct A6XX_VFD_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_VS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    bool							mergedregs;
    bool							earlypreamble;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_CTRL_REG0(struct A6XX_SP_VS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x003fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.mergedregs                        << 20) |
            (fields.earlypreamble                     << 21) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_CTRL_REG0(...) pack_A6XX_SP_VS_CTRL_REG0((struct A6XX_SP_VS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_VS_BRANCH_COND {
    uint32_t							a6xx_sp_vs_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_BRANCH_COND(struct A6XX_SP_VS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_vs_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_vs_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_BRANCH_COND(...) pack_A6XX_SP_VS_BRANCH_COND((struct A6XX_SP_VS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_VS_PRIMITIVE_CNTL {
    uint32_t							out;
    uint32_t							flags_regid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_PRIMITIVE_CNTL(struct A6XX_SP_VS_PRIMITIVE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.out                               & 0xffffffc0) == 0);
    assert((fields.flags_regid                       & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00003fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_PRIMITIVE_CNTL,
        .value =
            (fields.out                               <<  0) |
            (fields.flags_regid                       <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_PRIMITIVE_CNTL(...) pack_A6XX_SP_VS_PRIMITIVE_CNTL((struct A6XX_SP_VS_PRIMITIVE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_VS_OUT_REG {
    uint32_t							a_regid;
    uint32_t							a_compmask;
    uint32_t							b_regid;
    uint32_t							b_compmask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_OUT_REG(uint32_t i, struct A6XX_SP_VS_OUT_REG fields)
{
#ifndef NDEBUG
    assert((fields.a_regid                           & 0xffffff00) == 0);
    assert((fields.a_compmask                        & 0xfffffff0) == 0);
    assert((fields.b_regid                           & 0xffffff00) == 0);
    assert((fields.b_compmask                        & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_OUT_REG(i),
        .value =
            (fields.a_regid                           <<  0) |
            (fields.a_compmask                        <<  8) |
            (fields.b_regid                           << 16) |
            (fields.b_compmask                        << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_OUT_REG(i, ...) pack_A6XX_SP_VS_OUT_REG(i, (struct A6XX_SP_VS_OUT_REG) { __VA_ARGS__ })

struct A6XX_SP_VS_VPC_DST_REG {
    uint32_t							outloc0;
    uint32_t							outloc1;
    uint32_t							outloc2;
    uint32_t							outloc3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_VPC_DST_REG(uint32_t i, struct A6XX_SP_VS_VPC_DST_REG fields)
{
#ifndef NDEBUG
    assert((fields.outloc0                           & 0xffffff00) == 0);
    assert((fields.outloc1                           & 0xffffff00) == 0);
    assert((fields.outloc2                           & 0xffffff00) == 0);
    assert((fields.outloc3                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_VPC_DST_REG(i),
        .value =
            (fields.outloc0                           <<  0) |
            (fields.outloc1                           <<  8) |
            (fields.outloc2                           << 16) |
            (fields.outloc3                           << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_VPC_DST_REG(i, ...) pack_A6XX_SP_VS_VPC_DST_REG(i, (struct A6XX_SP_VS_VPC_DST_REG) { __VA_ARGS__ })

struct A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_vs_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_vs_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_vs_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_VS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_VS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_OBJ_START(struct A6XX_SP_VS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_VS_OBJ_START(...) pack_A6XX_SP_VS_OBJ_START((struct A6XX_SP_VS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_VS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_PVT_MEM_PARAM(struct A6XX_SP_VS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_PVT_MEM_PARAM(...) pack_A6XX_SP_VS_PVT_MEM_PARAM((struct A6XX_SP_VS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_VS_PVT_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_PVT_MEM_ADDR(struct A6XX_SP_VS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_VS_PVT_MEM_ADDR(...) pack_A6XX_SP_VS_PVT_MEM_ADDR((struct A6XX_SP_VS_PVT_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_VS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_PVT_MEM_SIZE(struct A6XX_SP_VS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_PVT_MEM_SIZE(...) pack_A6XX_SP_VS_PVT_MEM_SIZE((struct A6XX_SP_VS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_VS_TEX_COUNT {
    uint32_t							a6xx_sp_vs_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_TEX_COUNT(struct A6XX_SP_VS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_vs_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_vs_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_TEX_COUNT(...) pack_A6XX_SP_VS_TEX_COUNT((struct A6XX_SP_VS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_VS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_CONFIG(struct A6XX_SP_VS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_CONFIG(...) pack_A6XX_SP_VS_CONFIG((struct A6XX_SP_VS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_VS_INSTRLEN {
    uint32_t							a6xx_sp_vs_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_INSTRLEN(struct A6XX_SP_VS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_vs_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_INSTRLEN,
        .value =
            (fields.a6xx_sp_vs_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_INSTRLEN(...) pack_A6XX_SP_VS_INSTRLEN((struct A6XX_SP_VS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_VS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_HS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    bool							earlypreamble;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_CTRL_REG0(struct A6XX_SP_HS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x001fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.earlypreamble                     << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_CTRL_REG0(...) pack_A6XX_SP_HS_CTRL_REG0((struct A6XX_SP_HS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_HS_WAVE_INPUT_SIZE {
    uint32_t							a6xx_sp_hs_wave_input_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_WAVE_INPUT_SIZE(struct A6XX_SP_HS_WAVE_INPUT_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_hs_wave_input_size        & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_WAVE_INPUT_SIZE,
        .value =
            (fields.a6xx_sp_hs_wave_input_size        <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_WAVE_INPUT_SIZE(...) pack_A6XX_SP_HS_WAVE_INPUT_SIZE((struct A6XX_SP_HS_WAVE_INPUT_SIZE) { __VA_ARGS__ })

struct A6XX_SP_HS_BRANCH_COND {
    uint32_t							a6xx_sp_hs_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_BRANCH_COND(struct A6XX_SP_HS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_hs_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_hs_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_BRANCH_COND(...) pack_A6XX_SP_HS_BRANCH_COND((struct A6XX_SP_HS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_hs_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_hs_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_hs_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_HS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_HS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_OBJ_START(struct A6XX_SP_HS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_HS_OBJ_START(...) pack_A6XX_SP_HS_OBJ_START((struct A6XX_SP_HS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_HS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_PVT_MEM_PARAM(struct A6XX_SP_HS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_PVT_MEM_PARAM(...) pack_A6XX_SP_HS_PVT_MEM_PARAM((struct A6XX_SP_HS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_HS_PVT_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_PVT_MEM_ADDR(struct A6XX_SP_HS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_HS_PVT_MEM_ADDR(...) pack_A6XX_SP_HS_PVT_MEM_ADDR((struct A6XX_SP_HS_PVT_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_HS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_PVT_MEM_SIZE(struct A6XX_SP_HS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_PVT_MEM_SIZE(...) pack_A6XX_SP_HS_PVT_MEM_SIZE((struct A6XX_SP_HS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_HS_TEX_COUNT {
    uint32_t							a6xx_sp_hs_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_TEX_COUNT(struct A6XX_SP_HS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_hs_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_hs_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_TEX_COUNT(...) pack_A6XX_SP_HS_TEX_COUNT((struct A6XX_SP_HS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_HS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_CONFIG(struct A6XX_SP_HS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_CONFIG(...) pack_A6XX_SP_HS_CONFIG((struct A6XX_SP_HS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_HS_INSTRLEN {
    uint32_t							a6xx_sp_hs_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_INSTRLEN(struct A6XX_SP_HS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_hs_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_INSTRLEN,
        .value =
            (fields.a6xx_sp_hs_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_INSTRLEN(...) pack_A6XX_SP_HS_INSTRLEN((struct A6XX_SP_HS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_HS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_DS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    bool							earlypreamble;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_CTRL_REG0(struct A6XX_SP_DS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x001fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.earlypreamble                     << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_CTRL_REG0(...) pack_A6XX_SP_DS_CTRL_REG0((struct A6XX_SP_DS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_DS_BRANCH_COND {
    uint32_t							a6xx_sp_ds_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_BRANCH_COND(struct A6XX_SP_DS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_ds_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_ds_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_BRANCH_COND(...) pack_A6XX_SP_DS_BRANCH_COND((struct A6XX_SP_DS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_DS_PRIMITIVE_CNTL {
    uint32_t							out;
    uint32_t							flags_regid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_PRIMITIVE_CNTL(struct A6XX_SP_DS_PRIMITIVE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.out                               & 0xffffffc0) == 0);
    assert((fields.flags_regid                       & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00003fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_PRIMITIVE_CNTL,
        .value =
            (fields.out                               <<  0) |
            (fields.flags_regid                       <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_PRIMITIVE_CNTL(...) pack_A6XX_SP_DS_PRIMITIVE_CNTL((struct A6XX_SP_DS_PRIMITIVE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_DS_OUT_REG {
    uint32_t							a_regid;
    uint32_t							a_compmask;
    uint32_t							b_regid;
    uint32_t							b_compmask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_OUT_REG(uint32_t i, struct A6XX_SP_DS_OUT_REG fields)
{
#ifndef NDEBUG
    assert((fields.a_regid                           & 0xffffff00) == 0);
    assert((fields.a_compmask                        & 0xfffffff0) == 0);
    assert((fields.b_regid                           & 0xffffff00) == 0);
    assert((fields.b_compmask                        & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_OUT_REG(i),
        .value =
            (fields.a_regid                           <<  0) |
            (fields.a_compmask                        <<  8) |
            (fields.b_regid                           << 16) |
            (fields.b_compmask                        << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_OUT_REG(i, ...) pack_A6XX_SP_DS_OUT_REG(i, (struct A6XX_SP_DS_OUT_REG) { __VA_ARGS__ })

struct A6XX_SP_DS_VPC_DST_REG {
    uint32_t							outloc0;
    uint32_t							outloc1;
    uint32_t							outloc2;
    uint32_t							outloc3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_VPC_DST_REG(uint32_t i, struct A6XX_SP_DS_VPC_DST_REG fields)
{
#ifndef NDEBUG
    assert((fields.outloc0                           & 0xffffff00) == 0);
    assert((fields.outloc1                           & 0xffffff00) == 0);
    assert((fields.outloc2                           & 0xffffff00) == 0);
    assert((fields.outloc3                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_VPC_DST_REG(i),
        .value =
            (fields.outloc0                           <<  0) |
            (fields.outloc1                           <<  8) |
            (fields.outloc2                           << 16) |
            (fields.outloc3                           << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_VPC_DST_REG(i, ...) pack_A6XX_SP_DS_VPC_DST_REG(i, (struct A6XX_SP_DS_VPC_DST_REG) { __VA_ARGS__ })

struct A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_ds_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_ds_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_ds_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_DS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_DS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_OBJ_START(struct A6XX_SP_DS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_DS_OBJ_START(...) pack_A6XX_SP_DS_OBJ_START((struct A6XX_SP_DS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_DS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_PVT_MEM_PARAM(struct A6XX_SP_DS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_PVT_MEM_PARAM(...) pack_A6XX_SP_DS_PVT_MEM_PARAM((struct A6XX_SP_DS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_DS_PVT_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_PVT_MEM_ADDR(struct A6XX_SP_DS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_DS_PVT_MEM_ADDR(...) pack_A6XX_SP_DS_PVT_MEM_ADDR((struct A6XX_SP_DS_PVT_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_DS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_PVT_MEM_SIZE(struct A6XX_SP_DS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_PVT_MEM_SIZE(...) pack_A6XX_SP_DS_PVT_MEM_SIZE((struct A6XX_SP_DS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_DS_TEX_COUNT {
    uint32_t							a6xx_sp_ds_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_TEX_COUNT(struct A6XX_SP_DS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_ds_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_ds_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_TEX_COUNT(...) pack_A6XX_SP_DS_TEX_COUNT((struct A6XX_SP_DS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_DS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_CONFIG(struct A6XX_SP_DS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_CONFIG(...) pack_A6XX_SP_DS_CONFIG((struct A6XX_SP_DS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_DS_INSTRLEN {
    uint32_t							a6xx_sp_ds_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_INSTRLEN(struct A6XX_SP_DS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_ds_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_INSTRLEN,
        .value =
            (fields.a6xx_sp_ds_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_INSTRLEN(...) pack_A6XX_SP_DS_INSTRLEN((struct A6XX_SP_DS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_DS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_GS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    bool							earlypreamble;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_CTRL_REG0(struct A6XX_SP_GS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x001fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.earlypreamble                     << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_CTRL_REG0(...) pack_A6XX_SP_GS_CTRL_REG0((struct A6XX_SP_GS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_GS_PRIM_SIZE {
    uint32_t							a6xx_sp_gs_prim_size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PRIM_SIZE(struct A6XX_SP_GS_PRIM_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_gs_prim_size              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PRIM_SIZE,
        .value =
            (fields.a6xx_sp_gs_prim_size              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_PRIM_SIZE(...) pack_A6XX_SP_GS_PRIM_SIZE((struct A6XX_SP_GS_PRIM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_GS_BRANCH_COND {
    uint32_t							a6xx_sp_gs_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_BRANCH_COND(struct A6XX_SP_GS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_gs_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_gs_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_BRANCH_COND(...) pack_A6XX_SP_GS_BRANCH_COND((struct A6XX_SP_GS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_GS_PRIMITIVE_CNTL {
    uint32_t							out;
    uint32_t							flags_regid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PRIMITIVE_CNTL(struct A6XX_SP_GS_PRIMITIVE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.out                               & 0xffffffc0) == 0);
    assert((fields.flags_regid                       & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x00003fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PRIMITIVE_CNTL,
        .value =
            (fields.out                               <<  0) |
            (fields.flags_regid                       <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_PRIMITIVE_CNTL(...) pack_A6XX_SP_GS_PRIMITIVE_CNTL((struct A6XX_SP_GS_PRIMITIVE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_GS_OUT_REG {
    uint32_t							a_regid;
    uint32_t							a_compmask;
    uint32_t							b_regid;
    uint32_t							b_compmask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_OUT_REG(uint32_t i, struct A6XX_SP_GS_OUT_REG fields)
{
#ifndef NDEBUG
    assert((fields.a_regid                           & 0xffffff00) == 0);
    assert((fields.a_compmask                        & 0xfffffff0) == 0);
    assert((fields.b_regid                           & 0xffffff00) == 0);
    assert((fields.b_compmask                        & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0fff0fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_OUT_REG(i),
        .value =
            (fields.a_regid                           <<  0) |
            (fields.a_compmask                        <<  8) |
            (fields.b_regid                           << 16) |
            (fields.b_compmask                        << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_OUT_REG(i, ...) pack_A6XX_SP_GS_OUT_REG(i, (struct A6XX_SP_GS_OUT_REG) { __VA_ARGS__ })

struct A6XX_SP_GS_VPC_DST_REG {
    uint32_t							outloc0;
    uint32_t							outloc1;
    uint32_t							outloc2;
    uint32_t							outloc3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_VPC_DST_REG(uint32_t i, struct A6XX_SP_GS_VPC_DST_REG fields)
{
#ifndef NDEBUG
    assert((fields.outloc0                           & 0xffffff00) == 0);
    assert((fields.outloc1                           & 0xffffff00) == 0);
    assert((fields.outloc2                           & 0xffffff00) == 0);
    assert((fields.outloc3                           & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_VPC_DST_REG(i),
        .value =
            (fields.outloc0                           <<  0) |
            (fields.outloc1                           <<  8) |
            (fields.outloc2                           << 16) |
            (fields.outloc3                           << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_VPC_DST_REG(i, ...) pack_A6XX_SP_GS_VPC_DST_REG(i, (struct A6XX_SP_GS_VPC_DST_REG) { __VA_ARGS__ })

struct A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_gs_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_gs_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_gs_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_GS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_GS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_OBJ_START(struct A6XX_SP_GS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_GS_OBJ_START(...) pack_A6XX_SP_GS_OBJ_START((struct A6XX_SP_GS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_GS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PVT_MEM_PARAM(struct A6XX_SP_GS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_PVT_MEM_PARAM(...) pack_A6XX_SP_GS_PVT_MEM_PARAM((struct A6XX_SP_GS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_GS_PVT_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PVT_MEM_ADDR(struct A6XX_SP_GS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_GS_PVT_MEM_ADDR(...) pack_A6XX_SP_GS_PVT_MEM_ADDR((struct A6XX_SP_GS_PVT_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_GS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PVT_MEM_SIZE(struct A6XX_SP_GS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_PVT_MEM_SIZE(...) pack_A6XX_SP_GS_PVT_MEM_SIZE((struct A6XX_SP_GS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_GS_TEX_COUNT {
    uint32_t							a6xx_sp_gs_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_TEX_COUNT(struct A6XX_SP_GS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_gs_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_gs_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_TEX_COUNT(...) pack_A6XX_SP_GS_TEX_COUNT((struct A6XX_SP_GS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_GS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_CONFIG(struct A6XX_SP_GS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_CONFIG(...) pack_A6XX_SP_GS_CONFIG((struct A6XX_SP_GS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_GS_INSTRLEN {
    uint32_t							a6xx_sp_gs_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_INSTRLEN(struct A6XX_SP_GS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_gs_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_INSTRLEN,
        .value =
            (fields.a6xx_sp_gs_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_INSTRLEN(...) pack_A6XX_SP_GS_INSTRLEN((struct A6XX_SP_GS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_GS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_VS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_TEX_SAMP(struct A6XX_SP_VS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_VS_TEX_SAMP(...) pack_A6XX_SP_VS_TEX_SAMP((struct A6XX_SP_VS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_HS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_TEX_SAMP(struct A6XX_SP_HS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_HS_TEX_SAMP(...) pack_A6XX_SP_HS_TEX_SAMP((struct A6XX_SP_HS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_DS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_TEX_SAMP(struct A6XX_SP_DS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_DS_TEX_SAMP(...) pack_A6XX_SP_DS_TEX_SAMP((struct A6XX_SP_DS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_GS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_TEX_SAMP(struct A6XX_SP_GS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_GS_TEX_SAMP(...) pack_A6XX_SP_GS_TEX_SAMP((struct A6XX_SP_GS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_VS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_VS_TEX_CONST(struct A6XX_SP_VS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_VS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_VS_TEX_CONST(...) pack_A6XX_SP_VS_TEX_CONST((struct A6XX_SP_VS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_HS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_HS_TEX_CONST(struct A6XX_SP_HS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_HS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_HS_TEX_CONST(...) pack_A6XX_SP_HS_TEX_CONST((struct A6XX_SP_HS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_DS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_DS_TEX_CONST(struct A6XX_SP_DS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_DS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_DS_TEX_CONST(...) pack_A6XX_SP_DS_TEX_CONST((struct A6XX_SP_DS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_GS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_GS_TEX_CONST(struct A6XX_SP_GS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_GS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_GS_TEX_CONST(...) pack_A6XX_SP_GS_TEX_CONST((struct A6XX_SP_GS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_FS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    enum a6xx_threadsize					threadsize;
    bool							unk21;
    bool							varying;
    bool							diff_fine;
    bool							unk24;
    bool							unk25;
    bool							pixlodenable;
    bool							unk27;
    bool							earlypreamble;
    bool							mergedregs;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_CTRL_REG0(struct A6XX_SP_FS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.threadsize                        & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x9fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.threadsize                        << 20) |
            (fields.unk21                             << 21) |
            (fields.varying                           << 22) |
            (fields.diff_fine                         << 23) |
            (fields.unk24                             << 24) |
            (fields.unk25                             << 25) |
            (fields.pixlodenable                      << 26) |
            (fields.unk27                             << 27) |
            (fields.earlypreamble                     << 28) |
            (fields.mergedregs                        << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_CTRL_REG0(...) pack_A6XX_SP_FS_CTRL_REG0((struct A6XX_SP_FS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_FS_BRANCH_COND {
    uint32_t							a6xx_sp_fs_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_BRANCH_COND(struct A6XX_SP_FS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_fs_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_fs_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_BRANCH_COND(...) pack_A6XX_SP_FS_BRANCH_COND((struct A6XX_SP_FS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_fs_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_fs_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_fs_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_FS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_FS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_OBJ_START(struct A6XX_SP_FS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_FS_OBJ_START(...) pack_A6XX_SP_FS_OBJ_START((struct A6XX_SP_FS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_FS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PVT_MEM_PARAM(struct A6XX_SP_FS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_PVT_MEM_PARAM(...) pack_A6XX_SP_FS_PVT_MEM_PARAM((struct A6XX_SP_FS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_FS_PVT_MEM_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PVT_MEM_ADDR(struct A6XX_SP_FS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_FS_PVT_MEM_ADDR(...) pack_A6XX_SP_FS_PVT_MEM_ADDR((struct A6XX_SP_FS_PVT_MEM_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_FS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PVT_MEM_SIZE(struct A6XX_SP_FS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_PVT_MEM_SIZE(...) pack_A6XX_SP_FS_PVT_MEM_SIZE((struct A6XX_SP_FS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_BLEND_CNTL {
    uint32_t							enable_blend;
    bool							unk8;
    bool							dual_color_in_enable;
    bool							alpha_to_coverage;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_BLEND_CNTL(struct A6XX_SP_BLEND_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.enable_blend                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_BLEND_CNTL,
        .value =
            (fields.enable_blend                      <<  0) |
            (fields.unk8                              <<  8) |
            (fields.dual_color_in_enable              <<  9) |
            (fields.alpha_to_coverage                 << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_BLEND_CNTL(...) pack_A6XX_SP_BLEND_CNTL((struct A6XX_SP_BLEND_CNTL) { __VA_ARGS__ })

struct A6XX_SP_SRGB_CNTL {
    bool							srgb_mrt0;
    bool							srgb_mrt1;
    bool							srgb_mrt2;
    bool							srgb_mrt3;
    bool							srgb_mrt4;
    bool							srgb_mrt5;
    bool							srgb_mrt6;
    bool							srgb_mrt7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_SRGB_CNTL(struct A6XX_SP_SRGB_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_SRGB_CNTL,
        .value =
            (fields.srgb_mrt0                         <<  0) |
            (fields.srgb_mrt1                         <<  1) |
            (fields.srgb_mrt2                         <<  2) |
            (fields.srgb_mrt3                         <<  3) |
            (fields.srgb_mrt4                         <<  4) |
            (fields.srgb_mrt5                         <<  5) |
            (fields.srgb_mrt6                         <<  6) |
            (fields.srgb_mrt7                         <<  7) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_SRGB_CNTL(...) pack_A6XX_SP_SRGB_CNTL((struct A6XX_SP_SRGB_CNTL) { __VA_ARGS__ })

struct A6XX_SP_FS_RENDER_COMPONENTS {
    uint32_t							rt0;
    uint32_t							rt1;
    uint32_t							rt2;
    uint32_t							rt3;
    uint32_t							rt4;
    uint32_t							rt5;
    uint32_t							rt6;
    uint32_t							rt7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_RENDER_COMPONENTS(struct A6XX_SP_FS_RENDER_COMPONENTS fields)
{
#ifndef NDEBUG
    assert((fields.rt0                               & 0xfffffff0) == 0);
    assert((fields.rt1                               & 0xfffffff0) == 0);
    assert((fields.rt2                               & 0xfffffff0) == 0);
    assert((fields.rt3                               & 0xfffffff0) == 0);
    assert((fields.rt4                               & 0xfffffff0) == 0);
    assert((fields.rt5                               & 0xfffffff0) == 0);
    assert((fields.rt6                               & 0xfffffff0) == 0);
    assert((fields.rt7                               & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_RENDER_COMPONENTS,
        .value =
            (fields.rt0                               <<  0) |
            (fields.rt1                               <<  4) |
            (fields.rt2                               <<  8) |
            (fields.rt3                               << 12) |
            (fields.rt4                               << 16) |
            (fields.rt5                               << 20) |
            (fields.rt6                               << 24) |
            (fields.rt7                               << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_RENDER_COMPONENTS(...) pack_A6XX_SP_FS_RENDER_COMPONENTS((struct A6XX_SP_FS_RENDER_COMPONENTS) { __VA_ARGS__ })

struct A6XX_SP_FS_OUTPUT_CNTL0 {
    bool							dual_color_in_enable;
    uint32_t							depth_regid;
    uint32_t							sampmask_regid;
    uint32_t							stencilref_regid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_OUTPUT_CNTL0(struct A6XX_SP_FS_OUTPUT_CNTL0 fields)
{
#ifndef NDEBUG
    assert((fields.depth_regid                       & 0xffffff00) == 0);
    assert((fields.sampmask_regid                    & 0xffffff00) == 0);
    assert((fields.stencilref_regid                  & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffff01) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_OUTPUT_CNTL0,
        .value =
            (fields.dual_color_in_enable              <<  0) |
            (fields.depth_regid                       <<  8) |
            (fields.sampmask_regid                    << 16) |
            (fields.stencilref_regid                  << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_OUTPUT_CNTL0(...) pack_A6XX_SP_FS_OUTPUT_CNTL0((struct A6XX_SP_FS_OUTPUT_CNTL0) { __VA_ARGS__ })

struct A6XX_SP_FS_OUTPUT_CNTL1 {
    uint32_t							mrt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_OUTPUT_CNTL1(struct A6XX_SP_FS_OUTPUT_CNTL1 fields)
{
#ifndef NDEBUG
    assert((fields.mrt                               & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_OUTPUT_CNTL1,
        .value =
            (fields.mrt                               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_OUTPUT_CNTL1(...) pack_A6XX_SP_FS_OUTPUT_CNTL1((struct A6XX_SP_FS_OUTPUT_CNTL1) { __VA_ARGS__ })

struct A6XX_SP_FS_OUTPUT_REG {
    uint32_t							regid;
    bool							half_precision;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_OUTPUT_REG(uint32_t i, struct A6XX_SP_FS_OUTPUT_REG fields)
{
#ifndef NDEBUG
    assert((fields.regid                             & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_OUTPUT_REG(i),
        .value =
            (fields.regid                             <<  0) |
            (fields.half_precision                    <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_OUTPUT_REG(i, ...) pack_A6XX_SP_FS_OUTPUT_REG(i, (struct A6XX_SP_FS_OUTPUT_REG) { __VA_ARGS__ })

struct A6XX_SP_FS_MRT_REG {
    enum a6xx_format						color_format;
    bool							color_sint;
    bool							color_uint;
    bool							unk10;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_MRT_REG(uint32_t i, struct A6XX_SP_FS_MRT_REG fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_MRT_REG(i),
        .value =
            (fields.color_format                      <<  0) |
            (fields.color_sint                        <<  8) |
            (fields.color_uint                        <<  9) |
            (fields.unk10                             << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_MRT_REG(i, ...) pack_A6XX_SP_FS_MRT_REG(i, (struct A6XX_SP_FS_MRT_REG) { __VA_ARGS__ })

struct A6XX_SP_FS_PREFETCH_CNTL {
    uint32_t							count;
    bool							unk3;
    uint32_t							unk4;
    uint32_t							unk12;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PREFETCH_CNTL(struct A6XX_SP_FS_PREFETCH_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.count                             & 0xfffffff8) == 0);
    assert((fields.unk4                              & 0xffffff00) == 0);
    assert((fields.unk12                             & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0x00007fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PREFETCH_CNTL,
        .value =
            (fields.count                             <<  0) |
            (fields.unk3                              <<  3) |
            (fields.unk4                              <<  4) |
            (fields.unk12                             << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_PREFETCH_CNTL(...) pack_A6XX_SP_FS_PREFETCH_CNTL((struct A6XX_SP_FS_PREFETCH_CNTL) { __VA_ARGS__ })

struct A6XX_SP_FS_PREFETCH_CMD {
    uint32_t							src;
    uint32_t							samp_id;
    uint32_t							tex_id;
    uint32_t							dst;
    uint32_t							wrmask;
    bool							half;
    uint32_t							cmd;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PREFETCH_CMD(uint32_t i, struct A6XX_SP_FS_PREFETCH_CMD fields)
{
#ifndef NDEBUG
    assert((fields.src                               & 0xffffff80) == 0);
    assert((fields.samp_id                           & 0xfffffff0) == 0);
    assert((fields.tex_id                            & 0xffffffe0) == 0);
    assert((fields.dst                               & 0xffffffc0) == 0);
    assert((fields.wrmask                            & 0xfffffff0) == 0);
    assert((fields.cmd                               & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PREFETCH_CMD(i),
        .value =
            (fields.src                               <<  0) |
            (fields.samp_id                           <<  7) |
            (fields.tex_id                            << 11) |
            (fields.dst                               << 16) |
            (fields.wrmask                            << 22) |
            (fields.half                              << 26) |
            (fields.cmd                               << 27) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_PREFETCH_CMD(i, ...) pack_A6XX_SP_FS_PREFETCH_CMD(i, (struct A6XX_SP_FS_PREFETCH_CMD) { __VA_ARGS__ })

struct A6XX_SP_FS_BINDLESS_PREFETCH_CMD {
    uint32_t							samp_id;
    uint32_t							tex_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_BINDLESS_PREFETCH_CMD(uint32_t i, struct A6XX_SP_FS_BINDLESS_PREFETCH_CMD fields)
{
#ifndef NDEBUG
    assert((fields.samp_id                           & 0xffff0000) == 0);
    assert((fields.tex_id                            & 0xffff0000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_BINDLESS_PREFETCH_CMD(i),
        .value =
            (fields.samp_id                           <<  0) |
            (fields.tex_id                            << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_BINDLESS_PREFETCH_CMD(i, ...) pack_A6XX_SP_FS_BINDLESS_PREFETCH_CMD(i, (struct A6XX_SP_FS_BINDLESS_PREFETCH_CMD) { __VA_ARGS__ })

struct A6XX_SP_FS_TEX_COUNT {
    uint32_t							a6xx_sp_fs_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_TEX_COUNT(struct A6XX_SP_FS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_fs_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_fs_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_TEX_COUNT(...) pack_A6XX_SP_FS_TEX_COUNT((struct A6XX_SP_FS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_UNKNOWN_A9A8 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_A9A8(struct A6XX_SP_UNKNOWN_A9A8 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_A9A8,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_A9A8(...) pack_A6XX_SP_UNKNOWN_A9A8((struct A6XX_SP_UNKNOWN_A9A8) { __VA_ARGS__ })

struct A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_FS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_CS_CTRL_REG0 {
    enum a3xx_threadmode					threadmode;
    uint32_t							halfregfootprint;
    uint32_t							fullregfootprint;
    bool							unk13;
    uint32_t							branchstack;
    enum a6xx_threadsize					threadsize;
    bool							unk21;
    bool							unk22;
    bool							earlypreamble;
    bool							mergedregs;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_CTRL_REG0(struct A6XX_SP_CS_CTRL_REG0 fields)
{
#ifndef NDEBUG
    assert((fields.threadmode                        & 0xfffffffe) == 0);
    assert((fields.halfregfootprint                  & 0xffffffc0) == 0);
    assert((fields.fullregfootprint                  & 0xffffffc0) == 0);
    assert((fields.branchstack                       & 0xffffffc0) == 0);
    assert((fields.threadsize                        & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x80ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_CTRL_REG0,
        .value =
            (fields.threadmode                        <<  0) |
            (fields.halfregfootprint                  <<  1) |
            (fields.fullregfootprint                  <<  7) |
            (fields.unk13                             << 13) |
            (fields.branchstack                       << 14) |
            (fields.threadsize                        << 20) |
            (fields.unk21                             << 21) |
            (fields.unk22                             << 22) |
            (fields.earlypreamble                     << 23) |
            (fields.mergedregs                        << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_CTRL_REG0(...) pack_A6XX_SP_CS_CTRL_REG0((struct A6XX_SP_CS_CTRL_REG0) { __VA_ARGS__ })

struct A6XX_SP_CS_UNKNOWN_A9B1 {
    uint32_t							shared_size;
    bool							unk5;
    bool							unk6;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_UNKNOWN_A9B1(struct A6XX_SP_CS_UNKNOWN_A9B1 fields)
{
#ifndef NDEBUG
    assert((fields.shared_size                       & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_UNKNOWN_A9B1,
        .value =
            (fields.shared_size                       <<  0) |
            (fields.unk5                              <<  5) |
            (fields.unk6                              <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_UNKNOWN_A9B1(...) pack_A6XX_SP_CS_UNKNOWN_A9B1((struct A6XX_SP_CS_UNKNOWN_A9B1) { __VA_ARGS__ })

struct A6XX_SP_CS_BRANCH_COND {
    uint32_t							a6xx_sp_cs_branch_cond;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_BRANCH_COND(struct A6XX_SP_CS_BRANCH_COND fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_cs_branch_cond            & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_BRANCH_COND,
        .value =
            (fields.a6xx_sp_cs_branch_cond            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_BRANCH_COND(...) pack_A6XX_SP_CS_BRANCH_COND((struct A6XX_SP_CS_BRANCH_COND) { __VA_ARGS__ })

struct A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET {
    uint32_t							a6xx_sp_cs_obj_first_exec_offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET(struct A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_cs_obj_first_exec_offset  & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET,
        .value =
            (fields.a6xx_sp_cs_obj_first_exec_offset  <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET(...) pack_A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET((struct A6XX_SP_CS_OBJ_FIRST_EXEC_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_CS_OBJ_START {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_OBJ_START(struct A6XX_SP_CS_OBJ_START fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_OBJ_START,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_CS_OBJ_START(...) pack_A6XX_SP_CS_OBJ_START((struct A6XX_SP_CS_OBJ_START) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_PVT_MEM_PARAM {
    uint32_t							memsizeperitem;
    uint32_t							hwstacksizeperthread;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_PVT_MEM_PARAM(struct A6XX_SP_CS_PVT_MEM_PARAM fields)
{
#ifndef NDEBUG
    assert(((fields.memsizeperitem >> 9)             & 0xffffff00) == 0);
    assert((fields.hwstacksizeperthread              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xff0000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_PVT_MEM_PARAM,
        .value =
            ((fields.memsizeperitem >> 9)             <<  0) |
            (fields.hwstacksizeperthread              << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_PVT_MEM_PARAM(...) pack_A6XX_SP_CS_PVT_MEM_PARAM((struct A6XX_SP_CS_PVT_MEM_PARAM) { __VA_ARGS__ })

struct A6XX_SP_CS_PVT_MEM_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_PVT_MEM_ADDR(struct A6XX_SP_CS_PVT_MEM_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_PVT_MEM_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_PVT_MEM_ADDR(...) pack_A6XX_SP_CS_PVT_MEM_ADDR((struct A6XX_SP_CS_PVT_MEM_ADDR) { __VA_ARGS__ })

struct A6XX_SP_CS_PVT_MEM_SIZE {
    uint32_t							totalpvtmemsize;
    bool							perwavememlayout;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_PVT_MEM_SIZE(struct A6XX_SP_CS_PVT_MEM_SIZE fields)
{
#ifndef NDEBUG
    assert(((fields.totalpvtmemsize >> 12)           & 0xfffc0000) == 0);
    assert((fields.unknown                           & 0x8003ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_PVT_MEM_SIZE,
        .value =
            ((fields.totalpvtmemsize >> 12)           <<  0) |
            (fields.perwavememlayout                  << 31) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_PVT_MEM_SIZE(...) pack_A6XX_SP_CS_PVT_MEM_SIZE((struct A6XX_SP_CS_PVT_MEM_SIZE) { __VA_ARGS__ })

struct A6XX_SP_CS_TEX_COUNT {
    uint32_t							a6xx_sp_cs_tex_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_TEX_COUNT(struct A6XX_SP_CS_TEX_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_cs_tex_count              & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_TEX_COUNT,
        .value =
            (fields.a6xx_sp_cs_tex_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_TEX_COUNT(...) pack_A6XX_SP_CS_TEX_COUNT((struct A6XX_SP_CS_TEX_COUNT) { __VA_ARGS__ })

struct A6XX_SP_CS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_CONFIG(struct A6XX_SP_CS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_CONFIG(...) pack_A6XX_SP_CS_CONFIG((struct A6XX_SP_CS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_CS_INSTRLEN {
    uint32_t							a6xx_sp_cs_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_INSTRLEN(struct A6XX_SP_CS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_cs_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_INSTRLEN,
        .value =
            (fields.a6xx_sp_cs_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_INSTRLEN(...) pack_A6XX_SP_CS_INSTRLEN((struct A6XX_SP_CS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET {
    uint32_t							offset;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET(struct A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET fields)
{
#ifndef NDEBUG
    assert(((fields.offset >> 11)                    & 0xfff80000) == 0);
    assert((fields.unknown                           & 0x0007ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET,
        .value =
            ((fields.offset >> 11)                    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET(...) pack_A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET((struct A6XX_SP_CS_PVT_MEM_HW_STACK_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_CS_CNTL_0 {
    uint32_t							wgidconstid;
    uint32_t							wgsizeconstid;
    uint32_t							wgoffsetconstid;
    uint32_t							localidregid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_CNTL_0(struct A6XX_SP_CS_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.wgidconstid                       & 0xffffff00) == 0);
    assert((fields.wgsizeconstid                     & 0xffffff00) == 0);
    assert((fields.wgoffsetconstid                   & 0xffffff00) == 0);
    assert((fields.localidregid                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_CNTL_0,
        .value =
            (fields.wgidconstid                       <<  0) |
            (fields.wgsizeconstid                     <<  8) |
            (fields.wgoffsetconstid                   << 16) |
            (fields.localidregid                      << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_CNTL_0(...) pack_A6XX_SP_CS_CNTL_0((struct A6XX_SP_CS_CNTL_0) { __VA_ARGS__ })

struct A6XX_SP_CS_CNTL_1 {
    uint32_t							linearlocalidregid;
    bool							single_sp_core;
    enum a6xx_threadsize					threadsize;
    bool							threadsize_scalar;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_CNTL_1(struct A6XX_SP_CS_CNTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.linearlocalidregid                & 0xffffff00) == 0);
    assert((fields.threadsize                        & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_CNTL_1,
        .value =
            (fields.linearlocalidregid                <<  0) |
            (fields.single_sp_core                    <<  8) |
            (fields.threadsize                        <<  9) |
            (fields.threadsize_scalar                 << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_CNTL_1(...) pack_A6XX_SP_CS_CNTL_1((struct A6XX_SP_CS_CNTL_1) { __VA_ARGS__ })

struct A6XX_SP_FS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_TEX_SAMP(struct A6XX_SP_FS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_FS_TEX_SAMP(...) pack_A6XX_SP_FS_TEX_SAMP((struct A6XX_SP_FS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_TEX_SAMP {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_TEX_SAMP(struct A6XX_SP_CS_TEX_SAMP fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_TEX_SAMP,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_CS_TEX_SAMP(...) pack_A6XX_SP_CS_TEX_SAMP((struct A6XX_SP_CS_TEX_SAMP) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_FS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_TEX_CONST(struct A6XX_SP_FS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_FS_TEX_CONST(...) pack_A6XX_SP_FS_TEX_CONST((struct A6XX_SP_FS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_TEX_CONST {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_TEX_CONST(struct A6XX_SP_CS_TEX_CONST fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_TEX_CONST,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_CS_TEX_CONST(...) pack_A6XX_SP_CS_TEX_CONST((struct A6XX_SP_CS_TEX_CONST) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_BINDLESS_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_BINDLESS_BASE_ADDR(uint32_t i, struct A6XX_SP_CS_BINDLESS_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_BINDLESS_BASE_ADDR(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_CS_BINDLESS_BASE_ADDR(i, ...) pack_A6XX_SP_CS_BINDLESS_BASE_ADDR(i, (struct A6XX_SP_CS_BINDLESS_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_IBO {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_IBO(struct A6XX_SP_CS_IBO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_IBO,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_CS_IBO(...) pack_A6XX_SP_CS_IBO((struct A6XX_SP_CS_IBO) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_CS_IBO_COUNT {
    uint32_t							a6xx_sp_cs_ibo_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CS_IBO_COUNT(struct A6XX_SP_CS_IBO_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_cs_ibo_count              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CS_IBO_COUNT,
        .value =
            (fields.a6xx_sp_cs_ibo_count              <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CS_IBO_COUNT(...) pack_A6XX_SP_CS_IBO_COUNT((struct A6XX_SP_CS_IBO_COUNT) { __VA_ARGS__ })

struct A6XX_SP_MODE_CONTROL {
    bool							constant_demotion_enable;
    enum a6xx_isam_mode						isammode;
    bool							shared_consts_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_MODE_CONTROL(struct A6XX_SP_MODE_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.isammode                          & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_MODE_CONTROL,
        .value =
            (fields.constant_demotion_enable          <<  0) |
            (fields.isammode                          <<  1) |
            (fields.shared_consts_enable              <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_MODE_CONTROL(...) pack_A6XX_SP_MODE_CONTROL((struct A6XX_SP_MODE_CONTROL) { __VA_ARGS__ })

struct A6XX_SP_FS_CONFIG {
    bool							bindless_tex;
    bool							bindless_samp;
    bool							bindless_ibo;
    bool							bindless_ubo;
    bool							enabled;
    uint32_t							ntex;
    uint32_t							nsamp;
    uint32_t							nibo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_CONFIG(struct A6XX_SP_FS_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.ntex                              & 0xffffff00) == 0);
    assert((fields.nsamp                             & 0xffffffe0) == 0);
    assert((fields.nibo                              & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x1fffff0f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_CONFIG,
        .value =
            (fields.bindless_tex                      <<  0) |
            (fields.bindless_samp                     <<  1) |
            (fields.bindless_ibo                      <<  2) |
            (fields.bindless_ubo                      <<  3) |
            (fields.enabled                           <<  8) |
            (fields.ntex                              <<  9) |
            (fields.nsamp                             << 17) |
            (fields.nibo                              << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_CONFIG(...) pack_A6XX_SP_FS_CONFIG((struct A6XX_SP_FS_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_FS_INSTRLEN {
    uint32_t							a6xx_sp_fs_instrlen;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FS_INSTRLEN(struct A6XX_SP_FS_INSTRLEN fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_fs_instrlen               & 0xf0000000) == 0);
    assert((fields.unknown                           & 0x0fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FS_INSTRLEN,
        .value =
            (fields.a6xx_sp_fs_instrlen               <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FS_INSTRLEN(...) pack_A6XX_SP_FS_INSTRLEN((struct A6XX_SP_FS_INSTRLEN) { __VA_ARGS__ })

struct A6XX_SP_BINDLESS_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_BINDLESS_BASE_ADDR(uint32_t i, struct A6XX_SP_BINDLESS_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_BINDLESS_BASE_ADDR(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_BINDLESS_BASE_ADDR(i, ...) pack_A6XX_SP_BINDLESS_BASE_ADDR(i, (struct A6XX_SP_BINDLESS_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_IBO {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_IBO(struct A6XX_SP_IBO fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_IBO,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_IBO(...) pack_A6XX_SP_IBO((struct A6XX_SP_IBO) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_IBO_COUNT {
    uint32_t							a6xx_sp_ibo_count;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_IBO_COUNT(struct A6XX_SP_IBO_COUNT fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_ibo_count                 & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_IBO_COUNT,
        .value =
            (fields.a6xx_sp_ibo_count                 <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_IBO_COUNT(...) pack_A6XX_SP_IBO_COUNT((struct A6XX_SP_IBO_COUNT) { __VA_ARGS__ })

struct A6XX_SP_2D_DST_FORMAT {
    bool							norm;
    bool							sint;
    bool							uint;
    enum a6xx_format						color_format;
    bool							srgb;
    uint32_t							mask;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_2D_DST_FORMAT(struct A6XX_SP_2D_DST_FORMAT fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.mask                              & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_2D_DST_FORMAT,
        .value =
            (fields.norm                              <<  0) |
            (fields.sint                              <<  1) |
            (fields.uint                              <<  2) |
            (fields.color_format                      <<  3) |
            (fields.srgb                              << 11) |
            (fields.mask                              << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_2D_DST_FORMAT(...) pack_A6XX_SP_2D_DST_FORMAT((struct A6XX_SP_2D_DST_FORMAT) { __VA_ARGS__ })

struct A6XX_SP_UNKNOWN_AE00 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_AE00(struct A6XX_SP_UNKNOWN_AE00 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_AE00,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_AE00(...) pack_A6XX_SP_UNKNOWN_AE00((struct A6XX_SP_UNKNOWN_AE00) { __VA_ARGS__ })

struct A6XX_SP_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_sp_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_ADDR_MODE_CNTL(struct A6XX_SP_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_sp_addr_mode_cntl            & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_sp_addr_mode_cntl            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_ADDR_MODE_CNTL(...) pack_A6XX_SP_ADDR_MODE_CNTL((struct A6XX_SP_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_NC_MODE_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_NC_MODE_CNTL(struct A6XX_SP_NC_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_NC_MODE_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_NC_MODE_CNTL(...) pack_A6XX_SP_NC_MODE_CNTL((struct A6XX_SP_NC_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_CHICKEN_BITS {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CHICKEN_BITS(struct A6XX_SP_CHICKEN_BITS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CHICKEN_BITS,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CHICKEN_BITS(...) pack_A6XX_SP_CHICKEN_BITS((struct A6XX_SP_CHICKEN_BITS) { __VA_ARGS__ })

struct A6XX_SP_FLOAT_CNTL {
    bool							f16_no_inf;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_FLOAT_CNTL(struct A6XX_SP_FLOAT_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000008) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_FLOAT_CNTL,
        .value =
            (fields.f16_no_inf                        <<  3) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_FLOAT_CNTL(...) pack_A6XX_SP_FLOAT_CNTL((struct A6XX_SP_FLOAT_CNTL) { __VA_ARGS__ })

struct A6XX_SP_PERFCTR_ENABLE {
    bool							vs;
    bool							hs;
    bool							ds;
    bool							gs;
    bool							fs;
    bool							cs;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PERFCTR_ENABLE(struct A6XX_SP_PERFCTR_ENABLE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x0000003f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PERFCTR_ENABLE,
        .value =
            (fields.vs                                <<  0) |
            (fields.hs                                <<  1) |
            (fields.ds                                <<  2) |
            (fields.gs                                <<  3) |
            (fields.fs                                <<  4) |
            (fields.cs                                <<  5) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PERFCTR_ENABLE(...) pack_A6XX_SP_PERFCTR_ENABLE((struct A6XX_SP_PERFCTR_ENABLE) { __VA_ARGS__ })

struct A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE(struct A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE(...) pack_A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE((struct A6XX_SP_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE) { __VA_ARGS__ })

struct A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR(struct A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR(...) pack_A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR((struct A6XX_SP_PS_TP_BORDER_COLOR_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_UNKNOWN_B182 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_B182(struct A6XX_SP_UNKNOWN_B182 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_B182,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_B182(...) pack_A6XX_SP_UNKNOWN_B182((struct A6XX_SP_UNKNOWN_B182) { __VA_ARGS__ })

struct A6XX_SP_UNKNOWN_B183 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_B183(struct A6XX_SP_UNKNOWN_B183 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_B183,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_B183(...) pack_A6XX_SP_UNKNOWN_B183((struct A6XX_SP_UNKNOWN_B183) { __VA_ARGS__ })

struct A6XX_SP_UNKNOWN_B190 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_B190(struct A6XX_SP_UNKNOWN_B190 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_B190,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_B190(...) pack_A6XX_SP_UNKNOWN_B190((struct A6XX_SP_UNKNOWN_B190) { __VA_ARGS__ })

struct A6XX_SP_UNKNOWN_B191 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_UNKNOWN_B191(struct A6XX_SP_UNKNOWN_B191 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_UNKNOWN_B191,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_UNKNOWN_B191(...) pack_A6XX_SP_UNKNOWN_B191((struct A6XX_SP_UNKNOWN_B191) { __VA_ARGS__ })

struct A6XX_SP_TP_RAS_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    uint32_t							unk2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_RAS_MSAA_CNTL(struct A6XX_SP_TP_RAS_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unk2                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x0000000f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_RAS_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.unk2                              <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_RAS_MSAA_CNTL(...) pack_A6XX_SP_TP_RAS_MSAA_CNTL((struct A6XX_SP_TP_RAS_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_SP_TP_DEST_MSAA_CNTL {
    enum a3xx_msaa_samples					samples;
    bool							msaa_disable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_DEST_MSAA_CNTL(struct A6XX_SP_TP_DEST_MSAA_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x00000007) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_DEST_MSAA_CNTL,
        .value =
            (fields.samples                           <<  0) |
            (fields.msaa_disable                      <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_DEST_MSAA_CNTL(...) pack_A6XX_SP_TP_DEST_MSAA_CNTL((struct A6XX_SP_TP_DEST_MSAA_CNTL) { __VA_ARGS__ })

struct A6XX_SP_TP_BORDER_COLOR_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_BORDER_COLOR_BASE_ADDR(struct A6XX_SP_TP_BORDER_COLOR_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_BORDER_COLOR_BASE_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_TP_BORDER_COLOR_BASE_ADDR(...) pack_A6XX_SP_TP_BORDER_COLOR_BASE_ADDR((struct A6XX_SP_TP_BORDER_COLOR_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_TP_SAMPLE_CONFIG {
    uint32_t							unk0;
    bool							location_enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_SAMPLE_CONFIG(struct A6XX_SP_TP_SAMPLE_CONFIG fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x00000003) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_SAMPLE_CONFIG,
        .value =
            (fields.unk0                              <<  0) |
            (fields.location_enable                   <<  1) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_SAMPLE_CONFIG(...) pack_A6XX_SP_TP_SAMPLE_CONFIG((struct A6XX_SP_TP_SAMPLE_CONFIG) { __VA_ARGS__ })

struct A6XX_SP_TP_SAMPLE_LOCATION_0 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_SAMPLE_LOCATION_0(struct A6XX_SP_TP_SAMPLE_LOCATION_0 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_SAMPLE_LOCATION_0,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_SAMPLE_LOCATION_0(...) pack_A6XX_SP_TP_SAMPLE_LOCATION_0((struct A6XX_SP_TP_SAMPLE_LOCATION_0) { __VA_ARGS__ })

struct A6XX_SP_TP_SAMPLE_LOCATION_1 {
    float							sample_0_x;
    float							sample_0_y;
    float							sample_1_x;
    float							sample_1_y;
    float							sample_2_x;
    float							sample_2_y;
    float							sample_3_x;
    float							sample_3_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_SAMPLE_LOCATION_1(struct A6XX_SP_TP_SAMPLE_LOCATION_1 fields)
{
#ifndef NDEBUG
    assert((((int32_t)(fields.sample_0_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_0_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_1_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_2_y * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_x * 16.0))    & 0xfffffff0) == 0);
    assert((((int32_t)(fields.sample_3_y * 16.0))    & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_SAMPLE_LOCATION_1,
        .value =
            (((int32_t)(fields.sample_0_x * 16.0))    <<  0) |
            (((int32_t)(fields.sample_0_y * 16.0))    <<  4) |
            (((int32_t)(fields.sample_1_x * 16.0))    <<  8) |
            (((int32_t)(fields.sample_1_y * 16.0))    << 12) |
            (((int32_t)(fields.sample_2_x * 16.0))    << 16) |
            (((int32_t)(fields.sample_2_y * 16.0))    << 20) |
            (((int32_t)(fields.sample_3_x * 16.0))    << 24) |
            (((int32_t)(fields.sample_3_y * 16.0))    << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_SAMPLE_LOCATION_1(...) pack_A6XX_SP_TP_SAMPLE_LOCATION_1((struct A6XX_SP_TP_SAMPLE_LOCATION_1) { __VA_ARGS__ })

struct A6XX_SP_TP_WINDOW_OFFSET {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_WINDOW_OFFSET(struct A6XX_SP_TP_WINDOW_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_WINDOW_OFFSET,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_WINDOW_OFFSET(...) pack_A6XX_SP_TP_WINDOW_OFFSET((struct A6XX_SP_TP_WINDOW_OFFSET) { __VA_ARGS__ })

struct A6XX_SP_TP_MODE_CNTL {
    enum a6xx_isam_mode						isammode;
    uint32_t							unk3;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_TP_MODE_CNTL(struct A6XX_SP_TP_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.isammode                          & 0xfffffffc) == 0);
    assert((fields.unk3                              & 0xffffffc0) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_TP_MODE_CNTL,
        .value =
            (fields.isammode                          <<  0) |
            (fields.unk3                              <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_TP_MODE_CNTL(...) pack_A6XX_SP_TP_MODE_CNTL((struct A6XX_SP_TP_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_SP_PS_2D_SRC_INFO {
    enum a6xx_format						color_format;
    enum a6xx_tile_mode						tile_mode;
    enum a3xx_color_swap					color_swap;
    bool							flags;
    bool							srgb;
    enum a3xx_msaa_samples					samples;
    bool							filter;
    bool							unk17;
    bool							samples_average;
    bool							unk19;
    bool							unk20;
    bool							unk21;
    bool							unk22;
    uint32_t							unk23;
    bool							unk28;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_INFO(struct A6XX_SP_PS_2D_SRC_INFO fields)
{
#ifndef NDEBUG
    assert((fields.color_format                      & 0xffffff00) == 0);
    assert((fields.tile_mode                         & 0xfffffffc) == 0);
    assert((fields.color_swap                        & 0xfffffffc) == 0);
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.unk23                             & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x17ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_INFO,
        .value =
            (fields.color_format                      <<  0) |
            (fields.tile_mode                         <<  8) |
            (fields.color_swap                        << 10) |
            (fields.flags                             << 12) |
            (fields.srgb                              << 13) |
            (fields.samples                           << 14) |
            (fields.filter                            << 16) |
            (fields.unk17                             << 17) |
            (fields.samples_average                   << 18) |
            (fields.unk19                             << 19) |
            (fields.unk20                             << 20) |
            (fields.unk21                             << 21) |
            (fields.unk22                             << 22) |
            (fields.unk23                             << 23) |
            (fields.unk28                             << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_INFO(...) pack_A6XX_SP_PS_2D_SRC_INFO((struct A6XX_SP_PS_2D_SRC_INFO) { __VA_ARGS__ })

struct A6XX_SP_PS_2D_SRC_SIZE {
    uint32_t							width;
    uint32_t							height;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_SIZE(struct A6XX_SP_PS_2D_SRC_SIZE fields)
{
#ifndef NDEBUG
    assert((fields.width                             & 0xffff8000) == 0);
    assert((fields.height                            & 0xffff8000) == 0);
    assert((fields.unknown                           & 0x3fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_SIZE,
        .value =
            (fields.width                             <<  0) |
            (fields.height                            << 15) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_SIZE(...) pack_A6XX_SP_PS_2D_SRC_SIZE((struct A6XX_SP_PS_2D_SRC_SIZE) { __VA_ARGS__ })

struct A6XX_SP_PS_2D_SRC {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC(struct A6XX_SP_PS_2D_SRC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC(...) pack_A6XX_SP_PS_2D_SRC((struct A6XX_SP_PS_2D_SRC) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_PS_2D_SRC_PITCH {
    uint32_t							unk0;
    uint32_t							pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_PITCH(struct A6XX_SP_PS_2D_SRC_PITCH fields)
{
#ifndef NDEBUG
    assert((fields.unk0                              & 0xfffffe00) == 0);
    assert(((fields.pitch >> 6)                      & 0xffff8000) == 0);
    assert((fields.unknown                           & 0x00ffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_PITCH,
        .value =
            (fields.unk0                              <<  0) |
            ((fields.pitch >> 6)                      <<  9) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_PITCH(...) pack_A6XX_SP_PS_2D_SRC_PITCH((struct A6XX_SP_PS_2D_SRC_PITCH) { __VA_ARGS__ })

struct A6XX_SP_PS_2D_SRC_PLANE1 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_PLANE1(struct A6XX_SP_PS_2D_SRC_PLANE1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_PLANE1,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_PLANE1(...) pack_A6XX_SP_PS_2D_SRC_PLANE1((struct A6XX_SP_PS_2D_SRC_PLANE1) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_PS_2D_SRC_PLANE_PITCH {
    uint32_t							a6xx_sp_ps_2d_src_plane_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_PLANE_PITCH(struct A6XX_SP_PS_2D_SRC_PLANE_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_sp_ps_2d_src_plane_pitch >> 6) & 0xfffff000) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_PLANE_PITCH,
        .value =
            ((fields.a6xx_sp_ps_2d_src_plane_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_PLANE_PITCH(...) pack_A6XX_SP_PS_2D_SRC_PLANE_PITCH((struct A6XX_SP_PS_2D_SRC_PLANE_PITCH) { __VA_ARGS__ })

struct A6XX_SP_PS_2D_SRC_PLANE2 {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_PLANE2(struct A6XX_SP_PS_2D_SRC_PLANE2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_PLANE2,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_PLANE2(...) pack_A6XX_SP_PS_2D_SRC_PLANE2((struct A6XX_SP_PS_2D_SRC_PLANE2) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_PS_2D_SRC_FLAGS {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_FLAGS(struct A6XX_SP_PS_2D_SRC_FLAGS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_FLAGS,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_FLAGS(...) pack_A6XX_SP_PS_2D_SRC_FLAGS((struct A6XX_SP_PS_2D_SRC_FLAGS) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_SP_PS_2D_SRC_FLAGS_PITCH {
    uint32_t							a6xx_sp_ps_2d_src_flags_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_2D_SRC_FLAGS_PITCH(struct A6XX_SP_PS_2D_SRC_FLAGS_PITCH fields)
{
#ifndef NDEBUG
    assert(((fields.a6xx_sp_ps_2d_src_flags_pitch >> 6) & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_2D_SRC_FLAGS_PITCH,
        .value =
            ((fields.a6xx_sp_ps_2d_src_flags_pitch >> 6) <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_2D_SRC_FLAGS_PITCH(...) pack_A6XX_SP_PS_2D_SRC_FLAGS_PITCH((struct A6XX_SP_PS_2D_SRC_FLAGS_PITCH) { __VA_ARGS__ })

struct A6XX_SP_PS_UNKNOWN_B4CD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_UNKNOWN_B4CD(struct A6XX_SP_PS_UNKNOWN_B4CD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_UNKNOWN_B4CD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_UNKNOWN_B4CD(...) pack_A6XX_SP_PS_UNKNOWN_B4CD((struct A6XX_SP_PS_UNKNOWN_B4CD) { __VA_ARGS__ })

struct A6XX_SP_PS_UNKNOWN_B4CE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_UNKNOWN_B4CE(struct A6XX_SP_PS_UNKNOWN_B4CE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_UNKNOWN_B4CE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_UNKNOWN_B4CE(...) pack_A6XX_SP_PS_UNKNOWN_B4CE((struct A6XX_SP_PS_UNKNOWN_B4CE) { __VA_ARGS__ })

struct A6XX_SP_PS_UNKNOWN_B4CF {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_UNKNOWN_B4CF(struct A6XX_SP_PS_UNKNOWN_B4CF fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_UNKNOWN_B4CF,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_UNKNOWN_B4CF(...) pack_A6XX_SP_PS_UNKNOWN_B4CF((struct A6XX_SP_PS_UNKNOWN_B4CF) { __VA_ARGS__ })

struct A6XX_SP_PS_UNKNOWN_B4D0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_PS_UNKNOWN_B4D0(struct A6XX_SP_PS_UNKNOWN_B4D0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_PS_UNKNOWN_B4D0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_PS_UNKNOWN_B4D0(...) pack_A6XX_SP_PS_UNKNOWN_B4D0((struct A6XX_SP_PS_UNKNOWN_B4D0) { __VA_ARGS__ })

struct A6XX_SP_WINDOW_OFFSET {
    uint32_t							x;
    uint32_t							y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_SP_WINDOW_OFFSET(struct A6XX_SP_WINDOW_OFFSET fields)
{
#ifndef NDEBUG
    assert((fields.x                                 & 0xffffc000) == 0);
    assert((fields.y                                 & 0xffffc000) == 0);
    assert((fields.unknown                           & 0x3fff3fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_SP_WINDOW_OFFSET,
        .value =
            (fields.x                                 <<  0) |
            (fields.y                                 << 16) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_SP_WINDOW_OFFSET(...) pack_A6XX_SP_WINDOW_OFFSET((struct A6XX_SP_WINDOW_OFFSET) { __VA_ARGS__ })

struct A6XX_TPL1_DBG_ECO_CNTL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_DBG_ECO_CNTL(struct A6XX_TPL1_DBG_ECO_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_DBG_ECO_CNTL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_DBG_ECO_CNTL(...) pack_A6XX_TPL1_DBG_ECO_CNTL((struct A6XX_TPL1_DBG_ECO_CNTL) { __VA_ARGS__ })

struct A6XX_TPL1_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_tpl1_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_ADDR_MODE_CNTL(struct A6XX_TPL1_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_tpl1_addr_mode_cntl          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_tpl1_addr_mode_cntl          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_ADDR_MODE_CNTL(...) pack_A6XX_TPL1_ADDR_MODE_CNTL((struct A6XX_TPL1_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_TPL1_UNKNOWN_B602 {
    uint32_t							a6xx_tpl1_unknown_b602;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_UNKNOWN_B602(struct A6XX_TPL1_UNKNOWN_B602 fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_tpl1_unknown_b602            & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_UNKNOWN_B602,
        .value =
            (fields.a6xx_tpl1_unknown_b602            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_UNKNOWN_B602(...) pack_A6XX_TPL1_UNKNOWN_B602((struct A6XX_TPL1_UNKNOWN_B602) { __VA_ARGS__ })

struct A6XX_TPL1_NC_MODE_CNTL {
    bool							mode;
    uint32_t							lower_bit;
    bool							min_access_length;
    uint32_t							upper_bit;
    uint32_t							unk6;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_NC_MODE_CNTL(struct A6XX_TPL1_NC_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.lower_bit                         & 0xfffffffc) == 0);
    assert((fields.upper_bit                         & 0xfffffffe) == 0);
    assert((fields.unk6                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0x000000df) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_NC_MODE_CNTL,
        .value =
            (fields.mode                              <<  0) |
            (fields.lower_bit                         <<  1) |
            (fields.min_access_length                 <<  3) |
            (fields.upper_bit                         <<  4) |
            (fields.unk6                              <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_NC_MODE_CNTL(...) pack_A6XX_TPL1_NC_MODE_CNTL((struct A6XX_TPL1_NC_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_TPL1_UNKNOWN_B605 {
    uint32_t							a6xx_tpl1_unknown_b605;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_UNKNOWN_B605(struct A6XX_TPL1_UNKNOWN_B605 fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_tpl1_unknown_b605            & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_UNKNOWN_B605,
        .value =
            (fields.a6xx_tpl1_unknown_b605            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_UNKNOWN_B605(...) pack_A6XX_TPL1_UNKNOWN_B605((struct A6XX_TPL1_UNKNOWN_B605) { __VA_ARGS__ })

struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0(struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0(...) pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0((struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_0) { __VA_ARGS__ })

struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1(struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1(...) pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1((struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_1) { __VA_ARGS__ })

struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2(struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2(...) pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2((struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_2) { __VA_ARGS__ })

struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3(struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3(...) pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3((struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_3) { __VA_ARGS__ })

struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4(struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4(...) pack_A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4((struct A6XX_TPL1_BICUBIC_WEIGHTS_TABLE_4) { __VA_ARGS__ })

struct A6XX_HLSQ_VS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_VS_CNTL(struct A6XX_HLSQ_VS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_VS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_VS_CNTL(...) pack_A6XX_HLSQ_VS_CNTL((struct A6XX_HLSQ_VS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_HS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_HS_CNTL(struct A6XX_HLSQ_HS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_HS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_HS_CNTL(...) pack_A6XX_HLSQ_HS_CNTL((struct A6XX_HLSQ_HS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_DS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_DS_CNTL(struct A6XX_HLSQ_DS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_DS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_DS_CNTL(...) pack_A6XX_HLSQ_DS_CNTL((struct A6XX_HLSQ_DS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_GS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_GS_CNTL(struct A6XX_HLSQ_GS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_GS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_GS_CNTL(...) pack_A6XX_HLSQ_GS_CNTL((struct A6XX_HLSQ_GS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_LOAD_STATE_GEOM_CMD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_GEOM_CMD(struct A6XX_HLSQ_LOAD_STATE_GEOM_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_GEOM_CMD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_GEOM_CMD(...) pack_A6XX_HLSQ_LOAD_STATE_GEOM_CMD((struct A6XX_HLSQ_LOAD_STATE_GEOM_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR(struct A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR(...) pack_A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR((struct A6XX_HLSQ_LOAD_STATE_GEOM_EXT_SRC_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_HLSQ_LOAD_STATE_GEOM_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_GEOM_DATA(struct A6XX_HLSQ_LOAD_STATE_GEOM_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_GEOM_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_GEOM_DATA(...) pack_A6XX_HLSQ_LOAD_STATE_GEOM_DATA((struct A6XX_HLSQ_LOAD_STATE_GEOM_DATA) { __VA_ARGS__ })

struct A6XX_HLSQ_FS_CNTL_0 {
    enum a6xx_threadsize					threadsize;
    bool							varyings;
    uint32_t							unk2;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_FS_CNTL_0(struct A6XX_HLSQ_FS_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.threadsize                        & 0xfffffffe) == 0);
    assert((fields.unk2                              & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0x00000fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_FS_CNTL_0,
        .value =
            (fields.threadsize                        <<  0) |
            (fields.varyings                          <<  1) |
            (fields.unk2                              <<  2) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_FS_CNTL_0(...) pack_A6XX_HLSQ_FS_CNTL_0((struct A6XX_HLSQ_FS_CNTL_0) { __VA_ARGS__ })

struct A6XX_HLSQ_UNKNOWN_B981 {
    bool							a6xx_hlsq_unknown_b981;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_UNKNOWN_B981(struct A6XX_HLSQ_UNKNOWN_B981 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_UNKNOWN_B981,
        .value =
            (fields.a6xx_hlsq_unknown_b981            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_UNKNOWN_B981(...) pack_A6XX_HLSQ_UNKNOWN_B981((struct A6XX_HLSQ_UNKNOWN_B981) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTROL_1_REG {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTROL_1_REG(struct A6XX_HLSQ_CONTROL_1_REG fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTROL_1_REG,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTROL_1_REG(...) pack_A6XX_HLSQ_CONTROL_1_REG((struct A6XX_HLSQ_CONTROL_1_REG) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTROL_2_REG {
    uint32_t							faceregid;
    uint32_t							sampleid;
    uint32_t							samplemask;
    uint32_t							centerrhw;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTROL_2_REG(struct A6XX_HLSQ_CONTROL_2_REG fields)
{
#ifndef NDEBUG
    assert((fields.faceregid                         & 0xffffff00) == 0);
    assert((fields.sampleid                          & 0xffffff00) == 0);
    assert((fields.samplemask                        & 0xffffff00) == 0);
    assert((fields.centerrhw                         & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTROL_2_REG,
        .value =
            (fields.faceregid                         <<  0) |
            (fields.sampleid                          <<  8) |
            (fields.samplemask                        << 16) |
            (fields.centerrhw                         << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTROL_2_REG(...) pack_A6XX_HLSQ_CONTROL_2_REG((struct A6XX_HLSQ_CONTROL_2_REG) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTROL_3_REG {
    uint32_t							ij_persp_pixel;
    uint32_t							ij_linear_pixel;
    uint32_t							ij_persp_centroid;
    uint32_t							ij_linear_centroid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTROL_3_REG(struct A6XX_HLSQ_CONTROL_3_REG fields)
{
#ifndef NDEBUG
    assert((fields.ij_persp_pixel                    & 0xffffff00) == 0);
    assert((fields.ij_linear_pixel                   & 0xffffff00) == 0);
    assert((fields.ij_persp_centroid                 & 0xffffff00) == 0);
    assert((fields.ij_linear_centroid                & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTROL_3_REG,
        .value =
            (fields.ij_persp_pixel                    <<  0) |
            (fields.ij_linear_pixel                   <<  8) |
            (fields.ij_persp_centroid                 << 16) |
            (fields.ij_linear_centroid                << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTROL_3_REG(...) pack_A6XX_HLSQ_CONTROL_3_REG((struct A6XX_HLSQ_CONTROL_3_REG) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTROL_4_REG {
    uint32_t							ij_persp_sample;
    uint32_t							ij_linear_sample;
    uint32_t							xycoordregid;
    uint32_t							zwcoordregid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTROL_4_REG(struct A6XX_HLSQ_CONTROL_4_REG fields)
{
#ifndef NDEBUG
    assert((fields.ij_persp_sample                   & 0xffffff00) == 0);
    assert((fields.ij_linear_sample                  & 0xffffff00) == 0);
    assert((fields.xycoordregid                      & 0xffffff00) == 0);
    assert((fields.zwcoordregid                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTROL_4_REG,
        .value =
            (fields.ij_persp_sample                   <<  0) |
            (fields.ij_linear_sample                  <<  8) |
            (fields.xycoordregid                      << 16) |
            (fields.zwcoordregid                      << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTROL_4_REG(...) pack_A6XX_HLSQ_CONTROL_4_REG((struct A6XX_HLSQ_CONTROL_4_REG) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTROL_5_REG {
    uint32_t							linelengthregid;
    uint32_t							foveationqualityregid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTROL_5_REG(struct A6XX_HLSQ_CONTROL_5_REG fields)
{
#ifndef NDEBUG
    assert((fields.linelengthregid                   & 0xffffff00) == 0);
    assert((fields.foveationqualityregid             & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTROL_5_REG,
        .value =
            (fields.linelengthregid                   <<  0) |
            (fields.foveationqualityregid             <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTROL_5_REG(...) pack_A6XX_HLSQ_CONTROL_5_REG((struct A6XX_HLSQ_CONTROL_5_REG) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_CNTL(struct A6XX_HLSQ_CS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_CNTL(...) pack_A6XX_HLSQ_CS_CNTL((struct A6XX_HLSQ_CS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_0 {
    uint32_t							kerneldim;
    uint32_t							localsizex;
    uint32_t							localsizey;
    uint32_t							localsizez;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_0(struct A6XX_HLSQ_CS_NDRANGE_0 fields)
{
#ifndef NDEBUG
    assert((fields.kerneldim                         & 0xfffffffc) == 0);
    assert((fields.localsizex                        & 0xfffffc00) == 0);
    assert((fields.localsizey                        & 0xfffffc00) == 0);
    assert((fields.localsizez                        & 0xfffffc00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_0,
        .value =
            (fields.kerneldim                         <<  0) |
            (fields.localsizex                        <<  2) |
            (fields.localsizey                        << 12) |
            (fields.localsizez                        << 22) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_0(...) pack_A6XX_HLSQ_CS_NDRANGE_0((struct A6XX_HLSQ_CS_NDRANGE_0) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_1 {
    uint32_t							globalsize_x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_1(struct A6XX_HLSQ_CS_NDRANGE_1 fields)
{
#ifndef NDEBUG
    assert((fields.globalsize_x                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_1,
        .value =
            (fields.globalsize_x                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_1(...) pack_A6XX_HLSQ_CS_NDRANGE_1((struct A6XX_HLSQ_CS_NDRANGE_1) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_2 {
    uint32_t							globaloff_x;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_2(struct A6XX_HLSQ_CS_NDRANGE_2 fields)
{
#ifndef NDEBUG
    assert((fields.globaloff_x                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_2,
        .value =
            (fields.globaloff_x                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_2(...) pack_A6XX_HLSQ_CS_NDRANGE_2((struct A6XX_HLSQ_CS_NDRANGE_2) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_3 {
    uint32_t							globalsize_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_3(struct A6XX_HLSQ_CS_NDRANGE_3 fields)
{
#ifndef NDEBUG
    assert((fields.globalsize_y                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_3,
        .value =
            (fields.globalsize_y                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_3(...) pack_A6XX_HLSQ_CS_NDRANGE_3((struct A6XX_HLSQ_CS_NDRANGE_3) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_4 {
    uint32_t							globaloff_y;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_4(struct A6XX_HLSQ_CS_NDRANGE_4 fields)
{
#ifndef NDEBUG
    assert((fields.globaloff_y                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_4,
        .value =
            (fields.globaloff_y                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_4(...) pack_A6XX_HLSQ_CS_NDRANGE_4((struct A6XX_HLSQ_CS_NDRANGE_4) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_5 {
    uint32_t							globalsize_z;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_5(struct A6XX_HLSQ_CS_NDRANGE_5 fields)
{
#ifndef NDEBUG
    assert((fields.globalsize_z                      & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_5,
        .value =
            (fields.globalsize_z                      <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_5(...) pack_A6XX_HLSQ_CS_NDRANGE_5((struct A6XX_HLSQ_CS_NDRANGE_5) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_NDRANGE_6 {
    uint32_t							globaloff_z;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_NDRANGE_6(struct A6XX_HLSQ_CS_NDRANGE_6 fields)
{
#ifndef NDEBUG
    assert((fields.globaloff_z                       & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_NDRANGE_6,
        .value =
            (fields.globaloff_z                       <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_NDRANGE_6(...) pack_A6XX_HLSQ_CS_NDRANGE_6((struct A6XX_HLSQ_CS_NDRANGE_6) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_CNTL_0 {
    uint32_t							wgidconstid;
    uint32_t							wgsizeconstid;
    uint32_t							wgoffsetconstid;
    uint32_t							localidregid;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_CNTL_0(struct A6XX_HLSQ_CS_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.wgidconstid                       & 0xffffff00) == 0);
    assert((fields.wgsizeconstid                     & 0xffffff00) == 0);
    assert((fields.wgoffsetconstid                   & 0xffffff00) == 0);
    assert((fields.localidregid                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_CNTL_0,
        .value =
            (fields.wgidconstid                       <<  0) |
            (fields.wgsizeconstid                     <<  8) |
            (fields.wgoffsetconstid                   << 16) |
            (fields.localidregid                      << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_CNTL_0(...) pack_A6XX_HLSQ_CS_CNTL_0((struct A6XX_HLSQ_CS_CNTL_0) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_CNTL_1 {
    uint32_t							linearlocalidregid;
    bool							single_sp_core;
    enum a6xx_threadsize					threadsize;
    bool							threadsize_scalar;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_CNTL_1(struct A6XX_HLSQ_CS_CNTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.linearlocalidregid                & 0xffffff00) == 0);
    assert((fields.threadsize                        & 0xfffffffe) == 0);
    assert((fields.unknown                           & 0x000007ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_CNTL_1,
        .value =
            (fields.linearlocalidregid                <<  0) |
            (fields.single_sp_core                    <<  8) |
            (fields.threadsize                        <<  9) |
            (fields.threadsize_scalar                 << 10) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_CNTL_1(...) pack_A6XX_HLSQ_CS_CNTL_1((struct A6XX_HLSQ_CS_CNTL_1) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_KERNEL_GROUP_X {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_KERNEL_GROUP_X(struct A6XX_HLSQ_CS_KERNEL_GROUP_X fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_KERNEL_GROUP_X,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_KERNEL_GROUP_X(...) pack_A6XX_HLSQ_CS_KERNEL_GROUP_X((struct A6XX_HLSQ_CS_KERNEL_GROUP_X) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_KERNEL_GROUP_Y {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_KERNEL_GROUP_Y(struct A6XX_HLSQ_CS_KERNEL_GROUP_Y fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_KERNEL_GROUP_Y,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_KERNEL_GROUP_Y(...) pack_A6XX_HLSQ_CS_KERNEL_GROUP_Y((struct A6XX_HLSQ_CS_KERNEL_GROUP_Y) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_KERNEL_GROUP_Z {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_KERNEL_GROUP_Z(struct A6XX_HLSQ_CS_KERNEL_GROUP_Z fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_KERNEL_GROUP_Z,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_KERNEL_GROUP_Z(...) pack_A6XX_HLSQ_CS_KERNEL_GROUP_Z((struct A6XX_HLSQ_CS_KERNEL_GROUP_Z) { __VA_ARGS__ })

struct A6XX_HLSQ_LOAD_STATE_FRAG_CMD {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_FRAG_CMD(struct A6XX_HLSQ_LOAD_STATE_FRAG_CMD fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_FRAG_CMD,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_FRAG_CMD(...) pack_A6XX_HLSQ_LOAD_STATE_FRAG_CMD((struct A6XX_HLSQ_LOAD_STATE_FRAG_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR(struct A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR,
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR(...) pack_A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR((struct A6XX_HLSQ_LOAD_STATE_FRAG_EXT_SRC_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_HLSQ_LOAD_STATE_FRAG_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_LOAD_STATE_FRAG_DATA(struct A6XX_HLSQ_LOAD_STATE_FRAG_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_LOAD_STATE_FRAG_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_LOAD_STATE_FRAG_DATA(...) pack_A6XX_HLSQ_LOAD_STATE_FRAG_DATA((struct A6XX_HLSQ_LOAD_STATE_FRAG_DATA) { __VA_ARGS__ })

struct A6XX_HLSQ_CS_BINDLESS_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_BINDLESS_BASE_ADDR(uint32_t i, struct A6XX_HLSQ_CS_BINDLESS_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_BINDLESS_BASE_ADDR(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_write = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_HLSQ_CS_BINDLESS_BASE_ADDR(i, ...) pack_A6XX_HLSQ_CS_BINDLESS_BASE_ADDR(i, (struct A6XX_HLSQ_CS_BINDLESS_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_HLSQ_CS_UNKNOWN_B9D0 {
    uint32_t							shared_size;
    bool							unk5;
    bool							unk6;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CS_UNKNOWN_B9D0(struct A6XX_HLSQ_CS_UNKNOWN_B9D0 fields)
{
#ifndef NDEBUG
    assert((fields.shared_size                       & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x0000007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CS_UNKNOWN_B9D0,
        .value =
            (fields.shared_size                       <<  0) |
            (fields.unk5                              <<  5) |
            (fields.unk6                              <<  6) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CS_UNKNOWN_B9D0(...) pack_A6XX_HLSQ_CS_UNKNOWN_B9D0((struct A6XX_HLSQ_CS_UNKNOWN_B9D0) { __VA_ARGS__ })

struct A6XX_HLSQ_DRAW_CMD {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_DRAW_CMD(struct A6XX_HLSQ_DRAW_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_DRAW_CMD,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_DRAW_CMD(...) pack_A6XX_HLSQ_DRAW_CMD((struct A6XX_HLSQ_DRAW_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_DISPATCH_CMD {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_DISPATCH_CMD(struct A6XX_HLSQ_DISPATCH_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_DISPATCH_CMD,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_DISPATCH_CMD(...) pack_A6XX_HLSQ_DISPATCH_CMD((struct A6XX_HLSQ_DISPATCH_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_EVENT_CMD {
    uint32_t							state_id;
    enum vgt_event_type						event;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_EVENT_CMD(struct A6XX_HLSQ_EVENT_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.event                             & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x00ff007f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_EVENT_CMD,
        .value =
            (fields.state_id                          << 16) |
            (fields.event                             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_EVENT_CMD(...) pack_A6XX_HLSQ_EVENT_CMD((struct A6XX_HLSQ_EVENT_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_INVALIDATE_CMD {
    bool							vs_state;
    bool							hs_state;
    bool							ds_state;
    bool							gs_state;
    bool							fs_state;
    bool							cs_state;
    bool							cs_ibo;
    bool							gfx_ibo;
    bool							cs_shared_const;
    bool							gfx_shared_const;
    uint32_t							cs_bindless;
    uint32_t							gfx_bindless;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_INVALIDATE_CMD(struct A6XX_HLSQ_INVALIDATE_CMD fields)
{
#ifndef NDEBUG
    assert((fields.cs_bindless                       & 0xffffffe0) == 0);
    assert((fields.gfx_bindless                      & 0xffffffe0) == 0);
    assert((fields.unknown                           & 0x000fffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_INVALIDATE_CMD,
        .value =
            (fields.vs_state                          <<  0) |
            (fields.hs_state                          <<  1) |
            (fields.ds_state                          <<  2) |
            (fields.gs_state                          <<  3) |
            (fields.fs_state                          <<  4) |
            (fields.cs_state                          <<  5) |
            (fields.cs_ibo                            <<  6) |
            (fields.gfx_ibo                           <<  7) |
            (fields.cs_shared_const                   << 19) |
            (fields.gfx_shared_const                  <<  8) |
            (fields.cs_bindless                       <<  9) |
            (fields.gfx_bindless                      << 14) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_INVALIDATE_CMD(...) pack_A6XX_HLSQ_INVALIDATE_CMD((struct A6XX_HLSQ_INVALIDATE_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_FS_CNTL {
    uint32_t							constlen;
    bool							enabled;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_FS_CNTL(struct A6XX_HLSQ_FS_CNTL fields)
{
#ifndef NDEBUG
    assert(((fields.constlen >> 2)                   & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000001ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_FS_CNTL,
        .value =
            ((fields.constlen >> 2)                   <<  0) |
            (fields.enabled                           <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_FS_CNTL(...) pack_A6XX_HLSQ_FS_CNTL((struct A6XX_HLSQ_FS_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_SHARED_CONSTS {
    bool							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_SHARED_CONSTS(struct A6XX_HLSQ_SHARED_CONSTS fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000001) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_SHARED_CONSTS,
        .value =
            (fields.enable                            <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_SHARED_CONSTS(...) pack_A6XX_HLSQ_SHARED_CONSTS((struct A6XX_HLSQ_SHARED_CONSTS) { __VA_ARGS__ })

struct A6XX_HLSQ_BINDLESS_BASE_ADDR {
    __bo_type							bo;
    uint32_t							bo_offset;
    uint64_t							unknown;
    uint64_t							qword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_BINDLESS_BASE_ADDR(uint32_t i, struct A6XX_HLSQ_BINDLESS_BASE_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_BINDLESS_BASE_ADDR(i),
        .value =
            fields.unknown | fields.qword,
        .bo = fields.bo,
        .is_address = true,
        .bo_offset = fields.bo_offset,
        .bo_shift = 0
    };

    return pair;

}

#define A6XX_HLSQ_BINDLESS_BASE_ADDR(i, ...) pack_A6XX_HLSQ_BINDLESS_BASE_ADDR(i, (struct A6XX_HLSQ_BINDLESS_BASE_ADDR) { __VA_ARGS__ }), { .reg = 0 }

struct A6XX_HLSQ_2D_EVENT_CMD {
    uint32_t							state_id;
    enum vgt_event_type						event;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_2D_EVENT_CMD(struct A6XX_HLSQ_2D_EVENT_CMD fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.event                             & 0xffffff80) == 0);
    assert((fields.unknown                           & 0x0000ff7f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_2D_EVENT_CMD,
        .value =
            (fields.state_id                          <<  8) |
            (fields.event                             <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_2D_EVENT_CMD(...) pack_A6XX_HLSQ_2D_EVENT_CMD((struct A6XX_HLSQ_2D_EVENT_CMD) { __VA_ARGS__ })

struct A6XX_HLSQ_UNKNOWN_BE00 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_UNKNOWN_BE00(struct A6XX_HLSQ_UNKNOWN_BE00 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_UNKNOWN_BE00,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_UNKNOWN_BE00(...) pack_A6XX_HLSQ_UNKNOWN_BE00((struct A6XX_HLSQ_UNKNOWN_BE00) { __VA_ARGS__ })

struct A6XX_HLSQ_UNKNOWN_BE01 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_UNKNOWN_BE01(struct A6XX_HLSQ_UNKNOWN_BE01 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_UNKNOWN_BE01,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_UNKNOWN_BE01(...) pack_A6XX_HLSQ_UNKNOWN_BE01((struct A6XX_HLSQ_UNKNOWN_BE01) { __VA_ARGS__ })

struct A6XX_HLSQ_UNKNOWN_BE04 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_UNKNOWN_BE04(struct A6XX_HLSQ_UNKNOWN_BE04 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_UNKNOWN_BE04,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_UNKNOWN_BE04(...) pack_A6XX_HLSQ_UNKNOWN_BE04((struct A6XX_HLSQ_UNKNOWN_BE04) { __VA_ARGS__ })

struct A6XX_HLSQ_ADDR_MODE_CNTL {
    enum a5xx_address_mode					a6xx_hlsq_addr_mode_cntl;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_ADDR_MODE_CNTL(struct A6XX_HLSQ_ADDR_MODE_CNTL fields)
{
#ifndef NDEBUG
    assert((fields.a6xx_hlsq_addr_mode_cntl          & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_ADDR_MODE_CNTL,
        .value =
            (fields.a6xx_hlsq_addr_mode_cntl          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_ADDR_MODE_CNTL(...) pack_A6XX_HLSQ_ADDR_MODE_CNTL((struct A6XX_HLSQ_ADDR_MODE_CNTL) { __VA_ARGS__ })

struct A6XX_HLSQ_UNKNOWN_BE08 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_UNKNOWN_BE08(struct A6XX_HLSQ_UNKNOWN_BE08 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_UNKNOWN_BE08,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_UNKNOWN_BE08(...) pack_A6XX_HLSQ_UNKNOWN_BE08((struct A6XX_HLSQ_UNKNOWN_BE08) { __VA_ARGS__ })

struct A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE(struct A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE(...) pack_A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE((struct A6XX_HLSQ_CONTEXT_SWITCH_GFX_PREEMPTION_SAFE_MODE) { __VA_ARGS__ })

struct A6XX_CP_EVENT_START {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_EVENT_START(struct A6XX_CP_EVENT_START fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_EVENT_START,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_EVENT_START(...) pack_A6XX_CP_EVENT_START((struct A6XX_CP_EVENT_START) { __VA_ARGS__ })

struct A6XX_CP_EVENT_END {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_EVENT_END(struct A6XX_CP_EVENT_END fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_EVENT_END,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_EVENT_END(...) pack_A6XX_CP_EVENT_END((struct A6XX_CP_EVENT_END) { __VA_ARGS__ })

struct A6XX_CP_2D_EVENT_START {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_2D_EVENT_START(struct A6XX_CP_2D_EVENT_START fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_2D_EVENT_START,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_2D_EVENT_START(...) pack_A6XX_CP_2D_EVENT_START((struct A6XX_CP_2D_EVENT_START) { __VA_ARGS__ })

struct A6XX_CP_2D_EVENT_END {
    uint32_t							state_id;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CP_2D_EVENT_END(struct A6XX_CP_2D_EVENT_END fields)
{
#ifndef NDEBUG
    assert((fields.state_id                          & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x000000ff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CP_2D_EVENT_END,
        .value =
            (fields.state_id                          <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CP_2D_EVENT_END(...) pack_A6XX_CP_2D_EVENT_END((struct A6XX_CP_2D_EVENT_END) { __VA_ARGS__ })

struct A6XX_TEX_SAMP_0 {
    bool							mipfilter_linear_near;
    enum a6xx_tex_filter					xy_mag;
    enum a6xx_tex_filter					xy_min;
    enum a6xx_tex_clamp						wrap_s;
    enum a6xx_tex_clamp						wrap_t;
    enum a6xx_tex_clamp						wrap_r;
    enum a6xx_tex_aniso						aniso;
    float							lod_bias;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_SAMP_0(struct A6XX_TEX_SAMP_0 fields)
{
#ifndef NDEBUG
    assert((fields.xy_mag                            & 0xfffffffc) == 0);
    assert((fields.xy_min                            & 0xfffffffc) == 0);
    assert((fields.wrap_s                            & 0xfffffff8) == 0);
    assert((fields.wrap_t                            & 0xfffffff8) == 0);
    assert((fields.wrap_r                            & 0xfffffff8) == 0);
    assert((fields.aniso                             & 0xfffffff8) == 0);
    assert((((int32_t)(fields.lod_bias * 256.0))     & 0xffffe000) == 0);
    assert((fields.unknown                           & 0xfff9ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_SAMP_0,
        .value =
            (fields.mipfilter_linear_near             <<  0) |
            (fields.xy_mag                            <<  1) |
            (fields.xy_min                            <<  3) |
            (fields.wrap_s                            <<  5) |
            (fields.wrap_t                            <<  8) |
            (fields.wrap_r                            << 11) |
            (fields.aniso                             << 14) |
            (((int32_t)(fields.lod_bias * 256.0))     << 19) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_SAMP_0(...) pack_A6XX_TEX_SAMP_0((struct A6XX_TEX_SAMP_0) { __VA_ARGS__ })

struct A6XX_TEX_SAMP_1 {
    bool							clampenable;
    enum adreno_compare_func					compare_func;
    bool							cubemapseamlessfiltoff;
    bool							unnorm_coords;
    bool							mipfilter_linear_far;
    float							max_lod;
    float							min_lod;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_SAMP_1(struct A6XX_TEX_SAMP_1 fields)
{
#ifndef NDEBUG
    assert((fields.compare_func                      & 0xfffffff8) == 0);
    assert((((uint32_t)(fields.max_lod * 256.0))     & 0xfffff000) == 0);
    assert((((uint32_t)(fields.min_lod * 256.0))     & 0xfffff000) == 0);
    assert((fields.unknown                           & 0xffffff7f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_SAMP_1,
        .value =
            (fields.clampenable                       <<  0) |
            (fields.compare_func                      <<  1) |
            (fields.cubemapseamlessfiltoff            <<  4) |
            (fields.unnorm_coords                     <<  5) |
            (fields.mipfilter_linear_far              <<  6) |
            (((uint32_t)(fields.max_lod * 256.0))     <<  8) |
            (((uint32_t)(fields.min_lod * 256.0))     << 20) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_SAMP_1(...) pack_A6XX_TEX_SAMP_1((struct A6XX_TEX_SAMP_1) { __VA_ARGS__ })

struct A6XX_TEX_SAMP_2 {
    enum a6xx_reduction_mode					reduction_mode;
    bool							chroma_linear;
    uint32_t							bcolor;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_SAMP_2(struct A6XX_TEX_SAMP_2 fields)
{
#ifndef NDEBUG
    assert((fields.reduction_mode                    & 0xfffffffc) == 0);
    assert((fields.bcolor                            & 0xfe000000) == 0);
    assert((fields.unknown                           & 0xffffffa3) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_SAMP_2,
        .value =
            (fields.reduction_mode                    <<  0) |
            (fields.chroma_linear                     <<  5) |
            (fields.bcolor                            <<  7) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_SAMP_2(...) pack_A6XX_TEX_SAMP_2((struct A6XX_TEX_SAMP_2) { __VA_ARGS__ })

struct A6XX_TEX_SAMP_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_SAMP_3(struct A6XX_TEX_SAMP_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_SAMP_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_SAMP_3(...) pack_A6XX_TEX_SAMP_3((struct A6XX_TEX_SAMP_3) { __VA_ARGS__ })

struct A6XX_TEX_CONST_0 {
    enum a6xx_tile_mode						tile_mode;
    bool							srgb;
    enum a6xx_tex_swiz						swiz_x;
    enum a6xx_tex_swiz						swiz_y;
    enum a6xx_tex_swiz						swiz_z;
    enum a6xx_tex_swiz						swiz_w;
    uint32_t							miplvls;
    bool							chroma_midpoint_x;
    bool							chroma_midpoint_y;
    enum a3xx_msaa_samples					samples;
    enum a6xx_format						fmt;
    enum a3xx_color_swap					swap;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_0(struct A6XX_TEX_CONST_0 fields)
{
#ifndef NDEBUG
    assert((fields.tile_mode                         & 0xfffffffc) == 0);
    assert((fields.swiz_x                            & 0xfffffff8) == 0);
    assert((fields.swiz_y                            & 0xfffffff8) == 0);
    assert((fields.swiz_z                            & 0xfffffff8) == 0);
    assert((fields.swiz_w                            & 0xfffffff8) == 0);
    assert((fields.miplvls                           & 0xfffffff0) == 0);
    assert((fields.samples                           & 0xfffffffc) == 0);
    assert((fields.fmt                               & 0xffffff00) == 0);
    assert((fields.swap                              & 0xfffffffc) == 0);
    assert((fields.unknown                           & 0xfffffff7) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_0,
        .value =
            (fields.tile_mode                         <<  0) |
            (fields.srgb                              <<  2) |
            (fields.swiz_x                            <<  4) |
            (fields.swiz_y                            <<  7) |
            (fields.swiz_z                            << 10) |
            (fields.swiz_w                            << 13) |
            (fields.miplvls                           << 16) |
            (fields.chroma_midpoint_x                 << 16) |
            (fields.chroma_midpoint_y                 << 18) |
            (fields.samples                           << 20) |
            (fields.fmt                               << 22) |
            (fields.swap                              << 30) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_0(...) pack_A6XX_TEX_CONST_0((struct A6XX_TEX_CONST_0) { __VA_ARGS__ })

struct A6XX_TEX_CONST_1 {
    uint32_t							width;
    uint32_t							height;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_1(struct A6XX_TEX_CONST_1 fields)
{
#ifndef NDEBUG
    assert((fields.width                             & 0xffff8000) == 0);
    assert((fields.height                            & 0xffff8000) == 0);
    assert((fields.unknown                           & 0x3fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_1,
        .value =
            (fields.width                             <<  0) |
            (fields.height                            << 15) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_1(...) pack_A6XX_TEX_CONST_1((struct A6XX_TEX_CONST_1) { __VA_ARGS__ })

struct A6XX_TEX_CONST_2 {
    bool							buffer;
    uint32_t							pitchalign;
    uint32_t							pitch;
    enum a6xx_tex_type						type;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_2(struct A6XX_TEX_CONST_2 fields)
{
#ifndef NDEBUG
    assert((fields.pitchalign                        & 0xfffffff0) == 0);
    assert((fields.pitch                             & 0xffc00000) == 0);
    assert((fields.type                              & 0xfffffff8) == 0);
    assert((fields.unknown                           & 0xffffff9f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_2,
        .value =
            (fields.buffer                            <<  4) |
            (fields.pitchalign                        <<  0) |
            (fields.pitch                             <<  7) |
            (fields.type                              << 29) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_2(...) pack_A6XX_TEX_CONST_2((struct A6XX_TEX_CONST_2) { __VA_ARGS__ })

struct A6XX_TEX_CONST_3 {
    uint32_t							array_pitch;
    uint32_t							min_layersz;
    bool							tile_all;
    bool							flag;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_3(struct A6XX_TEX_CONST_3 fields)
{
#ifndef NDEBUG
    assert(((fields.array_pitch >> 12)               & 0xffffc000) == 0);
    assert(((fields.min_layersz >> 12)               & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x1f803fff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_3,
        .value =
            ((fields.array_pitch >> 12)               <<  0) |
            ((fields.min_layersz >> 12)               << 23) |
            (fields.tile_all                          << 27) |
            (fields.flag                              << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_3(...) pack_A6XX_TEX_CONST_3((struct A6XX_TEX_CONST_3) { __VA_ARGS__ })

struct A6XX_TEX_CONST_4 {
    uint32_t							base_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_4(struct A6XX_TEX_CONST_4 fields)
{
#ifndef NDEBUG
    assert(((fields.base_lo >> 5)                    & 0xf8000000) == 0);
    assert((fields.unknown                           & 0xffffffe0) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_4,
        .value =
            ((fields.base_lo >> 5)                    <<  5) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_4(...) pack_A6XX_TEX_CONST_4((struct A6XX_TEX_CONST_4) { __VA_ARGS__ })

struct A6XX_TEX_CONST_5 {
    uint32_t							base_hi;
    uint32_t							depth;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_5(struct A6XX_TEX_CONST_5 fields)
{
#ifndef NDEBUG
    assert((fields.base_hi                           & 0xfffe0000) == 0);
    assert((fields.depth                             & 0xffffe000) == 0);
    assert((fields.unknown                           & 0x3fffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_5,
        .value =
            (fields.base_hi                           <<  0) |
            (fields.depth                             << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_5(...) pack_A6XX_TEX_CONST_5((struct A6XX_TEX_CONST_5) { __VA_ARGS__ })

struct A6XX_TEX_CONST_6 {
    float							min_lod_clamp;
    uint32_t							plane_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_6(struct A6XX_TEX_CONST_6 fields)
{
#ifndef NDEBUG
    assert((((uint32_t)(fields.min_lod_clamp * 256.0)) & 0xfffff000) == 0);
    assert((fields.plane_pitch                       & 0xff000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_6,
        .value =
            (((uint32_t)(fields.min_lod_clamp * 256.0)) <<  0) |
            (fields.plane_pitch                       <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_6(...) pack_A6XX_TEX_CONST_6((struct A6XX_TEX_CONST_6) { __VA_ARGS__ })

struct A6XX_TEX_CONST_7 {
    uint32_t							flag_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_7(struct A6XX_TEX_CONST_7 fields)
{
#ifndef NDEBUG
    assert(((fields.flag_lo >> 5)                    & 0xf8000000) == 0);
    assert((fields.unknown                           & 0xffffffe0) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_7,
        .value =
            ((fields.flag_lo >> 5)                    <<  5) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_7(...) pack_A6XX_TEX_CONST_7((struct A6XX_TEX_CONST_7) { __VA_ARGS__ })

struct A6XX_TEX_CONST_8 {
    uint32_t							flag_hi;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_8(struct A6XX_TEX_CONST_8 fields)
{
#ifndef NDEBUG
    assert((fields.flag_hi                           & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x0001ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_8,
        .value =
            (fields.flag_hi                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_8(...) pack_A6XX_TEX_CONST_8((struct A6XX_TEX_CONST_8) { __VA_ARGS__ })

struct A6XX_TEX_CONST_9 {
    uint32_t							flag_buffer_array_pitch;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_9(struct A6XX_TEX_CONST_9 fields)
{
#ifndef NDEBUG
    assert(((fields.flag_buffer_array_pitch >> 4)    & 0xfffe0000) == 0);
    assert((fields.unknown                           & 0x0001ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_9,
        .value =
            ((fields.flag_buffer_array_pitch >> 4)    <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_9(...) pack_A6XX_TEX_CONST_9((struct A6XX_TEX_CONST_9) { __VA_ARGS__ })

struct A6XX_TEX_CONST_10 {
    uint32_t							flag_buffer_pitch;
    uint32_t							flag_buffer_logw;
    uint32_t							flag_buffer_logh;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_10(struct A6XX_TEX_CONST_10 fields)
{
#ifndef NDEBUG
    assert(((fields.flag_buffer_pitch >> 6)          & 0xffffff80) == 0);
    assert((fields.flag_buffer_logw                  & 0xfffffff0) == 0);
    assert((fields.flag_buffer_logh                  & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0000ff7f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_10,
        .value =
            ((fields.flag_buffer_pitch >> 6)          <<  0) |
            (fields.flag_buffer_logw                  <<  8) |
            (fields.flag_buffer_logh                  << 12) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_10(...) pack_A6XX_TEX_CONST_10((struct A6XX_TEX_CONST_10) { __VA_ARGS__ })

struct A6XX_TEX_CONST_11 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_11(struct A6XX_TEX_CONST_11 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_11,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_11(...) pack_A6XX_TEX_CONST_11((struct A6XX_TEX_CONST_11) { __VA_ARGS__ })

struct A6XX_TEX_CONST_12 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_12(struct A6XX_TEX_CONST_12 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_12,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_12(...) pack_A6XX_TEX_CONST_12((struct A6XX_TEX_CONST_12) { __VA_ARGS__ })

struct A6XX_TEX_CONST_13 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_13(struct A6XX_TEX_CONST_13 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_13,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_13(...) pack_A6XX_TEX_CONST_13((struct A6XX_TEX_CONST_13) { __VA_ARGS__ })

struct A6XX_TEX_CONST_14 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_14(struct A6XX_TEX_CONST_14 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_14,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_14(...) pack_A6XX_TEX_CONST_14((struct A6XX_TEX_CONST_14) { __VA_ARGS__ })

struct A6XX_TEX_CONST_15 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_TEX_CONST_15(struct A6XX_TEX_CONST_15 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_TEX_CONST_15,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_TEX_CONST_15(...) pack_A6XX_TEX_CONST_15((struct A6XX_TEX_CONST_15) { __VA_ARGS__ })

struct A6XX_UBO_0 {
    uint32_t							base_lo;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UBO_0(struct A6XX_UBO_0 fields)
{
#ifndef NDEBUG
    assert((fields.base_lo                           & 0x00000000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UBO_0,
        .value =
            (fields.base_lo                           <<  0) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UBO_0(...) pack_A6XX_UBO_0((struct A6XX_UBO_0) { __VA_ARGS__ })

struct A6XX_UBO_1 {
    uint32_t							base_hi;
    uint32_t							size;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_UBO_1(struct A6XX_UBO_1 fields)
{
#ifndef NDEBUG
    assert((fields.base_hi                           & 0xfffe0000) == 0);
    assert((fields.size                              & 0xffff8000) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_UBO_1,
        .value =
            (fields.base_hi                           <<  0) |
            (fields.size                              << 17) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_UBO_1(...) pack_A6XX_UBO_1((struct A6XX_UBO_1) { __VA_ARGS__ })

struct A6XX_PDC_GPU_ENABLE_PDC {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_ENABLE_PDC(struct A6XX_PDC_GPU_ENABLE_PDC fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_ENABLE_PDC,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_ENABLE_PDC(...) pack_A6XX_PDC_GPU_ENABLE_PDC((struct A6XX_PDC_GPU_ENABLE_PDC) { __VA_ARGS__ })

struct A6XX_PDC_GPU_SEQ_START_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_SEQ_START_ADDR(struct A6XX_PDC_GPU_SEQ_START_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_SEQ_START_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_SEQ_START_ADDR(...) pack_A6XX_PDC_GPU_SEQ_START_ADDR((struct A6XX_PDC_GPU_SEQ_START_ADDR) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CONTROL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CONTROL(struct A6XX_PDC_GPU_TCS0_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CONTROL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CONTROL(...) pack_A6XX_PDC_GPU_TCS0_CONTROL((struct A6XX_PDC_GPU_TCS0_CONTROL) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK(struct A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK(...) pack_A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK((struct A6XX_PDC_GPU_TCS0_CMD_ENABLE_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK(struct A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK(...) pack_A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK((struct A6XX_PDC_GPU_TCS0_CMD_WAIT_FOR_CMPL_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CMD0_MSGID {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CMD0_MSGID(struct A6XX_PDC_GPU_TCS0_CMD0_MSGID fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CMD0_MSGID,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CMD0_MSGID(...) pack_A6XX_PDC_GPU_TCS0_CMD0_MSGID((struct A6XX_PDC_GPU_TCS0_CMD0_MSGID) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CMD0_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CMD0_ADDR(struct A6XX_PDC_GPU_TCS0_CMD0_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CMD0_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CMD0_ADDR(...) pack_A6XX_PDC_GPU_TCS0_CMD0_ADDR((struct A6XX_PDC_GPU_TCS0_CMD0_ADDR) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS0_CMD0_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS0_CMD0_DATA(struct A6XX_PDC_GPU_TCS0_CMD0_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS0_CMD0_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS0_CMD0_DATA(...) pack_A6XX_PDC_GPU_TCS0_CMD0_DATA((struct A6XX_PDC_GPU_TCS0_CMD0_DATA) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CONTROL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CONTROL(struct A6XX_PDC_GPU_TCS1_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CONTROL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CONTROL(...) pack_A6XX_PDC_GPU_TCS1_CONTROL((struct A6XX_PDC_GPU_TCS1_CONTROL) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK(struct A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK(...) pack_A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK((struct A6XX_PDC_GPU_TCS1_CMD_ENABLE_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK(struct A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK(...) pack_A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK((struct A6XX_PDC_GPU_TCS1_CMD_WAIT_FOR_CMPL_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CMD0_MSGID {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CMD0_MSGID(struct A6XX_PDC_GPU_TCS1_CMD0_MSGID fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CMD0_MSGID,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CMD0_MSGID(...) pack_A6XX_PDC_GPU_TCS1_CMD0_MSGID((struct A6XX_PDC_GPU_TCS1_CMD0_MSGID) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CMD0_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CMD0_ADDR(struct A6XX_PDC_GPU_TCS1_CMD0_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CMD0_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CMD0_ADDR(...) pack_A6XX_PDC_GPU_TCS1_CMD0_ADDR((struct A6XX_PDC_GPU_TCS1_CMD0_ADDR) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS1_CMD0_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS1_CMD0_DATA(struct A6XX_PDC_GPU_TCS1_CMD0_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS1_CMD0_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS1_CMD0_DATA(...) pack_A6XX_PDC_GPU_TCS1_CMD0_DATA((struct A6XX_PDC_GPU_TCS1_CMD0_DATA) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CONTROL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CONTROL(struct A6XX_PDC_GPU_TCS2_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CONTROL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CONTROL(...) pack_A6XX_PDC_GPU_TCS2_CONTROL((struct A6XX_PDC_GPU_TCS2_CONTROL) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK(struct A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK(...) pack_A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK((struct A6XX_PDC_GPU_TCS2_CMD_ENABLE_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK(struct A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK(...) pack_A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK((struct A6XX_PDC_GPU_TCS2_CMD_WAIT_FOR_CMPL_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CMD0_MSGID {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CMD0_MSGID(struct A6XX_PDC_GPU_TCS2_CMD0_MSGID fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CMD0_MSGID,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CMD0_MSGID(...) pack_A6XX_PDC_GPU_TCS2_CMD0_MSGID((struct A6XX_PDC_GPU_TCS2_CMD0_MSGID) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CMD0_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CMD0_ADDR(struct A6XX_PDC_GPU_TCS2_CMD0_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CMD0_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CMD0_ADDR(...) pack_A6XX_PDC_GPU_TCS2_CMD0_ADDR((struct A6XX_PDC_GPU_TCS2_CMD0_ADDR) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS2_CMD0_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS2_CMD0_DATA(struct A6XX_PDC_GPU_TCS2_CMD0_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS2_CMD0_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS2_CMD0_DATA(...) pack_A6XX_PDC_GPU_TCS2_CMD0_DATA((struct A6XX_PDC_GPU_TCS2_CMD0_DATA) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CONTROL {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CONTROL(struct A6XX_PDC_GPU_TCS3_CONTROL fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CONTROL,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CONTROL(...) pack_A6XX_PDC_GPU_TCS3_CONTROL((struct A6XX_PDC_GPU_TCS3_CONTROL) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK(struct A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK(...) pack_A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK((struct A6XX_PDC_GPU_TCS3_CMD_ENABLE_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK(struct A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK(...) pack_A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK((struct A6XX_PDC_GPU_TCS3_CMD_WAIT_FOR_CMPL_BANK) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CMD0_MSGID {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CMD0_MSGID(struct A6XX_PDC_GPU_TCS3_CMD0_MSGID fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CMD0_MSGID,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CMD0_MSGID(...) pack_A6XX_PDC_GPU_TCS3_CMD0_MSGID((struct A6XX_PDC_GPU_TCS3_CMD0_MSGID) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CMD0_ADDR {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CMD0_ADDR(struct A6XX_PDC_GPU_TCS3_CMD0_ADDR fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CMD0_ADDR,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CMD0_ADDR(...) pack_A6XX_PDC_GPU_TCS3_CMD0_ADDR((struct A6XX_PDC_GPU_TCS3_CMD0_ADDR) { __VA_ARGS__ })

struct A6XX_PDC_GPU_TCS3_CMD0_DATA {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_TCS3_CMD0_DATA(struct A6XX_PDC_GPU_TCS3_CMD0_DATA fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_TCS3_CMD0_DATA,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_TCS3_CMD0_DATA(...) pack_A6XX_PDC_GPU_TCS3_CMD0_DATA((struct A6XX_PDC_GPU_TCS3_CMD0_DATA) { __VA_ARGS__ })

struct A6XX_PDC_GPU_SEQ_MEM_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_PDC_GPU_SEQ_MEM_0(struct A6XX_PDC_GPU_SEQ_MEM_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_PDC_GPU_SEQ_MEM_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_PDC_GPU_SEQ_MEM_0(...) pack_A6XX_PDC_GPU_SEQ_MEM_0((struct A6XX_PDC_GPU_SEQ_MEM_0) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_A {
    uint32_t							ping_index;
    uint32_t							ping_blk_sel;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_A(struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_A fields)
{
#ifndef NDEBUG
    assert((fields.ping_index                        & 0xffffff00) == 0);
    assert((fields.ping_blk_sel                      & 0xffffff00) == 0);
    assert((fields.unknown                           & 0x0000ffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_SEL_A,
        .value =
            (fields.ping_index                        <<  0) |
            (fields.ping_blk_sel                      <<  8) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_SEL_A(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_A((struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_A) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_B {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_B(struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_B fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_SEL_B,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_SEL_B(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_B((struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_B) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_C {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_C(struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_C fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_SEL_C,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_SEL_C(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_C((struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_C) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_D {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_D(struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_D fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_SEL_D,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_SEL_D(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_SEL_D((struct A6XX_CX_DBGC_CFG_DBGBUS_SEL_D) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLT {
    uint32_t							traceen;
    uint32_t							granu;
    uint32_t							segt;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_CNTLT(struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLT fields)
{
#ifndef NDEBUG
    assert((fields.traceen                           & 0xffffffc0) == 0);
    assert((fields.granu                             & 0xfffffff8) == 0);
    assert((fields.segt                              & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xf000703f) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_CNTLT,
        .value =
            (fields.traceen                           <<  0) |
            (fields.granu                             << 12) |
            (fields.segt                              << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_CNTLT(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_CNTLT((struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLT) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLM {
    uint32_t							enable;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_CNTLM(struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLM fields)
{
#ifndef NDEBUG
    assert((fields.enable                            & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0x0f000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_CNTLM,
        .value =
            (fields.enable                            << 24) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_CNTLM(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_CNTLM((struct A6XX_CX_DBGC_CFG_DBGBUS_CNTLM) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0(struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0((struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_0) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1(struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1((struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_1) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2(struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2((struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_2) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3(struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3((struct A6XX_CX_DBGC_CFG_DBGBUS_IVTL_3) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0(struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0((struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_0) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1(struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1((struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_1) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2(struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2((struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_2) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3(struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3((struct A6XX_CX_DBGC_CFG_DBGBUS_MASKL_3) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0 {
    uint32_t							bytel0;
    uint32_t							bytel1;
    uint32_t							bytel2;
    uint32_t							bytel3;
    uint32_t							bytel4;
    uint32_t							bytel5;
    uint32_t							bytel6;
    uint32_t							bytel7;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0(struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0 fields)
{
#ifndef NDEBUG
    assert((fields.bytel0                            & 0xfffffff0) == 0);
    assert((fields.bytel1                            & 0xfffffff0) == 0);
    assert((fields.bytel2                            & 0xfffffff0) == 0);
    assert((fields.bytel3                            & 0xfffffff0) == 0);
    assert((fields.bytel4                            & 0xfffffff0) == 0);
    assert((fields.bytel5                            & 0xfffffff0) == 0);
    assert((fields.bytel6                            & 0xfffffff0) == 0);
    assert((fields.bytel7                            & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0,
        .value =
            (fields.bytel0                            <<  0) |
            (fields.bytel1                            <<  4) |
            (fields.bytel2                            <<  8) |
            (fields.bytel3                            << 12) |
            (fields.bytel4                            << 16) |
            (fields.bytel5                            << 20) |
            (fields.bytel6                            << 24) |
            (fields.bytel7                            << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0((struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_0) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1 {
    uint32_t							bytel8;
    uint32_t							bytel9;
    uint32_t							bytel10;
    uint32_t							bytel11;
    uint32_t							bytel12;
    uint32_t							bytel13;
    uint32_t							bytel14;
    uint32_t							bytel15;
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1(struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1 fields)
{
#ifndef NDEBUG
    assert((fields.bytel8                            & 0xfffffff0) == 0);
    assert((fields.bytel9                            & 0xfffffff0) == 0);
    assert((fields.bytel10                           & 0xfffffff0) == 0);
    assert((fields.bytel11                           & 0xfffffff0) == 0);
    assert((fields.bytel12                           & 0xfffffff0) == 0);
    assert((fields.bytel13                           & 0xfffffff0) == 0);
    assert((fields.bytel14                           & 0xfffffff0) == 0);
    assert((fields.bytel15                           & 0xfffffff0) == 0);
    assert((fields.unknown                           & 0xffffffff) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1,
        .value =
            (fields.bytel8                            <<  0) |
            (fields.bytel9                            <<  4) |
            (fields.bytel10                           <<  8) |
            (fields.bytel11                           << 12) |
            (fields.bytel12                           << 16) |
            (fields.bytel13                           << 20) |
            (fields.bytel14                           << 24) |
            (fields.bytel15                           << 28) |
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1((struct A6XX_CX_DBGC_CFG_DBGBUS_BYTEL_1) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1(struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1((struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF1) { __VA_ARGS__ })

struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2(struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2(...) pack_A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2((struct A6XX_CX_DBGC_CFG_DBGBUS_TRACE_BUF2) { __VA_ARGS__ })

struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0(struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0(...) pack_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0((struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_0) { __VA_ARGS__ })

struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1 {
    uint32_t							unknown;
    uint32_t							dword;
};

static inline struct fd_reg_pair
pack_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1(struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1 fields)
{
#ifndef NDEBUG
    assert((fields.unknown                           & 0x00000000) == 0);
#endif

    struct fd_reg_pair pair = {
        .reg = REG_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1,
        .value =
            fields.unknown | fields.dword,
    };

    return pair;

}

#define A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1(...) pack_A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1((struct A6XX_CX_MISC_SYSTEM_CACHE_CNTL_1) { __VA_ARGS__ })


#endif /* A6XX_XML_STRUCTS */
